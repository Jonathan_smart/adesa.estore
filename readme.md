Multi-tenanted app that consumes the eStore API for grs.co.uk and smartvehiclesales.co.uk

## Site tenant integration setting keys

* `Views/Lot/IncludeFinanceTab`: If present an individual lot will show finance quotes
* `VehicleSearchHandler`: Switches implementation of `Sfs.EStore.Web.App.ThirdParties.IVehicleSearchHandler`

# Liquid markup

## Search query parameters

 - `vehicleType` - CAR or VAN
 - `make` - Indexed vehicle manufacturer name e.g. 'MERCEDES-BENZ'
 - `model` - e.g. 'E CLASS'
 - `derivative` - e.g. 'E250 CDI BlueEFFICIENCY Sport 5dr Tip Auto'
 - `minPrice`
 - `maxPrice`
 - `fuelType`
 - `category` - e.g. '3DR'
 - `mileage` - Facet search e.g. '[NULL TO Ix10000]' or '[Ix20000 TO Ix30000]'
 - `maxAge` - In years e.g. '3'
 - `transmission`
 - `q` - Free text search e.g. 'red diesel bmw 1 series'

## Tags

### Most Viewed

    {% most_viewed %}

#### Optional markup

Pre-defined filters

Can use any of the available search query params

    {% most_viewed vehicleType:'CAR' make:'BMW' %}