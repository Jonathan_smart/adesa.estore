param (
    [string]$v = ""
)

If ($v -eq "") {
    Write-Host "Version not specified. Use: ./bump-version.ps1 x.x" -ForegroundColor Red
    Exit 1
}

Function SetVersion([string]$path, [string]$version) {
    $newAssemblyVersion = 'AssemblyVersion("'+$version+'")'

    (Get-Content $path) | 
    Foreach-Object {$_ -replace 'AssemblyVersion\(\"[0-9.]*\"\)', $newAssemblyVersion} | 
    Set-Content $path -Encoding UTF8
}

SetVersion ".\src\Sfs.EStore.Web.App\Properties\AssemblyInfo.cs" $v

Write-Host "Files modified successfully, version bumped to $v"
Write-Host "Now commit to source control: git commit -am ""Bumped version number to $v""" -ForegroundColor Yellow
Write-Host ""