# Environment parameters
param (
    [string]$apiUrl = "",
    [string]$ravenUrl = "",
    [string]$navConn = "",
    [string]$ivendi = "",
    [string]$cloudFilesImageProxyBaseUrl = "http://d3l2oke4gxqliw.cloudfront.net/",
    [string]$raygunApiKey = "",
    [bool]$help = $False
)

Function Help ()
{
    Write-Output "Example usage"
    Write-Output ""
    Write-Host ".\transform.ps1 -apiUrl -ravenUrl -navConn [-ivendi] [-cloudFilesImageProxyBaseUrl] [-raygunApiKey]" -foregroundcolor "Cyan"
    Write-Output ""
    Write-Output "Paramters"
    Write-Output ""
    Write-Output "apiUrl: Base URL to the EStore API (should end in a trailing /)"
    Write-Output "ravenUrl: URL to the RavenDb server"
    Write-Output "navConn: Navision connection string"
    Write-Output "ivendi: URL for iVendi finance service"
    Write-Output "cloudFilesImageProxyBaseUrl (http://d3l2oke4gxqliw.cloudfront.net/): Cloud Files proxy to use for image manipulation"
    Write-Output "raygunApiKey: The Raygun application API key"
    Write-Output "help: Show this message (-help 1)"
    Write-Output ""
}

if ($help) {
    Help
    return;
}

if (($apiUrl -eq "") -or ($ravenUrl -eq "") -or ($navConn -eq "")) {
    Write-Host "Incorrect useage found. Please see below help for expected parameters" -foregroundcolor "Red"
    Write-Output ""
    Write-Output "Actual parameters given: "
    Write-Host "apiUrl: $apiUrl"
    Write-Host "ravenUrl: $ravenUrl"
    Write-Host "navConn: $navConn"
    Write-Host "ivendi: $ivendi"
    Write-Host "cloudFilesImageProxyBaseUrl: $cloudFilesImageProxyBaseUrl"
    Write-Host "raygunApiKey: $raygunApiKey"
    Write-Output ""
    Write-Output ""
    Help
    exit 1;
}

# Do the deployment
$cwd = (split-path $MyInvocation.MyCommand.Path)
$appRoot = $cwd + "\..\src\Sfs.EStore.Web.App"

echo "Updating web.config"
$webConfig = $appRoot + "\web.config"
[xml]$xml = (Get-Content $webConfig)

$xml.SelectSingleNode("/configuration/connectionStrings/add[@name='NavisionContext']").SetAttribute("connectionString", $navConn)
$xml.SelectSingleNode("/configuration/RaygunSettings").SetAttribute("apikey", $raygunApiKey)
$xml.SelectSingleNode("/configuration/appSettings/add[@key='Raven/Url']").SetAttribute("value", $ravenUrl)
$xml.SelectSingleNode("/configuration/appSettings/add[@key='Api/BaseAddress']").SetAttribute("value", $apiUrl)
$xml.SelectSingleNode("/configuration/appSettings/add[@key='CloudFiles/ImageProxyBaseUrl']").SetAttribute("value", $cloudFilesImageProxyBaseUrl)
if ($ivendi -ne "") {
    $xml.SelectSingleNode("/configuration/appSettings/add[@key='iVendi/Endpoint']").SetAttribute("value", $ivendi)
}

$xml.SelectSingleNode("/configuration/system.web/compilation").SetAttribute("debug", "false")
$xml.SelectSingleNode("/configuration/cassette").SetAttribute("cacheDirectory", "")

$xml.Save($webConfig)

echo "Finished updating web.config"
exit 0;