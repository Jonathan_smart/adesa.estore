# Environment parameters
param (
    [string]$dest = "",
    [bool]$help = $False
)

Function Help ()
{
    Write-Output "Example usage"
    Write-Output ""
    Write-Host ".\deploy.ps1 -dest `"deployment destination`"" -foregroundcolor "Cyan"
    Write-Output ""
    Write-Output "Paramters"
    Write-Output ""
    Write-Output "dest: The destination path"
    Write-Output "help: Show this message (-help 1)"
    Write-Output ""
}

if ($help) {
    Write-Output ""
    Help
    return;
}

if ($dest -eq "") {
    Write-Host "Incorrect useage found. Please see below help for expected parameters" -foregroundcolor "Red"
    Write-Output ""
    Help
    exit 1;
}

# Do the deployment
$cwd = (split-path $MyInvocation.MyCommand.Path)
$appRoot = "$cwd\..\src\Sfs.EStore.Web.App"


# Setting app as 'offline'
$appOfflinePath = "$cwd\app_offline.htm"
Write-Output "placing app_offline: $appOfflinePath"
xcopy /Y $appOfflinePath $dest

# Clean destination
Write-Output "cleaning destination"
Remove-Item ($dest+"\*") -recurse -exclude app_offline.htm, log4net.config, aspnet_client

# Write files to destination
Write-Output "Copying files to destination"
ROBOCOPY $appRoot $dest "favicon.ico" "Global.asax" "web.config"

Write-Output "copying areas file structure"
$areas = @("Account", "SiteAdmin", "Vehicles", "Retail", "GmGlobalConnect")
foreach ($areaName in $areas) {
    Write-Output "Copying files to aear $areaName"
    ROBOCOPY ("$appRoot\Areas\$areaName\Content") ("$dest\Areas\$areaName\Content") /E /NS /NC /NFL /NDL /NP 
    ROBOCOPY ("$appRoot\Areas\$areaName\Scripts") ("$dest\Areas\$areaName\Scripts") /E /NS /NC /NFL /NDL /NP 
    ROBOCOPY ("$appRoot\Areas\$areaName\Views") ("$dest\Areas\$areaName\Views") /E /NS /NC /NFL /NDL /NP 
}

Write-Output "copying remaining app structure"
ROBOCOPY "$appRoot\bin" "$dest\bin" *.dll /E /NS /NC /NFL /NDL /NP 
ROBOCOPY "$appRoot\Content" "$dest\Content" /E /NS /NC /NFL /NDL /NP  /XD "usercontent"
ROBOCOPY "$appRoot\Views" "$dest\Views" /E /NS /NC /NFL /NDL /NP 
ROBOCOPY "$appRoot\Scripts" "$dest\Scripts" /E /NS /NC /NFL /NDL /NP 
If (Test-Path "$dest\app_Offline.htm"){
    Write-Output "removing app_offline"
    Remove-Item "$dest\app_Offline.htm"
}
exit 0;