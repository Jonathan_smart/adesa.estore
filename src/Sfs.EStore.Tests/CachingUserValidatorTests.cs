﻿//using System;
//using Moq;
//using Sfs.EStore.Web.App.Code;
//using Xunit;

//namespace Sfs.EStore.Tests
//{
//    public class CachingUserValidatorTests
//    {
//        private readonly DateTime _baseSystemTime = new DateTime(2013, 1, 1, 1, 1, 30);
//        private readonly Mock<IUserValidator> _mockProxy = new Mock<IUserValidator>();

//        public CachingUserValidatorTests()
//        {
//            SystemTime.UtcDateTime = () => _baseSystemTime;
//        }

//        public CachingUserValidator CreateSut()
//        {
//            return CreateSutExpiringIn(999);
//        }

//        public CachingUserValidator CreateSutExpiringIn(int seconds)
//        {
//            return new CachingUserValidator(() => _mockProxy.Object, seconds);
//        }

//        private void SetProxyForValidatedUser(string username)
//        {
//            var mockIdentity = new Mock<IUserIdentity> { DefaultValue = DefaultValue.Mock };
//            mockIdentity.Setup(x => x.UserName).Returns(username);

//            _mockProxy.Setup(x => x.Validate(username, It.IsAny<string>()))
//                .Returns(mockIdentity.Object);
//        }

//        private void SetProxyForNotValidatedUser(string username = "foo")
//        {
//            _mockProxy.Setup(x => x.Validate(username, It.IsAny<string>()))
//                .Returns((IUserIdentity) null);
//        }

//        [Fact]
//        public void WhenRequestIsNotCached_ShouldQueryProxy()
//        {
//            CreateSutExpiringIn(seconds: 5).Validate("foo", "bar");
//            _mockProxy.Verify(x => x.Validate(It.IsAny<string>(), It.IsAny<string>()), Times.Once());
//        }

//        [Fact]
//        public void WhenSecondRequestIsCalledBeforeCacheTimeout_ShouldNotQueryProxy()
//        {
//            SetProxyForValidatedUser("foo");
//            var sut = CreateSutExpiringIn(seconds: 5);

//            SystemTime.UtcDateTime = () => _baseSystemTime;
//            sut.Validate("foo", "bar");
//            SystemTime.UtcDateTime = () => _baseSystemTime.AddSeconds(4);
//            sut.Validate("foo", "bar");

//            _mockProxy.Verify(x => x.Validate(It.IsAny<string>(), It.IsAny<string>()), Times.Once());
//        }

//        [Fact]
//        public void WhenSecondRequestIsCalledBeforeCacheTimeoutAndUserIsNotValid_ShouldReQueryProxy()
//        {
//            const string username = "foo";
//            SetProxyForNotValidatedUser();
//            var sut = CreateSutExpiringIn(seconds: 5);

//            SystemTime.UtcDateTime = () => _baseSystemTime;
//            sut.Validate("foo", "bar");
//            SystemTime.UtcDateTime = () => _baseSystemTime.AddSeconds(4);
//            sut.Validate(username, "bar");

//            _mockProxy.Verify(x => x.Validate(It.IsAny<string>(), It.IsAny<string>()), Times.Exactly(2));
//        }

//        [Fact]
//        public void WhenSecondRequestIsCalledAfterCacheTimeout_ShouldReQueryProxy()
//        {
//            SetProxyForValidatedUser("foo");
//            var sut = CreateSutExpiringIn(seconds: 5);

//            SystemTime.UtcDateTime = () => _baseSystemTime;
//            sut.Validate("foo", "bar");
//            SystemTime.UtcDateTime = () => _baseSystemTime.AddSeconds(6);
//            sut.Validate("foo", "bar");

//            _mockProxy.Verify(x => x.Validate(It.IsAny<string>(), It.IsAny<string>()), Times.Exactly(2));
//        }

//        [Fact]
//        public void WhenSecondRequestIsForADifferentUser_ShouldReQueryProxy()
//        {
//            SetProxyForValidatedUser("foo");
//            SetProxyForValidatedUser("another");
//            var sut = CreateSut();

//            SystemTime.UtcDateTime = () => _baseSystemTime;
//            sut.Validate("foo", "bar");
//            sut.Validate("another", "bar");

//            _mockProxy.Verify(x => x.Validate(It.IsAny<string>(), It.IsAny<string>()), Times.Exactly(2));
//        }

//        [Fact]
//        public void WhenSecondRequestIsAfterCacheExpired_AndThirdRequestIsCalled_ShouldQueryProxyTwice()
//        {
//            SetProxyForValidatedUser("foo");
//            var sut = CreateSutExpiringIn(seconds: 5);

//            SystemTime.UtcDateTime = () => _baseSystemTime;
//            sut.Validate("foo", "bar");
//            SystemTime.UtcDateTime = () => _baseSystemTime.AddSeconds(6);
//            sut.Validate("foo", "bar");
//            sut.Validate("foo", "bar");

//            _mockProxy.Verify(x => x.Validate(It.IsAny<string>(), It.IsAny<string>()), Times.Exactly(2));
//        }

//        [Fact]
//        public void WhenReturningACachedIdentity_ShouldReturnIdentityFromProxyCall()
//        {
//            SetProxyForValidatedUser("foo");
//            var sut = CreateSutExpiringIn(seconds: 5);

//            SystemTime.UtcDateTime = () => _baseSystemTime;
//            sut.Validate("foo", "bar");
//            var identity = sut.Validate("foo", "bar");
//            Assert.Equal("foo", identity.UserName);
//        }
//    }
//}
