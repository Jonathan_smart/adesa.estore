using Sfs.EStore.Web.App.Areas.Vehicles;
using Xunit;

namespace Sfs.EStore.Tests
{
    public class StringToSlugTests
    {
        [Fact]
        public void ShouldReplaceSpacesWithHyphens()
        {
            Assert.Equal("the-quick-brown-fox", "the quick brown fox".ToSlug());
        }

        [Fact]
        public void ShouldReplaceMultipleSpacesWithSingleHyphen()
        {
            Assert.Equal("the-quick-brown-fox", "the    quick  brown    fox".ToSlug());
        }

        [Fact]
        public void ShouldReturnEmptyStringForNull()
        {
            Assert.Equal("", ((string)null).ToSlug());
        }

        [Fact]
        public void ShouldRemoveNonAlphaNumbericCharacters()
        {
            Assert.Equal("the-lazy-dog", "th�^e �$%lazy d(%%og)(*&^".ToSlug());
        }

        [Fact]
        public void ShouldReplaceUpperCaseCharactersWithLowerCase()
        {
            Assert.Equal("the-lazy-dog", "The-Lazy-Dog".ToSlug());
        }
    }
}