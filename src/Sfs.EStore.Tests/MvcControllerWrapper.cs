using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Moq;
using Newtonsoft.Json.Linq;
using Raven.Client;
using Sfs.EStore.Web.App;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.Cms;
using Sfs.EStore.Web.App.Code.Security;
using Sfs.EStore.Web.App.Controllers;

namespace Sfs.EStore.Tests
{
    public class MvcControllerWrapper<TController> where TController : ApplicationController
    {
        public MvcControllerWrapper(TController controller, IDocumentStore store)
        {
            Controller = controller;
            Controller.Session = store.OpenSession();

            UserMock = new Mock<IWebApplicationPrincipal>();
            UserMock.Setup(x => x.Identity.IsAuthenticated).Returns(true);

            ContextMock = new Mock<HttpContextBase>{DefaultValue = DefaultValue.Mock};
            ContextMock.Setup(ctx => ctx.User).Returns(UserMock.Object);
            controller.User = UserMock.Object;

            Controller.ControllerContext = controller.ControllerContext = new ControllerContext(ContextMock.Object, new RouteData(), controller);
            UniqueSessionId.Store = () => Controller.WebSession;

            MockCookies();

            var urlHelperMock = new Mock<UrlHelper>();
            urlHelperMock.Setup(x => x.Content(It.IsAny<string>())).Returns("RandomTestString");
            Controller.Url = urlHelperMock.Object;

            ApiProxyFakeHandler = new FakeHandler
                                      {
                                          InnerHandler = new HttpClientHandler(),
                                          Response = new HttpResponseMessage(HttpStatusCode.OK)
                                                         {
                                                             Content = new StreamContent(
                                                                 new MemoryStream(
                                                                     Encoding.ASCII.GetBytes(
                                                                         JObject.FromObject(ApiResponses.LotActiveBidOnBuyItNow()).ToString())))
                                                         }.SetJsonContentTypeHeader()
                                      };

            Controller.ApiProxy = new Lazy<AuthorizedHttpClient>(() =>
                                                                     {
                                                                         var client = new HttpClient(ApiProxyFakeHandler)
                                                                                          {
                                                                                              BaseAddress =
                                                                                                  new Uri(
                                                                                                  "http://localhost/fake/api")
                                                                                          };

                                                                         return new AuthorizedHttpClient(client, new NullWebApplicationPrincipal());
                                                                     });

            Tenant = new SiteTenant();
            Controller.Tenant = new Lazy<SiteTenant>(() => Tenant);
            
            var mockPermissions = new Mock<SiteTenantActivityPermissions>();
            mockPermissions.Setup(x => x[It.IsAny<string>()]).Returns(
                new SiteTenantActivityPermissions.ActivityPermission(PageAccess.Everyone));
            Permissions = mockPermissions.Object;
        }

        private void MockCookies()
        {
            var cookies = new HttpCookieCollection();

            Mock.Get(ContextMock.Object.Response)
                .Setup(x => x.Cookies)
                .Returns(() => cookies);
            Mock.Get(ContextMock.Object.Response)
                .Setup(x => x.SetCookie(It.IsAny<HttpCookie>()))
                .Callback((HttpCookie cookie) => cookies.Add(cookie));
        }

        public TController Controller { get; private set; }

        public Mock<HttpContextBase> ContextMock { get; private set; }
        public Mock<HttpRequestBase> RequestMock { get { return Mock.Get(ContextMock.Object.Request); } }
        public Mock<IWebApplicationPrincipal> UserMock { get; private set; }

        public SiteTenant Tenant { get; set; }
        public SiteTenantActivityPermissions Permissions
        {
            get { return Controller.Permissions.Value; }
            set { Controller.Permissions = new Lazy<SiteTenantActivityPermissions>(() => value); }
        }
        public FakeHandler ApiProxyFakeHandler { get; private set; }
    }
}