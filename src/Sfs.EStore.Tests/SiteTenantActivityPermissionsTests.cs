﻿using System;
using Moq;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.Cms;
using Sfs.EStore.Web.App.Code.Security;
using Xunit;

namespace Sfs.EStore.Tests
{
    public class SiteTenantActivityPermissionsTests
    {
        private static Mock<IWebApplicationPrincipal> MockUser(bool isGuest = false, bool isAuthenticated = false, bool isInRole = false)
        {
            var principalMock = new Mock<IWebApplicationPrincipal>();
            var userMock = new Mock<IWebApplicationIdentity>();
            userMock.Setup(x => x.IsGuest).Returns(isGuest);
            userMock.Setup(x => x.IsAuthenticated).Returns(isAuthenticated);
            principalMock.Setup(x => x.IsInRole(It.IsAny<string>())).Returns(isInRole);

            principalMock.Setup(x => x.Identity).Returns(userMock.Object);

            return principalMock;
        }

        [Fact]
        public void GivenAnAllPermissionAndAuthenticatedUser_IsAllowedFor_ShouldReturnTrue()
        {
            var userMock = MockUser(isAuthenticated: true);

            const string ActivityKey = "My/Activity";
            var permissions = new SiteTenantActivityPermissions()
                .AddActivity(ActivityKey, PageAccess.Everyone);

            Assert.True(permissions[ActivityKey].IsAllowedFor(userMock.Object));
        }

        [Fact]
        public void GivenAnAllPermissionAndGuestUser_IsAllowedFor_ShouldReturnTrue()
        {
            var userMock = MockUser(isGuest: true);

            const string ActivityKey = "My/Activity";
            var permissions = new SiteTenantActivityPermissions()
                .AddActivity(ActivityKey, PageAccess.Everyone);

            Assert.True(permissions[ActivityKey].IsAllowedFor(userMock.Object));
        }

        [Fact]
        public void GivenAnAllPermissionAndAnonymousUser_IsAllowedFor_ShouldReturnTrue()
        {
            var userMock = MockUser();

            const string ActivityKey = "My/Activity";
            var permissions = new SiteTenantActivityPermissions()
                .AddActivity(ActivityKey, PageAccess.Everyone);

            Assert.True(permissions[ActivityKey].IsAllowedFor(userMock.Object));
        }

        [Fact]
        public void GivenAGuestPermissionAndGuestUser_IsAllowedFor_ShouldReturnTrue()
        {
            var userMock = MockUser(isGuest: true);

            const string ActivityKey = "My/Activity";
            var permissions = new SiteTenantActivityPermissions()
                .AddActivity(ActivityKey, PageAccess.Guest);

            Assert.True(permissions[ActivityKey].IsAllowedFor(userMock.Object));
        }

        [Fact]
        public void GivenAGuestPermissionAndAnonymousUser_IsAllowedFor_ShouldReturnFalse()
        {
            var userMock = MockUser();

            const string ActivityKey = "My/Activity";
            var permissions = new SiteTenantActivityPermissions()
                .AddActivity(ActivityKey, PageAccess.Guest);

            Assert.False(permissions[ActivityKey].IsAllowedFor(userMock.Object));
        }

        [Fact]
        public void GivenAGuestPermissionAndAuthenticatedUser_IsAllowedFor_ShouldReturnTrue()
        {
            var userMock = MockUser(isAuthenticated: true);

            const string ActivityKey = "My/Activity";
            var permissions = new SiteTenantActivityPermissions()
                .AddActivity(ActivityKey, PageAccess.Guest);

            Assert.True(permissions[ActivityKey].IsAllowedFor(userMock.Object));
        }

        [Fact]
        public void GivenAnAuthenticatedPermissionAndAuthenticatedUser_IsAllowedFor_ShouldReturnTrue()
        {
            var userMock = MockUser(isAuthenticated: true);

            const string ActivityKey = "My/Activity";
            var permissions = new SiteTenantActivityPermissions()
                .AddActivity(ActivityKey, PageAccess.Authenticated);

            Assert.True(permissions[ActivityKey].IsAllowedFor(userMock.Object));
        }

        [Fact]
        public void GivenAnAuthenticatedPermissionAndGuestUser_IsAllowedFor_ShouldReturnFalse()
        {
            var userMock = MockUser(isGuest: true);

            const string ActivityKey = "My/Activity";
            var permissions = new SiteTenantActivityPermissions()
                .AddActivity(ActivityKey, PageAccess.Authenticated);

            Assert.False(permissions[ActivityKey].IsAllowedFor(userMock.Object));
        }

        [Fact]
        public void GivenAnAuthenticatedPermissionAndAnonymousUser_IsAllowedFor_ShouldReturnFalse()
        {
            var userMock = MockUser();

            const string ActivityKey = "My/Activity";
            var permissions = new SiteTenantActivityPermissions()
                .AddActivity(ActivityKey, PageAccess.Authenticated);

            Assert.False(permissions[ActivityKey].IsAllowedFor(userMock.Object));
        }

        [Fact]
        public void GivenADenyPermissionAndAnonymousUser_IsAllowedFor_ShouldReturnFalse()
        {
            var userMock = MockUser();

            const string ActivityKey = "My/Activity";
            var permissions = new SiteTenantActivityPermissions()
                .AddActivity(ActivityKey, PageAccess.Deny);

            Assert.False(permissions[ActivityKey].IsAllowedFor(userMock.Object));
        }

        [Fact]
        public void GivenADenyPermissionAndGuestUser_IsAllowedFor_ShouldReturnFalse()
        {
            var userMock = MockUser(isGuest: true);

            const string ActivityKey = "My/Activity";
            var permissions = new SiteTenantActivityPermissions()
                .AddActivity(ActivityKey, PageAccess.Deny);

            Assert.False(permissions[ActivityKey].IsAllowedFor(userMock.Object));
        }

        [Fact]
        public void GivenADenyPermissionAndAuthenticatedUser_IsAllowedFor_ShouldReturnFalse()
        {
            var userMock = MockUser(isAuthenticated: true);

            const string ActivityKey = "My/Activity";
            var permissions = new SiteTenantActivityPermissions()
                .AddActivity(ActivityKey, PageAccess.Deny);

            Assert.False(permissions[ActivityKey].IsAllowedFor(userMock.Object));
        }

        [Fact]
        public void GivenARole_SecureRole_PermissionAndAuthenticatedUserIsNotInRole_IsAllowedFor_ShouldReturnFalse()
        {
            var userMock = MockUser(isAuthenticated: true, isInRole: false);

            const string ActivityKey = "My/Activity";
            var permissions = new SiteTenantActivityPermissions()
                .AddActivity(ActivityKey, "roles:SecureRole");

            Assert.False(permissions[ActivityKey].IsAllowedFor(userMock.Object));
        }

        [Fact]
        public void GivenARolePermissionAndAuthenticatedUserIsInRole_IsAllowedFor_ShouldReturnTrue()
        {
            var userMock = MockUser(isAuthenticated: true, isInRole: true);

            const string ActivityKey = "My/Activity";
            var permissions = new SiteTenantActivityPermissions()
                .AddActivity(ActivityKey, "roles:SecureRole");

            Assert.True(permissions[ActivityKey].IsAllowedFor(userMock.Object));
        }
    }
}
