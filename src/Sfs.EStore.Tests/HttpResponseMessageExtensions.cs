using System.Globalization;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Sfs.EStore.Tests
{
    public static class HttpResponseMessageExtensions
    {
        public static HttpResponseMessage SetJsonContentTypeHeader(this HttpResponseMessage @this)
        {
            @this.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return @this;
        }

        public static TValue ReadAs<TValue>(this HttpResponseMessage @this)
        {
            return @this.Content.ReadAsAsync<TValue>().Result;
        }

        public static HttpResponseMessage AddTotalResultsHeader(this HttpResponseMessage @this, int totalResults)
        {
            @this.Headers.Add("X-TotalResults", totalResults.ToString(CultureInfo.InvariantCulture));
            return @this;
        }
    }
}