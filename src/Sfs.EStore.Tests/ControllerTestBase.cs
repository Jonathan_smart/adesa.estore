﻿using Raven.Client;
using Raven.Client.Embedded;

namespace Sfs.EStore.Tests
{
    public abstract class ControllerTestBase
    {
        private readonly IDocumentStore _store;

        protected ControllerTestBase()
        {
            _store = new EmbeddableDocumentStore
            {
                RunInMemory = true
            }.Initialize();
        }

        public  IDocumentStore Store { get { return _store; } }
    }
}
