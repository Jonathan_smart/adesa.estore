﻿using Moq;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Controllers;
using Sfs.EStore.Web.App.DomainEvents;
using Sfs.EStore.Web.App.Models;
using Xunit;

namespace Sfs.EStore.Tests.SessionControllerTests
{
    public class GivenValidLoginCredentials : ControllerTestBase
    {
        private UserLoggedIn _handledDomainEvent;

        public GivenValidLoginCredentials()
        {
            new DomainEventSystemUnderTest().Handle<UserLoggedIn>(args => _handledDomainEvent = args);

            // Given
            var mockValidator = new Mock<IUserValidator>();
            var uIdentityValidation = new UserIdentityValidation
                                          {
                                              IsAuthenticated = true,
                                              UserIdentity = new UserIdentity(userName: "foo", roles: new string[0], token: "")
                                          };
            
            mockValidator.Setup(x => x.Validate(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(uIdentityValidation);

            var context = new MvcControllerWrapper<SessionsController>(new SessionsController(mockValidator.Object),
                                                                       Store);
            
            // When
            context.Controller.Login(new SessionsLoginModel());
        }

        [Fact]
        public void ShouldRaiseUserLoggedInDomainEvent()
        {
            Assert.NotNull(_handledDomainEvent);
        }
    }
}
