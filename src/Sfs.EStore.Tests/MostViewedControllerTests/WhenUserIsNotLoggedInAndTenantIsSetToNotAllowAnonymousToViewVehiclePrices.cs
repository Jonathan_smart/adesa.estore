﻿using System.Collections.Generic;
using System.Net;
using Moq;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Code.Cms;
using Sfs.EStore.Web.App.Controllers.Api;
using Sfs.EStore.Web.App.Models;
using Sfs.EStore.Web.App.ThirdParties;
using Xunit;

namespace Sfs.EStore.Tests.MostViewedControllerTests
{
    public class WhenUserIsNotLoggedInAndTenantIsSetToNotAllowAnonymousToViewVehiclePrices : ControllerTestBase
    {
        private readonly ApiControllerWrapper<MostViewedController> _sut;

        public WhenUserIsNotLoggedInAndTenantIsSetToNotAllowAnonymousToViewVehiclePrices()
        {
            var tenant = this.CreateTenant();
            
            _sut = new ApiControllerWrapper<MostViewedController>(Store, new MostViewedController(new EmptyVehicleSearchHandler(), NullListingViewModelVisitor.Instance).WithJsonPostRequest())
                       {
                           Tenant = tenant,
                           Permissions = this.SetTenantPermissions(tenant, new[]
                                                  {
                                                      new KeyValuePair<string, string>("/Lots/View", PageAccess.Everyone),
                                                      new KeyValuePair<string, string>("/Lots/View/Prices", PageAccess.Authenticated)
                                                  })
                       };

            _sut.SetApiProxyResponse(HttpStatusCode.OK, ApiResponses.LotSearchResults())
                .SetJsonContentTypeHeader()
                .AddTotalResultsHeader(It.IsAny<int>());
        }

        [Fact]
        public void PriceInformationShouldNotBeIncluded()
        {
            var result = _sut.Controller.Post(new LotsGetParams()).Result.ReadAs<ListingSummaryViewModel[]>();

            Assert.NotEmpty(result);

            foreach(var lot in result)
            {
                Assert.Null(lot.SellingStatus.CurrentPrice);
            }
        }
    }
}
