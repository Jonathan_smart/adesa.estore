using System.Collections.Generic;
using System.Net;
using Moq;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Controllers.Api;
using Sfs.EStore.Web.App.Models;
using Sfs.EStore.Web.App.ThirdParties;
using Xunit;

namespace Sfs.EStore.Tests.MostViewedControllerTests
{
    public class WhenUserIsLoggedInAndTenantIsSetToNotAllowAllowAnonymousToViewVehicles : ControllerTestBase
    {
        private readonly ApiControllerWrapper<MostViewedController> _sut;

        public WhenUserIsLoggedInAndTenantIsSetToNotAllowAllowAnonymousToViewVehicles()
        {
            var tenant = this.CreateTenant();
            this.SetTenantPermissions(tenant, new[]
                                                  {
                                                      new KeyValuePair<string, string>("/Lots/View", "")
                                                  });

            _sut =
                new ApiControllerWrapper<MostViewedController>(Store,
                    new MostViewedController(new EmptyVehicleSearchHandler(), NullListingViewModelVisitor.Instance).WithJsonPostRequest())
                    {
                        Tenant = tenant
                    };

            _sut.SetAuthenticatedUser("foo");

            _sut.SetApiProxyResponse(HttpStatusCode.OK, ApiResponses.LotSearchResults())
                .SetJsonContentTypeHeader()
                .AddTotalResultsHeader(It.IsAny<int>());
        }

        [Fact]
        public void ShouldReturnLotsArray()
        {
            Assert.NotEmpty(_sut.Controller.Post(new LotsGetParams()).Result.ReadAs<ListingSummaryViewModel[]>());
        }
    }
}