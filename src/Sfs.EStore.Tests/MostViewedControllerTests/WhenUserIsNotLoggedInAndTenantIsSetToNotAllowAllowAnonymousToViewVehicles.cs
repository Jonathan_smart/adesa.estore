using System.Collections.Generic;
using System.Net;
using Moq;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Code.Http;
using Sfs.EStore.Web.App.Controllers.Api;
using Sfs.EStore.Web.App.Models;
using Sfs.EStore.Web.App.ThirdParties;
using Xunit;

namespace Sfs.EStore.Tests.MostViewedControllerTests
{
    public class WhenUserIsNotLoggedInAndTenantIsSetToNotAllowAllowAnonymousToViewVehicles : ControllerTestBase
    {
        private readonly ApiControllerWrapper<MostViewedController> _sut;

        public WhenUserIsNotLoggedInAndTenantIsSetToNotAllowAllowAnonymousToViewVehicles()
        {
            var tenant = this.CreateTenant();

            _sut = new ApiControllerWrapper<MostViewedController>(Store, new MostViewedController(new EmptyVehicleSearchHandler(), NullListingViewModelVisitor.Instance).WithJsonPostRequest())
                       {
                           Tenant = tenant,
                           Permissions = this.SetTenantPermissions(tenant, new[]
                                                  {
                                                      new KeyValuePair<string, string>("/Lots/View", "")
                                                  })
                       };

            _sut.SetApiProxyResponse(HttpStatusCode.OK, ApiResponses.LotSearchResults())
                .SetJsonContentTypeHeader()
                .AddTotalResultsHeader(It.IsAny<int>());
        }

        [Fact]
        public void ShouldReturnEmptyArray()
        {
            _sut.Controller.HasAttribute<AuthoriseActivityAttribute>();
            Assert.Equal("/Lots/View", _sut.Controller.GetAttribute<AuthoriseActivityAttribute>().Activity);
        }
    }
}