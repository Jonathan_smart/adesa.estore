﻿using Sfs.EStore.Web.App.Controllers.Api;
using Sfs.EStore.Web.App.Models;
using Xunit;

namespace Sfs.EStore.Tests
{
    public class LotsGetParamsTests
    {
        [Fact]
        public void FooBar()
        {
            var search = new LotsGetParams
            {
                Q = "foo",
                Make = "bar"
            };

            Assert.Equal("q=foo&make=bar", search.ToUrlParams());
        }

        [Fact]
        public void FooBar2()
        {
            var search = new LotsGetParams
            {
                Q = "foo",
                Make = "bar",
                MaxAge = 3
            };

            Assert.Equal("q=foo&make=bar&maxage=3", search.ToUrlParams());
        }
    }
}
