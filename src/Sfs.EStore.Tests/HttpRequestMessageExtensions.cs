using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Sfs.EStore.Tests
{
    public static class HttpRequestMessageExtensions
    {
        public static HttpRequestMessage ConfigureJsonFormatter(this HttpRequestMessage @this)
        {
            var httpConfiguration = @this.GetOrAddHttpConfiguration();
            
            var settings = httpConfiguration.Formatters.JsonFormatter.SerializerSettings;
            settings.Formatting = Formatting.Indented;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            return @this;
        }

        public static HttpRequestMessage SetFakeHandler(this HttpRequestMessage @this, FakeHandler handler)
        {
            var configuration = @this.GetOrAddHttpConfiguration();
            configuration.MessageHandlers.Add(handler);

            return @this;
        }

        public static FakeHandler SetFakeHandler(this HttpRequestMessage @this)
        {
            var handler = new FakeHandler
                              {
                                  InnerHandler = new HttpClientHandler()
                              };

            @this.SetFakeHandler(handler);
            return handler;
        }

        private static HttpConfiguration GetOrAddHttpConfiguration(this HttpRequestMessage @this)
        {
            return @this.Properties.ContainsKey(HttpPropertyKeys.HttpConfigurationKey)
                       ? (HttpConfiguration) @this.Properties[HttpPropertyKeys.HttpConfigurationKey]
                       : @this.CreateHttpConfiguration();
        }

        private static HttpConfiguration CreateHttpConfiguration(this HttpRequestMessage @this)
        {
            var httpConfiguration = new HttpConfiguration();
            @this.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, httpConfiguration);

            return httpConfiguration;
        }
    }
}