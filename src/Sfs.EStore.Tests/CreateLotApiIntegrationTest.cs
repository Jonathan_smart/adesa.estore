using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using Xunit;

namespace Sfs.EStore.Tests
{
    public class CreateLotApiIntegrationTest
    {
        [Fact(Skip = "Used for manual integration testing only")]
        public void CreateALot()
        {
            var model = CreateValidModel();
            model.LotId = "lots/10161";
            model.Title = "McLaren's Spider";
            model.Make = "McLaren";
            model.Model = "Spider";

            var proxy = new HttpClient
                            {
                                BaseAddress = new Uri("http://localhost/estore.api/lots/")
                            };
            proxy.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("SFS", "Z3JzLWRhdGEtZmVlZDo=");

            proxy.PutAsJsonAsync("lots", model)
                .ContinueWith(t =>
                                  {
                                      var responseObject = t.Result.Content.ReadAsAsync<JObject>().Result;
                                      Console.WriteLine(responseObject);

                                      t.Result.EnsureSuccessStatusCode();
                                  })
                .Wait();
        }

        private static TestModel CreateValidModel()
        {
            var model = new TestModel
                            {
                                //LotId = "lots/10066", // (optional: specified if updating a new lot will be created if no lotId is specified)
                                CustomerAssetId = "V001234", // Your asset id (optional)
                                StartDate = DateTime.UtcNow, // (optional)
                                EndDate = DateTime.UtcNow.AddDays(5), // (optional)
                                Title = "VAUXHALL CORSA 11 1.4i 16v (100PS)", // (required)
                                Make = "Vauxhall", // (required)
                                Model = "Corsa", // (required)
                                Derivative = "5 Dr Hatch", // 
                                ModelYear = 2010, // 
                                EngineDescription = "1.4i 16v (100PS)", // 
                                EngineSizeCc = 1380, // (optional)
                                Bodystyle = "SRi", // 
                                Vin = "", // 
                                Mileage = 6000, // (optional)
                                VatStatus = "Qualifying", // 
                                Transmission = "Manual", // 
                                FuelType = "Diesel", // 
                                RegistrationPlate = "AB10CDE", // 
                                RegistrationDate = new DateTime(2010, 6, 1), // 
                                RegistrationYearAndPlate = "10:10", // 
                                ExteriorColor = "Sky Blue", // 
                                InteriorTrim = "Black Cloth", // 
                                VehicleType = "CAR", // 
                                BuyItNowPrice = 12700, // (required)
                                Images = new[]
                                             {
                                                 new {Name = "bar.jpg", Path = "http://i.telegraph.co.uk/multimedia/archive/02370/12C_Spider_4_2370457b.jpg"},
                                                 new {Name = "foo.jpg", Path = "http://www.extremetech.com/wp-content/uploads/2012/12/Audi-A1.jpg"},
                                                 new {Name = "bar.jpg", Path = "http://www.topgear.com/uk/assets/cms/c460bcef-2fd0-405f-8011-9c0ea7f5e7c7/Meduim%20Image%20(optional).jpg?p=121207_12:00"}
                                             },
                                Data = new Dictionary<string, object>
                                           {
                                               {"audio", "B&O"}
                                           }
                            };

            return model;
        }

        internal class TestModel
        {
            public string LotId;
            public string CustomerAssetId;
            public Dictionary<string, object> Data;
            public object Images;
            public int BuyItNowPrice;
            public string VehicleType;
            public string InteriorTrim;
            public string ExteriorColor;
            public string RegistrationYearAndPlate;
            public DateTime RegistrationDate;
            public string RegistrationPlate;
            public string FuelType;
            public string Transmission;
            public string VatStatus;
            public int Mileage;
            public string Vin;
            public string Bodystyle;
            public int EngineSizeCc;
            public string EngineDescription;
            public int ModelYear;
            public string Derivative;
            public string Model;
            public string Make;
            public string Title;
            public DateTime EndDate;
            public DateTime StartDate;
        }
    }
}