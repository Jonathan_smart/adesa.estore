﻿using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.ListingViewModelVisitors;

namespace Sfs.EStore.Tests
{
    internal class NullListingViewModelVisitor : IListingViewModelVisitor, IListingSummaryViewModelVisitor
    {
        public void Visit(ListingViewModel model)
        {
        }

        public void Visit(ListingSummaryViewModel model)
        {
        }

        public readonly static NullListingViewModelVisitor Instance = new NullListingViewModelVisitor();
    }
}
