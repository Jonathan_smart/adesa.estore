using System;
using Ninject;
using Sfs.EStore.Web.App;

namespace Sfs.EStore.Tests
{
    public class DomainEventSystemUnderTest : IDisposable
    {
        private readonly IKernel _kernel = new StandardKernel();

        public DomainEventSystemUnderTest()
        {
            DomainEvent.Container = _kernel;
        }

        public void Handle<T>(IHandles<T> handler)
            where T : IDomainEvent
        {
            _kernel.Bind<IHandles<T>>()
                .ToMethod(ctx => handler)
                .InSingletonScope();
        }

        public void Handle<T>(Action<T> callback)
            where T : IDomainEvent
        {
            Handle(new FakeDomainEventHandler<T>(callback));
        }

        public void Dispose()
        {
            if (!_kernel.IsDisposed)
                _kernel.Dispose();
        }
    }

    public class FakeDomainEventHandler<T> : IHandles<T>
        where T : IDomainEvent
    {
        private readonly Action<T> _callback;

        public FakeDomainEventHandler(Action<T> callback)
        {
            _callback = callback;
        }

        public void Handle(T args)
        {
            _callback(args);
        }
    }
}