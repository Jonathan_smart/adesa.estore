using System.Collections.Generic;
using System.Web.Mvc;
using Moq;
using Sfs.EStore.Web.App.Areas.Vehicles.Controllers;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Code.Cms;
using Sfs.EStore.Web.App.ThirdParties;
using Xunit;

namespace Sfs.EStore.Tests.LotsControllerTests
{
    public class WhenUserIsAnonymousAndTenantDoesNotAllowAnonToViewPrices : ControllerTestBase
    {
        private readonly MvcControllerWrapper<LotsController> _sut;

        public WhenUserIsAnonymousAndTenantDoesNotAllowAnonToViewPrices()
        {
            var tenant = this.CreateTenant();

            _sut = new MvcControllerWrapper<LotsController>(new LotsController(new EmptyVehicleSearchHandler(), NullListingViewModelVisitor.Instance), Store)
                       {
                           Tenant = tenant,
                           Permissions = this.SetTenantPermissions(tenant, new[]
                                                  {
                                                      new KeyValuePair<string, string>("/Lots/View", PageAccess.Everyone),
                                                      new KeyValuePair<string, string>("/Lots/View/Prices", PageAccess.Authenticated)
                                                  })
                       };

            _sut.UserMock.Setup(x => x.Identity.IsAuthenticated).Returns(false);
        }

        [Fact]
        public void ShowShouldReturnUnauthorizedResult()
        {
            var result = Assert.IsType<ViewResult>(_sut.Controller.Show(It.IsAny<int>()));

            var model = Assert.IsType<ListingViewModel>(result.Model);

            Assert.Null(model.SellingStatus.CurrentPrice);
        }
    }
}