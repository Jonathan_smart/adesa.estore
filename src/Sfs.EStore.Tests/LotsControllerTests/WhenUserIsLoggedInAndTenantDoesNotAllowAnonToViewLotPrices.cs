using System.Collections.Generic;
using System.Web.Mvc;
using Moq;
using Sfs.EStore.Web.App.Areas.Vehicles.Controllers;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Code.Cms;
using Sfs.EStore.Web.App.ThirdParties;
using Xunit;

namespace Sfs.EStore.Tests.LotsControllerTests
{
    public class WhenUserIsLoggedInAndTenantDoesNotAllowAnonToViewLotPrices : ControllerTestBase
    {
        private readonly MvcControllerWrapper<LotsController> _sut;

        public WhenUserIsLoggedInAndTenantDoesNotAllowAnonToViewLotPrices()
        {
            var tenant = this.CreateTenant();

            _sut = new MvcControllerWrapper<LotsController>(new LotsController(new EmptyVehicleSearchHandler(), NullListingViewModelVisitor.Instance), Store)
                       {
                           Tenant = tenant,
                           Permissions = this.SetTenantPermissions(tenant, new[]
                                                  {
                                                      new KeyValuePair<string, string>("/Lots/View", PageAccess.Everyone),
                                                      new KeyValuePair<string, string>("/Lots/View/Prices", PageAccess.Authenticated)
                                                  })
                       };
        }

        [Fact]
        public void ShowShouldShowPriceInformation()
        {
            var model = Assert.IsType<ListingViewModel>(((ViewResult)_sut.Controller.Show(It.IsAny<int>())).Model);

            Assert.NotNull(model.SellingStatus.CurrentPrice);
        }
    }
}