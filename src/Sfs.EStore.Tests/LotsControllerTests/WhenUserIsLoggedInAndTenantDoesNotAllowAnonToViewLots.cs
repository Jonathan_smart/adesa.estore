using System.Collections.Generic;
using System.Web.Mvc;
using Moq;
using Sfs.EStore.Web.App.Areas.Vehicles.Controllers;
using Sfs.EStore.Web.App.ThirdParties;
using Xunit;

namespace Sfs.EStore.Tests.LotsControllerTests
{
    public class WhenUserIsLoggedInAndTenantDoesNotAllowAnonToViewLots : ControllerTestBase
    {
        private readonly MvcControllerWrapper<LotsController> _sut;

        public WhenUserIsLoggedInAndTenantDoesNotAllowAnonToViewLots()
        {
            var tenant = this.CreateTenant();
            this.SetTenantPermissions(tenant, new[]
                                                  {
                                                      new KeyValuePair<string, string>("/Lots/View", "")
                                                  });

            _sut = new MvcControllerWrapper<LotsController>(new LotsController(new EmptyVehicleSearchHandler(), NullListingViewModelVisitor.Instance), Store)
                       {
                           Tenant = tenant,
                           Permissions = this.SetTenantPermissions(tenant, new[]
                                                                               {
                                                                                   new KeyValuePair<string, string>(
                                                                                       "/Lots/View", "")
                                                                               })
                       };
        }

        [Fact]
        public void IndexShouldReturnView()
        {
            Assert.IsNotType<HttpUnauthorizedResult>(_sut.Controller.Index());
        }

        [Fact]
        public void ShowShouldReturnView()
        {
            Assert.IsNotType<HttpUnauthorizedResult>(_sut.Controller.Show(It.IsAny<int>()));
        }
    }
}