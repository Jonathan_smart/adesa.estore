using System;
using System.Net;
using Moq;
using Sfs.Darker.CommandProcessor;
using Sfs.EStore.Ports.GmGlobalConnect;
using Xunit;
// ReSharper disable ExceptionNotDocumented

namespace Sfs.EStore.Tests.Gm_global_connect_authenticate_spec
{
    public class MissingData
    {
        public static readonly Func<Authenticate> ValidCommand = () => new Authenticate
        {
            UserId = "test@GBC100",
            DealerCode = "GBC100",
            Email = "tester@example.com",
            IvUser = "test@GBC100",
            Role = "APP USED VEHICLES SALES",
            RemoteAddress = IPAddress.Parse("198.208.32.77")
        };

        public static readonly Func<Authenticate> ValidCommandAlt = () => new Authenticate
        {
            UserId = "test_2@GBC999",
            DealerCode = "GBC100",
            Email = "tester@example.com",
            IvUser = "test_2@GBC999",
            Role = "APP USED VEHICLES SALES MANAGER",
            RemoteAddress = IPAddress.Parse("198.208.32.78")
        };

        internal static void Process(Authenticate cmd,
            ICustomerStore customerStore = null,
            ISetBuyerPermissions buyerPermissions = null,
            IUserIdentityStore identityStore = null,
            IRemoteAddressAuthenticator remoteAddressAuthenticator = null)
        {
            var positiveKnownDealerCodesChecker = new Mock<ICustomerStore>();
            positiveKnownDealerCodesChecker.Setup(x => x.Get(It.IsAny<string>())).Returns(new KnownCustomer("ACode", "A Name"));
            
            var positiveBuyerPermissions = new Mock<ISetBuyerPermissions>();
            positiveBuyerPermissions.Setup(x => x.Allow(It.IsAny<Buyer>())).Returns(true);
            positiveBuyerPermissions.Setup(x => x.Revoke(It.IsAny<Buyer>())).Returns(true);

            var positiveIdentityStore = new Mock<IUserIdentityStore>();
            var authenticatedUser = new AuthenticatedUser(
                token: "UserToken",
                username: It.IsAny<string>(),
                roles: new[] { It.IsAny<string>() },
                email: It.IsAny<string>());
            positiveIdentityStore.Setup(x => x.AuthenticateAs(It.IsAny<User>())).Returns(authenticatedUser);

            var positiveRemoteAddressAuthenticator = new Mock<IRemoteAddressAuthenticator>();
            positiveRemoteAddressAuthenticator.Setup(x => x.IsAuthorised(It.IsAny<IPAddress>())).Returns(true);

            // When 
            new GmGlobalConnectAuthenticater(
                customerStore ?? positiveKnownDealerCodesChecker.Object,
                buyerPermissions ?? positiveBuyerPermissions.Object,
                identityStore ?? positiveIdentityStore.Object,
                remoteAddressAuthenticator ?? positiveRemoteAddressAuthenticator.Object,
                new Mock<ILog>().Object).Handle(cmd);
        }

        internal static void ProcessAndAssertUnauthenticated(Authenticate cmd,
            ICustomerStore customerStore = null,
            ISetBuyerPermissions buyerPermissions = null)
        {
            Process(cmd, customerStore, buyerPermissions);

            // Then
            Assert.False(cmd.Result.Authenticated);
        }
    }

    public class A_missing_user_id
    {
        [Fact]
        public void Is_unauthenticated()
        {
            // Given
            var cmd = MissingData.ValidCommand();
            cmd.UserId = null;

            // When, Then
            MissingData.ProcessAndAssertUnauthenticated(cmd);
        }
    }

    public class A_missing_iv_user
    {
        [Fact]
        public void Is_unauthenticated()
        {
            // Given
            var cmd = MissingData.ValidCommand();
            cmd.IvUser = null;

            // When, Then
            MissingData.ProcessAndAssertUnauthenticated(cmd);
        }
    }

    public class A_user_id_that_does_not_match_iv_user
    {
        [Fact]
        public void Is_unauthenticated()
        {
            // Given
            var cmd = MissingData.ValidCommand();
            cmd.IvUser = MissingData.ValidCommandAlt().UserId;

            // When, Then
            MissingData.ProcessAndAssertUnauthenticated(cmd);
        }
    }

    public class A_missing_dealer_code
    {
        [Fact]
        public void Is_unauthenticated()
        {
            // Given
            var cmd = MissingData.ValidCommand();
            cmd.DealerCode = null;

            // When, Then
            MissingData.ProcessAndAssertUnauthenticated(cmd);
        }
    }

    public class A_missing_email_address
    {
        [Fact]
        public void Is_unauthenticated()
        {
            // Given
            var cmd = MissingData.ValidCommand();
            cmd.Email = null;

            // When, Then
            MissingData.ProcessAndAssertUnauthenticated(cmd);
        }
    }

    public class A_missing_role
    {
        [Fact]
        public void Is_unauthenticated()
        {
            // Given
            var cmd = MissingData.ValidCommand();
            cmd.Role = null;

            // When, Then
            MissingData.ProcessAndAssertUnauthenticated(cmd);
        }
    }

    public class An_unknown_role
    {
        [Fact]
        public void Is_unauthenticated()
        {
            // Given
            var cmd = MissingData.ValidCommand();
            cmd.Role = "Random Role";

            // When, Then
            MissingData.ProcessAndAssertUnauthenticated(cmd);
        }
    }

    public class An_unknown_dealer_code
    {
        private const string UnknownDealerCode = "random unknown dealer code";
        [Fact]
        public void Is_unauthenticated()
        {
            var customerStoreMock = new Mock<ICustomerStore>();

            // Given
            var cmd = MissingData.ValidCommand();
            cmd.DealerCode = UnknownDealerCode;
            customerStoreMock.Setup(x => x.Get(UnknownDealerCode)).Returns((KnownCustomer)null);

            // When, Then
            MissingData.ProcessAndAssertUnauthenticated(cmd, customerStore: customerStoreMock.Object);
        }
    }

    public class A_user_with_role_APP_USED_VEHICLE_SALES_MANAGER
    {
        private readonly Authenticate _cmd;
        private readonly Mock<ISetBuyerPermissions> _buyerPermissions;

        public A_user_with_role_APP_USED_VEHICLE_SALES_MANAGER()
        {
            _buyerPermissions = new Mock<ISetBuyerPermissions>();

            // Given
            _cmd = MissingData.ValidCommandAlt();

            // When
            MissingData.Process(_cmd, buyerPermissions: _buyerPermissions.Object);
        }

        [Fact]
        public void Has_buying_permissions_revoked()
        {
            // Then
            _buyerPermissions.Verify(x => x.Revoke(It.IsAny<Buyer>()));
        }

        [Fact]
        public void Is_unable_to_purchase()
        {
            // Then
            Assert.False(_cmd.Result.SetupForPurchasing);
        }
    }

    public class A_user_with_role_APP_USED_VEHICLE_SALES
    {
        private readonly Authenticate _cmd;
        private readonly Mock<ISetBuyerPermissions> _buyerPermissions;

        public A_user_with_role_APP_USED_VEHICLE_SALES()
        {
            _buyerPermissions = new Mock<ISetBuyerPermissions>();
            _buyerPermissions.Setup(x => x.Allow(It.IsAny<Buyer>())).Returns(true);

            // Given
            _cmd = MissingData.ValidCommand();

            // When
            MissingData.Process(_cmd, buyerPermissions: _buyerPermissions.Object);
        }

        [Fact]
        public void Is_allowed_for_purchases()
        {
            // Then
            _buyerPermissions.Verify(x => x.Allow(It.IsAny<Buyer>()));
        }

        [Fact]
        public void Is_able_to_purchase()
        {
            // Then
            Assert.True(_cmd.Result.SetupForPurchasing);
        }
    }

    public class A_user_with_role_APP_USED_VEHICLE_SALES_and_allow_buying_fails
    {
        private readonly Authenticate _cmd;
        private readonly Mock<ISetBuyerPermissions> _buyerPermissions;

        public A_user_with_role_APP_USED_VEHICLE_SALES_and_allow_buying_fails()
        {
            _buyerPermissions = new Mock<ISetBuyerPermissions>();
            _buyerPermissions.Setup(x => x.Allow(It.IsAny<Buyer>())).Returns(false);

            // Given
            _cmd = MissingData.ValidCommand();

            // When
            MissingData.Process(_cmd, buyerPermissions: _buyerPermissions.Object);
        }

        [Fact]
        public void Is_unable_to_purchase()
        {
            // Then
            Assert.False(_cmd.Result.SetupForPurchasing);
        }
    }

    public class A_valid_user
    {
        private readonly Authenticate _cmd;
        private readonly AuthenticatedUser _authenticatedUser;

        public A_valid_user()
        {
            // Given
            _cmd = MissingData.ValidCommand();
            var identityStore = new Mock<IUserIdentityStore>();
            _authenticatedUser = new AuthenticatedUser(
                token: "UserToken",
                username: _cmd.UserId,
                roles: new[] { _cmd.Role },
                email: _cmd.Email);
            identityStore.Setup(x => x.AuthenticateAs(It.IsAny<User>())).Returns(_authenticatedUser);

            // When
            MissingData.Process(_cmd, identityStore: identityStore.Object);

        }

        [Fact]
        public void Returns_an_authenticated_user()
        {
            Assert.NotNull(_cmd.Result.AuthenticatedUser);
        }

        [Fact]
        public void Returns_an_authorization_token()
        {
            Assert.Equal(_authenticatedUser.Token, _cmd.Result.AuthenticatedUser.Token);
        }

        [Fact]
        public void Returns_an_authorised_user_username()
        {
            Assert.Equal(_authenticatedUser.Username, _cmd.Result.AuthenticatedUser.Username);
        }

        [Fact]
        public void Returns_an_authorised_user_roles()
        {
            Assert.Equal(_authenticatedUser.Roles, _cmd.Result.AuthenticatedUser.Roles);
        }

        [Fact]
        public void Returns_an_authorised_user_email()
        {
            Assert.Equal(_authenticatedUser.Email, _cmd.Result.AuthenticatedUser.Email);
        }
    }

    public class An_unauthorised_remote_host
    {
        private readonly Authenticate _cmd;

        public An_unauthorised_remote_host()
        {
            var remoteAddressAuthenticator = new Mock<IRemoteAddressAuthenticator>();

            // Given
            _cmd = MissingData.ValidCommand();
            remoteAddressAuthenticator.Setup(x => x.IsAuthorised(It.IsAny<IPAddress>())).Returns(false);

            // When
            MissingData.Process(_cmd, remoteAddressAuthenticator: remoteAddressAuthenticator.Object);

        }

        [Fact]
        public void Is_unauthenticated()
        {
            // Then
            Assert.False(_cmd.Result.Authenticated);
        }
    }
}