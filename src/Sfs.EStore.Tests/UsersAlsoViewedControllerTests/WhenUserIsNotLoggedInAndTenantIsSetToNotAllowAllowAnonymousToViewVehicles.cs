using System.Collections.Generic;
using System.Net;
using Moq;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Controllers.Api;
using Sfs.EStore.Web.App.ThirdParties;
using Xunit;

namespace Sfs.EStore.Tests.UsersAlsoViewedControllerTests
{
    public class WhenUserIsNotLoggedInAndTenantIsSetToNotAllowAllowAnonymousToViewVehicles : ControllerTestBase
    {
        private readonly ApiControllerWrapper<UsersAlsoViewedController> _sut;

        public WhenUserIsNotLoggedInAndTenantIsSetToNotAllowAllowAnonymousToViewVehicles()
        {
            var tenant = this.CreateTenant();

            _sut = new ApiControllerWrapper<UsersAlsoViewedController>(Store, new UsersAlsoViewedController(new EmptyVehicleSearchHandler(), NullListingViewModelVisitor.Instance).WithJsonPostRequest())
                       {
                           Tenant = tenant,
                           Permissions = this.SetTenantPermissions(tenant, new[]
                                                  {
                                                      new KeyValuePair<string, string>("/Lots/View", ""),
                                                      new KeyValuePair<string, string>("/Lots/View/Prices", "")
                                                  })
                       };

            _sut.SetApiProxyResponse(HttpStatusCode.OK, ApiResponses.LotSearchResults())
                .SetJsonContentTypeHeader()
                .AddTotalResultsHeader(It.IsAny<int>());
        }

        [Fact]
        public void ShouldReturnEmptyArray()
        {
            Assert.Empty(_sut.Controller.Get(new UsersAlsoViewedController.UsersAlsoViewedGetModel()).Result.ReadAs<ListingSummaryViewModel[]>());
        }
    }
}