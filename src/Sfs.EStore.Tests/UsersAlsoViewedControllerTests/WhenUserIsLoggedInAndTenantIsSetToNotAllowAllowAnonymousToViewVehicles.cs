using System.Collections.Generic;
using System.Net;
using Moq;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Code.Cms;
using Sfs.EStore.Web.App.Controllers.Api;
using Sfs.EStore.Web.App.ThirdParties;
using Xunit;

namespace Sfs.EStore.Tests.UsersAlsoViewedControllerTests
{
    public class WhenUserIsLoggedInAndTenantIsSetToNotAllowAllowAnonymousToViewVehicles : ControllerTestBase
    {
        private readonly ApiControllerWrapper<UsersAlsoViewedController> _sut;

        public WhenUserIsLoggedInAndTenantIsSetToNotAllowAllowAnonymousToViewVehicles()
        {
            var tenant = this.CreateTenant();
            this.SetTenantPermissions(tenant, new[]
                                                  {
                                                      new KeyValuePair<string, string>("/Lots/View", PageAccess.Everyone),
                                                      new KeyValuePair<string, string>("/Lots/View/Prices", PageAccess.Authenticated)
                                                  });

            _sut = new ApiControllerWrapper<UsersAlsoViewedController>(Store, new UsersAlsoViewedController(new EmptyVehicleSearchHandler(), NullListingViewModelVisitor.Instance).WithJsonPostRequest())
                       {
                           Tenant = tenant
                       };

            _sut.SetAuthenticatedUser("foo");

            _sut.SetApiProxyResponse(HttpStatusCode.OK, ApiResponses.LotSearchResults())
                .SetJsonContentTypeHeader()
                .AddTotalResultsHeader(It.IsAny<int>());
        }

        [Fact]
        public void ShouldReturnLotsArray()
        {
            Assert.NotEmpty(_sut.Controller.Get(new UsersAlsoViewedController.UsersAlsoViewedGetModel()).Result.ReadAs<ListingSummaryViewModel[]>());
        }
    }
}