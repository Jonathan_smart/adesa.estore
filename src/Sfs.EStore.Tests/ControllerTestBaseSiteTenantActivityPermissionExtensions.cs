using System.Collections.Generic;
using Sfs.EStore.Web.App.Code;

namespace Sfs.EStore.Tests
{
    public static class ControllerTestBaseSiteTenantActivityPermissionExtensions
    {
        public static SiteTenant CreateTenant(this ControllerTestBase @this)
        {
            SiteTenant tenant;

            using (var s = @this.Store.OpenSession())
            {
                tenant = new SiteTenant();
                s.Store(tenant);

                s.SaveChanges();
            }

            return tenant;
        }

        public static SiteTenantActivityPermissions SetTenantPermissions(this ControllerTestBase @this, SiteTenant tenant, params KeyValuePair<string, string>[] permissions)
        {
            SiteTenantActivityPermissions activityPermissions;
            
            using (var s = @this.Store.OpenSession())
            {
                activityPermissions = new SiteTenantActivityPermissions()
                    .SetIdFromTenant(tenant);

                s.Store(activityPermissions);

                foreach (var permission in permissions)
                {
                    activityPermissions.AddActivity(permission.Key, permission.Value);
                }

                s.SaveChanges();

                
            }
            
            return activityPermissions;
        }
    }
}