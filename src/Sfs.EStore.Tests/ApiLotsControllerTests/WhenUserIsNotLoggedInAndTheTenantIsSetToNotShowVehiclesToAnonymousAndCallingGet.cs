using Sfs.EStore.Web.App.Code.Http;
using Sfs.EStore.Web.App.Controllers.Api;
using Sfs.EStore.Web.App.ThirdParties;
using Xunit;

namespace Sfs.EStore.Tests.ApiLotsControllerTests
{
    public class ControllerShouldHaveAuthorizeActivityAttribute : ControllerTestBase
    {
        private readonly AuthoriseActivityAttribute _attribute;

        public ControllerShouldHaveAuthorizeActivityAttribute()
        {
            _attribute = new LotsController(new EmptyVehicleSearchHandler(), NullListingViewModelVisitor.Instance, NullListingViewModelVisitor.Instance)
                .GetAttribute<AuthoriseActivityAttribute>();
        }

        [Fact]
        public void Attribute_ShouldBeOnClass()
        {
            Assert.NotNull(_attribute);
        }

        [Fact]
        public void Attribute_ShouldHaveActivityValueLotsView()
        {
            Assert.Equal("/Lots/View", _attribute.Activity);
        }
    }
}