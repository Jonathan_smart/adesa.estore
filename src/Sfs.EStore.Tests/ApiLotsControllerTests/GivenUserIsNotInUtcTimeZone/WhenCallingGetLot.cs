using System;
using System.Net;
using Moq;
using Newtonsoft.Json.Linq;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Controllers.Api;
using Sfs.EStore.Web.App.ThirdParties;
using Xunit;

namespace Sfs.EStore.Tests.ApiLotsControllerTests.GivenUserIsNotInUtcTimeZone
{
    public class WhenCallingGetLot : ControllerTestBase
    {
        private readonly ApiControllerWrapper<LotsController> _sut;

        public WhenCallingGetLot()
        {
            SystemTime.UtcDateTime = () => new DateTime(2013, 05, 24, 10, 20, 00);

            var tenant = this.CreateTenant();
            this.SetTenantPermissions(tenant);

            _sut = new ApiControllerWrapper<LotsController>(Store,
                new LotsController(new EmptyVehicleSearchHandler(), NullListingViewModelVisitor.Instance, NullListingViewModelVisitor.Instance).WithJsonPostRequest());

            _sut.SetAuthenticatedUser("foo");

            _sut.SetApiProxyResponse(HttpStatusCode.OK,
                                     ApiResponses.LotActiveBidOnBuyItNow())
                .SetJsonContentTypeHeader();
        }

        [Fact]
        public void EndDateShouldBeInUtcTime()
        {
            AssertResult(item =>
                         Assert.Equal(SystemTime.UtcNow, item["listingInfo"].Value<DateTime?>("endTime")));
        }

        [Fact]
        public void StartDateShouldBeInUtcTime()
        {
            AssertResult(item =>
                         Assert.Equal(SystemTime.UtcNow, item["listingInfo"].Value<DateTime?>("startTime")));
        }

        [Fact]
        public void BidDateShouldBeInUtcTime()
        {
            AssertResult(item =>
                             {
                                 foreach (JToken token in item["sellingStatus"]["bids"])
                                 {
                                     Assert.Equal(SystemTime.UtcNow, token.Value<DateTime?>("time"));
                                 }
                             });
        }

        [Fact]
        public void UserStatusBidLimitDateShouldBeInUtcTime()
        {
            AssertResult(item =>
                         Assert.Equal(SystemTime.UtcNow, item["biddingStatus"].Value<DateTime?>("maximumBidTime")));
        }

        private void AssertResult(Action<JToken> assertion)
        {
            var item = _sut.Controller.Get(It.IsAny<int>()).Result.ReadAs<JObject>();

            assertion(item);
        }
    }
}