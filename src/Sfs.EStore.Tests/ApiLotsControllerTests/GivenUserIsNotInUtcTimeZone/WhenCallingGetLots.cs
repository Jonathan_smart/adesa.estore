using System;
using System.Net;
using Newtonsoft.Json.Linq;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Controllers.Api;
using Sfs.EStore.Web.App.Models;
using Sfs.EStore.Web.App.ThirdParties;
using Xunit;

namespace Sfs.EStore.Tests.ApiLotsControllerTests.GivenUserIsNotInUtcTimeZone
{
    public class WhenCallingGetLots : ControllerTestBase
    {
        private readonly ApiControllerWrapper<LotsController> _sut;

        public WhenCallingGetLots()
        {
            SystemTime.UtcDateTime = () => new DateTime(2013, 05, 24, 10, 20, 00);
            _sut =
                new ApiControllerWrapper<LotsController>(Store,
                    new LotsController(new EmptyVehicleSearchHandler(), NullListingViewModelVisitor.Instance, NullListingViewModelVisitor.Instance).WithJsonPostRequest());

            _sut.SetAuthenticatedUser("foo");

            _sut.SetApiProxyResponse(HttpStatusCode.OK,
                                     new[]
                                         {
                                             new
                                                 {
                                                     ListingInfo = new
                                                                         {
                                                                             StartTime = SystemTime.UtcNow,
                                                                             EndTime = SystemTime.UtcNow
                                                                         }
                                                 }
                                         })
                .SetJsonContentTypeHeader();
        }

        [Fact]
        public void EndDateShouldBeInLocalTime()
        {
            AssertEachResult(item =>
                Assert.Equal(SystemTime.UtcNow.ToLocalTime(), item["listingInfo"].Value<DateTime?>("endTime")));
        }

        [Fact]
        public void StartDateShouldBeInLocalTime()
        {
            AssertEachResult(item =>
                Assert.Equal(SystemTime.UtcNow.ToLocalTime(), item["listingInfo"].Value<DateTime?>("startTime")));
        }

        private void AssertEachResult(Action<JToken> assertion)
        {
            var jArray = _sut.Controller.Post(new LotsGetParams())
                .Result.ReadAs<JArray>();

            foreach (var item in jArray)
            {
                assertion(item);
            }
        }
    }
}