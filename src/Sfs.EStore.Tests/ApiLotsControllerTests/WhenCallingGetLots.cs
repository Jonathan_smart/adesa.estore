using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using Sfs.EStore.Web.App.Controllers.Api;
using Sfs.EStore.Web.App.DomainEvents;
using Sfs.EStore.Web.App.Models;
using Sfs.EStore.Web.App.ThirdParties;
using Xunit;

namespace Sfs.EStore.Tests.ApiLotsControllerTests
{
    public class WhenCallingGetLots : ControllerTestBase
    {
        private readonly ApiControllerWrapper<LotsController> _sut;
        private UserSearched _handledDomainEvent;
        private readonly HttpResponseMessage _response;

        public WhenCallingGetLots()
        {
            var tenant = this.CreateTenant();
            this.SetTenantPermissions(tenant, new[]
                                                  {
                                                      new KeyValuePair<string, string>("/Lots/View", ""),
                                                      new KeyValuePair<string, string>("/Lots/View/Prices", "")
                                                  });

            _sut = new ApiControllerWrapper<LotsController>(Store, new LotsController(new EmptyVehicleSearchHandler(), NullListingViewModelVisitor.Instance, NullListingViewModelVisitor.Instance).WithJsonPostRequest())
                       {
                           Tenant = tenant
                       };

            _sut.SetAuthenticatedUser("foo");
            new DomainEventSystemUnderTest().Handle<UserSearched>(args => _handledDomainEvent = args);

            _sut.SetApiProxyResponse(HttpStatusCode.OK, ApiResponses.LotSearchResults())
                .SetJsonContentTypeHeader()
                .AddTotalResultsHeader(2);

            _response = _sut.Controller.Post(new LotsGetParams()).Result;
        }

        [Fact]
        public void ShouldContainXTotalResultsHeader()
        {
            var header = _response.Headers.FirstOrDefault(x => x.Key == "X-TotalResults");

            Assert.NotNull(header.Key);
            Assert.Equal(header.Value.First(), "2");
        }

        [Fact]
        public void ShouldRaiseUserSearchedDomainEvent()
        {
            Assert.NotNull(_handledDomainEvent);
        }
    }
}