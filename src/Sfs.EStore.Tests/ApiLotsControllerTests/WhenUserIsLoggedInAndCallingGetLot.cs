using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json.Linq;
using Sfs.EStore.Web.App.Controllers.Api;
using Sfs.EStore.Web.App.ThirdParties;
using Xunit;

namespace Sfs.EStore.Tests.ApiLotsControllerTests
{
    public class WhenUserIsLoggedInAndCallingGetLot : ControllerTestBase
    {
        private readonly ApiControllerWrapper<LotsController> _sut;

        public WhenUserIsLoggedInAndCallingGetLot()
        {
            var tenant = this.CreateTenant();
            this.SetTenantPermissions(tenant, new[]
                                                  {
                                                      new KeyValuePair<string, string>("/Lots/View", ""),
                                                      new KeyValuePair<string, string>("/Lots/View/Prices", "")
                                                  });

            _sut = new ApiControllerWrapper<LotsController>(Store, new LotsController(new EmptyVehicleSearchHandler(), NullListingViewModelVisitor.Instance, NullListingViewModelVisitor.Instance).WithJsonPostRequest())
                       {
                           Tenant = tenant
                       };

            _sut.SetAuthenticatedUser("foo");

            _sut.SetApiProxyResponse(HttpStatusCode.OK, ApiResponses.LotActiveBidOnBuyItNow())
                .SetJsonContentTypeHeader();
        }

        [Fact]
        public void ShouldSeeCurrentPrice()
        {
            _sut.Controller.Get(100).Result
                .ReadAs<JObject>()["sellingStatus"]["currentPrice"]
                .AssertThatToken<decimal?>("amount", isEqualTo: 14760);
        }

        [Fact]
        public void ShouldSeeBuyItNowPrice()
        {
            _sut.Controller.Get(100).Result
                .ReadAs<JObject>()["listingInfo"]["buyItNowPrice"]
                .AssertThatToken<decimal?>("amount", isEqualTo: 39360);
        }

        [Fact]
        public void ShouldSeeNumberOfBids()
        {
            _sut.Controller.Get(100).Result
                .ReadAs<JObject>()
                .AssertThat<object>(
                    valueAccessor: j => j["sellingStatus"].Value<int>("bidCount"),
                    assertion: j => Assert.Equal(1, j));
        }

        [Fact]
        public void ShouldSeeBidPrice()
        {
            _sut.Controller.Get(100).Result
                .ReadAs<JObject>()
                .AssertThat<object>(
                    valueAccessor: j => j["sellingStatus"].Value<JArray>("bids")[0]["amount"].Value<decimal?>("amount"),
                    assertion: j => Assert.Equal(14760m, j));
        }

        [Fact]
        public void ShouldSeeMinimumBidRequired()
        {
            _sut.Controller.Get(100).Result
                .ReadAs<JObject>()
                .AssertThat<decimal?>(j => j["sellingStatus"]["minimumToBid"].Value<decimal?>("amount"), isEqualTo: 14785m);
        }

        [Fact]
        public void ShouldSeeWasPrice()
        {
            _sut.Controller.Get(100).Result
                .ReadAs<JObject>()
                .AssertThat<decimal?>(j => j["discountPriceInfo"]["wasPrice"].Value<decimal?>("amount"), isEqualTo: 15000m);
        }
    }
}