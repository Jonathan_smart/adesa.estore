using System.Collections.Generic;
using System.Net;
using Moq;
using Newtonsoft.Json.Linq;
using Sfs.EStore.Web.App.Code.Cms;
using Sfs.EStore.Web.App.Controllers.Api;
using Sfs.EStore.Web.App.ThirdParties;
using Xunit;
using Sfs.EStore.Web.App.Code;

namespace Sfs.EStore.Tests.ApiLotsControllerTests
{
    public class WhenUserIsNotLoggedInAndTheTenantIsSetNotToShowPricesToAnonymousAndCallingGetLot : ControllerTestBase
    {
        private readonly ApiControllerWrapper<LotsController> _sut;

        public WhenUserIsNotLoggedInAndTheTenantIsSetNotToShowPricesToAnonymousAndCallingGetLot()
        {
            var tenant = this.CreateTenant();

            _sut = new ApiControllerWrapper<LotsController>(Store,
                                                            new LotsController(new EmptyVehicleSearchHandler(), NullListingViewModelVisitor.Instance, NullListingViewModelVisitor.Instance)
                                                                .WithJsonPostRequest())
                       {
                           Tenant = tenant,
                           Permissions = this.SetTenantPermissions(tenant, new[]
                                                                               {
                                                                                   new KeyValuePair<string, string>(
                                                                                       "/Lots/View", PageAccess.Everyone),
                                                                                   new KeyValuePair<string, string>(
                                                                                       "/Lots/View/Prices", PageAccess.Authenticated)
                                                                               })
                       };

            _sut.SetApiProxyResponse(HttpStatusCode.OK, ApiResponses.LotActiveBidOnBuyItNow())
                .SetJsonContentTypeHeader();

            //_sut.Session
        }

        [Fact]
        public void CurrentPriceShouldBeNull()
        {
            _sut.Controller.Get(It.IsAny<int>()).Result
                .ReadAs<JObject>()
                .AssertThat("currentPrice", isEqualTo: (decimal?)null);
        }

        [Fact]
        public void BuyItNowPriceShouldBeNull()
        {
            _sut.Controller.Get(It.IsAny<int>()).Result
                .ReadAs<JObject>()
                .AssertThat(j => j["listingInfo"]["buyItNowPrice"].ToObject(typeof(object)), isEqualTo: null);
        }

        [Fact]
        public void WasPriceShouldBeNull()
        {
            _sut.Controller.Get(It.IsAny<int>()).Result
                .ReadAs<JObject>()
                .AssertThat(j => j["discountPriceInfo"]["wasPrice"], isEqualTo: null);
        }

        [Fact]
        public void BiddingNumberOfBidsShouldBeAvailable()
        {
            _sut.Controller.Get(It.IsAny<int>()).Result
                .ReadAs<JObject>()
                .AssertThat(valueAccessor: j => j["sellingStatus"].Value<int>("bidCount"),
                            isEqualTo: 1);
        }

        [Fact]
        public void BiddingBidsPriceShouldBeNull()
        {
            _sut.Controller.Get(It.IsAny<int>()).Result
                .ReadAs<JObject>()
                .AssertThat<JToken>(valueAccessor: j => j["sellingStatus"].Value<JArray>("bids")[0]["amount"],
                                    assertion: j => Assert.False(j.HasValues));
        }

        [Fact]
        public void BiddingMinimumBidRequiredShouldBeNull()
        {
            _sut.Controller.Get(It.IsAny<int>()).Result
                .ReadAs<JObject>()
                .AssertThat<object>(valueAccessor: j => j["sellingStatus"]["minimumToBid"],
                                    isEqualTo: null);
        }
    }
}