using System.Collections.Generic;
using System.Linq;
using System.Net;
using Sfs.EStore.Web.App.Code.Cms;
using Sfs.EStore.Web.App.Controllers.Api;
using Sfs.EStore.Web.App.Models;
using Sfs.EStore.Web.App.ThirdParties;
using Xunit;

namespace Sfs.EStore.Tests.ApiLotsControllerTests
{
    public class WhenCallingGetLots2 : ControllerTestBase
    {
        private readonly ApiControllerWrapper<LotsController> _sut;

        public WhenCallingGetLots2()
        {
            var tenant = this.CreateTenant();
            this.SetTenantPermissions(tenant, new[]
                                                  {
                                                      new KeyValuePair<string, string>("/Lots/View", PageAccess.Everyone),
                                                      new KeyValuePair<string, string>("/Lots/View/Prices", PageAccess.Authenticated)
                                                  });

            _sut = new ApiControllerWrapper<LotsController>(Store,
                new LotsController(new EmptyVehicleSearchHandler(), NullListingViewModelVisitor.Instance, NullListingViewModelVisitor.Instance).WithJsonPostRequest())
                       {
                           Tenant = tenant
                       };

            _sut.SetAuthenticatedUser("foo");

            _sut.SetApiProxyResponse(HttpStatusCode.OK, ApiResponses.LotSearchResults())
                .SetJsonContentTypeHeader()
                .AddTotalResultsHeader(2);
        }

        [Fact]
        public void ShouldContainXTotalResultsHeader()
        {
            var header = _sut.Controller.Post(new LotsGetParams())
                .Result.Headers.FirstOrDefault(x => x.Key == "X-TotalResults");

            Assert.NotNull(header.Key);
            Assert.Equal(header.Value.First(), "2");
        }
    }
}