using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json.Linq;
using Sfs.EStore.Web.App.Code.Cms;
using Sfs.EStore.Web.App.Controllers.Api;
using Sfs.EStore.Web.App.Models;
using Sfs.EStore.Web.App.ThirdParties;
using Xunit;

namespace Sfs.EStore.Tests.ApiLotsControllerTests
{
    public class WhenUserIsNotLoggedInAndTheTenantIsSetNotToShowPricesToAnonymousAndCallingGetLots : ControllerTestBase
    {
        private readonly ApiControllerWrapper<LotsController> _sut;

        public WhenUserIsNotLoggedInAndTheTenantIsSetNotToShowPricesToAnonymousAndCallingGetLots()
        {
            var tenant = this.CreateTenant();

            _sut = new ApiControllerWrapper<LotsController>(Store, new LotsController(new EmptyVehicleSearchHandler(), NullListingViewModelVisitor.Instance, NullListingViewModelVisitor.Instance).WithJsonPostRequest())
                       {
                           Tenant = tenant,
                           Permissions = this.SetTenantPermissions(tenant, new[]
                                                  {
                                                      new KeyValuePair<string, string>("/Lots/View", PageAccess.Everyone),
                                                      new KeyValuePair<string, string>("/Lots/View/Prices", PageAccess.Authenticated)
                                                  })
                       };

            _sut.SetApiProxyResponse(HttpStatusCode.OK, ApiResponses.LotSearchResults())
                .SetJsonContentTypeHeader()
                .AddTotalResultsHeader(2);
        }

        private JArray Result()
        {
            return _sut.Controller.Post(new LotsGetParams()).Result.ReadAs<JArray>();
        }

        [Fact]
        public void CurrentPriceShouldBeNull()
        {
            Result().AssertThatEach(j => j["sellingStatus"].Value<decimal?>("currentPrice"), isEqualTo: (decimal?)null);
        }

        [Fact]
        public void BuyItNowPriceShouldBeNull()
        {
            Result().AssertThatEach(j => j["listingInfo"].Value<decimal?>("buyItNowPrice"), isEqualTo: (decimal?)null);
        }

        [Fact]
        public void WasPriceShouldBeNull()
        {
            Result().AssertThatEach(j => j["discountPriceInfo"]["wasPrice"], isEqualTo: (object)null);
        }

        [Fact]
        public void BiddingNumberOfBidsShouldBeAvailable()
        {
            Result().AssertThatEach(j => j["sellingStatus"].Value<int>("bidCount"), isEqualTo: 1);
        }
    }
}