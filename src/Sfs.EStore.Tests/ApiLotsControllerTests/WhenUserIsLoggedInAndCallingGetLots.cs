using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using Moq;
using Newtonsoft.Json.Linq;
using Sfs.EStore.Web.App.Controllers.Api;
using Sfs.EStore.Web.App.Models;
using Sfs.EStore.Web.App.ThirdParties;
using Xunit;

namespace Sfs.EStore.Tests.ApiLotsControllerTests
{
    public class WhenUserIsLoggedInAndCallingGetLots : ControllerTestBase
    {
        private readonly ApiControllerWrapper<LotsController> _sut;

        public WhenUserIsLoggedInAndCallingGetLots()
        {
            var tenant = this.CreateTenant();
            this.SetTenantPermissions(tenant, new[]
                                                  {
                                                      new KeyValuePair<string, string>("/Lots/View", ""),
                                                      new KeyValuePair<string, string>("/Lots/View/Prices", "")
                                                  });

            _sut = new ApiControllerWrapper<LotsController>(Store, new LotsController(new EmptyVehicleSearchHandler(), NullListingViewModelVisitor.Instance, NullListingViewModelVisitor.Instance).WithJsonPostRequest())
                       {
                           Tenant = tenant
                       };

            _sut.SetAuthenticatedUser("foo");

            _sut.SetApiProxyResponse(HttpStatusCode.OK, ApiResponses.LotSearchResults())
                .SetJsonContentTypeHeader()
                .AddTotalResultsHeader(It.IsAny<int>());
        }

        private HttpResponseMessage Result()
        {
            return _sut.Controller.Post(new LotsGetParams()).Result;
        }

        [Fact]
        public void ShouldSeeCurrentPrice()
        {
            AssertThatEachResult(j => j["sellingStatus"]["currentPrice"].Value<decimal?>("amount"), 8000, 6000);
        }

        [Fact]
        public void ShouldSeeBuyItNowPrice()
        {
            AssertThatEachResult(j => j["listingInfo"]["buyItNowPrice"].Value<decimal?>("amount"), 14000, 10500);
        }

        [Fact]
        public void ShouldSeeNumberOfBids()
        {
            AssertThatEachResult(j => j["sellingStatus"].Value<int>("bidCount"), isEqualTo: 1);
        }

        [Fact]
        public void ShouldSeeWasPrice()
        {
            AssertThatEachResult(j => j["discountPriceInfo"]["wasPrice"].Value<decimal?>("amount"), isEqualTo: 13000);
        }

        private void AssertThatEachResult<TValue>(Func<JObject, TValue> valueAccessor, params TValue[] isEqualTo)
        {
            Result().ReadAs<JArray>().AssertThatEach(valueAccessor, isEqualTo);
        }
    }
}