using System;
using Raven.Client;
using Sfs.EStore.Web.App.Code.Security;

namespace Sfs.EStore.Tests.AffiliateControllerTests
{
    public class TicketFactory
    {
        private readonly IDocumentStore _store;

        public TicketFactory(IDocumentStore store)
        {
            _store = store;
        }

        public GuestPassTicket Create(string source, TimeSpan timeToLive, string campaign, string redirectTo = "")
        {
            var ticket = new GuestPassTicket("adminUser", source, timeToLive, campaign, redirectTo);

            return ticket;
        }

        public GuestPassTicket CreateAndStore(string source, TimeSpan timeToLive, string campaign, string redirectTo = "")
        {
            var ticket = Create(source, timeToLive, campaign, redirectTo);

            using (var session = _store.OpenSession())
            {
                session.Store(ticket);
                session.SaveChanges();
            }

            return ticket;
        }
    }
}