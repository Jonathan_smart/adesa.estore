using System;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.Security;
using Sfs.EStore.Web.App.Controllers;
using Xunit;

namespace Sfs.EStore.Tests.AffiliateControllerTests
{
    public class GivenAnOldStyleTripleDESCryptoServiceProviderEncryptedTicket : ControllerTestBase
    {
        private readonly ActionResult _actionResult;

        private const string Source = "foo";
        private const string Campaign = "MyCampaign";
        private const string RedirectTo = "/bar";

        public GivenAnOldStyleTripleDESCryptoServiceProviderEncryptedTicket()
        {
            GuestPass.Key = Convert.FromBase64String("S/wCSUvUVrObTlRtj7gVoxRwEm7UdppT");
            GuestPass.IV = Convert.FromBase64String("MDJGKDqSUP0=");
            SystemTime.UtcDateTime = () => new DateTime(2013, 5, 30, 15, 29, 00);

            // When
            new TicketFactory(Store).Create(Source, TimeSpan.FromHours(1), Campaign);
            const string oldStyleTokenFromTicket = "JGpP4iWsbQGvN1OUr4K5rVOGP3LLkdy4/ymveQ2El7y7/t2eBQvu8WxTIgs8fWLiWhKn6+h9XMrM3EnxygpmiRJzGdV2F3L6ZN0wWlZ2GTEmhfPpKmmvUlEMrbFCoEmNctNKqEm+pTLZ4pWYRwqKJODxB8PF87rCcALYungmziCnKWt4lPqZrSkkk/3h7Zyj/IEGX70QWgZQWNr6vyIKmk9x1EZPCUFDAKtCYhVAu68wd+mExopbfU9qO1nZw+eb9PgihupoPOI=";
            var ctx = new MvcControllerWrapper<AffiliateController>(new AffiliateController(), Store);

            _actionResult = ctx.Controller.Index(oldStyleTokenFromTicket, RedirectTo);
            using (ctx.Controller.Session)
            {
                ctx.Controller.Session.SaveChanges();
            }
        }

        [Fact]
        public void Then_ShouldReturnStatus200OK()
        {
            Assert.IsType<RedirectResult>(_actionResult);
        }
    }
}