using System.Web.Mvc;
using Sfs.EStore.Web.App.Controllers;
using Xunit;

namespace Sfs.EStore.Tests.AffiliateControllerTests
{
    public class GivenAnInvalidTicket : ControllerTestBase
    {
        private readonly ActionResult _actionResult;

        public GivenAnInvalidTicket()
        {
            // When
            const string invalidToken = "foobar";
            _actionResult = new MvcControllerWrapper<AffiliateController>(new AffiliateController(), Store)
                .Controller.Index(invalidToken);
        }

        [Fact]
        public void Then_ShouldReturn401Unauthorized()
        {
            Assert.IsType<HttpUnauthorizedResult>(_actionResult);
        }
    }
}