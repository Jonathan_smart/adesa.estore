using System;
using System.Web.Mvc;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.Security;
using Sfs.EStore.Web.App.Controllers;
using Xunit;

namespace Sfs.EStore.Tests.AffiliateControllerTests
{
    public class GivenANonExistantToken : ControllerTestBase
    {
        private readonly ActionResult _actionResult;
        private const string Source = "foo";

        public GivenANonExistantToken()
        {
            SystemTime.UtcDateTime = () => new DateTime(2013, 5, 30, 15, 29, 00);
            // When
            var nonExistantToken = "";

            using (var session = Store.OpenSession())
            {
                var id = new TicketFactory(Store).CreateAndStore(Source, TimeSpan.FromHours(1), "MyCampaign", "/bar").Id;

                var ticket = session.Load<GuestPassTicket>(id);
                nonExistantToken = GuestPass.Encrypt(ticket);

                session.Delete(ticket);
                session.SaveChanges();
            }

            var ctx = new MvcControllerWrapper<AffiliateController>(new AffiliateController(), Store);

            _actionResult = ctx.Controller.Index(nonExistantToken);
        }

        [Fact]
        public void Then_ShouldReturn401Unauthorized()
        {
            Assert.IsType<HttpUnauthorizedResult>(_actionResult);
        }
    }
}