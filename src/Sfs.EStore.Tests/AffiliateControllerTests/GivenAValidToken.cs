using System;
using System.Web;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.Security;
using Sfs.EStore.Web.App.Controllers;
using Xunit;

namespace Sfs.EStore.Tests.AffiliateControllerTests
{
    public class GivenAValidToken : ControllerTestBase
    {
        private readonly GuestPassTicket _ticket;
        private readonly HttpCookie _cookie;
        private const string Source = "foo";

        public GivenAValidToken()
        {
            SystemTime.UtcDateTime = () => new DateTime(2013, 5, 30, 15, 29, 00);
            // When
            _ticket = new TicketFactory(Store).CreateAndStore(Source, TimeSpan.FromHours(1), "MyCampaign", "/bar");

            var ctx = new MvcControllerWrapper<AffiliateController>(new AffiliateController(), Store);

            ctx.Controller.Index(GuestPass.Encrypt(_ticket));

            _cookie = ctx.Controller.Response.Cookies[GuestPass.CookieName];
        }

        [Fact]
        public void Then_ShouldReturnSetCookie()
        {
            Assert.NotNull(_cookie);
        }

        [Fact]
        public void ThenCookieValue_ShouldEqualToken()
        {
            Assert.Equal(GuestPass.Encrypt(_ticket), _cookie.Value);
        }

        [Fact]
        public void ThenCookieExpiryDate_ShouldEqual1HourFromNow()
        {
            Assert.Equal(SystemTime.UtcNow + _ticket.TimeToLive, _cookie.Expires);
        }
    }
}