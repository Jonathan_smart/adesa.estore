using System;
using System.Web.Mvc;
using Sfs.EStore.Web.App.Code.Security;
using Sfs.EStore.Web.App.Controllers;
using Xunit;

namespace Sfs.EStore.Tests.AffiliateControllerTests
{
    public class GivenAValidTokenAndRedirect : ControllerTestBase
    {
        private readonly ActionResult _actionResult;
        private const string RedirectUrl = "http://google.com";

        public GivenAValidTokenAndRedirect()
        {
            // When
            var ticket = new TicketFactory(Store).CreateAndStore("foo", TimeSpan.FromHours(1), "MyCampaign", RedirectUrl);
            var token = GuestPass.Encrypt(ticket);
            var ctx = new MvcControllerWrapper<AffiliateController>(new AffiliateController(), Store);

            _actionResult = ctx.Controller.Index(token);
        }

        [Fact]
        public void Then_ShouldReturnRedirectResultToUrl()
        {
            Assert.Equal(RedirectUrl,
                         Assert.IsType<RedirectResult>(_actionResult).Url);
        }
    }
}