using System;
using System.Web;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.Security;
using Sfs.EStore.Web.App.Controllers;
using Xunit;

namespace Sfs.EStore.Tests.AffiliateControllerTests
{
    public class GivenAnOldStyleNonPersistentTicket : ControllerTestBase
    {
        private readonly HttpCookie _cookie;
        private readonly GuestPassTicket _ticket;

        private const string Source = "foo";
        private const string Campaign = "MyCampaign";
        private const string RedirectTo = "/bar";

        public GivenAnOldStyleNonPersistentTicket()
        {
            GuestPass.Key = Convert.FromBase64String("S/wCSUvUVrObTlRtj7gVoxRwEm7UdppT");
            GuestPass.IV = Convert.FromBase64String("MDJGKDqSUP0=");
            SystemTime.UtcDateTime = () => new DateTime(2013, 5, 30, 15, 29, 00);

            // When
            _ticket = new TicketFactory(Store).Create(Source, TimeSpan.FromHours(1), Campaign);
            const string oldStyleToken = "JGpP4iWsbQGvN1OUr4K5rVOGP3LLkdy4/ymveQ2El7y7/t2eBQvu8WxTIgs8fWLiWhKn6+h9XMrM3EnxygpmiRJzGdV2F3L6ZN0wWlZ2GTEmhfPpKmmvUlEMrbFCoEmNctNKqEm+pTLZ4pWYRwqKJODxB8PF87rCcALYungmziCnKWt4lPqZrSkkk/3h7Zyj/IEGX70QWgZQWNr6vyIKmk9x1EZPCUFDAKtCYhVAu68wd+mExopbfU9qO1nZw+eb9PgihupoPOI=";

            var ctx = new MvcControllerWrapper<AffiliateController>(new AffiliateController(), Store);

            ctx.Controller.Index(oldStyleToken, RedirectTo);
            using (ctx.Controller.Session)
            {
                ctx.Controller.Session.SaveChanges();
            }

            _cookie = ctx.Controller.Response.Cookies[GuestPass.CookieName];
        }

        [Fact]
        public void Then_ShouldReturnSetCookie()
        {
            Assert.NotNull(_cookie);
        }

        [Fact]
        public void Then_ShouldCreateNewGuestPassTicket()
        {
            var decryptedCookie = GuestPass.Decrypt(_cookie.Value);

            using (var session = Store.OpenSession())
            {
                var ticket = session.Load<GuestPassTicket>(decryptedCookie.Id);

                Assert.NotNull(ticket);
                Assert.Equal(decryptedCookie.Id, ticket.Id);
                Assert.Equal(RedirectTo, ticket.RedirectTo);
                Assert.Equal(Source, ticket.Source);
                Assert.Equal(Campaign, ticket.Campaign);
            }
        }
    }
}