using System;
using System.Web.Mvc;
using Sfs.EStore.Web.App.Code.Security;
using Sfs.EStore.Web.App.Controllers;
using Xunit;

namespace Sfs.EStore.Tests.AffiliateControllerTests
{
    public class GivenAValidTokenWithoutRedirect : ControllerTestBase
    {
        private readonly ActionResult _actionResult;

        public GivenAValidTokenWithoutRedirect()
        {
            // When
            var ticket = new TicketFactory(Store).CreateAndStore("foo", TimeSpan.FromHours(1), "MyCampaign");
            
            var token = GuestPass.Encrypt(ticket);
            var ctx = new MvcControllerWrapper<AffiliateController>(new AffiliateController(), Store);

            _actionResult = ctx.Controller.Index(token);
        }

        [Fact]
        public void Then_ShouldReturnRedirectToRouteResult()
        {
            var redirectToRoute = Assert.IsType<RedirectToRouteResult>(_actionResult);

            Assert.Equal("cmspages", redirectToRoute.RouteValues["controller"]);
            Assert.Equal("index", redirectToRoute.RouteValues["action"]);
            Assert.Equal("", redirectToRoute.RouteValues["area"]);
            Assert.Equal("home", redirectToRoute.RouteValues["id"]);
        }
    }
}