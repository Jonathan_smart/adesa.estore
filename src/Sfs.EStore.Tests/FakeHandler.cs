using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Sfs.EStore.Tests
{
    public class FakeHandler : DelegatingHandler
    {
        public HttpResponseMessage Response { get; set; }
        
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
                                                               CancellationToken cancellationToken)
        {
            return Response == null
                       ? base.SendAsync(request, cancellationToken)
                       : Task.Factory.StartNew(() => Response, cancellationToken);
        }
    }
}