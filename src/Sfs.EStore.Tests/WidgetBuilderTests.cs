﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Sfs.EStore.Web.App;
using Sfs.EStore.Web.App.Code.Widgets;
using Xunit;

namespace Sfs.EStore.Tests
{
    public class WidgetBuilderTests
    {
        private readonly WidgetSet _widgetSet;
        public WidgetBuilderTests()
        {
            _widgetSet = new WidgetSet(new Dictionary<string, Func<Widget>>
                {
                    { "first", () => new BasicWidget() },
                    { "second", () => new BasicWidget() },
                    { "third", () => new BasicWidget() },
                    { "single", () => new BasicWidget() }
                });
        }

        public static HtmlHelper CreateHtmlHelper()
        {
            return new HtmlHelper(new ViewContext { Writer = new StringWriter() }, new ViewPage());
        }

        public static string ExecuteWidgetBuilder(WidgetBuilder widgetBuilder)
        {
            var handler = CreateHtmlHelper();
            widgetBuilder.RenderOutput(handler);
            return handler.ViewContext.Writer.ToString();
        }

        [Fact]
        public void CanParseToFindNoWidgets()
        {
            const string body = "this is a body with no widgets";
            var builder = new WidgetBuilder(_widgetSet).ApplyBodyContent(body);
            Assert.Empty(builder.MatchWidgetPairs);
        }

        [Fact]
        public void CanParseToFindSingleWidgetWithNoProperties()
        {
            const string firstTag = "{{first}}";
            var body = string.Format("this is a body with a single widget {0}", firstTag);
            var builder = new WidgetBuilder(_widgetSet).ApplyBodyContent(body);
            ExecuteWidgetBuilder(builder);
            Assert.Single(builder.MatchWidgetPairs);
            var pair = builder.MatchWidgetPairs.First();
            Assert.Empty(pair.Widget.Properties);
        }

        [Fact]
        public void CanParseToFindSingleWidgetWithSingleProperty()
        {
            const string name = "first";
            const string propName = "name";
            const string propValue = "value";
            const string firstTag = "{{" + name + " " + propName + ":\"" + propValue + "\"}}";
            var body = string.Format("this is a body with a single widget with a single value {0}", firstTag);
            var builder = new WidgetBuilder(_widgetSet).ApplyBodyContent(body);
            ExecuteWidgetBuilder(builder);
            Assert.Single(builder.MatchWidgetPairs);
            var pair = builder.MatchWidgetPairs.First();
            Assert.Single(pair.Widget.Properties);
            var property = pair.Widget.Properties.First();
            Assert.Equal(propName, property.Key);
            Assert.Equal(propValue, property.Value);
            Assert.IsType<string>(property.Value);
        }

        [Fact]
        public void CanParseToFindWidgetWithIntValueProperty()
        {
            const string propName = "int";
            const int propValue = 20;
            var firstTag = "{{first " + propName + ":\"" + propValue + "\"}}";
            var body = string.Format("this is the body {0}", firstTag);
            var builder = new WidgetBuilder(_widgetSet).ApplyBodyContent(body).RenderOutput(CreateHtmlHelper());
            Assert.Single(builder.MatchWidgetPairs);
            Assert.Single(builder.MatchWidgetPairs[0].Widget.Properties);
            Assert.IsType<int>(builder.MatchWidgetPairs[0].Widget.Properties[propName]);
            Assert.Equal(propValue, builder.MatchWidgetPairs[0].Widget.Properties[propName]);
        }

        [Fact]
        public void CanParseToFindWidgetWithDecimalValueProperty()
        {
            const string propName = "decimal";
            const decimal propValue = 20.2m;
            var firstTag = "{{first " + propName + ":\"" + propValue + "\"}}";
            var body = string.Format("this is the body {0}", firstTag);
            var builder = new WidgetBuilder(_widgetSet).ApplyBodyContent(body).RenderOutput(CreateHtmlHelper());
            Assert.Single(builder.MatchWidgetPairs);
            Assert.Single(builder.MatchWidgetPairs[0].Widget.Properties);
            Assert.IsType<decimal>(builder.MatchWidgetPairs[0].Widget.Properties[propName]);
            Assert.Equal(propValue, builder.MatchWidgetPairs[0].Widget.Properties[propName]);
        }

        [Fact]
        public void CanParseToFindWidgetWithDateTimeValueProperty()
        {
            const string propName = "datetime";
            var now = DateTime.Now;
            var propValue = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second);
            Console.WriteLine("propValue: " + propValue);
            var firstTag = "{{first " + propName + ":\"" + propValue + "\"}}";
            var body = string.Format("this is the body {0}", firstTag);
            var builder = new WidgetBuilder(_widgetSet).ApplyBodyContent(body).RenderOutput(CreateHtmlHelper());
            Assert.Single(builder.MatchWidgetPairs);
            Assert.Single(builder.MatchWidgetPairs[0].Widget.Properties);
            Assert.IsType<DateTime>(builder.MatchWidgetPairs[0].Widget.Properties[propName]);
            Assert.Equal(propValue, builder.MatchWidgetPairs[0].Widget.Properties[propName]);
        }

        [Fact]
        public void CanParseToFindThreeWidgetsEachWithThreeProperties()
        {
            const string body = "this is a body with multiple widgets with multiple values {{first name1:\"value1\" name2:\"value2\" name3:\"value3\"}}" +
                                " {{second name1:\"value1\" name2:\"value2\" name3:\"value3\"}}{{third name1:\"value1\" name2:\"value2\" name3:\"value3\"}}";
            var builder = new WidgetBuilder(_widgetSet).ApplyBodyContent(body);
            var response = ExecuteWidgetBuilder(builder);
            Assert.Equal(3, builder.MatchWidgetPairs.Count);
            foreach (var pair in builder.MatchWidgetPairs)
            {
                Assert.Equal(3, pair.Widget.Properties.Count);
            }
            Console.WriteLine(response);
        }

        [Fact]
        public void CanParseToFindNoWidgetsWithInvalidCharsInNameOrProperties()
        {
            const string body = "this is a body with invalid widgets {{}} {{first-}} {{-second}} {{third name-=\"value\"}} {{fourth name=\"value}} {{fifth name=\"value=\"}} {{sixth";
            var builder = new WidgetBuilder(_widgetSet).ApplyBodyContent(body);
            Assert.Empty(builder.MatchWidgetPairs);
        }

        [Fact]
        public void CanParseToFindTwoIdenticalWidgets()
        {
            const string widgetText = "{{single name:\"value\"}}";
            const string body = "this is the body with two identical widgets " + widgetText + widgetText;
            var builder = new WidgetBuilder(_widgetSet).ApplyBodyContent(body);
            ExecuteWidgetBuilder(builder);
            var pairs = builder.MatchWidgetPairs;
            Assert.Equal(2, pairs.Count);
            Assert.Equal(pairs[0].Match.Value, pairs[1].Match.Value);
            Assert.Equal(pairs[0].Widget.Properties, pairs[1].Widget.Properties);
        }

        [Fact]
        public void CanParseToFindWidgetWhenTagInCaps()
        {
            const string widgetText = "{{FIRST NAME:\"VALUE\"}}";
            const string body = "this is the body " + widgetText;
            var builder = new WidgetBuilder(_widgetSet).ApplyBodyContent(body);
            ExecuteWidgetBuilder(builder);
            var pairs = builder.MatchWidgetPairs;
            Assert.Single(pairs);
        }

        [Fact]
        public void CanParseToFindPropertyWhenTagInCaps()
        {
            const string widgetText = "{{FIRST NAME:\"VALUE\"}}";
            const string body = "this is the body " + widgetText;
            var builder = new WidgetBuilder(_widgetSet).ApplyBodyContent(body);
            ExecuteWidgetBuilder(builder);
            var pairs = builder.MatchWidgetPairs;
            Assert.Single(pairs);
            var pair = pairs.First();
            var prop = pair.Widget.Properties["name"];
            Assert.NotNull(prop);
        }

        [Fact]
        public void CanRenderOutputWithMultipleTags()
        {
            const string widgetText = "{{single name:\"value\"}}";
            const string formatString = "this is the body, tag 1: {0}; tag 2: {0}";
            var initialBody = string.Format(formatString, widgetText);
            var expectedBody = string.Format(formatString, BasicWidget.Text);
            var builder = new WidgetBuilder(_widgetSet).ApplyBodyContent(initialBody);
            var response = ExecuteWidgetBuilder(builder);
            Assert.Equal(expectedBody, response);
            Console.WriteLine(expectedBody);
            Console.WriteLine(response);
        }

        [Fact]
        public void CanRenderOutputWithSimpleWidgetTag()
        {
            const string widgetText = "{{single name:\"value\"}}";
            const string initialContent = "this is the body ";
            const string contentWithWidgetTags = initialContent + widgetText;
            var contentWithExpectedRenderedText = initialContent + BasicWidget.Text;
            var builder = new WidgetBuilder(_widgetSet).ApplyBodyContent(contentWithWidgetTags);
            var response = ExecuteWidgetBuilder(builder);
            Assert.Equal(contentWithExpectedRenderedText, response);
        }

        [Fact]
        public void CanRenderOutputWithNoWidgetTags()
        {
            const string initialContent = "this is the body";
            var builder = new WidgetBuilder(_widgetSet).ApplyBodyContent(initialContent);
            var response = ExecuteWidgetBuilder(builder);
            Assert.Equal(initialContent, response);
        }

        [Fact]
        public void CanRenderOutputWithTextBeforeAndAfterWidget()
        {
            const string widgetText = "{{single name:\"value\"}}";
            const string formatString = "this is the widget: {0}.  This is after the widget";
            var initialContent = string.Format(formatString, widgetText);
            var expectedOutput = string.Format(formatString, BasicWidget.Text);
            var builder = new WidgetBuilder(_widgetSet).ApplyBodyContent(initialContent);
            var actualOutput = ExecuteWidgetBuilder(builder);
            Assert.Equal(expectedOutput, actualOutput);
            Console.WriteLine(expectedOutput);
            Console.WriteLine(actualOutput);
        }
    }

    public class BasicWidget : Widget
    {
        public static string Text = "this is the replacement content for the empty widget";
        public override void Execute(HtmlHelper htmlHelper)
        {
            htmlHelper.ViewContext.Writer.Write(Text);
        }
    }
}
