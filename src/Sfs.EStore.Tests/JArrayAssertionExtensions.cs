using System;
using Newtonsoft.Json.Linq;

namespace Sfs.EStore.Tests
{
    public static class JArrayAssertionExtensions
    {
        public static void AssertThat<TValue>(this JArray @this, Func<JArray, TValue> valueAccessor,
                                              Action<TValue> assertion)
        {
            var value = valueAccessor(@this);
            assertion(value);
        }

        public static void AssertThatEach<TValue>(this JArray @this, Func<JObject, TValue> itemValueAccessor, params TValue[] isEqualTo)
        {
            if (isEqualTo == null) throw new ArgumentNullException("isEqualTo");

            Func<int, TValue> expectedValue = index => isEqualTo[0];

            if (isEqualTo.Length > 1)
                expectedValue = index => isEqualTo[index];

            @this.AssertThat(j => j, a =>
                                         {
                                             for (var index = 0; index < a.Count; index++)
                                             {
                                                 var item = (JObject) a[index];
                                                 item.AssertThat(itemValueAccessor,
                                                                 isEqualTo: expectedValue(index));
                                             }
                                         });
        }
    }
}