﻿//using System;
//using Ninject;
//using Sfs.EStore.Web.App;
//using Xunit;

//namespace Sfs.EStore.Tests.DomainEventsTests
//{
//    public class Tests
//    {
//        [Fact]
//        public void X()
//        {
//            var called = false;
//            Action<TestDomainEvent> callback = args => called = true;
//            var kernel = new StandardKernel();
//            kernel.Bind<IHandles<TestDomainEvent>>()
//                .ToMethod(ctx => new TestDomainEventHandler(callback)).InTransientScope();
//            DomainEvents.Container = kernel;

//            DomainEvents.Raise(new TestDomainEvent());

//            Assert.True(called);
//        }

//        [Fact]
//        public void Y()
//        {
//            TestDomainEvent called = null;
//            Action<TestDomainEvent> callback = args => called = args;
//            var kernel = new StandardKernel();
//            kernel.Bind<IHandles<TestDomainEvent>>()
//                .ToMethod(ctx => new TestDomainEventHandler(callback)).InSingletonScope();
//            DomainEvents.Container = kernel;

//            DomainEvents.Raise(new TestDomainEvent { Value = "first" });
//            Assert.Equal("first", called.Value);

//            DomainEvents.Raise(new TestDomainEvent { Value = "second" });
//            Assert.Equal("second", called.Value);
//        }

//        public class TestDomainEvent : IDomainEvent
//        {
//            public string Value { get; set; }
//        }

//        public class TestDomainEventHandler : IHandles<TestDomainEvent>
//        {
//            private readonly Action<TestDomainEvent> _callback;

//            public TestDomainEventHandler(Action<TestDomainEvent> callback)
//            {
//                _callback = callback;
//            }

//            public void Handle(TestDomainEvent args)
//            {
//                _callback(args);
//            }
//        }
//    }
//}
