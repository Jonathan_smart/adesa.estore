﻿using System;
using System.Security.Cryptography;
using System.Web;
using Sfs.EStore.Web.App.Code.Security;
using Xunit;

namespace Sfs.EStore.Tests
{
    public class GuestPassTests
    {
        [Fact]
        public void RoundTripEncryptionAndDecryptionWorks()
        {
            var ticket = new GuestPassTicket("adminUser", "Foo", TimeSpan.FromDays(90), "MyCampaign", "/bar");
            var encrypted = GuestPass.Encrypt(ticket);
            var decrypted = GuestPass.Decrypt(encrypted);

            //Assert.Equal(ticket.Source, decrypted.Source);
            Assert.Equal(ticket.Id, decrypted.Id);
        }

        [Fact(Skip = "Used only to generate a key and IV")]
        public void CreateKeyAndIvForConfig()
        {
            var x = new TripleDESCryptoServiceProvider();

            Console.WriteLine("KEY:");
            Console.WriteLine(Convert.ToBase64String(x.Key));
            Console.WriteLine(HttpUtility.UrlEncode(Convert.ToBase64String(x.Key)));

            Console.WriteLine("IV:");
            Console.WriteLine(Convert.ToBase64String(x.IV));
            Console.WriteLine(HttpUtility.UrlEncode(Convert.ToBase64String(x.IV)));
        }

        [Fact(Skip = "Used only to generate a key and IV")]
        public void TestMinutes()
        {
            GuestPass.Key = Convert.FromBase64String("S/wCSUvUVrObTlRtj7gVoxRwEm7UdppT");
            GuestPass.IV = Convert.FromBase64String("MDJGKDqSUP0=");

            var ticket = new GuestPassTicket("adminUser", "Foo", TimeSpan.FromMinutes(2), "MyCampaign", "/bar");
            var encrypted = GuestPass.Encrypt(ticket);
            Console.WriteLine(encrypted);
            Console.WriteLine(HttpUtility.UrlEncode(encrypted));
            Console.WriteLine(string.Format("http://localhost.smartvehiclesales.co.uk/affiliate?token={0}", HttpUtility.UrlEncode(encrypted)));
        }
    }
}
