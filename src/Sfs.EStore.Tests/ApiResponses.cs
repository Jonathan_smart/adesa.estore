﻿using System;
using Sfs.EStore.Web.App.Code;

namespace Sfs.EStore.Tests
{
    public static class ApiResponses
    {
        public static readonly Func<object> LotActiveBidOnBuyItNow =
            () => new
                      {
                          ListingId = 4921,
                          Title = "VAUXHALL ASTRA GTC 10 2.0i 16v Turbo (280PS) S/S",
                          PrimaryImage = new {
                              Url = "http://images.smartfleetsolutions.com/StockImages/StkImg287527.JPG"
                          },
                          MoreImages = new []{
                              new {
                                  Url = "http://images.smartfleetsolutions.com/StockImages/StkImg287527.JPG"
                              },new {
                                  Url = "http://images.smartfleetsolutions.com/StockImages/StkImg287527.JPG"
                              }
                          },
                          DiscountPriceInfo = new
                          {
                              RetailPrice = new
                              {
                                  Amount = 16650,
                                  Currency = "GBP"
                              },
                              WasPrice = new
                              {
                                  Amount = 15000,
                                  Currency = "GBP"
                              }
                          },
                          ListingInfo = new {
                              BuyItNowAvailable = true,
                              BuyItNowPrice = new {
                                  Amount = 39360,
                                  Currency = "GBP"
                              },
                              StartTime = SystemTime.UtcNow,
                              EndTime = SystemTime.UtcNow,
                              ListingType = "AuctionWithBin"
                          },
                          SellingStatus = new {
                              BidCount = 1,
                              CurrentPrice = new {
                                  Amount = 14760,
                                  Currency = "GBP"
                              },
                              SellingState = "Active",
                              ReserveMet = false,
                              MinimumToBid = new {
                                  Amount = 14785,
                                  Currency = "GBP"
                              },
                              TimeLeft = 0,
                              Bids = new [] {
                                  new {
                                      User = "",
                                      Time = SystemTime.UtcNow,
                                      Amount = new {
                                          Amount = 14760,
                                          Currency = "GBP"
                                      },
                                      Position = 1
                                  }
                              }
                          },
                          BiddingStatus = new {
                              MaximumBid = new {
                                  Amount = 0,
                                  Currency = "GBP"
                              },
                              MaximumBidTime = SystemTime.UtcNow,
                              BiddingStatus = "Winning"
                          },
                          VehicleInfo = new {
                              Make = "Vauxhall",
                              Model = "Astra",
                              Derivative = "3 Dr GTC",
                              VatStatus = (string) null,
                              Fuel = "Petrol",
                              Transmission = "Manual",
                              Registration = new {
                                  Plate = "BG12DLU",
                                  Date = (DateTime?)null,
                                  Yap = "12:12"
                              },
                              Mileage = 49616,
                              VehicleType = "CAR",
                              Specification = new {
                                  Economy = new {
                                      Urban = 52.3,
                                      ExtraUrban = 72.4,
                                      Combined = 62.8
                                  },
                                  Co2 = 117,
                                  Dimensions = (object)null,
                                  Engine = (object)null,
                                  Equipment = new {
                                      Items = new [] {
                                          new {
                                              Category = "Paint or Trim and Upholstery",
                                              Name = "Alcantara Upholstery"
                                          },
                                          new {
                                              Category = "Safety",
                                              Name = "Anti Lock Brakes"
                                          }
                                      }
                                  }
                              },
                              EngineDescription = "1.6",
                              BodyColour = "OLYMPIC WHITE",
                          }
                      };

        public static readonly Func<object[]> LotSearchResults =
            () => new object[]
                      {
                          new
                              {
                                  ListingId = 193,
                                  Title = "Vauxhall Astra 1.6 CDTi (77PS)",
                                  ListingInfo = new
                                                    {
                                                        BuyItNowAvailable = true,
                                                        BuyItNowPrice = new
                                                                            {
                                                                                Amount = 14000,
                                                                                Currency = "GBP"
                                                                            },
                                                        StartTime = SystemTime.UtcNow,
                                                        EndTime = SystemTime.UtcNow
                                                    },
                                  SellingStatus = new
                                                      {
                                                          BidCount = 1,
                                                          SellingState = "Active",
                                                          CurrentPrice = new
                                                                             {
                                                                                 Amount = 8000,
                                                                                 Currency = "GBP"
                                                                             }
                                                      },
                                  VehicleInfo = new
                                                    {
                                                        Make = "Vauxhall",
                                                        Model = "Astra",
                                                        Derivative = "Foo Bar",
                                                        Registration = new
                                                                           {
                                                                               Plate = "AB12CDE",
                                                                               Yap = "12:12"
                                                                           },
                                                        Mileage = 6000,
                                                        VatStatus = (string) null
                                                    },
                                  DiscountPriceInfo = new
                                  {
                                      RetailPrice = new
                                      {
                                          Amount = 16650,
                                          Currency = "GBP"
                                      },
                                      WasPrice = new
                                      {
                                          Amount = 13000,
                                          Currency = "GBP"
                                      }
                                  },
                                  ImageId = (string) null
                              },
                          new
                              {
                                  ListingId = 162,
                                  Title = "BMW 1 SERIES 2.0 (177BHP)",
                                  ListingInfo = new
                                                    {
                                                        BuyItNowAvailable = true,
                                                        BuyItNowPrice = new
                                                                            {
                                                                                Amount = 10500,
                                                                                Currency = "GBP"
                                                                            },
                                                        StartTime = SystemTime.UtcNow,
                                                        EndTime = SystemTime.UtcNow
                                                    },
                                  SellingStatus = new
                                                      {
                                                          BidCount = 1,
                                                          SellingState = "Active",
                                                          CurrentPrice = new
                                                                             {
                                                                                 Amount = 6000,
                                                                                 Currency = "GBP"
                                                                             }
                                                      },
                                  VehicleInfo = new
                                                    {
                                                        Make = "BMW",
                                                        Model = "1 SERIES",
                                                        Derivative = "Foo Bar",
                                                        Registration = new
                                                                           {
                                                                               Plate = "LB10YYO",
                                                                               Yap = "10:10"
                                                                           },
                                                        Mileage = 42000,
                                                        VatStatus = (string) null
                                                    },
                                  DiscountPriceInfo = new
                                  {
                                      RetailPrice = new
                                      {
                                          Amount = 16650,
                                          Currency = "GBP"
                                      },
                                      WasPrice = new
                                      {
                                          Amount = 13000,
                                          Currency = "GBP"
                                      }
                                  },
                                  ImageId = (string) null
                              }
                      };
    }
}
