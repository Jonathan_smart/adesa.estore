using System;
using Newtonsoft.Json.Linq;
using Xunit;

namespace Sfs.EStore.Tests
{
    public static class JObjectAssertionExtensions
    {
        public static void AssertThatToken<TValue>(this JToken @this, string property, TValue isEqualTo)
        {
            @this.AssertThatToken<TValue>(valueAccessor: j => j.Value<TValue>(property),
                               assertion: j => Assert.Equal(isEqualTo, j));
        }

        public static void AssertThat<TValue>(this JObject @this, string property, TValue isEqualTo)
        {
            @this.AssertThat<TValue>(valueAccessor: j => j.Value<TValue>(property),
                               assertion: j => Assert.Equal(isEqualTo, j));
        }

        public static void AssertThat<TValue>(this JObject @this, Func<JObject, TValue> valueAccessor,
                                              TValue isEqualTo)
        {
            @this.AssertThat(valueAccessor,
                               assertion: j => Assert.Equal(isEqualTo, j));
        }

        //public static void AssertThatBidding<TValue>(this JObject @this, string property, TValue isEqualTo)
        //{
        //    @this.AssertThat<TValue>(valueAccessor: j => j["bidding"].Value<TValue>(property),
        //                       assertion: j => Assert.Equal(isEqualTo, j));
        //}

        public static void AssertThatToken<TValue>(this JToken @this, Func<JToken, TValue> valueAccessor,
                                              Action<TValue> assertion)
        {
            var j = valueAccessor(@this);
            assertion(j);
        }

        public static void AssertThat<TValue>(this JObject @this, Func<JObject, TValue> valueAccessor,
                                              Action<TValue> assertion)
        {
            var j = valueAccessor(@this);
            assertion(j);
        }
    }
}