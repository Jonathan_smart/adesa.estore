using System.Net.Http;
using System.Web.Http;

namespace Sfs.EStore.Tests
{
    public static class ApiControllerExtensions
    {
        public static TController WithJsonPostRequest<TController>(this TController @this, string requestUri = null)
            where TController : ApiController
        {
            @this.Request =
                new HttpRequestMessage(HttpMethod.Post, requestUri ?? "http://localhost/api/fake")
                    .ConfigureJsonFormatter();

            return @this;
        }

        public static TController WithGetRequest<TController>(this TController @this, string requestUri = null)
            where TController : ApiController
        {
            @this.Request =
                new HttpRequestMessage(HttpMethod.Get, requestUri ?? "http://localhost/api/fake");

            return @this;
        }
    }
}