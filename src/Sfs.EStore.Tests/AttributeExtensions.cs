using System;
using System.Linq;

namespace Sfs.EStore.Tests
{
    public static class AttributeExtensions
    {
        public static TAttribute GetAttribute<TAttribute>(this object @this)
            where TAttribute : Attribute
        {
            return Attribute.GetCustomAttributes(@this.GetType(), typeof (TAttribute))
                .Cast<TAttribute>()
                .FirstOrDefault();
        }

        public static bool HasAttribute<TAttribute>(this object @this)
            where TAttribute : Attribute
        {
            return @this.GetType().IsDefined(typeof (TAttribute), false);
        }
    }
}