using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web.Http.Controllers;
using Moq;
using Newtonsoft.Json.Linq;
using Raven.Client;
using Sfs.EStore.Web.App;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.Security;
using Sfs.EStore.Web.App.Controllers.Api;

namespace Sfs.EStore.Tests
{
    public class ApiControllerWrapper<TController> where TController : ApiApplicationController
    {
        public ApiControllerWrapper(IDocumentStore store, TController controller)
        {
            Controller = controller;
            Controller.Session = store.OpenAsyncSession();

            ApiProxyFakeHandler = new FakeHandler
                                      {
                                          InnerHandler = new HttpClientHandler()
                                      };

            var client = new HttpClient(ApiProxyFakeHandler)
                             {
                                 BaseAddress = new Uri("http://localhost/fake/api")
                             };

            Controller.ApiProxy = new AuthorizedHttpClient(client, new NullWebApplicationPrincipal());

            Tenant = new SiteTenant {Id = "SiteTenants/1"};

            var mockPermissions = new Mock<SiteTenantActivityPermissions>();
            mockPermissions.Setup(x => x[It.IsAny<string>()]).Returns(
                new SiteTenantActivityPermissions.ActivityPermission("*"));
            Permissions = mockPermissions.Object;

            SetAuthenticatedUser(null);
        }

        public TController Controller { get; private set; }

        public SiteTenant Tenant
        {
            get { return Controller.Tenant; }
            set { Controller.Tenant = value; }
        }

        public SiteTenantActivityPermissions Permissions
        {
            get { return Controller.Permissions; }
            set { Controller.Permissions = value; }
        }

        private FakeHandler ApiProxyFakeHandler { get; set; }

        public void SetAuthenticatedUser(string name)
        {
            Thread.CurrentPrincipal = new FakeWebApplicationPrincipal(name);
            Controller.User = string.IsNullOrEmpty(name)
                ? (IWebApplicationPrincipal) new NullWebApplicationPrincipal()
                : new GenericWebApplicationPrincipal("", new GenericWebApplicationIdentity(name, data: null));
        }

        public HttpResponseMessage SetApiProxyResponse<T>(HttpStatusCode statusCode, T[] content)
        {
            return SetApiProxyResponse(statusCode, JArray.FromObject(content).ToString());
        }

        public HttpResponseMessage SetApiProxyResponse<T>(HttpStatusCode statusCode, T content)
        {
            return SetApiProxyResponse(statusCode, JObject.FromObject(content).ToString());
        }

        public HttpResponseMessage SetApiProxyResponse(HttpStatusCode statusCode, string content)
        {
            var message = new HttpResponseMessage(statusCode)
                              {
                                  Content = new StreamContent(
                                      new MemoryStream(Encoding.ASCII.GetBytes(content)))
                              };
            ApiProxyFakeHandler.Response = message;

            return message;
        }

        private class FakeWebApplicationPrincipal : IWebApplicationPrincipal
        {
            private readonly string[] _roles;

            public FakeWebApplicationPrincipal(string name, params string[] roles)
            {
                _identity = new GenericWebApplicationIdentity(name ?? "", data: null);
                _roles = roles;
            }

            public bool IsInRole(string role)
            {
                return _roles.Any(x => x.Equals(role, StringComparison.InvariantCultureIgnoreCase));
            }

            IIdentity IPrincipal.Identity { get { return Identity; } }
            public IEnumerable<string> Roles()
            {
                return _roles;
            }

            private readonly IWebApplicationIdentity _identity;
            public IWebApplicationIdentity Identity
            {
                get { return _identity; }
            }

            public string ApiAccessKeyId { get; set; }
            public string UserAuthorizationToken { get; set; }
        }
    }
}