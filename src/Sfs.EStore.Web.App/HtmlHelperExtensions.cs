using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using Cassette.Views;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Code.Cms;
using Sfs.EStore.Web.App.Code.Widgets;
using Sfs.EStore.Web.App.Models;

namespace Sfs.EStore.Web.App
{
    public static class DictionaryExtensions
    {
        public static TValue TryGetValue<TValue>(this IDictionary<string, TValue> @this, string key, TValue defaultValue, bool caseSensitive = false)
        {
            if (@this == null) return defaultValue;

            var dict = new Dictionary<string, TValue>(@this, caseSensitive ? StringComparer.InvariantCulture : StringComparer.InvariantCultureIgnoreCase);
            TValue value;
            
            return dict.TryGetValue(key, out value) ? value : defaultValue;
        }

        public static TValue TryGetValue<TKey, TValue>(this IDictionary<TKey, TValue> @this, TKey key, TValue defaultValue)
        {
            TValue value;
            if (@this.TryGetValue(key, out value))
                return value;

            return defaultValue;
        }
    }

    public static class HtmlHelperExtensions
    {
        public static string FieldIdFor<T, TResult>(this HtmlHelper<T> html, Expression<Func<T, TResult>> expression)
        {
            return html.ViewData.TemplateInfo.GetFullHtmlFieldId(ExpressionHelper.GetExpressionText(expression)).Replace('[', '_').Replace(']', '_');
        }

        public static string PriceVatStatus<T>(this HtmlHelper<T> @html, bool pricesIncludeVat)
        {
            return !pricesIncludeVat ? "+VAT" : "";
        }

        public static string ToCurrencyString(this MoneyViewModel @this)
        {
            return MoneyHelper.Stringify(@this);
        }

        public static string Money<T>(this HtmlHelper<T> @html, MoneyViewModel model)
        {
            return model.ToCurrencyString();
        }
    }

    public static class MoneyHelper
    {
        public static string Stringify(MoneyViewModel money, int dp = 0)
        {
            // TODO: Lookup currency symbol based on currency string
            /*
             * foreach (var culture in cultures)
            {
                var lcid = culture.LCID;
                var regionInfo = new RegionInfo(lcid);
                var isoSymbol = regionInfo.ISOCurrencySymbol;

                if (!cultureIdLookup.ContainsKey(isoSymbol))
                {
                    cultureIdLookup[isoSymbol] = new List<Int32>();
                }

                cultureIdLookup[isoSymbol].Add(lcid);
                symbolLookup[isoSymbol] = regionInfo.CurrencySymbol;
            }
             * */
            if (money == null) return "";

            string symbol = money.Currency;
            switch (money.Currency)
            {
                case "GBP":
                    symbol = "\u00a3";
                    break;
                case "EUR":
                    symbol = "\u20ac";
                    break;
            }
            return string.Format("{0}{1}", symbol, money.Amount.ToString("n"+dp));
        }
    }

    public static class CmsHelperExtensions
    {
        public static void RenderCmsSection(this HtmlHelper helper, string source, ILiquidVariables model = null)
        {
            new CmsBodyRenderer(source, model).Render(helper);
        }

        public static void BundleScriptDependencies(this HtmlHelper @this, IEnumerable<CmsScript> scriptDependencies)
        {
            BundleScriptDependencies(scriptDependencies);
        }

        private static void BundleScriptDependencies(IEnumerable<CmsScript> scriptDependencies)
        {
            if (scriptDependencies == null) return;

            foreach (var cmsScript in scriptDependencies)
            {
                foreach (var dependency in cmsScript.Dependencies)
                {
                    Bundles.Reference(dependency);
                }
                var wrappedScript = string.Format("window.__onViewContentLoaded = window.__onViewContentLoaded || [];\r\n" +
                                                  "window.__onViewContentLoaded.push(function() {{\r\n\t{0}\r\n}});", cmsScript.Value.Replace("\r\n", "\r\n\t"));
                Bundles.AddInlineScript(wrappedScript, "cms-page-scripts", bundle =>
                {
                });
            }
        }
    }
}