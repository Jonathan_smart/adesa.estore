using System.IO;
using System.Web.Mvc;
using DotLiquid;
using Sfs.EStore.Web.App.Code.Widgets.Liquid.Drops;

namespace Sfs.EStore.Web.App
{
    public class LiquidTemplateRenderer
    {
        private readonly HtmlHelper _htmlHelper;
        private readonly TextWriter _writer;

        private readonly log4net.ILog _log = log4net.LogManager.GetLogger(typeof (LiquidTemplateRenderer));

        public LiquidTemplateRenderer(HtmlHelper htmlHelper, TextWriter writer)
        {
            _htmlHelper = htmlHelper;
            _writer = writer;
        }

        public void Render(string source, ILiquidVariables variables = null)
        {
            var template = Template.Parse(source);
            template.Registers.Add("Helper", _htmlHelper);
            template.Render(_writer, new RenderParameters { LocalVariables = Hash.FromAnonymousObject(variables) });
            if (template.Errors != null)
                template.Errors.ForEach(x => _log.Error("Error in liquid template rendering", x));
        }
    }

    public interface ILiquidVariables
    {
        UserDrop user { get; }
        CmsPageDrop page { get; }
    }
}