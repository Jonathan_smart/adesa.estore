using System;
using System.Collections.Generic;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Common;
using Serilog;
using Sfs.Darker.CommandProcessor;
using Sfs.EStore.Ports.GmGlobalConnect;
using Sfs.EStore.Ports.Listings;
using Sfs.EStore.Web.App.Areas.GmGlobalConnect.Ports;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Ports.Listings;

namespace Sfs.EStore.Web.App
{
    public class PortsNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<ICommandProcessor>().ToMethod(ctx => new NinjectCommandProcessor(() => Kernel));
            Kernel.Bind<ILog>().ToMethod(ctx => new SerilogLogger(Log.Logger.ForContext(ctx.Request.Target.Type)));

            BindGmGlobalConnectPorts();
            BindListingPorts();
        }

        private void BindListingPorts()
        {
            Kernel.Bind<IHandleRequests<GetListingInfo>>().To<EStoreApiGetListing>();
            Kernel.Bind<ITranslateListingDetails>().To<ListingDetailTranslator>();
        }

        private void BindGmGlobalConnectPorts()
        {
            Kernel.Bind<IHandleRequests<Authenticate>>().To<GmGlobalConnectAuthenticater>()
                .WithConstructorArgument("disableIvUserCheck", ctx =>
                {
                    var settings = CurrentIntegrationSettings();
                    string settingValue;
                    bool disabled;

                    return settings.TryGetValue("GlobalConnect/DisableIvUserCheck", out settingValue) &&
                           bool.TryParse(settingValue, out disabled) && disabled;
                });

            Kernel.Bind<ICustomerStore>().To<DatabaseCustomerStore>();
            Kernel.Bind<ISetBuyerPermissions>().To<EStoreApiBuyerPermissions>();
            Kernel.Bind<IUserIdentityStore>().To<EStoreApiUserIdentityStore>();
            Kernel.Bind<IRemoteAddressAuthenticator>().To<RegExTestRemoteAddressAuthenticator>()
                .InRequestScope()
                .WithConstructorArgument("pattern", (ctx) =>
                {
                    var settings = CurrentIntegrationSettings();
                    string pattern;

                    if (!settings.TryGetValue("GlobalConnect/AuthorisedIpAddressPattern", out pattern))
                    {
                        pattern = @"^(((0|127)\.0\.0\.1)|(::1))$";
                    }

                    return pattern;
                });
        }

        private IDictionary<string, string> CurrentIntegrationSettings()
        {
            var tenant = Kernel.Get<SiteTenant>();

            return tenant.IntegrationSettings;
        }

        internal class NinjectCommandProcessor : ICommandProcessor
        {
            private readonly Func<IKernel> _kernel;

            public NinjectCommandProcessor(Func<IKernel> kernel)
            {
                _kernel = kernel;
            }

            public void Send<T>(T command) where T : class, IRequest
            {
                var handler = _kernel().Get<IHandleRequests<T>>();

                handler.Handle(command);
            }
        }
    }
}