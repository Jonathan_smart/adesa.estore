using System;
using System.Web;

namespace Sfs.EStore.Web.App
{
    public static class UniqueSessionId
    {
        public static Func<HttpSessionStateBase> Store = () => new HttpSessionStateWrapper(HttpContext.Current.Session);

        public static string Value
        {
            get
            {
                var session = Store();
                var sessionId = session["UniqueSessionId"] ?? (session["UniqueSessionId"] = Guid.NewGuid());

                return sessionId.ToString();
            }
        }
    }
}