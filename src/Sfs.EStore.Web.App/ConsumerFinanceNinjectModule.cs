using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Common;
using Sfs.Darker.CommandProcessor;
using Sfs.EStore.Web.App.App_Start;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.ConsumerFinance.Adapters;
using Sfs.EStore.Web.App.ConsumerFinance.Ports;
using Sfs.EStore.Web.App.Controllers.Api;

namespace Sfs.EStore.Web.App
{
    public class ConsumerFinanceNinjectModule : NinjectModule
    {
        public override void Load()
        {
            var financeHttpClient = new HttpClient();
            Kernel.Bind<IHandleRequests<GetConsumerFinanceQuote>>().To<GetConsumerFinanceQuoteHandler>()
                .WithConstructorArgument("credential",
                    ctx =>
                    {
                        var tenant = ctx.Kernel.Get<SiteTenant>();

                        var username = tenant.IntegrationSettings["iVendi/Username"];
                        var password = tenant.IntegrationSettings["iVendi/Password"];

                        return new NetworkCredential(username, password);
                    })
                .WithConstructorArgument("httpClient", financeHttpClient)
                .WithConstructorArgument("endpoint", new Uri(ConfigurationManager.AppSettings["iVendi/Endpoint"]));


            Kernel.Bind<FinanceApplyLinkBuilder>().ToMethod(ctx =>
            {
                var tenant = ctx.Kernel.Get<SiteTenant>();
                var dealerCode = tenant.IntegrationSettings["iVendi/DealerCode"];
                var httpContext = NinjectWebCommon.GetHttpContext(SimpleInjectorHttpRequestMessageProvider.CurrentMessage);

                return new FinanceApplyLinkBuilder(dealerCode, httpContext.Request.UrlReferrer);
            }).InRequestScope();
        }
    }
}