extern alias project;
using project::Microsoft.Ajax.Utilities;
using Cassette;
using Cassette.Scripts;
using Cassette.TinyIoC;

namespace Sfs.EStore.Web.App
{
    public class CassetteIoCConfiguration : IConfiguration<TinyIoCContainer>
    {
        public void Configure(TinyIoCContainer configurable)
        {
            var settings = new CodeSettings { AlwaysEscapeNonAscii = true };
            var minifier = new MicrosoftJavaScriptMinifier(settings);
            configurable.Register<IJavaScriptMinifier>(minifier);
        }
    }
}