using System.Web;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Areas.Retail;

namespace Sfs.EStore.Web.App
{
    internal static class HttpContextBaseExtensions
    {
        public static SiteTenant CurrentTenant(this HttpContextBase @this)
        {
            return (SiteTenant)@this.Items["Tenant"];
        }

        public static SiteTenantActivityPermissions CurrentTenantPermissions(this HttpContextBase @this)
        {
            return (SiteTenantActivityPermissions)@this.Items["TenantPermissions"];
        }
    }
}