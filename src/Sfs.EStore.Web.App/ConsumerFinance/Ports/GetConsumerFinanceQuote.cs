﻿using System;
using iTextSharp.text.pdf.qrcode;
using Sfs.Darker.CommandProcessor;

namespace Sfs.EStore.Web.App.ConsumerFinance.Ports
{
    public class GetConsumerFinanceQuote : Command
    {
        public GetConsumerFinanceQuote(RequestParameters parameters, RequestFigures figures, RequestVehicle vehicle)
            : base(Guid.NewGuid())
        {
            if (parameters == null) throw new ArgumentNullException("parameters");
            if (figures == null) throw new ArgumentNullException("figures");
            if (vehicle == null) throw new ArgumentNullException("vehicle");

            Parameters = parameters;
            Figures = figures;
            Vehicle = vehicle;
        }

        public QuoteResult Result { get; set; }

        public RequestParameters Parameters { get; private set; }
        public RequestFigures Figures { get; private set; }
        public RequestVehicle Vehicle { get; private set; }


        public class RequestParameters
        {
            public RequestParameters(int termInMonths)
            {
                TermInMonths = termInMonths;
            }

            public int TermInMonths { get; private set; }
        }

        public class RequestFigures
        {
            public RequestFigures(decimal cashPrice, decimal cashDeposit, int ammualMileage)
            {
                CashPrice = cashPrice;
                CashDeposit = cashDeposit;
                AmmualMileage = ammualMileage;
            }

            public decimal CashPrice { get; private set; }
            public decimal CashDeposit { get; private set; }
            public int AmmualMileage { get; private set; }
        }

        public class RequestVehicle
        {
            public RequestVehicle(int currentOdometerReader, DateTime registrationDate, string registrationMark,
                int capId, RequestVehicleClass @class)
            {
                CurrentOdometerReader = currentOdometerReader;
                RegistrationDate = registrationDate;
                RegistrationMark = registrationMark;
                CapId = capId;
                Class = @class;
            }

            public int CurrentOdometerReader { get; private set; }
            public DateTime RegistrationDate { get; private set; }
            public string RegistrationMark { get; private set; }
            public int CapId { get; private set; }
            public RequestVehicleClass Class { get; private set; }
            public string Make { get; private set; }
            public string Model { get; private set; }
        }

        public enum RequestVehicleClass
        {
            Car, 
            LCV
        }

        public class QuoteResult
        {
            public QuoteResult(QuoteResultItem[] quotes)
            {
                Quotes = quotes;
            }

            public QuoteResultItem[] Quotes { get; private set; }
        }

        public class QuoteResultItem
        {
            public QuoteResultItem(Guid id, QuoteFacilityType facilityType, decimal totalCashPrice, decimal totalDeposit, decimal advance, 
                decimal acceptanceFee, decimal optionToPurchaseFee, decimal totalPayable, 
                decimal firstPayment, decimal regularPayment, decimal finalPayment, 
                int numberOfRegularPayments, int term, decimal apr, decimal aer, int vehicleTermDistance, AssetInfo asset)
            {
                Id = id;
                FacilityType = facilityType;
                TotalCashPrice = totalCashPrice;
                TotalDeposit = totalDeposit;
                Advance = advance;
                AcceptanceFee = acceptanceFee;
                OptionToPurchaseFee = optionToPurchaseFee;
                TotalPayable = totalPayable;
                FirstPayment = firstPayment;
                RegularPayment = regularPayment;
                FinalPayment = finalPayment;
                NumberOfRegularPayments = numberOfRegularPayments;
                Term = term;
                APR = apr;
                AER = aer;
                VehicleTermDistance = vehicleTermDistance;
                Asset = asset;
            }

            public Guid Id { get; private set; }
            public QuoteFacilityType FacilityType { get; private set; }
            public decimal TotalCashPrice { get; private set; }
            public decimal TotalDeposit { get; private set; }
            public decimal Advance { get; private set; }
            public decimal AcceptanceFee { get; private set; }
            public decimal OptionToPurchaseFee { get; private set; }
            public decimal TotalPayable { get; private set; }
            public decimal FirstPayment { get; private set; }
            public decimal RegularPayment { get; private set; }
            public decimal FinalPayment { get; private set; }
            public int NumberOfRegularPayments { get; private set; }
            public int Term { get; private set; }
            public decimal APR { get; private set; }
            public decimal AER { get; private set; }

            public int VehicleTermDistance { get; private set; }
            public AssetInfo Asset { get; private set; }

            public class AssetInfo
            {
                public AssetInfo(string vrm, RequestVehicleClass @class)
                {
                    Vrm = vrm;
                    Class = @class;
                }

                public string Vrm { get; private set; }
                public RequestVehicleClass Class { get; private set; }
            }
        }

        public enum QuoteFacilityType
        {
            PCP,
            HP,
            LP
        }
    }
}
