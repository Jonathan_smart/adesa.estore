using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using Sfs.Darker.CommandProcessor;
using Sfs.EStore.Web.App.ConsumerFinance.Ports;

namespace Sfs.EStore.Web.App.ConsumerFinance.Adapters
{
    public class GetConsumerFinanceQuoteHandler : RequestHandler<GetConsumerFinanceQuote>
    {
        private readonly NetworkCredential _credential;
        private readonly HttpClient _httpClient;
        private readonly Uri _endpoint;

        public GetConsumerFinanceQuoteHandler(NetworkCredential credential, HttpClient httpClient, Uri endpoint, ILog logger)
            : base(logger)
        {
            _credential = credential;
            _httpClient = httpClient;
            _endpoint = endpoint;
        }

        public override GetConsumerFinanceQuote Handle(GetConsumerFinanceQuote cmd)
        {
            Logger.Debug("Consumer finance quote for {figures}", cmd);

            var response = _httpClient.PostAsJsonAsync(_endpoint, new
            {
                Credentials = new
                {
                    Username = _credential.UserName,
                    Password = _credential.Password
                },
                //DealershipEnrichment = new
                //{
                //    IpAddress = "192.168.0.1",
                //    Email = "tester@example.com",
                //    Postcode = "M5 3EB",
                //    SearchMeta = "{Color : Blue, Style : Family}",
                //    UserGenerated = 1
                //},
                QuoteRequests = new[]
                {
                    new
                    {
                        QuoteeUID = "",
                        GlobalRequestParameters = new
                        {
                            Term = cmd.Parameters.TermInMonths,
                            TermUnit = "Month",
                        },
                        Requests = new[]
                        {
                            new
                            {
                                Figures = new
                                {
                                    CashPrice = cmd.Figures.CashPrice,
                                    CashDeposit = cmd.Figures.CashDeposit,
                                    Asset = new
                                    {
                                        AnnualDistance = cmd.Figures.AmmualMileage
                                    }
                                },
                                Asset = new
                                {
                                    CurrentOdometerReading = cmd.Vehicle.CurrentOdometerReader,
                                    RegistrationDate = cmd.Vehicle.RegistrationDate,
                                    RegistrationMark = cmd.Vehicle.RegistrationMark,
                                    Condition = "Used",
                                    Source = "Default",
                                    Identity = cmd.Vehicle.CapId,
                                    IdentityType = "RVC", // cap id
                                    //StockIdentity = "88546",
                                    Class = cmd.Vehicle.Class.ToString()
                                }
                            }
                        }
                    }
                }
            });

            var responseResult = response.Result.Content.ReadAsAsync<QuoteResponse>().Result;

            if (responseResult.HasErrors || !responseResult.HasQuoteResults)
            {
                return cmd;
            }

            var productQuotes = responseResult.QuoteResults
                .Where(x => x.HasResults)
                .SelectMany(x => x.Results.Where(r => r.HasProductGroup && !r.HasErrors))
                .Where(x => x.HasProductGroup)
                .SelectMany(x => x.ProductGroups.Where(g => g.HasProductQuote && !g.HasErrors).Select(g => new { x.Asset, ProductGroup = g }))
                .SelectMany(x => x.ProductGroup.ProductQuotes.Where(q => q.Figures != null && !q.HasErrors).Select(q => new { x.Asset, Quote = q }))
                .ToArray();

            cmd.Result = new GetConsumerFinanceQuote.QuoteResult(
                productQuotes
                    .Where(x => Map(x.Quote.FacilityType) != null)
                    .Select(
                        x => new GetConsumerFinanceQuote.QuoteResultItem(x.Quote.QuoteUid,
                            Map(x.Quote.FacilityType).Value,
                            x.Quote.Figures.TotalCashPrice, x.Quote.Figures.TotalDeposit, x.Quote.Figures.Advance,
                            x.Quote.Figures.AcceptanceFee, x.Quote.Figures.OptionToPurchaseFee,
                            x.Quote.Figures.TotalPayable,
                            x.Quote.Figures.FirstPayment, x.Quote.Figures.RegularPayment, x.Quote.Figures.FinalPayment,
                            x.Quote.Figures.NumberOfRegularPayments, x.Quote.Figures.Term,
                            x.Quote.Figures.APR/ 100,
                            x.Quote.Figures.AER / 100,
                            x.Asset.TermDistance,
                            new GetConsumerFinanceQuote.QuoteResultItem.AssetInfo(cmd.Vehicle.RegistrationMark, cmd.Vehicle.Class)))
                    .ToArray()
                );

            return cmd;
        }

        private static GetConsumerFinanceQuote.QuoteFacilityType? Map(string facilityType)
        {
            GetConsumerFinanceQuote.QuoteFacilityType value;

            if (Enum.TryParse(facilityType, out value))
            {
                return value;
            }

            return null;
        }

        internal class QuoteResponse
        {
            public Guid QuotedResultsUid { get; set; }
            public Guid ResponseUid { get; set; }
            public QuoteResult[] QuoteResults { get; set; }
            public bool HasQuoteResults { get; set; }
            public bool HasErrors { get; set; }
            public bool HasWarnings { get; set; }
        }

        internal class QuoteResult
        {
            public Guid QuotedResultUid { get; set; }
            public Result[] Results { get; set; }
            public bool HasResults { get; set; }
            public bool HasErrors { get; set; }
            public bool HasWarnings { get; set; }
        }

        internal class Result
        {
            public Guid ResultUid { get; set; }
            public ResultAsset Asset { get; set; }
            public ProductGroup[] ProductGroups { get; set; }
            public bool HasProductGroup { get; set; }
            public bool HasErrors { get; set; }
            public bool HasWarnings { get; set; }
        }

        internal class ResultAsset
        {
            public int TermDistance { get; set; }
        }

        internal class ProductGroup
        {
            public ProductQuote[] ProductQuotes { get; set; }
            public bool HasProductQuote { get; set; }
            public bool HasErrors { get; set; }
            public bool HasWarnings { get; set; }
        }

        internal class ProductQuote
        {
            public ProductQuoteFigures Figures { get; set; }
            public string FacilityType { get; set; }
            public string ProductName { get; set; }
            public Guid QuoteUid { get; set; }
            public DateTime ExiraryDate { get; set; }
            public bool HasErrors { get; set; }
            public bool HasWarnings { get; set; }
        }

        internal class ProductQuoteFigures
        {
            public decimal TotalCashPrice { get; set; }
            public decimal TotalDeposit { get; set; }
            public decimal Advance { get; set; }
            public decimal InterestCharges { get; set; }
            public decimal AcceptanceFee { get; set; }
            public decimal OptionToPurchaseFee { get; set; }
            public decimal TotalCharges { get; set; }
            public decimal TotalPayable { get; set; }
            public decimal FirstPayment { get; set; }
            public decimal RegularPayment { get; set; }
            public decimal FinalPayment { get; set; }
            public int NumberOfRegularPayments { get; set; }
            public int Term { get; set; }
            public decimal APR { get; set; }
            public decimal AER { get; set; }
        }
    }
}