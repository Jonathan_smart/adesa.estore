using System.ComponentModel.DataAnnotations;

namespace Sfs.EStore.Web.App.Models
{
    public class SendToFriendPostModel
    {
        public string SenderFullName { get; set; }
        public string SenderEmail { get; set; }

        public string RecipientFullName { get; set; }
        [Required]
        public string RecipientEmail { get; set; }

        public string Message { get; set; }
    }
}