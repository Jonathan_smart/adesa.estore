using System.Web;
using Sfs.EStore.Web.App.Code.Security;
using Sfs.EStore.Web.App.Code.Widgets.Liquid.Drops;

namespace Sfs.EStore.Web.App.Models
{
    public class ManufacturerPageLiquidVariables : ILiquidVariables
    {
        public ManufacturerPageLiquidVariables(IWebApplicationIdentity identity, HttpRequestBase httpRequest)
        {
            user = new UserDrop(identity);
            page = new CmsPageDrop(httpRequest);
        }

        // ReSharper disable InconsistentNaming
        public string body { get; set; }
        public string page_title { get; set; }

        public string vehicle_type_query_term { get; set; }
        public string manufacturer_query_term { get; set; }

        public string image_url { get; set; }
        public UserDrop user { get; private set; }
        public CmsPageDrop page { get; private set; }
        // ReSharper restore InconsistentNaming
    }
}