using Sfs.EStore.Web.App.ThirdParties;

namespace Sfs.EStore.Web.App.Models
{
    public class LotsGetParams : ISecurable
    {
        public string[] LotIds { get; set; }
        public string Q { get; set; }
        public string VehicleType { get; set; }
        public string[] VehicleTypes { get; set; }
        public string Category { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Derivative { get; set; }
        public int? MaxMileage { get; set; }
        public string Mileage { get; set; }
        public int? MaxAge { get; set; }
        public string Transmission { get; set; }
        public string FuelType { get; set; }
        public int? MaxPrice { get; set; }
        public int? MinPrice { get; set; }
        public string[] LimitToRoles { get; set; }
        public string Top { get; set; }
        public string Skip { get; set; }
        public string SortBy { get; set; }
        /// <summary>
        /// Include Listing type: Auctions
        /// </summary>
        public int? LtAuction { get; set; }
        /// <summary>
        /// Include Listing type: Fixed Price
        /// </summary>
        public int? LtBin { get; set; }

        private int? _ctdn;

        /// <summary>
        /// Condition
        /// </summary>
        public int? Cdtn
        {
            get { return _ctdn; }
            set
            {
                _ctdn = value;
                if (value.HasValue) Condition = new[] {_ctdn.Value};
                else Condition = new int[0];
            }
        }

        public int[] Condition { get; set; }
        public string LSale { get; set; }
        public int? LPreview { get; set; }
        public string Registration { get; set; }
        public string Yap { get; set; }
        public string Source { get; set; }
        public string Engine { get; set; }
        public string ExteriorColour { get; set; }
        public string Equipment { get; set; }
        public string[] Exclude { get; set; }
        public string User { get; set; }
        public string AuctionStatus { get; set; }
        public string Marquee { get; set; }
        public string[] Makes { get; set; }
    }
}