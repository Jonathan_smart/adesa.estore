namespace Sfs.EStore.Web.App.Models
{
    public class BookAServicePostModel
    {
        public string FullName { get; set; }
        public string AddressLine1 { get; set; }
        public string PostCode { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }

        public string Registration { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string EngineSize { get; set; }
        public string Mileage { get; set; }

        public bool MotRequired { get; set; }
        public bool CourtesyVehicleRequired { get; set; }
        public string PreferredDate { get; set; }
        public string Comments { get; set; }
    }
}