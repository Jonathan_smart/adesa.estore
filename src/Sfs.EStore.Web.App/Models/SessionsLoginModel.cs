﻿using System.ComponentModel.DataAnnotations;

namespace Sfs.EStore.Web.App.Models
{
    public class SessionsLoginModel
    {
        public bool RememberMe { get; set; }

        [Required, Display(Name="Username/Email")]
        public string Username { get; set; }

        [Required, DataType(DataType.Password)]
        public string Password { get; set; }

        public string ReturnUrl { get; set; }
    }
}