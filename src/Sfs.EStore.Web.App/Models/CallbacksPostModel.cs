using System.ComponentModel.DataAnnotations;
using Sfs.EStore.Web.App.Code;

namespace Sfs.EStore.Web.App.Models
{
    public class CallbacksPostModel
    {
        public string LotIds { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string TelephoneNumber { get; set; }
        [Required]
        public string Message { get; set; }

        public object ToApi(SiteVisitor visitor)
        {
            return new
            {
                LotIds = (LotIds ?? "").Split(','),
                TelephoneNumber,
                Email,
                FirstName,
                LastName,
                Message,
                OriginalVisit = visitor == null ? null : new
                {
                    Source = new
                    {
                        Type = visitor.OriginalVisit.Source.Type.ToString(),
                        visitor.OriginalVisit.Source.Value
                    },
                    At = visitor.OriginalVisit.StartedAt
                },
                LastVisit = visitor == null ? null : new
                {
                    Source = new
                    {
                        Type = visitor.LastVisit.Source.Type.ToString(),
                        visitor.LastVisit.Source.Value
                    },
                    At = visitor.LastVisit.StartedAt
                }
            };
        }
    }
}