using Sfs.EStore.Web.App.Code.Widgets.Liquid.Drops;

namespace Sfs.EStore.Web.App.Models
{
    public class CmsPageLiquidVariables : ILiquidVariables
    {
        public CmsPageLiquidVariables(UserDrop user, CmsPageDrop page)
        {
            this.user = user;
            this.page = page;
        }

        // ReSharper disable InconsistentNaming
        public UserDrop user { get; private set; }
        public CmsPageDrop page { get; private set; }
        // ReSharper restore InconsistentNaming
    }
}