using Sfs.EStore.Web.App.ThirdParties;

namespace Sfs.EStore.Web.App.Models
{
    public class SingleLotGetParams : ISecurable
    {
        public string[] LimitToRoles { get; set; }
        public string Include { get; set; }
    }
}