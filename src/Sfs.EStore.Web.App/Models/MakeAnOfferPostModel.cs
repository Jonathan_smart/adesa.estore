using System;
using System.ComponentModel.DataAnnotations;

namespace Sfs.EStore.Web.App.Models
{
    public class MakeAnOfferPostModel
    {
        [Required]
        public string FullName { get; set; }
        public string AddressLine1 { get; set; }
        public string PostCode { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }

        public decimal OfferAmount { get; set; }

        public TenantEnrichmentInfo TenantEnrichment { get; set; }

        public class TenantEnrichmentInfo
        {
            public string IpAddress { get; set; }

            public string OriginalVisitType { get; set; }
            public string OriginalVisitSource { get; set; }
            public DateTime? OriginalVisitAt { get; set; }
            public string LatestVisitType { get; set; }
            public string LatestVisitSource { get; set; }
            public DateTime? LatestVisitAt { get; set; }
        }
    }
}