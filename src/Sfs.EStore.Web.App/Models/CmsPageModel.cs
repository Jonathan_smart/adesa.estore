using System;
using Sfs.EStore.Web.App.Code.Cms;

namespace Sfs.EStore.Web.App.Models
{
    public class CmsPageModel
    {
        /// <exception cref="ArgumentNullException"><paramref name="page"/> is <see langword="null" />.</exception>
        public CmsPageModel(CmsPage page, ILiquidVariables variables = null)
        {
            if (page == null) throw new ArgumentNullException("page");

            MetaData = MetaDataModel.CreateFrom(page.MetaData);
            BodyCssClass = page.BodyCssClass;
            Source = page.Body;
            PageTitle = page.PageTitle;
            PermaName = page.PermaName;
            Scripts = page.Scripts;

            Variables = variables;
        }

        public MetaDataModel MetaData { get; private set; }
        public string BodyCssClass { get; private set; }
        public string Source { get; private set; }
        public string PageTitle { get; private set; }
        public string PermaName { get; private set; }

        public CmsScript[] Scripts { get; set; }

        public ILiquidVariables Variables { get; private set; }
    }
}