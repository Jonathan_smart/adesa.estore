using System;
using Sfs.EStore.Web.App.Code.Cms;
using Sfs.EStore.Web.App.Code.Security;

namespace Sfs.EStore.Web.App.Models
{
    public class SearchPageModel
    {
        private readonly CmsPage _cmsPage;

        /// <exception cref="ArgumentNullException"><paramref name="page" /> is <see langword="null" />.</exception>
        public SearchPageModel(CmsPage cmsPage)
        {
            _cmsPage = cmsPage;
            _cmsPageModel = new CmsPageModel(_cmsPage);
            PredefinedQueryParams = new PredefinedQuery();
        }

        private readonly CmsPageModel _cmsPageModel;
        public CmsPageModel CmsPage
        {
            get { return _cmsPageModel; }
        }

        public bool ShowSaleFilter { get; set; }
        public bool ShowAuction { get; set; }
        public bool AllowOrderByTimeEndingSoonest { get; set; }

        public PredefinedQuery PredefinedQueryParams { get; private set; }

        public bool PageFound()
        {
            return _cmsPage != null;
        }

        public bool CanBeAccessedAs(IWebApplicationPrincipal user)
        {
            return _cmsPage.CanAccessPageAs(user);
        }

        public class PredefinedQuery
        {
            public string VehicleType { get; set; }
            public string Make { get; set; }
            public string Model { get; set; }
            public bool? LtAuction { get; set; }
            public bool? LtBin { get; set; }
            public bool? LotAuctionPreview { get; set; }
            public string[] Makes { get; set; }
        }
    }
}