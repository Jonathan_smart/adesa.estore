using System;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;

namespace Sfs.EStore.Web.App.Models
{
    public class PreviousValuationModel
    {
        public Guid Id { get; set; }
        public DateTime CreatedOn { get; set; }

        public string Registration { get; set; }
        public int Mileage { get; set; }

        public PartExValuationModel Valuation { get; set; }
    }

    public class PartExValuationModel
    {
        public MoneyViewModel Clean { get; set; }
        public MoneyViewModel Average { get; set; }
        public MoneyViewModel Below { get; set; }
    }

    public class PartExVrmLookupModel
    {
        public string Make { get; set; }
        public string Model { get; set; }
        public string Derivative { get; set; }
        public string RegistrationNumber { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string VehicleType { get; set; }
        public string FuelType { get; set; }
        public string Transmission { get; set; }
        public string Colour { get; set; }
        public int? EngineCapacity { get; set; }
    }
}