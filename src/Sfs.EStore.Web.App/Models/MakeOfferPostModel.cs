﻿using System.ComponentModel.DataAnnotations;
namespace Sfs.EStore.Web.App.Models
{
    public class MakeOfferPostModel
    {
        public string LotId { get; set; }
        [Required]
        public decimal Offer { get; set; }
        public string Comments { get; set; }
    }
}