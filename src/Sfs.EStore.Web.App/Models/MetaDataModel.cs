using System.ComponentModel.DataAnnotations;
using Sfs.EStore.Web.App.Code.Cms;

namespace Sfs.EStore.Web.App.Models
{
    public class MetaDataModel
    {
        public MetaDataModel()
        {
            Description = Title = CanonicalUrl = "";
        }

        public string Description { get; set; }
        public string Title { get; set; }
        [Display(Name = "Canonical URL")]
        public string CanonicalUrl { get; set; }

        public void CopyTo(MetaData metaData)
        {
            metaData.CanonicalUrl = CanonicalUrl;
            metaData.Description = Description;
            metaData.Title = Title;
        }

        public static MetaDataModel CreateFrom(MetaData metaData)
        {
            if (metaData == null) return null;

            return new MetaDataModel
            {
                CanonicalUrl = metaData.CanonicalUrl,
                Description = metaData.Description,
                Title = metaData.Title
            };
        }
    }
}