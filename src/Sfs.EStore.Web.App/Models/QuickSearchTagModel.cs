﻿namespace Sfs.EStore.Web.App.Models
{
    public class QuickSearchTagModel
    {
        public QuickSearchTagModel()
        {
            PredefinedQueryParams = new PredefinedQuery();
        }

        public bool HideVehicleTypeSelection { get; set; }

        public bool HideAdvanced { get; set; }

        public PredefinedQuery PredefinedQueryParams { get; private set; }

        public class PredefinedQuery
        {
            public string VehicleType { get; set; }
            public string Make { get; set; }
            public string Model { get; set; }
        }
    }
}