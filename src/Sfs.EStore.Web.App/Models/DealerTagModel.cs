﻿
namespace Sfs.EStore.Web.App.Models
{
    public class DealerTagModel
    {
        public string FoundImageUrl { get; set; }
        public string SelectImageUrl { get; set; }
    }
}