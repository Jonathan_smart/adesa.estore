﻿using System;
using System.Web;
using Cassette.Views;
using Sfs.EStore.Web.App.Areas.Retail;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.Security;
using Sfs.EStore.Web.App.Models;
using System.IO;

namespace Sfs.EStore.Web.App
{
    public abstract class WebViewPage : System.Web.Mvc.WebViewPage
    {
        private readonly Lazy<ViewTenant> _tenant = new Lazy<ViewTenant>(
            () =>
            {
                var ctx = new HttpContextWrapper(HttpContext.Current);
                return new ViewTenant(ctx.CurrentTenant(), ctx.CurrentTenantPermissions());
            });

        public new IWebApplicationPrincipal User
        {
            get { return (base.User as IWebApplicationPrincipal); }
        }

        public bool IsCurrentUserAllowed(string activity)
        {
            return Tenant.Permissions[activity].IsAllowedFor(User);
        }

        public ViewTenant Tenant
        {
            get { return _tenant.Value; }
        }

    }

    public abstract class WebViewPage<T> : System.Web.Mvc.WebViewPage<T>
    {
        private readonly Lazy<ViewTenant> _tenant = new Lazy<ViewTenant>(
            () =>
            {
                var ctx = new HttpContextWrapper(HttpContext.Current);
                return new ViewTenant(ctx.CurrentTenant(), ctx.CurrentTenantPermissions());
            });

        public new IWebApplicationPrincipal User
        {
            get { return (base.User as IWebApplicationPrincipal); }
        }

        public bool IsCurrentUserAllowed(string activity)
        {
            return Tenant.Permissions[activity].IsAllowedFor(User);
        }

        public ViewTenant Tenant
        {
            get { return _tenant.Value; }
        }

        public WebViewPage<T> SetMetaData(MetaDataModel metaData)
        {
            if (metaData != null)
            {
                ViewBag.MetaTitle = metaData.Title;
                ViewBag.MetaData = metaData;
            }
            return this;
        }

        public WebViewPage<T> SetPageTitle(string title)
        {
            ViewBag.PageTitle = title;
            return this;
        }


        protected WebViewPage<T> SetPageStyle()
        {
            var retail = new RetailView(User);

            if (Path.GetFileName(Request.Url.AbsolutePath).ToLower()!="markup")
            {
                    ViewBag.RetailCSS = retail.Marque == "found"
                                 ? "areas/retail/content/found/css/retail.scss"
                                 : "areas/retail/content/select/css/retail.scss";

                    Bundles.Reference(ViewBag.RetailCSS);
            }

            return this;
        }
        protected string CurrentBrand()
        {
            HttpCookie cookie = Request.Cookies["_retail"];
            const string brand = "MarqueName";
            bool cookieExists = cookie != null && cookie.HasKeys;

            if (cookieExists)
            {
                return cookie.Values[brand];
            }

            return string.Empty;
        }


        public WebViewPage<T> SetBodyCssClass(string cssClass)
        {
            ViewBag.BodyCssClass = cssClass;
            return this;
        }
    }

    public class ViewTenant
    {
        private readonly SiteTenant _tenant;
        private readonly SiteTenantActivityPermissions _tenantActivityPermissions;

        public ViewTenant(SiteTenant tenant, SiteTenantActivityPermissions tenantActivityPermissions)
        {
            _tenant = tenant;
            _tenantActivityPermissions = tenantActivityPermissions;
        }

        public string Name { get { return _tenant.Name; } }
        public string ThemeName { get { return _tenant.ThemeName; } }
        public SiteTenantActivityPermissions Permissions { get { return _tenantActivityPermissions; } }

        public SiteTenantSettings Settings { get { return _tenant.Settings; } }

        public bool IntegratesWith(string setting)
        {
            if (!_tenant.IntegrationSettings.ContainsKey(setting)) return false;

            bool value;
            bool.TryParse(_tenant.IntegrationSettings[setting], out value);

            return value;
        }
    }


}