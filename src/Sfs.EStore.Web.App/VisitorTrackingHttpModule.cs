using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.Security;
using log4net;
using Raven.Client;

namespace Sfs.EStore.Web.App
{
    public static class VisitorCookie
    {
        public static string Encrypt(string id)
        {
            var buffer = Encoding.ASCII.GetBytes(id);
            return Convert.ToBase64String(buffer);
        }

        public static string Decrypt(string token)
        {
            var buffer = Convert.FromBase64String(token);
            return Encoding.ASCII.GetString(buffer);
        }
    }

    public class VisitorTrackingHttpModule : IHttpModule
    {
        private const string VisitorCookieName = "__uvid3";
        private const string VisitHasBeenTrackedSessionKey = "__uvid3s";

        private static readonly ILog Logger = LogManager.GetLogger(typeof(VisitorTrackingHttpModule));

        public void Init(HttpApplication context)
        {
            context.BeginRequest += (sender, args) => ApplicationBeginRequest(new HttpContextWrapper(context.Context));
        }

        internal static string VisitorId(HttpContextBase context)
        {
            var cookie = context.Request.Cookies[VisitorCookieName];
            if (cookie != null)
                return VisitorCookie.Decrypt(cookie.Value);

            return null;
        }

        private void ApplicationBeginRequest(HttpContextBase context)
        {
            if (context.Request == null) return;

            if (VisitHasBeenTracked(context.Request.Cookies)) return;

            var siteVisitorId = VisitorId(context);

            var visitor = GetVisitor(siteVisitorId, session =>
            {
                var v = new SiteVisitor();
                session.Store(v);

                SetVisitorCookie(context, v.Id);

                return v;
            });

            var visit = ThisVisit(context);
            visitor.HasNewVisit(visit);

            SetVisitAsTracked(context.Response.Cookies);
        }

        private static SiteVisit ThisVisit(HttpContextBase context)
        {
            var source = ExtractSourceParamsFrom(context);

            return new SiteVisit(source, context.Request.Url.PathAndQuery);
        }

        private static void SetVisitorCookie(HttpContextBase context, string id)
        {
            context.Response.Cookies.Add(new HttpCookie(VisitorCookieName, VisitorCookie.Encrypt(id))
            {
                Expires = SystemTime.UtcNow.AddYears(3)
            });
        }

        private static bool VisitHasBeenTracked(HttpCookieCollection cookies)
        {
            return cookies[VisitHasBeenTrackedSessionKey] != null;
        }

        private static void SetVisitAsTracked(HttpCookieCollection cookies)
        {
            cookies.Add(new HttpCookie(VisitHasBeenTrackedSessionKey, "1"));
        }

        private SiteVisitor GetVisitor(string visitorId, Func<IDocumentSession, SiteVisitor> ifDoesNotExist)
        {
            var session = CmsHttpModule.CurrentTenantSession();
            SiteVisitor visitor = null;

            if (visitorId != null)
                visitor = session.Load<SiteVisitor>(visitorId);

            return visitor ?? ifDoesNotExist(session);
        }

        private static SiteVisitSource ExtractSourceParamsFrom(HttpContextBase context)
        {
            Debug.Assert(context.Request != null, "context.Request != null");
            Debug.Assert(context.Request.Url != null, "context.Request.Url != null");

            if (context.Request.UrlReferrer != null)
            {
                return new SiteVisitSource(SiteVisitSource.SiteVisitSourceType.Referral,
                    context.Request.UrlReferrer.ToString());
            }

            if (context.Request.Url.PathAndQuery.StartsWith("/affiliate?token="))
            {
                var queryParams = context.Request.Url.Query.Split(new[] {"&"}, StringSplitOptions.RemoveEmptyEntries);
                var encodedToken = queryParams.First(x => x.StartsWith("?token="))
                    .Replace("?token=", "");

                var token = HttpUtility.UrlDecode(encodedToken);

                var guestPass = "";

                try
                {
                    var ticket = GuestPass.Decrypt(token);

                    guestPass = ticket.Id;
                }
                catch(ArgumentException ex)
                {
                    Logger.Error("Failed to decrypt token", ex);
                }


                return new SiteVisitSource(SiteVisitSource.SiteVisitSourceType.GuestPass, guestPass);
            }

            return new SiteVisitSource(SiteVisitSource.SiteVisitSourceType.Direct,
                context.Request.Url.PathAndQuery);
        }

        public void Dispose()
        {
        }
    }
}