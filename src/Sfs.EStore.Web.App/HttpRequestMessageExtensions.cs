using System;
using System.Net.Http;
using System.Web;

namespace Sfs.EStore.Web.App
{
    internal static class HttpRequestMessageExtensions
    {
        public static HttpContextWrapper GetHttpContext(this HttpRequestMessage @this)
        {
            if (@this == null) throw new ArgumentNullException("@this");

            return @this.Properties.ContainsKey("MS_HttpContext")
                ? ((HttpContextWrapper)@this.Properties["MS_HttpContext"])
                : HttpContext.Current != null
                    ? new HttpContextWrapper(HttpContext.Current)
                    : null;
        }
    }
}