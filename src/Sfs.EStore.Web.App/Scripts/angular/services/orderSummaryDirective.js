﻿/// <reference path="~/angular"/>
/// <reference path="./sfs-vehicle-services.js"/>


(function (module) {
    module.directive('orderSummary', function () {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                order: '='
            },
            template: '<div class="row">' +
        '<div class="col-sm-4">' +
            '<h4><span class="text-muted">Order Placed:</span> <b><span>{{order.date | formatDate:\'LL\' }}</span></b></h4>' +
            '<h5><span class="text-muted">Order #:</span> <b><span>{{order.type}} - {{order.id}}</span></b></h5>' +
            '<h6><span class="text-muted">Customer:</span> <b><span>{{order.sellToCustomer.name}}</span></b></h6>' +
            '<h6 ng-if="order.shipping.address"><span class="text-muted">Delivery address:</span> </b><span>{{order.shipping.address.name}}, {{order.shipping.address.postCode}}</span></b></h6>' +
        '</div>' +
        '<div class="col-sm-8">' +
            '<table class="table table-condensed table-responsive">' +
                '<thead>' +
                    '<tr>' +
                        '<th colspan="1">&nbsp;</th>' +
                        '<th class="table-col-money">Price</th>' +
                        '<th class="table-col-money">VAT</th>' +
                        '<th class="table-col-money">Total</th>' +
                    '</tr>' +
                '</thead>' +
                '<tbody>' +
                    '<tr data-ng-repeat="line in order.lines">' +
                        '<td>{{line.description}}</td>' +
                        '<td class="table-col-money">{{line.amount | money:2}}</td>' +
                        '<td class="table-col-money">{{line.vat | money:2}}</td>' +
                        '<td class="table-col-money">{{line.total | money:2}}</td>' +
                    '</tr>' +
                '</tbody>' +
                '<tfoot>' +
                    '<tr class="total-price-row">' +
                        '<td colspan="1">&nbsp;</td>' +
                        '<td class="table-col-money"><b>{{order.subTotal | money:2}}</b></td>' +
                        '<td class="table-col-money"><b>{{order.vat | money:2}}</b></td>' +
                        '<td class="table-col-money"><b>{{order.total | money:2}}</b></td>' +
                    '</tr>' +
                '</tfoot>' +
            '</table>' +
        '</div>' +
        
        '</div>' +


    '</div>'
        };
    });
}(angular.module('urlFilters')));
