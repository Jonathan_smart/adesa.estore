/// <reference path="~/jquery"/>
/// <reference path="~/angular"/>

angular.module('prettyDateFilters', [])
    .filter('notLessThanNow', ['$filter', function ($filter) {
        var jsonFilter = $filter('json');

        return function (input) {
            return moment(input).isBefore(moment())
                ? jsonFilter(new Date())
                : input;
        };
    }])
    .filter('notGreaterThanNow', ['$filter', function ($filter) {
        var jsonFilter = $filter('json');

        return function (input) {
            return moment(input).isAfter(moment())
                ? jsonFilter(new Date())
                : input;
        };
    }])
    .directive('timeLeft', ['$timeout', '$filter', '$window', '$location', 'sfsAuthService', function ($timeout, $filter, $window, $location, sfsAuthService) {
        var humanizeDuration = $filter('humanizeDuration');

        return function (scope, element, attrs) {
            var timeLeft, timeoutId;

            function updateTimeLeft() {
                //if (timeLeft <= 1000) {
                //    $timeout(function () {
                //        $window.location.href = $location.absUrl();
                //    }, 4000);
                //} else {
                    element.text(humanizeDuration(timeLeft));
                //}
            }

            scope.$watch('auth.isAuthenticated', function () {
                if (scope.auth.isAuthenticated) {
                    console.log("watching");
                } else {
                    console.log("EXPIRED");
                }
            });

            scope.$watch(attrs.timeLeft, function (value) {
                timeLeft = value;
                updateTimeLeft();
            });

            function updateLater() {
                timeoutId = $timeout(function () {

                    timeLeft -= 1000;
                    updateTimeLeft();
                    if (timeLeft <= 1000) return;
                    updateLater();
                }, 1000);
            }

            element.bind('$destroy', function () {
                $timeout.cancel(timeoutId);
            });

            updateLater();
        }
    }])
    .filter('humanizeDuration', [function () {
        return function (input, minValue) {
            var valueToHumanize = input;
            if (minValue !== undefined && valueToHumanize < minValue)
                valueToHumanize = minValue;
            var d = moment.duration(valueToHumanize);

            if (d.days() === 0 && d.hours() < 1)
                return d.hours() + 'h ' + d.minutes() + 'm ' + d.seconds() + 's';

            return d.days() + 'd ' + d.hours() + 'h ' + d.minutes() + 'm';
        };
    }])
    .filter('humanizeFromNow', function () {
        return function (input) {
            return moment.duration(moment(input).diff()).humanize(true);
        };
    })
    .filter('formatDate', function () {
        return function (input, format) {
            return moment(input).format(format || 'LLL');
        };
    });
