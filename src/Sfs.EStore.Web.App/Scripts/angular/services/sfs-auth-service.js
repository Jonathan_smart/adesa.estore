﻿/// <reference path="~/angular"/>
(function () {
    'use strict';

    var Auth = function ($http, $log, $q, siteUrls) {
        var self = this;
        var globalAuth = window.__sfs.auth;

        self.isAuthenticated = globalAuth.isAuthenticated;
        self.isGuest = globalAuth.isGuest;
        self.currentUser = globalAuth.currentUser;

        self.userClaims = window.__sfs.claims || [];

        function userHasClaim(claim) {
            return self.userClaims.indexOf(claim) !== -1;
        }

        self.isAuthorized = function(requiredClaims) {
            if (!requiredClaims) return true;

            if (!angular.isArray(requiredClaims)) {
              requiredClaims = [requiredClaims];
            }

            return self.isAuthenticated && requiredClaims.every(userHasClaim);
        };

        self.login = function (username, password) {
            var deferred = $q.defer();

            $http.post(siteUrls.path('sessions/login'), {
                username: username,
                password: password
            }, { ignoreAuthModule: true })
                .then(function (res) {
                    self.isAuthenticated = true;
                    self.isGuest = false;
                    self.currentUser = {
                        username: res.data.UserName || "",
                        firstName: res.data.FirstName || "",
                        lastName: res.data.LastName || "",
                        emailAddress: res.data.EmailAddress || "",
                        phoneNumber: res.data.PhoneNumber || ""
                    };
                    deferred.resolve(self.currentUser);
                }, function (res) {
                    deferred.reject(res);
                });

            return deferred.promise;
        };
    };

    angular.module('sfs-auth-service', [])
        .factory('sfsAuthService', ['$http', '$log', '$q', 'siteUrls', function ($http, $log, $q, siteUrls) {
            return new Auth($http, $log, $q, siteUrls);
        } ]);

    angular.module('sfs-auth-service')
        .directive('autoFillSync', ['$timeout', function ($timeout) {
            // Based on: http://stackoverflow.com/questions/14965968/angularjs-browser-autofill-workaround-by-using-a-directive
            return {
                require: 'ngModel',
                link: function (scope, elem, attrs, ngModel) {
                    var origVal = elem.val();

                    var interval = setInterval(function () {
                        var newVal = elem.val();

                        if (origVal !== newVal) {
                            scope.$apply(function() {
                                ngModel.$setViewValue(newVal);
                                origVal = newVal;
                            });
                        }

                    }, 500);

                    scope.$on('$destroy', function () {
                        clearInterval(interval);
                    });
                }
            };
        } ]);

    var LoginDialogController = function ($rootScope, $scope, $modalInstance, authService, sfsAuthService) {
        $scope.setUsername = function (username) {
            $scope.username = username;
        };
        $scope._HACK_MergeScope = function (username, password) {
            $scope.username = username;
            $scope.password = password;
        };
        var login = function () {

            if (!$scope.username || !$scope.password) return;

            $scope.submitting = true;

            sfsAuthService.login($scope.username, $scope.password)
                .then(function (res) {
                    $scope.attempted = true;
                    $scope.submitting = false;
                    authService.loginConfirmed();
                }, function (res) {
                    $scope.reason = (res.data || '').replace(/\"/g, '');
                    $scope.denied = true;
                    $scope.attempted = true;
                    $scope.submitting = false;
                });
        };
        $scope.submit = function () {
            login($scope.username, $scope.password);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $rootScope.$on('event:auth-loginConfirmed', function () {
            $modalInstance.close(true);
        });
    };
    var LoginService = function ($rootScope, $modal) {
        $rootScope.$on('event:auth-loginRequired', function () {
            $modal.open({
                dialogFade: true,
                backdrop: 'static',
                keyboard: false,
                templateUrl: 'loginform.html',
                controller: ['$rootScope', '$scope', '$modalInstance', 'authService', 'sfsAuthService', LoginDialogController]
            });
        });
    };

    angular.module('sfs-auth-service')
        .factory('sfsLoginOnRequest', ['$rootScope', '$modal', function ($rootScope, $modal) {
            return new LoginService($rootScope, $modal);
        } ]);
} ());
