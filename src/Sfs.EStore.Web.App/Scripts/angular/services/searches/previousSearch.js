/// <reference path="~/angular"/>
/// <reference path="./index.js"/>

(function () {
    "use strict";

    angular.module('components.searches')
    .factory('previousSearch', [
        '$cookies', '$filter',
        function ($cookies, $filter) {
            var cleanseParams = function(params) {
                var copy = angular.copy(params);

                for (var i in copy) {
                  if (copy[i] === null || copy[i] === undefined) {
                    delete copy[i];
                  }
                }

                delete copy['skip'];
                delete copy['top'];

                return copy;
            };

            return {
                url: function() {
                    var params = this.params();
                
                    if (!params) return;
                   
                    var searchUrl = $filter('searchUrl')(params);
                  
                    return searchUrl || undefined;
                },
                params: function() {
                    if (!$cookies.lastSearch) return;
         
                    return JSON.parse($cookies.lastSearch);
                },
                set: function (params) {
                 
                    var toSave = cleanseParams(params);
                    var searchUrl = $filter('searchUrl')(toSave);
                   
                    $cookies.lastSearch = JSON.stringify(toSave || {});
                }
            };
        }
    ]);
} ());