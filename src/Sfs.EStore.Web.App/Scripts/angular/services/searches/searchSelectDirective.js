/// <reference path="~/angular"/>
/// <reference path="../sfs-vehicle-services.js"/>
/// <reference path="./index.js"/>

(function () {

    angular.module('components.searches')
        .filter('valueToLabelFormatter', function () {
            return function (input) {
                if (input == "EMPTY_STRING") return "Unspecified";
                return input;
            };
        })
        .directive('facetSelect', ['$filter', '$log', '$timeout', function ($filter, $log, $timeout) {
            var labelValueFormatter = $filter('valueToLabelFormatter');
            return {
                restrict: 'EA',
                require: '^queryParams',
                transclude: true,
                replace: true,
                scope: { selectCssClass: '@ngSelectClass', value: '=ngValue', parent: '=ngParent', optionLabel: '@ngOptionLabel', valueFormatter: '&ngValueFormatter' },
                template: '<div ng-model="value">' +
                '  <select class="{{selectCssClass}} form-control" ng-disabled="(!list || list.length == 0 || (isParentSet && !parent))" ng-hide="value" ng-model="value" ng-options="value.key as value.label for value in list">' +
                '    <option value="">{{optionLabel}}</option>'+
                '  </select>' +
                '  <button type="button" ng-show="value" ng-click="value=undefined" class="btn search-selected-value">{{valueFormatter(value)}} <i class="icon-remove"></i></button>' +
                '</div>',
                link: function (scope, element, attrs, ctrl) {
                    scope.isParentSet = attrs['ngParent'];
                    scope.list = [];
                    if (!attrs['ngValueFormatter'])
                        scope.valueFormatter = function (label) {
                            return labelValueFormatter(label);
                        };

                    scope.$watch(function () {
                        return ctrl.facets;
                    }, function (newValue) {
                        var facetValues = (newValue[attrs.facet]);
                        scope.list = facetValues;
                    }, true);

                    // IE9 fix. Disabled select list selected option text is not updated.
                    // Can remove if not supporting IE9
                    $timeout(function () {
                      $(element).find('select').each(function (dx, elem) {
                        elem.options[elem.selectedIndex].text += ' '
                      });
                    });
                }
            };
        } ]);
} ());