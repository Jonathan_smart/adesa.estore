/// <reference path="~/angular"/>
/// <reference path="./index.js"/>

(function () {
    "use strict";

    var QueryParamsDirectiveController = function ($scope, $modal, $location, $element, $log, $http, Angularytics, searchParamValueFormatters, $filter, $timeout, siteUrls) {

        var numberFilter = $filter('number');

        $scope.searchParamValueFormatters = searchParamValueFormatters;
        $scope.params = angular.copy($scope.queryParams);
        var _this = this;
        _this.facets = {};

        $scope.estimatedTotalRecords = 0;
        //console.log($scope.params.ltauction);
        //if ($scope.params.ltauction != undefined)
        //    $scope.sortBy = 'endTime';

        $scope.applyQuery = function () {

            var queryParams = angular.copy($scope.params);
            for (var prop in queryParams) {
                if (queryParams.hasOwnProperty(prop) && (queryParams[prop] == null || queryParams[prop] == undefined || queryParams[prop] == ''))
                    delete queryParams[prop];
            }
            $scope.$emit('applySearchQuery', queryParams);
        };

        $scope.reset = function (params) {

            $scope.params = {
                //t: "a",
                vehicleType: $scope.params.vehicleType,
                lsale: $scope.params.lsale,
                ltauction: $scope.params.ltauction,
                LPreview: $scope.params.LPreview,
                makes: $scope.params.makes
            };
            $scope.$emit('queryParamsReset', $scope.params);
        };

        var paramsChangedTimeoutPromise;
        $scope.$watch('params', function (newValue, oldValue) {
            if (newValue === oldValue) return;

            var args = { params: newValue };

            $scope.isDirty = true;

            if (paramsChangedTimeoutPromise) {
                $timeout.cancel(paramsChangedTimeoutPromise);
            }

            paramsChangedTimeoutPromise = $timeout(function () {
                var facetsPromise = requeryFacets();
                $scope.$emit('queryParametersChanged', args);

                facetsPromise.then(function success() {
                    $log.debug('Search query changed', {
                        q: args.params.q,
                        totalResults: $scope.estimatedTotalRecords
                    });
                    Angularytics.trackEvent('Search query changed', args.params.q, $scope.estimatedTotalRecords);
                });
            }, 300);
        }, true);

        $scope.$watch('params.make',
            function (newValue, oldValue) {
                if (newValue != oldValue && (newValue || '') === '') {
                    $scope.params.model = '';
                }
            });
        $scope.$watch('params.model',
            function (newValue, oldValue) {
                if (newValue != oldValue && (newValue || '') === '') {
                    $scope.params.derivative = '';
                }
            });

        function facetMapper(facet, options) {
            (options || (options = {}));

            var defaultLabelFormatter = function (facetItem) {
                return searchParamValueFormatters.value(facetItem.range) + " (" + facetItem.hits + ")";
            };
            var defaultValueFormatter = function (facetItem) {
                return facetItem.range;
            };

            var labelFormatter = options.labelFormatter || defaultLabelFormatter;
            var valueFormatter = options.valueTransformer || defaultValueFormatter;
            var includeItemPredicate = options.predicate || function (item) { return item.hits > 0; };

            var result = [];

            for (var n = 0; n < facet.length; n++) {
                var item = facet[n];
                if (includeItemPredicate(item)) {
                    var newObj = { key: valueFormatter(item), label: labelFormatter(item) };

                    if (options.extender) $.extend(newObj, options.extender(item));

                    result.push(newObj);
                }
            }

            return result;
        }

        (function () {
            return $http.post(siteUrls.path('api/facets'), { facet: 'VehicleTypes' })
                .then(function (res) {
                    $scope.vehicleTypeFacets = facetMapper(res.data.results.vehicleType.values, {
                        labelFormatter: function (item) {
                            return item.range;
                        },
                        extender: function (item) {
                            return {
                                selected: ($scope.params.vehicleType && $scope.params.vehicleType == item.range)
                            };
                        }
                    });
                });
        }());

        function requeryFacets() {
            var payload = angular.copy($scope.params);

            return $http.post(siteUrls.path('api/facets'), payload)
                .then(function success(resp) {
                    $scope.estimatedTotalRecords = resp.headers("X-TotalResults");

                    var facetResults = resp.data.results;

                    _this.facets.vehicleType = facetMapper(facetResults.vehicleType.values);
                    _this.facets.category = facetMapper(facetResults.category.values);
                    _this.facets.make = facetMapper(facetResults.make.values);
                    _this.facets.model = facetMapper(facetResults.model.values);
                    _this.facets.derivative = facetMapper(facetResults.derivative.values);
                    _this.facets.fuelType = facetMapper(facetResults.fuelType.values);
                    _this.facets.engine = facetMapper(facetResults.engine.values);
                    _this.facets.transmission = facetMapper(facetResults.transmissionSimple.values);

                    if (facetResults.source != undefined)
                        _this.facets.source = facetMapper(facetResults.source.values);
                    _this.facets.mileage = facetMapper(facetResults.mileage_Range.values,
                        {
                            labelFormatter: function (item) {
                                return searchParamValueFormatters.range(item.range) + " (" + numberFilter(item.hits) + ")";
                            }
                        });
                    _this.facets.sale = facetMapper(facetResults.saleTitle.values,
                        {
                            predicate: function (item) { return item.hits > 0 && item.range != 'NULL_VALUE'; }
                        });

                    var maxAgeRange = ($scope.estimatedTotalRecords == 0)
                        ? []
                        : [
                            { "range": "1", "hits": 1 },
                            { "range": "2", "hits": 1 },
                            { "range": "3", "hits": 1 },
                            { "range": "4", "hits": 1 },
                            { "range": "5", "hits": 1 },
                            { "range": "7", "hits": 1 },
                            { "range": "10", "hits": 1 }
                        ];


                    _this.facets.maxAge = facetMapper(maxAgeRange,
                        {
                            labelFormatter: function (item) {
                                return searchParamValueFormatters.maxAgeRange(item.range);
                            }
                        });
                    _this.facets.yap = facetMapper(facetResults.registrationYaP.values);
                    _this.facets.exteriorColour = facetMapper(facetResults.exteriorColour.values);

                    if (facetResults.auctionStatus != null)
                        _this.facets.auctionstatus = facetMapper(facetResults.auctionStatus.values);
                }).then(function success() {
                    if (!$scope.facetEquipment) return;

                    var equipmentPayload = angular.extend(payload, { facet: 'Equipment' });
                    return $http.post(siteUrls.path('api/facets'), equipmentPayload).then(function (resp) {
                        var facetResults = resp.data.results;
                        _this.facets.equipment = facetMapper(facetResults.equipment.values);
                    });
                });
        };

        requeryFacets();

        $scope.showMyBidHistory = function () {

            $modal.open({
                keyboard: false,
                templateUrl: 'bidding.html',
                size: 'lg',
                controller: ['$scope', '$location', function ($scope, $location) {
    
                    $scope.working = false;
                    $scope.loaded = false;
                    $scope.listings = undefined;
                    $scope.search = $location.search() || {};

                    $scope.sortBy = 'endTime';

                    $scope.pagination = {
                        take: 20,
                        currentPage: 1
                    };

                    $scope.query = function () {
                        $location.search($scope.search);
                        executeQuery();
                    };

                    $scope.filterByStatus = function (status) {
                        $scope.filterStatus = status;
                        executeQuery();
                    };

                    $scope.setSortBy = function () {
                        $scope.sortBy = $('#sortBy').val();
                        executeQuery();
                    };

                    $scope.$watch('sortBy', function (newValue, oldValue) {
                        if (newValue == oldValue) return;

                        executeQuery();
                    });
                    var executeQuery = function () {
                        $scope.working = true;

                        $http.get(siteUrls.path('api/bids'), {
                            params: {
                                status: $scope.filterStatus || 'bidding',
                                sortBy: $scope.sortBy,
                                pagenumber: $scope.pagination.currentPage,
                                pagesize: $scope.pagination.take
                            }
                        })
                            .then(function (res) {
                                $scope.loaded = true;
                                $scope.working = false;
                                $scope.listings = res.data;

                                $scope.pagination.totalCount = res.headers("X-TotalResults");
                            }, function (res) {
                                $scope.loaded = true;
                                $scope.errorLoading = true;
                                $scope.working = false;
                            });
                    };

                    $scope.$watch('totalCount', function () {
                        $scope.pagination.noOfPages = Math.ceil($scope.totalCount / $scope.pagination.take);
                    });
                    $scope.$watch('pagination.currentPage', function (newValue, oldValue) {
                        if (newValue !== oldValue)
                            $scope.query();
                    });

                    $scope.bidding = function () {

                         executeQuery = function () {
                            $scope.working = true;

                            $http.get(siteUrls.path('api/bids'), {
                                params: {
                                    status: $scope.filterStatus || 'bidding',
                                    sortBy: $scope.sortBy,
                                    pagenumber: $scope.pagination.currentPage,
                                    pagesize: $scope.pagination.take
                                }
                            })
                                .then(function (res) {
                                    $scope.loaded = true;
                                    $scope.working = false;
                                    $scope.listings = res.data;

                                    $scope.pagination.totalCount = res.headers("X-TotalResults");
                                }, function (res) {
                                    $scope.loaded = true;
                                    $scope.errorLoading = true;
                                    $scope.working = false;
                                });
                        }
                 
                        executeQuery();
                    };
                    $scope.didntWin = function () {
                 
                         executeQuery = function () {
                            $scope.working = true;

                            $http.get(siteUrls.path('api/bids'), {
                                params: {
                                    status: 'lost',
                                    sortBy: $scope.sortBy,
                                    pagenumber: $scope.pagination.currentPage,
                                    pagesize: $scope.pagination.take
                                }
                            })
                                .then(function (res) {
                                    $scope.loaded = true;
                                    $scope.working = false;
                                    $scope.listings = res.data;

                                    $scope.pagination.totalCount = res.headers("X-TotalResults");
                                }, function (res) {
                                    $scope.loaded = true;
                                    $scope.errorLoading = true;
                                    $scope.working = false;
                                });
                        };

                        $scope.$watch('totalCount', function () {
                            $scope.pagination.noOfPages = Math.ceil($scope.totalCount / $scope.pagination.take);
                        });
                        $scope.$watch('pagination.currentPage', function (newValue, oldValue) {
                            if (newValue !== oldValue)
                                $scope.query();
                        });

                        executeQuery();
                    };

                    $scope.purchaseHistory = function () {
               

                         executeQuery = function () {
                            $scope.working = true;

                            $http.get(siteUrls.path('api/bids'), {
                                params: {
                                    status: 'won',
                                    sortBy: $scope.sortBy,
                                    pagenumber: $scope.pagination.currentPage,
                                    pagesize: $scope.pagination.take
                                }
                            })
                                .then(function (res) {
                                    $scope.loaded = true;
                                    $scope.working = false;
                                    $scope.listings = res.data;

                                    $scope.pagination.totalCount = res.headers("X-TotalResults");
                                }, function (res) {
                                    $scope.loaded = true;
                                    $scope.errorLoading = true;
                                    $scope.working = false;
                                });
                        };

                        $scope.$watch('totalCount', function () {
                            $scope.pagination.noOfPages = Math.ceil($scope.totalCount / $scope.pagination.take);
                        });
                        $scope.$watch('pagination.currentPage', function (newValue, oldValue) {
                            if (newValue !== oldValue)
                                $scope.query();
                        });

                        executeQuery();
                        return;
                    };

                }]
            });
        };
    }

    function QueryParamsDirective() {
        return {
            restrict: 'A',
            controller: ['$scope', '$modal', '$location', '$element', '$log', '$http', 'Angularytics', 'searchParamValueFormatters', '$filter', '$timeout', 'siteUrls', QueryParamsDirectiveController]
        };
    };

    angular.module('components.searches')
        .directive('queryParams', QueryParamsDirective)
        .factory('searchParamValueFormatters', [
            '$filter', function ($filter) {
                var rangeFilterToLabel = $filter('rangeFilterToLabel');

                var labelValueFormatter = function (label) {
                    if (label == "EMPTY_STRING") return "Unspecified";
                    return label;
                };

                return {
                    value: function (value) {
                        return labelValueFormatter(value);
                    },
                    range: function (range) {
                        return rangeFilterToLabel(range, { valuePostFix: ' miles' });
                    },
                    maxAgeRange: function (value) {
                        if (value == 1)
                            return "Up to " + value + " year old";
                        return "Up to " + value + " years old";
                    }
                };
            }
        ]);
}());
