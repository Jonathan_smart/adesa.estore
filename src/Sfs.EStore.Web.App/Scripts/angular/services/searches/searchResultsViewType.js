/// <reference path="~/angular"/>
/// <reference path="./index.js"/>

(function () {
    "use strict";

    angular.module('components.searches')
    .factory('searchResultsViewType', [
        '$cookies',
        function ($cookies) {

            function cookieName(key) {
                if (!key) return 'preferredView';

                return ('preferredView_' + key);
            }

            return {
                set: function(value, key) {
                    var name = cookieName(key);
                    $cookies[name] = (value);
                },
                get: function (key) {
                    var name = cookieName(key);
                    var value = $cookies[name];
                    return value;
                }
            }
        }
    ]);
} ());
