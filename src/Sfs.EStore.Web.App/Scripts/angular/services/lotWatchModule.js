﻿/// <reference path="~/angular"/>

(function () {

    angular.module('lotWatchModule', ['sfs-auth-service']);

    var WatchesService = function ($http, $q, siteUrls, sfsAuthService) {
        // var uri = function (listingId) {
        //     return siteUrls.path('api/listings/' + listingId + '/watches/');
        // };
        // var findUri = function () {
        //     return siteUrls.path('api/watches');
        // };
        var scope = {
            allowed: function() { return sfsAuthService.isAuthenticated; },
            find: function (options) {
                options = options || {};
                options.params = options.params || {
                    top: 10,
                    skip: 0
                };
                var req = $http.get(siteUrls.path('api/watches'), {
                    params: options.params
                });

                return req;
            },
            get: function (listingId) {
                return $http.get(siteUrls.path('api/listings/'+listingId+'/watches'), {
                    ignoreAuthModule: true
                }).then(function (res) {
                    return res.data.watched;
                }, function () {
                    $q.reject();
                });
            },
            add: function (listingId) {
                return $http({ 
                    method: 'POST', 
                    data: {},
                    url: siteUrls.path('api/listings/'+listingId+'/actions/add_watch') 
                }).then(function () {
                    return true;
                }, function () {
                    $q.reject();
                });
            },
            remove: function (listingId) {
                return $http({ 
                    method: 'POST', 
                    url: siteUrls.path('api/listings/'+listingId+'/actions/remove_watch') 
                }).then(function () {
                    return false;
                }, function () {
                    $q.reject();
                });
            }
        };

        return scope;
    };

    angular.module('lotWatchModule')
        .factory('watches', ['$http', '$q', 'siteUrls', 'sfsAuthService', WatchesService]);

    var LotWatchController = function ($scope, watches, sfsAuthService) {
        var listingId = ($scope.id || '').toString().replace('lots/', '');
        $scope.auth = sfsAuthService;

        $scope.watches = {
            isWatched: false,
            remove: function () {
                watches.remove(listingId).then(function (isWatched) { $scope.watches.isWatched = isWatched; });
            },
            add: function () {
                watches.add(listingId).then(function (isWatched) { $scope.watches.isWatched = isWatched; });
            }
        };

        $scope.$watch('id', function (newValue) {
            listingId = (newValue || '').toString().replace('lots/', '');
            getWatch();
        });

        var getWatch = function () {
            $scope.watches.isWatched = undefined;

            if (!listingId) return;
            if (!$scope.canWatch) return;

            watches.get(listingId).then(function (isWatched) { $scope.watches.isWatched = isWatched; });
        };

        var updateCanWatch = function () {
            $scope.canWatch = $scope.auth.isAuthenticated;
        };
        updateCanWatch();

        $scope.$watch('auth.isAuthenticated', function () {
            updateCanWatch();
        });
        $scope.$watch('canWatch', function () {
            getWatch();
        });
    };

    var WatchStatusToggleDirective = function () {
        return {
            restrict: 'A',
            transclude: true,
            replace: false,
            controller: ['$scope', 'watches', 'sfsAuthService', LotWatchController],
            scope: { id: '=ngListingId', cssClass: '@ngBtnClass', cssIconClass: '@ngIconClass', alwaysVisible: '=ngAlwaysVisible', addText: "@", removeText: "@", textClass: "@" },
            template: "<span data-ng-model=\"id\" data-ng-show=\"canWatch || alwaysVisible\">" +
            "<a data-ng-if=\"watches.isWatched\" class=\"{{cssClass}} btn-watches btn-watches-remove\" data-tooltip-popup-delay=\"800\" tooltip=\"You've added this to your saved vehicles. Click to remove\" ng-click=\"watches.remove()\"><i class=\"{{cssIconClass}} icon-star\"></i> <span class=\"{{textClass}}\">{{ removeText }}</span></a>" +
            "<a data-ng-if=\"!watches.isWatched\" class=\"{{cssClass}} btn-watches btn-watches-add\" data-tooltip-popup-delay=\"800\" tooltip=\"Add to your saved vehicles\" ng-click=\"watches.add()\"><i class=\"{{cssIconClass}} icon-star-empty\"></i> <span class=\"{{textClass}}\">{{ addText }}</span></a>" +
            "</span>"
        };
    };

    angular.module("lotWatchModule")
        .directive('watchStatusToggle', WatchStatusToggleDirective)
        
        .directive('watchToggle', [function() {
            return {
                restrict: 'E',
                replace: true,
                template: '<button ng-hide="hidden" ng-class="cssClass" ng-click="toggle()" ng-bind-html="text"></button>',
                scope: { listingId: '=ngListingId' },
                controller: ['$scope', 'watches', '$sce', '$log', function($scope, watches, $sce, $log) {
                    $scope.busy = true;

                    $scope.toggle = function() {
                        $scope.busy = true;

                        ($scope.watched ? watches.remove : watches.add)($scope.listingId)
                        .then(function success(watched) {
                            $scope.watched = watched;
                        }, function failed(resp) {
                            $log.error(resp);
                        })['finally'](function() {
                            $scope.busy = false;
                        });
                    };

                    var checkVisibility = function() {
                        var allowed = watches.allowed() || $scope.visibility == 'always'
                        if (!watches.allowed() && $scope.visibility != 'always') {
                            $scope.hidden = true;
                            return false;
                        }

                        $scope.hidden = false;
                        return true;
                    };

                    $scope.$watch('rawText', function(newValue) {
                        $scope.text = $sce.trustAsHtml(newValue);
                    });

                    $scope.$watch('visibility', function(newValue) {
                        checkVisibility();
                    });

                    var updateStatus = function() {
                        if (!$scope.listingId || $scope.hidden) return;

                        watches.get($scope.listingId)
                        .then(function success(watched) {
                            $scope.watched = watched;
                        }, function failed(resp) {
                            $log.error(resp);   
                            $scope.error = true;
                        })['finally'](function() {
                            $scope.busy = false;
                        });
                    };

                    $scope.$watch('listingId', function(newValue) {
                        if (!newValue) return;
                        updateStatus();
                    });

                    if (!checkVisibility()) {
                        return;
                    }
                    updateStatus();
                }],
                link: function(scope, element, attrs, ctrl, transclude) {
                    scope.visibility = (attrs.visible);
                    scope.$watch('watched', function(newValue) {
                        scope.rawText = newValue
                            ? (attrs.watchedText || 'Remove')
                            : (attrs.notWatchedText || 'Save')
                        scope.cssClass = newValue
                            ? (attrs.watchedClass || 'btn btn-default')
                            : (attrs.notWatchedClass || 'btn btn-default')
                    });
                }
            }
        }]);
} ());