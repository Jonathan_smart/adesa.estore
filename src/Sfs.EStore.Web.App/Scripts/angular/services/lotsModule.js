﻿(function () {
    "use strict";

    angular.module("lotsModule.Controllers", []);
    angular.module("lotsModule.Services", []);
    angular.module("lotsModule", ['lotsModule.Controllers', 'lotsModule.Services']);
} ());