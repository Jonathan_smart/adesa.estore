﻿/// <reference path="./lotsModule.js"/>

(function () {
    "use strict";

    var LotSpecCalloutDirectiveController = function ($rootScope, $scope, $filter, $http, $q, $attrs, siteUrls) {
        var buildCalloutSpecifications = function (calloutSpecifications) {

            if (!calloutSpecifications) return;

            $scope.calloutSpecifications = [];

            for (var i = 0; i < calloutSpecifications.length; i++) {
                var callout = calloutSpecifications[i];
                $scope.calloutSpecifications.push({ displayName: callout.name, emphasise: callout.emphasise });
            }
        };

        var isNumber = function (n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        };

        var initCalloutSecifications = function (newValue, specLimit) {
            var deferred = $q.defer();

            if (isNumber(newValue)) {
                deferred.resolve(newValue);
            } else {
                if (newValue.listingId)
                    deferred.resolve(newValue.listingId);
                else
                    deferred.reject();
            }

            deferred.promise.then(function (listingId) {
                $http.get(siteUrls.path('api/lots/' + listingId + '/speccallouts?limit=' + (specLimit || 6)))
                    .then(function (res) {
                        buildCalloutSpecifications(res.data);
                    });
            });
        };

        $scope.$watch($attrs.specLot, function (newValue, oldValue) {
            if (!newValue) return;
            if ($scope.calloutSpecifications && newValue === oldValue) return;
            if ($scope.calloutSpecifications && !isNumber(newValue) && newValue.id === oldValue.id) return;

            initCalloutSecifications(newValue, $attrs.specLimit);
        });
    };

    var LotSpecCalloutDirective = function () {
        return {
            restrict: 'A',
            controller: ['$rootScope', '$scope', '$filter', '$http', '$q', '$attrs', 'siteUrls', LotSpecCalloutDirectiveController]
        };
    };

    angular.module("lotsModule.Services")
        .directive('lotSpecCallout', LotSpecCalloutDirective);
} ());