﻿/// <reference path="./lotsModule.js"/>

(function () {
    "use strict";

    var LotService = function ($http, $q, siteUrls) {
        var lotUrl = function (listingId) {
            return siteUrls.path('api/lots/' + listingId);
        };

        var get = function (listingId) {
            return $http.get(lotUrl(listingId),
                {
                    params: {}
                });
        };

        return {
            get: function (listingId) {
                var deferred = $q.defer();
                get(listingId).then(function (res) {
                    deferred.resolve(res.data);
                }, function () {
                    deferred.reject();
                });
                return deferred.promise;
            }
        };
    };

    angular.module("lotsModule.Services")
        .factory('lotService', ['$http', '$q', 'siteUrls', LotService]);
} ());