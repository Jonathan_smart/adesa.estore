/// <reference path="~/angular"/>
/// <reference path="~/scripts/angular/components/"/>

(function () {

    angular.module('urlFilters', ['app.components'])
        .filter('slugify', function () {
            return function (input) {
                return input.toLowerCase()
                    .replace(/ /g, '-')
                    .replace(/[^\w-]+/g, '');
            };
        })
        .filter('retailSaving', function () {
            return function (vehicle) {
                if (!vehicle.sellingStatus.currentPrice || !vehicle.discountPriceInfo || !vehicle.discountPriceInfo.retailPrice)
                    return null;

                var currentPrice = vehicle.sellingStatus.currentPrice.amount;
                var retailPrice = vehicle.discountPriceInfo.retailPrice.amount;
                var delta = retailPrice - currentPrice;

                if (delta <= 0) return null;

                return {
                    amount: (delta),
                    currency: vehicle.sellingStatus.currentPrice.currency
                };
            };
        })
        .filter('imageUrl', ['vehicleImages', function (vehicleImages) {
            return function (input, size, overlay) {
                var dimensions = (size || '').toString();
                var width = dimensions;
                var height = "";
                if (dimensions.indexOf("x") >= 0) {
                    var parts = dimensions.split('x');
                    width = parts[0];
                    height = parts.length > 1 ? parts[1] : "";
                }

                if (!input)
                    return "";

                if (input.container) {
                    var options;
                    if (!width && !height) {
                        options = '-';
                    } else {
                        options = 'c_lpad,b_white';
                        if (width) options += ',w_' + width;
                        if (height) options += ',h_' + height;
                    }

                    if (overlay && vehicleImages.overlays[overlay] && vehicleImages.missingImage != input)
                        return vehicleImages.cloudFilesProxyUrl + input.container + '/' + options + '/' + vehicleImages.overlays[overlay] + '/' + input.name;

                    return vehicleImages.cloudFilesProxyUrl + input.container + '/' + options + '/' + input.name;
                }

                var url = input.url;
                if (url.indexOf("?") >= 0)
                    return url + "&width=" + width + "&height=" + height;

                return url + "?width=" + width + "&height=" + height;
            };
        }])
        .filter('listingUrl', ['$filter', function ($filter) {
            var slugifyFilter = $filter('slugify');

            return function (input) {
                if (!input) return "";

                var id = (input.listingId || "").toString().replace('lots/', '');

                return '/vehicles/' + id + '/' + slugifyFilter(input.title || '');
            };
        }])
        .filter('searchUrl', ['siteUrls', function (siteUrls) {
            return function (input) {
                var isIeVersionLessThan = function (v) {
                    var ieRegex = /MSIE \d+\.\d+/;
                    var matches = navigator.userAgent.match(ieRegex);
                    if (matches && matches.length > 0) {
                        var match = matches[0];
                        var version = parseFloat(match.substring(5, match.length));
                        return version < v;
                    }
                    return false;
                };

                var rootUrl = isIeVersionLessThan(10)
                    ? 'search' + '/#' + siteUrls.path('search?')
                    : 'search?';

                return siteUrls.path(rootUrl + ($.param(input).replace(/\+/g, ' ')));
            };
        }])
        .filter('rangeFilterToLabel', ['$log', '$filter',
            function ($log, $filter) {
                var number = $filter('number');

                return function (range, options) {
                    options = options || {};
                    if (!range)
                        return range;
                    if (range.length == 0) return range;

                    return range.replace('[NULL', 'Up to ').replace(' TO ', '').replace('NULL]', ' and over]').replace(']', '').replace(/(\d+)/gm, function (str) { return number(str) + (options.valuePostFix || ''); }).replace(/[ILFD]x/g, '').replace('[', '');
                };
            }])
        .directive('priceVatStatus', function () {
            return function (scope, element, attrs) {
                scope.$watch(attrs.priceVatStatus, function (value) {
                    if (!angular.isUndefined(value)) {
                        //element.text(value ? '' : '+VAT');
                    }
                });
            };
        })
        .directive('vehicleImage', ['$filter', 'vehicleImages', function ($filter, vehicleImages) {
            var imageUrlFilter = $filter('imageUrl');
            var defaultDimensions = '640x480';

            return {
                restrict: 'E',
                replace: true,
                scope: { image: '=', dimensions: '@', overlay: '@' },
                template: '<img ng-src="{{url}}" class="img-responsive" />',
                link: function (scope, elem, attrs) {

                    scope.$watch('image', function (value) {
                        scope.url = imageUrlFilter(value || vehicleImages.missingImage, attrs.dimensions || defaultDimensions, attrs.overlay);
                    });
                }
            };
        }]);
}());