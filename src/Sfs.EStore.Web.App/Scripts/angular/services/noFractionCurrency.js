/// <reference path="~/angular"/>
/// <reference path="./sfs-vehicle-services.js"/>

(function () {

    angular.module('urlFilters')
        .filter('noFractionCurrency',
            ['$filter', '$locale',
                function (filter, locale) {
                    var currencyFilter = filter('currency');
                    var formats = locale.NUMBER_FORMATS;
                    return function (amount, currencySymbol) {
                        var value = currencyFilter(amount, '\u20AC');
                        var sep = value.indexOf(formats.DECIMAL_SEP);
                        if (amount >= 0) {
                            return value.substring(0, sep);
                        }
                        return value.substring(0, sep) + ')';
                    };
                } ])
        .filter('money',
            ['$filter', '$locale',
                function (filter, locale) {
                    var number = filter('number');
                    return function (money, dp) {
                        if (!money) return "";
                        
                        var symbol;
                        var pre = "";
                        var suf = "";
                        switch (money.currency) {
                            case "GBP":
                                symbol = "\u00a3";
                                pre = "\u00a4";
                                break;
                            case "EUR":
                                symbol = "\u20ac";
                                suf = "\u00a0\u00a4";
                                break;
                            default:
                                symbol = money.currency;
                                suf = "\u00a0\u00a4";
                        }

                        return pre.replace('\u00a4', symbol) + number(money.amount, (dp || 0)) + suf.replace('\u00a4', symbol);
                    };
                } ]);
} ());
