/// <reference path="./index.js"/>

(function() {
  'use strict'

  angular.module('app.components')
    .filter('camelCaseToWords', [function() {
      return function(input) {
        return (input || '').replace(/(.+?)([A-Z])/g, '$1 $2');
      }
    }]);
})();