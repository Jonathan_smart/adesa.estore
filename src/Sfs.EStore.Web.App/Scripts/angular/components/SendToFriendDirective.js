/// <reference path="./index.js"/>

(function() {
    'use strict';

  angular.module('app.components')
    .directive('sendToFriend', ['$modal', 'Angularytics', 'siteUrls', function($modal, Angularytics, siteUrls) {
      var modalController = function($scope, $http, $modalInstance, listing) {
        $scope.listing = listing;
        $scope.submit = function(request) {

          $scope.submitting = true;
          $scope.error = false;

          $http.post(siteUrls.path('api/listings/'+listing.listingId+'/sendtofriend'), request || {})
          .then(function success(res) {
            $scope.submitted = true;
          }, function failure(res) {
            $scope.error = true;
          })['finally'](function() { $scope.submitting = false; });
        };
      };

      function link(scope, elem, attrs) {
        var listing;
        
        scope.$watch(attrs.sendToFriend, function(value) {
          if(angular.isObject(value))
            listing = value;
          else 
            listing = {listingId: value};
        });

        elem.bind('click', function() {
          Angularytics.trackEvent('Send to friend click', listing.vehicleInfo.vehicleType, listing.listingId);

          $modal.open({
              dialogFade: true,
              dialogClass: 'modal',
              backdrop: 'static',
              templateUrl: '/templates/send-to-friend-modal',
              controller: ['$scope', '$http', '$modalInstance', 'listing', modalController],
              resolve: {
                  listing: function () { return listing; }
              }
          });
        });
      };

      return {
        restrict: 'A',
        link: link
      }
    }]);
})();