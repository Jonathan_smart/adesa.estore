/// <reference path="./index.js"/>

(function() {
  'use strict'
  
  angular.module('app.components')
    .value('siteUrls', {
      path: function (path) {
        (path || (path = ''));
        if (path.indexOf('/') == 0)
          path = path.substr(1);

        var result = (window.__sfs.siteroot || '/') + path;
        return result;
      }
    });
})();