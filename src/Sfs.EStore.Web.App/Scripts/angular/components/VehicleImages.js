/// <reference path="./index.js"/>

(function() {
  'use strict'

  angular.module('app.components')
    .factory('vehicleImages', [function () {
      var global = window.__sfs.vehicleImages;
      return {
        cloudFilesProxyUrl: global.cloudFilesProxyUrl,
        overlays: {
          results: global.overlays.results,
          thumb: global.overlays.thumb,
          full: global.overlays.full
        },
        missingImage: global.missingImage
      };
    }]);
})();