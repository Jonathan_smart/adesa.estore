﻿(function(angular) {
    angular.module('app.Run.Setup', [])
        .run(['$rootScope', '$window', '$location', 'sfsAuthService', 'sfsLoginOnRequest', function ($rootScope, $window, $location, sfsAuthService, sfsLoginOnRequest) {
            $rootScope.appVersion = "0.0.0.0";
            $rootScope.auth = sfsAuthService;
            $rootScope.selectedVehicles = [];

            $rootScope.$on('$viewContentLoaded', function (event) {
                if ($window._gaq)
                   // $window._gaq.push(['_trackPageview', $location.url()]);
                    $window.ga('send', 'pageview', { page: $location.url() });
            });
        }]);
}).call(this, angular);