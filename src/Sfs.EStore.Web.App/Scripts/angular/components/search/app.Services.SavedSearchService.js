﻿(function (angular) {
    "use strict";

    var savedSearchesService = function ($q, $http, $log, siteUrls) {
        var apiUrl = function (path) {
            return siteUrls.path('api/' + (path || ''));
        };

        return {
            find: function () {
                var deferred = $q.defer();

                $http.get(apiUrl('saved-searches'))
                    .then(function (res) {
                        deferred.resolve(res.data);
                    }, function (res) {
                        $log.error(res);
                        deferred.reject();
                    });

                return deferred.promise;
            },
            add: function (search) {
                var deferred = $q.defer();

                $http.post(apiUrl('saved-searches'), search)
                    .then(function (res) {
                        deferred.resolve(res.data);
                    }, function (res) {
                        $log.error(res);
                        deferred.reject();
                    });

                return deferred.promise;
            },
            update: function (search) {
                var deferred = $q.defer();
                $http.post(apiUrl('saved-searches/' + search.id), search)
                    .then(function (res) {
                        deferred.resolve(res.data);
                    }, function (res) {
                        $log.error(res);
                        deferred.reject();
                    });

                return deferred.promise;
            },
            remove: function (search) {
                var deferred = $q.defer();

                $http({ method: 'POST', url: apiUrl('saved-searches/' + search.id + '/destroy') })
                    .then(function () {
                        deferred.resolve();
                    }, function (res) {
                        $log.error(res);
                        deferred.reject();
                    });

                return deferred.promise;
            }
        };
    };

    angular.module("app.Services.SavedSearchService", [])
        .factory('savedSearchesService', ['$q', '$http', '$log', 'siteUrls', savedSearchesService]);
}).call(this, angular);
