﻿(function(angular) {
    angular.module('app.Config.Html5Mode', [])
        .config(['$locationProvider', function ($locationProvider) {
            var isIeVersionLessThan = function (v) {
                var ieRegex = /MSIE \d+\.\d+/;
                var matches = navigator.userAgent.match(ieRegex);
                if (matches && matches.length > 0) {
                    var match = matches[0];
                    var version = parseFloat(match.substring(5, match.length));
                    return version < v;
                }
                return false;
            };

            $locationProvider.html5Mode(!isIeVersionLessThan(10));
        }]);
}).call(this, angular);