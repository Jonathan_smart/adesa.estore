﻿(function (angular) {
    "use strict";

    var ListingSearchController = function ($scope, $log, $http, $route, $location, Angularytics, searchResultsViewTypeService, savedSearchesDialog) {

        var payload = $location.search();
        payload.orderProp = $scope.sortBy = payload.orderProp || '';
        $scope.savedSearches = savedSearchesDialog;

        (function initialiseQueryParams() {
            var queryParams = angular.copy($location.search());

            if (queryParams.hasOwnProperty('currentPage')) delete queryParams.currentPage;
            if (queryParams.hasOwnProperty('orderProp')) delete queryParams.orderProp;

            var preFilters = $scope.preFilters || {};

            if (preFilters.defaults.vehicleType && !queryParams.vehicleType)
                queryParams.vehicleType = preFilters.defaults.vehicleType;

            if (preFilters.fixed.vehicleType !== undefined)
                queryParams.vehicleType = preFilters.fixed.vehicleType;
            if (preFilters.fixed.make !== undefined)
                queryParams.make = preFilters.fixed.make;
            if (preFilters.fixed.model !== undefined)
                queryParams.model = preFilters.fixed.model;
            if (preFilters.fixed.ltAuction !== undefined)
                queryParams.ltauction = preFilters.fixed.ltAuction;
            if (preFilters.fixed.lpreview !== undefined)
                queryParams.lpreview = preFilters.fixed.lpreview;
            if (preFilters.fixed.ltBin !== undefined)
                queryParams.ltbin = (preFilters.fixed.ltBin == 'True') ? 1 : 0;
            if (preFilters.fixed.makes !== undefined)
                queryParams.makes = preFilters.fixed.makes;

            $scope.queryParams = queryParams;
        })();

        $scope.currentPage = parseInt(payload.currentPage || 1) || 1;

        $scope.$on('currentPage', function (event, currentPage) {
            payload.currentPage = currentPage;
            gotoSearch();
        });

        $scope.$on('preferredViewType', function (event, preferredView) {
            searchResultsViewTypeService.set(preferredView.value, preferredView.key);
            $route.reload();
        });

        $scope.$on('sortBy', function (event, sortBy) {
            payload.currentPage = 1;
            payload.orderProp = sortBy;
            gotoSearch();
        });

        $scope.$on('queryParametersChanged', function (event, args) {
            $scope.queryIsDirty = true;
        });

        $scope.$on('queryParamsReset', function (event, params) {
            applyQueryParams(params);
        });

        $scope.$on('applySearchQuery', function (event, params) {
            applyQueryParams(params);
        });

        var applyQueryParams = function (params) {
            var soryBy = payload.orderProp;

            payload = params;
            if (soryBy) payload.orderProp = soryBy;

            gotoSearch();
        }

        var gotoSearch = function () {
            $location.search(payload);
            $scope.queryIsDirty = false;
        };
    };

    angular.module("app.ListingSearchController", [])
        .controller('ListingSearchController', ['$scope', '$log', '$http', '$route', '$location', 'Angularytics', 'searchResultsViewType', 'savedSearchesDialog', ListingSearchController]);
}).call(this, angular);
