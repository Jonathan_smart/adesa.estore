﻿(function (angular) {
    "use strict";

    var LotCarController = function ($scope, $rootScope) {

        $scope.selectVehicle = function () {         
            if ($rootScope.selectedVehicles.indexOf($scope.vehicle.vehicleInfo.registration.plate) == -1) {
                $rootScope.selectedVehicles.push($scope.vehicle.vehicleInfo.registration.plate);
            } else {
                var index = $rootScope.selectedVehicles.indexOf($scope.vehicle.vehicleInfo.registration.plate);
                $rootScope.selectedVehicles.splice(index, 1);
            }
        };
    }
    angular.module("app.LotCarController", [])
        .controller('LotCarController', ['$scope', '$rootScope', LotCarController]);
}).call(this, angular);
