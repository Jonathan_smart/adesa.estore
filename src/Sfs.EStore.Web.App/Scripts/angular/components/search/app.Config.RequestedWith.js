﻿(function(angular) {
    angular.module('app.Config.RequestedWith', [])
        .config(['$httpProvider', function ($httpProvider) {
            $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
        }]);
}).call(this, angular);