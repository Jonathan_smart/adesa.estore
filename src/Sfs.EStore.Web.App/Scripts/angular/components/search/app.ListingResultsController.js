﻿(function (angular) {
    "use strict";

    var ListingResultsController = function ($scope, $element, $log, $http, $modal, searchResultsViewType, siteUrls, previousSearch) {

        $scope.viewType = searchResultsViewType.get($scope.preferredViewPersistKey);

        $scope.pagination = {
            take: 20,
            maxSize: 7,
            noOfPages: 0,
            totalCount: 999
        };

        $scope.sort = function () {
            $scope.$emit('sortBy', $scope.sortBy);
        };

        $scope.setPreferredView = function (type, key) {
            $scope.$emit('preferredViewType', { value: type, key: key });
        };

        $scope.openTransportCostModal = function () {

            var modalInstance = $modal.open({
                dialogFade: true,
                dialogClass: 'modal',
                backdrop: 'static',
                templateUrl: '/templates/_TransportCostMultiple-modal',
                controller: ['$scope', function ($scope) {
                    //$scope.customers = customers;
                    //$scope.shippingMethods = shippingMethods
                    //$scope.item = {
                    //    "customer": customers[0],
                    //};

                    //$scope.selectCustomer = function (item) {
                    //    alert("custoemr load");
                    //    if (customerNumber !== null)
                    //        transportService
                    //                 .shipToAddresses(item.customer.no)
                    //                 .then(function (response) {
                    //                     $scope.shipToAddresses = response.data;
                    //                 });
                    //};
                    //Events
                    //$scope.selectShippingMethod = function (item) {

                    //    transportService
                    //      .find(item.shippingMethod.code)
                    //          .then(function (response) {
                    //              $scope.shippingAgentServices = response.data;

                    //              if ($scope.shippingAgentServices[0] === undefined) {
                    //                  $scope.shippingAgentServices = undefined;
                    //                  $scope.shipToAddresses = undefined;
                    //                  $scope.transportCost = 0;
                    //                  return;
                    //              }

                    //              item.shippingAgentServices = response.data[0];

                    //              if (item.shippingAgentServices.length == 1) {
                    //                  $scope.shippingAgentService = item.shippingAgentServices[0];
                    //              }

                    //              transportService
                    //                .shipToAddresses(item.customer.no)
                    //                    .then(function (response) {
                    //                        $scope.shipToAddresses = response.data;
                    //                        item.address = response.data[0];
                    //                        updateTransportCosts(item);
                    //                    });
                    //          });
                    //};

                    //var updateTransportCosts = function (items) {
                    //    var transportdata = [];
                    //    var translateModel = {
                    //        "address": items.address,
                    //        "customer": items.customer,
                    //        "shippingAgentServices": items.shippingAgentServices,
                    //        "shippingMethod": items.shippingMethod,
                    //        "registrationNo": registrationNo

                    //    };
                    //    transportdata.push(translateModel);

                    //    var currentTransportP;
                    //    var thisTransportP;
                    //    thisTransportP = currentTransportP = transportService.quote({
                    //        items: transportdata.map(function (item) {
                    //            return {
                    //                id: "1",//Only used on Basket Page
                    //                shippingMethod: item.shippingMethod.code,
                    //                shippingAgent: (item.shippingAgentServices || {}).shippingAgentCode,
                    //                shippingAgentService: (item.shippingAgentServices || {}).code,
                    //                from: {
                    //                    vehicleRegistrationNo: item.registrationNo
                    //                },
                    //                to: {
                    //                    customerNo: item.customer.no,
                    //                    addressCode: (item.address || {}).code
                    //                }
                    //            };
                    //        })
                    //    }).then(function success(resp) {
                    //        $scope.transportCost = resp.data[0].cost;
                    //    }, function failure(resp) {
                    //        for (var i = 0; i < items.length; i++) {
                    //            $scope.transportCost = null;
                    //        }
                    //    });
                    //};

                    $scope.close = function () {
                        modalInstance.close();
                    };

                }],
                //resolve: {
                //    customers: function () {
                //        return transportService
                //               .customer()
                //               .then(function (response) {
                //                   return response.data;
                //               });
                //    },
                //    shippingMethods: function () {
                //        return transportService
                //            .shippingMethods()
                //            .then(function (response) {
                //                return response.data;
                //            });
                //    },
                //    registrationNo: function () {
                //        return $scope.registrationNo;
                //    }
                //}
            });

        };

        $scope.$watch('currentPage', function (newValue, oldValue) {
            if (newValue !== oldValue && !$scope.queryIsDirty) {
                $scope.$emit('currentPage', newValue);
            }
        });

        (function getListingEquipments() {
            if (!$scope.listingEquipmentTake && !parseInt($scope.listingEquipmentTake)) return;
            $scope.equipments = {};
            $scope.$on('new-search-results', function (ev, newListings) {
                $scope.equipments = {};
                var req = {
                    url: siteUrls.path('api/listings/vehicle-equipment'),
                    method: 'GET',
                    params: {
                        limit: $scope.listingEquipmentTake,
                        id: newListings.map(function (l) {
                            return l.listingId;
                        })
                    }
                };
                $http(req).then(function success(resp) {
                    var results = resp.data;
                    var equipments = {};
                    results.forEach(function (vehicle) {
                        equipments[vehicle.listingId] = vehicle.equipment.items;
                    });
                    $scope.equipments = equipments;
                });
            });
        })();

        (function getListings() {
            $scope.isBusy = true;
            $scope.listings = [];
            var params = angular.extend({
                top: $scope.pagination.take,
                skip: ($scope.currentPage - 1) * $scope.pagination.take,
                sortby: $scope.sortBy || '',
                currentPage: $scope.currentPage
            }, $scope.queryParams);

            if (params.sortby === 'undefined') params.sortby = '';

            $log.log('Search on: %o', params);

            $http.post(siteUrls.path('api/lots'), params)
                .then(function (res) {
                    previousSearch.set(params);
                    $scope.pagination.totalCount = res.headers("X-TotalResults");
                    $scope.pagination.noOfPages = Math.ceil($scope.pagination.totalCount / $scope.pagination.take);

                    $scope.listings = res.data.map(function (vehicle) {
                        if (vehicle.currentPrice && vehicle.valuations && vehicle.valuations.retail && vehicle.valuations.retail.amount > vehicle.currentPrice.amount) {
                            vehicle.saving = {
                                amount: vehicle.valuations.retail.amount - vehicle.currentPrice.amount,
                                currency: vehicle.currentPrice.currency
                            };
                        }
                        return vehicle;
                    });
                }, function (ex) {
                    $log.error(ex);
                })['finally'](function () {
                    $scope.isBusy = false;
                    var newListings = $scope.listings;
                    $scope.$broadcast('new-search-results', newListings);
                });
        })();
    };

    angular.module("app.ListingResultsController", [])
        .controller('ListingResultsController', ['$scope', '$element', '$log', '$http','$modal', 'searchResultsViewType', 'siteUrls', 'previousSearch', ListingResultsController]);
}).call(this, angular);
