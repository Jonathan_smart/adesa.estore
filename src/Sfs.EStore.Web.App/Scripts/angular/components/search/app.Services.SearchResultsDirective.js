﻿/// <reference path="~/angular/cookies"/>

(function (angular) {
    "use strict";

    angular.module("app.Services.SearchResultsDirective", [])
        .directive('lotSearchResults', ['$templateCache', '$cookies', function ($templateCache, $cookies) {

            var cardTemplate = 'lot-search-card-view.html';
            var tableTemplate = 'lot-search-table-view.html';

            return {
                restrict: 'EA',
                replace: false,
                template: '<ng-include src="getTemplateUrl()" />',
                controller: ['$scope', function($scope) {
                  $scope.getTemplateUrl = function() {
                    switch ($scope.view) {
                        case 'table':
                            return tableTemplate;
                        default:
                            return cardTemplate;
                    }
                  };
                }],
                scope: {
                    view: '=',
                    vehicles: '=vehicles',
                    equipments: '=vehicleEquipments'
                }
            };
        }]);

    angular.module('app.Services.Transport',[])
       .service('transportService', function () {

           function transportService($http, siteUrls) {
               this.customer = function () {
                   return $http.get(siteUrls.path('api/customers'));
               }
               this.shippingMethods = function () {
                   return $http.get(siteUrls.path('api/shippingmethods'));
               }
               this.shipToAddresses = function (customerNo) {
                   if (!customerNo) return;
                   return $http.get(siteUrls.path('api/customers/' + customerNo + '/ship-to-addresses?type=vehicle'));
               }
               this.find = function (shippingMethod) {
                   return $http({
                       method: 'GET',
                       url: siteUrls.path('api/shippingservicesview'),
                       params: {
                           method: shippingMethod
                       }
                   })
               };

               this.quote = function (data) {
                   return $http({
                       method: 'POST',
                       url: siteUrls.path('api/transport'),
                       data: data
                   });
               };
           };
       });
}).call(this, angular);
