﻿(function (angular) {
    "use strict";

    var DialogController = function ($scope, savedSearchesService, dialog, data) {
        $scope.newSearch = data.newSearch;

        $scope.close = function () {
            dialog.close();
        };

        var querySavedSearches = function () {
            savedSearchesService.find().then(function (res) {
                $scope.list = res;
            });
        };

        querySavedSearches();

        $scope.createNewSearch = function (params) {
            $scope.newSearch = {
                criteria: params
            };
        };

        $scope.saveNewSearch = function (newSearch) {
            savedSearchesService.add(newSearch).then(function (res) {
                $scope.list.push(res);
                $scope.newSearch = undefined;
            });
        };

        $scope.saveSearch = function (search) {
            return savedSearchesService.update(search);
        };

        $scope.renameSearch = function (search) {
            search._renaming = true;
        };
        $scope.saveSearchName = function (search) {
            $scope.saveSearch(search)
                .then(function() {
                    search._renaming = false;
                });
        };
        $scope.deleteSearch = function (search) {
            savedSearchesService.remove(search)
                .then(function () {
                    $scope.list.splice($scope.list.indexOf(search), 1);
                });
        };
    };

    var SavedSearchesDialog = function ($log, $modal) {

        var createDialog = function (data) {
            return $modal.open({
                dialogFade: true,
                dialogClass: 'modal modal-saveSearches',
                size: 'lg',
                backdrop: true,
                keyboard: true,
                templateUrl: 'savedsearches.html',
                controller: ['$scope', 'savedSearchesService', '$modalInstance', 'data', DialogController],
                resolve: {
                    data: function () {
                        return data || {};
                    }
                }
            });
        };

        return {
            show: function () {
                return createDialog();
            },
            create: function (search) {
                return createDialog({
                    newSearch: search
                });
            }
        };
    };

    angular.module('app.Services.SavedSearches', [])
        .factory('savedSearchesDialog', ['$log', '$modal', SavedSearchesDialog])
        .filter('criterionTermToLabel', ['$log',
            function ($log) {
                return function (input) {
                    return input.replace(/([A-Z])/g, ' $1')
                        .replace(/^./, function (str) { return str.toUpperCase(); });
                };
            } ])
        .filter('criterionValueToLabel', ['$log', '$filter',
            function ($log, $filter) {
                var isNumber = function (n) {
                    return !isNaN(parseFloat(n)) && isFinite(n);
                };
                var number = $filter('number');
                var valueToLabelFormatter = $filter('valueToLabelFormatter');
                var rangeLabel = $filter('rangeFilterToLabel');
                return function (input) {
                    if (isNumber(input))
                        return number(input);

                    if (input.indexOf('[') == 0)
                        return rangeLabel(input);

                    return valueToLabelFormatter(input);
                };
            } ]);
}).call(this, angular);
