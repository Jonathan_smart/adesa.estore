/// <reference path="~/angular/angularytics"/>

(function() {
  'use strict'

  angular.module('app.components', ['angularytics']);
})();