﻿/// <reference path="~/angular"/>
/// <reference path="./app.js"/>

(function () {
    "use strict";

    var QuickSearchController = function ($scope, $location, $window, $http, $filter, searchParamValueFormatters) {
        $scope.searchParamValueFormatters = searchParamValueFormatters;
        $scope.busy = false;
        $scope.search = {
            isAdvanced: false
        };

        (function initialiseQueryParams() {

            var queryParams = angular.copy($location.search());

            if (queryParams.hasOwnProperty('currentPage')) delete queryParams.currentPage;
            if (queryParams.hasOwnProperty('orderProp')) delete queryParams.orderProp;

            var preFilters = $scope.preFilters || {};
            if (preFilters.defaults.vehicleType && !queryParams.vehicleType)
                queryParams.vehicleType = preFilters.defaults.vehicleType;

            if (preFilters.fixed.HideAdvanced !== undefined) {
                $scope.search = {
                    isAdvanced: true
                };
            }

            if (preFilters.fixed.vehicleType !== undefined)
                queryParams.vehicleType = preFilters.fixed.vehicleType;
            if (preFilters.fixed.make !== undefined)
                queryParams.make = preFilters.fixed.make;
            if (preFilters.fixed.model !== undefined)
                queryParams.model = preFilters.fixed.model;
            if (preFilters.fixed.ltAuction !== undefined)
                queryParams.ltauction = (preFilters.fixed.ltAuction == 'True') ? 1 : 0;
            if (preFilters.fixed.ltBin !== undefined)
                queryParams.ltbin = (preFilters.fixed.ltBin == 'True') ? 1 : 0;
            if (preFilters.fixed.makes !== undefined)
                queryParams.makes = preFilters.fixed.makes;

            $scope.queryParams = queryParams;
        })();

        $scope.$on('applySearchQuery', function (event, params) {
            runSearch(params);
        });

        var runSearch = function (params) {
            $scope.busy = true;

            $location.search(params);
            var searchUrl = $filter('searchUrl')(params);
            $window.location.href = searchUrl;
        };
    };

    angular.module("app.Controllers")
        .controller('QuickSearchController', ['$scope', '$location', '$window', '$http', '$filter', 'searchParamValueFormatters', QuickSearchController]);
}());