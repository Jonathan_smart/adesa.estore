﻿/// <reference path="./app.js"/>

(function () {
    "use strict";

    var loginController = function ($scope, $timeout, sfsAuthService) {
        $scope.attempted = false;
        $scope.success = false;

        $scope.setRedirectUrl = function (url) {
            $scope.redirectUrl = url;
        };

        var flashPromise = undefined;

        $scope.submit = function () {
            $scope.attempted = false;
            $timeout.cancel(flashPromise);
            sfsAuthService.login($scope.username, $scope.password,
                {
                    success: function (user) {
                        if ($scope.redirectUrl)
                            window.location = $scope.redirectUrl;
                        else {
                            window.location.reload();
                        }
                        $scope.attempted =
                            $scope.success = true;
                    },
                    error: function (res) {
                        $scope.attempted = true;
                        $scope.success = false;
                        flashPromise = $timeout(function () {
                            $scope.attempted = false;
                        }, 3000);
                    }
                });
        };
    };

    angular.module('app.Controllers')
        .controller('LoginController', ['$scope', '$timeout', 'sfsAuthService', loginController]);
} ());