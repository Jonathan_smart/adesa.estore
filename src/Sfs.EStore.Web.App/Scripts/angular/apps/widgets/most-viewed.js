﻿/// <reference path="./app.js"/>

(function () {
    var MostViewedCtrl = function ($scope, $http, siteUrls) {
        $scope.mostviewedlots = [];

        $scope.getMostViewed = function (top) {
            $http.post(siteUrls.path('api/mostviewed'),
                {
                    top: top,
                    viewingage: '3.00:00'
                })
                .then(function (res) {
                    $scope.mostviewedlots = res.data.map(function (vehicle) {
                        if (vehicle.currentPrice && vehicle.valuations && vehicle.valuations.retail && vehicle.valuations.retail.amount > vehicle.currentPrice.amount) {
                            vehicle.saving = {
                                amount: vehicle.valuations.retail.amount - vehicle.currentPrice.amount,
                                currency: vehicle.currentPrice.currency
                            };
                        }
                        return vehicle;
                    });
                });
        };
    };
    angular.module('app.Controllers')
        .controller('MostViewedCtrl', ['$scope', '$http', 'siteUrls', MostViewedCtrl]);
} ());