﻿/// <reference path="./app.js"/>

(function () {
    "use strict";

    var DealerController = function ($rootScope, $scope, $location, $window, $http) {
        $scope.dealers = [];
        $scope.links = [];

        $http.get('dealer/Get')
            .success(function (data) {
                $scope.dealers = data;
                $http.get('dealer/getDealer', { params: $scope.dealers[0] })
                    .success(function (childData) {
                        $scope.links = childData;
                        $scope.selectedDealer = getById($scope.dealers, $scope.dealers[0].No);
                    });
            });



        function getById(arr, id) {
            for (var d = 0, len = arr.length; d < len; d += 1) {
                if (arr[d].No === id) {
                    return arr[d];
                }
            }
        }

        $scope.GetValue = function () {

            $http.get('dealer/Get')
                .success(function (data) {
                    $scope.dealers = data;
                });
            var dealer = { "no": $scope.selectedDealer };
            $http.get('dealer/getDealer', { params: dealer })
                .success(function (data) {
                    $scope.links = data;
                    $scope.selectedDealer = getById($scope.dealers, $scope.selectedDealer);
                });

        };


        $scope.Submit = function (brand) {

            $scope.selectedDealer.Marquee = brand;

            $http.post('dealer/Setup/', { dealer: $scope.selectedDealer })
                .success(function (data) {
                    var url = window.location.href;
                    var arr = url.split("/");
                    var result = arr[0] + "//" + arr[2];

                    window.location.href = result + '/retail/Markup/';
                });
        }
    };

    angular.module("app.Controllers")
        .controller('DealerController', ['$rootScope', '$scope', '$location', '$window', '$http', DealerController]);

}());