/// <reference path="./app.js"/>

(function() {
    'use strict';

    var Controller = function($scope, $http, siteUrls) {
        $scope.working = true;
        $scope.error = false;

        $http.get(siteUrls.path('api/customersfinance'))
            .then(function success(res) {
                $scope.availableFinance = res.data;
            }, function failure(res) {
                $scope.error = true;
            })['finally'](function() { $scope.working = false; });
    };

    angular.module("app.Controllers")
        .controller('AvailableFinanceController', ['$scope', '$http', 'siteUrls', Controller]);
})();