/// <reference path="./app.js"/>

(function() {
  'use strict'

  var Controller = function($scope, $http, siteUrls) {
    $scope.submit = function(request) {
      $scope.submitting = true;
      $scope.error = false;

      $http.post(siteUrls.path('api/bookaservice'), request || {})
      .then(function success(res) {
        $scope.submitted = true;
      }, function failure(res) {
        $scope.error = true;
      })['finally'](function() { $scope.submitting = false; });
    };
  };

  angular.module("app.Controllers")
    .controller('BookAServiceController', ['$scope', '$http', 'siteUrls', Controller]);
})();