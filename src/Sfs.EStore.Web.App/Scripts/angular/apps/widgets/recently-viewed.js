﻿/// <reference path="~/angular"/>
/// <reference path="./app.js"/>

(function () {
    var RecentlyViewedCtrl = function($scope, $http, siteUrls) {
        $scope.recentlyviewedlots = [];

        $scope.getRecentlyViewed = function(top) {
            $http.get(siteUrls.path('api/recentlyviewed'),
                {
                    params: {
                        top: top
                    }
                })
                .then(function(res) {
                    $scope.recentlyviewedlots = res.data;
                });
        };
        };

    angular.module('app.Controllers')
        .controller('RecentlyViewedCtrl', ['$scope', '$http', 'siteUrls', RecentlyViewedCtrl]);
} ());