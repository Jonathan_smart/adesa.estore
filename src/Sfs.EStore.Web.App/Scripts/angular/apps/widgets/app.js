﻿/// <reference path="~/angular"/>
/// <reference path="~/scripts/angular/services/"/>
/// <reference path="~/scripts/angular/services/searches"/>
/// <reference path="~/scripts/angular/services/ui-utils/ui-reset.js"/>
/// <reference path="~/angular/bootstrap"/>
/// <reference path="~/angular/cookies"/>
/// <reference path="~/angular/route"/>
/// <reference path="~/scripts/angular/components/"/>

(function () {
    "use strict";

    angular.module("app.Services", ['app.components', 'urlFilters', 'prettyDateFilters', 'sfs-auth-service', 'lotWatchModule',
                                       'lotsModule', 'ui.bootstrap', 'ui.reset', 'components.searches']);
    angular.module("app.Controllers", []);
    angular.module("app", ["app.Services", "app.Controllers", 'ngRoute', 'ngCookies']);

    var app = angular.module("app");

    app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    } ]);
} ());
