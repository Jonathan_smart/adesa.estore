﻿/// <reference path="./app.js"/>
/// <reference path="~/scripts/angular/services/"/>
(function (module) {
    var OrderHistoryController = function ($scope, $http) {

        $scope.orders = [];

        $scope.selectedImages = [];

        $scope.busy = true;

        $scope.click = function (Path) {

            if ($scope.selectedImages.indexOf(Path) < 0) {
                $scope.selectedImages.push(Path);
            } else {
                var index = $scope.selectedImages.indexOf(Path);
                $scope.selectedImages.splice(index, 1);
            }
        };

        $scope.next = function (a) {
            if (a > 5)
                return true;

            return false;
        };

        $http.get('api/Orders')
            .then(function (res) {
                $scope.orders = res.data;
                $scope.busy = false;
            }, function (res) {
                $scope.logger.error(res);
                $scope.busy = false;
                $scope.error = res;
            });
    };
    module.controller('OrderHistoryController', ['$scope', '$http', OrderHistoryController])
        .directive('saveSelected', function () {
            return {
                restrict: 'A',
                scope: {
                    order: '=',
                    paths: '='
                },
                template: '<button  ng-disabled="paths.length<1" id="save" type="button" class="btn btn-primary" ng-click="click(paths,order)">Download Selected</button><div data-ng-show="busy"><img src="/content/themes/fga/img/ajax-loader.gif"/> Downloading... </div>',
                controller: ['$scope', '$element', '$http',  function ($scope, $element, $http) {
                    $scope.selectedImages=[];
                    $scope.click = function (paths, order) {
                        $scope.busy = true;
   
                        var filePaths = [];
                        filePaths = paths.map(function (a) { return a.filePath; });

                        $http({
                            method: 'POST',
                            url:'api/Image',
                            data: { imagesPaths: filePaths },
                            responseType: 'arraybuffer'
                        }).then(function success(response) {
                            var blob = new Blob([response.data], { 'type': "application/octet-stream" });
                            var ua = navigator.userAgent;

                            if (ua == "MSIE 10" || ua == "MSIE 11") {
                                window.navigator.msSaveOrOpenBlob(blob, order + ".zip");
                            } else {
                                var a = document.createElement('a');
                                a.href = URL.createObjectURL(blob);
                                a.download = order + ".zip";
                                a.click();

                                $scope.busy = false;
                            }
                        });
                    }
                }
            ]};
        })
       .directive('saveImages', function () {
           return {
               restrict: 'A',
               scope: {
                   order: '=',
                   paths: '='
               },
               template: '<button id="save" type="button" class="btn btn-primary" ng-click="click(paths,order)">Download All</button><div data-ng-show="busy"><img src="/content/themes/fga/img/ajax-loader.gif"/> Downloading... </div>',
               controller: ['$scope', '$element', '$http',  function ($scope, $element, $http) {
                   $scope.click = function (paths, order) {

                       $scope.busy = true;

                       var filePaths = [];
                       filePaths = paths.map(function (a) { return a.filePath; });

                       $http({
                           method: 'POST',
                           url: 'api/Image',
                           data: { imagesPaths: filePaths },
                           responseType: 'arraybuffer'
                       }).then(function success(response) {
                           var blob = new Blob([response.data], { 'type': "application/octet-stream" });
                           var ua = navigator.userAgent;

                           if (ua == "MSIE 10" || ua == "MSIE 11") {
                               window.navigator.msSaveOrOpenBlob(blob, order + ".zip");
                           } else {
                               var a = document.createElement('a');
                               a.href = URL.createObjectURL(blob);
                               a.download = order + ".zip";
                               a.click();

                               $scope.busy = false;
                           }
                       });
                   }
               }
           ]};
       });
}(angular.module('app.Controllers')));