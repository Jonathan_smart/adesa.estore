﻿/// <reference path="~/angular"/>
/// <reference path="~/angular/route"/>
/// <reference path="~/angular/bootstrap"/>
/// <reference path="~/scripts/angular/services/"/>

(function () {
    "use strict";

    angular.module("app.Services", ['sfs-auth-service', 'ui.bootstrap']);
    angular.module("app.Controllers", []);

    angular.module("app", ["app.Services", "app.Controllers", 'ngRoute', "urlFilters", "prettyDateFilters"]);
    var app = angular.module('app');

    app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    } ]);

    app.config(['$routeProvider', '$locationProvider', 'siteUrlsProvider', function ($routeProvider, $locationProvider, siteUrlsProvider) {
        var siteUrls = siteUrlsProvider.$get();
        $routeProvider
            .when('/order-history', {
                templateUrl: 'order-history.html',
                controller: 'OrderHistoryController'
            })
            .otherwise({ redirectTo: '/order-history' });
    }]);

    app.run(['$rootScope', '$log', 'sfsAuthService', 'sfsLoginOnRequest', function ($rootScope, $log, sfsAuthService, sfsLoginOnRequest) {
        $rootScope.appVersion = "0.0.0.0";

        $rootScope.auth = sfsAuthService;
        $rootScope.logger = $log;
    } ]);
} ());