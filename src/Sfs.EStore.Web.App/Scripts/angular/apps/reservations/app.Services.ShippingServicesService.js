/// <reference path="./app.js"/>

(function (module) {
  module.service('ShippingServicesService', [
    '$http', 'siteUrls',
    function($http, siteUrls) {

      this.find = function(shippingMethod, basketEntryNo) {
        return $http({
          method: 'GET',
          url: siteUrls.path('api/shippingservices'),
          params: {
            method: shippingMethod,
            entryNo: basketEntryNo
          }
        });
      };
    }
  ]);
} (angular.module('app.Services')));