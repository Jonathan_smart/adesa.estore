﻿/// <reference path="~/angular"/>
/// <reference path="~/angular/route"/>
/// <reference path="~/angular/bootstrap"/>
/// <reference path="~/scripts/angular/services/"/>

(function () {
    "use strict";

    angular.module("app.Services", ['sfs-auth-service', 'ui.bootstrap']);
    angular.module("app.Controllers", []);

    angular.module("app", ["app.Services", "app.Controllers", 'ngRoute', "urlFilters", "prettyDateFilters"]);
    var app = angular.module('app');

    app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    }]);


    app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
        $routeProvider
            .when('/reservations', {
                templateUrl: 'basket.html',
                controller: 'ReservationsController',
                reloadOnSearch: false,
                resolve: reservationsControllerResolver
            })
            .when('/reservations/checkout', {
                templateUrl: 'order-checkout.html',
                controller: 'CheckoutOrderController',
                resolve: basketCheckoutOrderControllerResolver
            })
            .when('/reservations/checkout/complete', {
                templateUrl: 'order-checkout-success.html',
                controller: 'CheckoutOrderSuccessController',
                resolve: basketOrderSuccessControllerResolver
            })
            .otherwise({ redirectTo: '/reservations' });
    }]);

    var basketOrderSuccessControllerResolver = {
        orders: ['$http', 'siteUrls', '$location', function ($http, siteUrls, $location) {
            return $http.get(siteUrls.path('api/orders') + '?' + ($location.search().orderNumbers || '').split(',').map(function (i) {
                return 'ids=' + i;
            }).join('&')).then(function (res) {
                return res.data;
            });
        }],
        failedItems: ['$http', 'siteUrls', '$location', function ($http, siteUrls, $location) {
            return $http({
                method: 'GET',
                url: siteUrls.path('api/basket'),
                params: {
                    no: ($location.search().failedBaskets || '').split(',')
                }
            }).then(function (res) {
                return res.data;
            });
        }]
    };

    var reservationsControllerResolver = {
        selected: ['$location', function ($location) {
            var selectedSearchParam = $location.search().selected;
            if (!selectedSearchParam) return [];

            var selectedSearch = selectedSearchParam.toString();

            if (typeof (selectedSearch) === 'string')
                selectedSearch = selectedSearch.split(',');
            return selectedSearch.map(function (i) { return parseInt(i); });
        }],
        customers: ['siteUrls', '$http', function (siteUrls, $http) {
            return $http.get(siteUrls.path('api/customers'))
                .then(function (res) {
                    return res.data;
                });
        }]
    };

    var lotControllerResolver = {
        shippingMethods: ['siteUrls', '$q', '$http', function (siteUrls, $q, $http) {
            return $http.get(siteUrls.path('api/shippingmethods'))
                .then(function (res) {
                    return res.data;
                });
        }],
        shipToAddresses: ['siteUrls', '$http', '$location', function (siteUrls, $http, $location) {
            var customerNo = $location.search().customer;
            if (!customerNo) return;

            return $http.get(siteUrls.path('api/customers/' + customerNo + '/ship-to-addresses?type=vehicle'))
                .then(function (res) {
                    return res.data;
                });
        }],
        v5Addresses: ['siteUrls', '$http', '$location', function (siteUrls, $http, $location) {
            var customerNo = $location.search().customer;
            if (!customerNo) return;

            return $http.get(siteUrls.path('api/customers/' + customerNo + '/ship-to-addresses?type=v5'))
                .then(function (res) {
                    return res.data;
                });
        }]
    };

    var basketCheckoutOrderControllerResolver = {
        customerNo: ['$location', function ($location) {
            return $location.search().customer;
        }],
        shipToAddresses: ['siteUrls', '$http', '$location', function (siteUrls, $http, $location) {
            var customerNo = $location.search().customer;
            if (!customerNo) return;

            return $http.get(siteUrls.path('api/customers/' + customerNo + '/ship-to-addresses?type=vehicle'))
                .then(function (res) {
                    return res.data;
                });
        }],
        v5Addresses: ['siteUrls', '$http', '$location', function (siteUrls, $http, $location) {
            var customerNo = $location.search().customer;
            if (!customerNo) return;

            return $http.get(siteUrls.path('api/customers/' + customerNo + '/ship-to-addresses?type=v5'))
                .then(function (res) {
                    return res.data;
                });
        }],
        reservations: ['siteUrls', '$location', '$q', '$http', function (siteUrls, $location, $q, $http) {
            var selected = [];
            var customeNo = "";

            if ($location.search().customer) {
                customeNo = $location.search().customer.toString();
            }

            if ($location.search().selected) {
                var selectedSearch = $location.search().selected.toString();

                if (typeof (selectedSearch) === 'string')
                    selectedSearch = selectedSearch.split(',');
                selected = selectedSearch;
            }

            if (selected.length == 0) {
                return [];
            }

            return $http({
                method: 'GET',
                url: siteUrls.path('api/basket'),
                params: {
                    no: selected,
                    cusNo: customeNo
                }
            }).then(function (res) {
                return res.data;
            });
        }],
        shippingMethods: ['siteUrls', '$q', '$http', function (siteUrls, $q, $http) {
            return $http.get(siteUrls.path('api/shippingmethods'))
                .then(function (res) {
                    return res.data;
                });
        }]
    };

    app.run(['$rootScope', '$log', 'sfsAuthService', 'sfsLoginOnRequest', function ($rootScope, $log, sfsAuthService, sfsLoginOnRequest) {
        $rootScope.appVersion = "0.0.0.0";

        $rootScope.auth = sfsAuthService;
        $rootScope.logger = $log;
    }]);
}());
