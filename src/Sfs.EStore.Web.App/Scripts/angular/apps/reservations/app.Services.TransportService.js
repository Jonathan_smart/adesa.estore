/// <reference path="./app.js"/>

(function (module) {
  module.service('TransportService', [
    '$http', 'siteUrls',
    function($http, siteUrls) {
        this.quote = function (data) {
        return $http({
          method: 'POST',
          url: siteUrls.path('api/transport'),
          data: data
        });
      };
    }
  ]);
} (angular.module('app.Services')));