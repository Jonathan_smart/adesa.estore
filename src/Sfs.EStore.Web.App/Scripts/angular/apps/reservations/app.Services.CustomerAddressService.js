/// <reference path="./app.js"/>

(function (module) {
  module.service('CustomerAddressService', [
    '$http', 'siteUrls',
    function($http, siteUrls) {

      this.create = function(customerNo, newAddress) {
        return $http({
          method: 'POST',
          url: siteUrls.path('api/customers/'+customerNo+'/ship-to-addresses'),
          data: newAddress
        }).then(function success(res) {
          return res.data;
        });
      };
    }
  ]);
} (angular.module('app.Services')));