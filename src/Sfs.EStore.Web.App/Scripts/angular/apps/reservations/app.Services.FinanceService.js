/// <reference path="./app.js"/>

(function (module) {
  module.service('FinanceService', [
    '$http', 'siteUrls',
    function($http, siteUrls) {
      this.find = function() {
        return $http({
          method: 'GET',
          url: siteUrls.path('api/checkoutfinanceoptions')
        }).then(function success(resp) {
          return resp;
        });
      };
    }
  ]);

  module.service('CustomerFinanceService', [
    '$http', 'siteUrls',
    function($http, siteUrls) {
      this.available = function(customer) {
        return $http({
          method: 'GET',
          url: siteUrls.path('api/customersfinance/'+customer)
        }).then(function success(res) {
          return res.data;
        });
      };
    }
  ]);
} (angular.module('app.Services')));