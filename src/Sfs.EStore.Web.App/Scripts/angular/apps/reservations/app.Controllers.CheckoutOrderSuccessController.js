﻿/// <reference path="./app.js"/>

(function (module) {
    var Controller = function ($scope, orders, failedItems, $log) {
        $scope.orders = orders;
        $scope.failedItems = failedItems;
    };
    module.controller('CheckoutOrderSuccessController', ['$scope', 'orders', 'failedItems', '$log', Controller]);
} (angular.module('app.Controllers')));