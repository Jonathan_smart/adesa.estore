/// <reference path="./app.js"/>

(function (module) {
  module.service('CheckoutService', [
    '$http', 'siteUrls',
    function($http, siteUrls) {
      this.create = function(payload) {
        return $http({
          method: 'POST',
          url: siteUrls.path('api/orders'),
          data: payload
        });
      };
    }
  ]);
} (angular.module('app.Services')));
