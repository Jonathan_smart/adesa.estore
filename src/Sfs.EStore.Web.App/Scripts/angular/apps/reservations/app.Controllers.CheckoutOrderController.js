﻿/// <reference path="./app.js"/>

(function (module) {

    var FinanceManager = [
      '$log', 'sfsAuthService', 'customerNo', 'CustomerFinanceService', 'FinanceService',
      function FinanceManager($log, sfsAuthService, customerNo, CustomerFinanceService, FinanceService) {
          this.isValid = function () { return true; };
          this.enabled = false;

          if (!sfsAuthService.isAuthorized('/Customers/Finance')) return;

          var self = this;
          this.credit = [];

          this.availableOptions = function (option) {
              return self.enabled || !option.credit;
          };

          this.taken = function (creditType, items) {
              var financedItems = items
                .filter(function (item) {
                    return item.checkout.finance &&
                           item.checkout.finance.creditType == creditType;
                });
              var sum = financedItems.reduce(function (sum, item) {
                  return sum + item.total();
              }, 0);
              return sum;
          };

          this.isValid = function (items) {
              if ((this.credit.length === 0) &&
                  (items.some(function (item) { return !item.checkout.finance || item.checkout.finance.creditType.length > 0; })))
                  return false;

              return this.credit.reduce(function (valid, credit) {
                  var taken = self.taken(credit.creditType, items);
                  return valid && (taken === 0 || taken <= credit.available);
              }, true);
          };

          CustomerFinanceService.available(customerNo)
          .then(function (data) {
              self.credit = data.credit;
              self.enabled = true;
          }, function failure(resp) {
              self.error = true;
              $log.error(resp.data);
              if (resp.data)
                  self.message = resp.data.message;
          });

          FinanceService.find()
          .then(function success(result) {
              self.options = result.data;
          });
      }
    ];

    var CustomerAddressManager = [
      '$log', '$modal', 'CustomerAddressService', 'customerNo',
      function CustomerAddressManager($log, $modal, CustomerAddressService, customerNo) {
          this.addNewV5Address = function (callback) {
              $log.info('Adding new V5 address');
              addNewAddress(callback);
          };
          this.addNewShipToAddress = function (callback) {
              $log.info('Adding new V5 address');
              addNewAddress(callback);
          };

          function addNewAddress(callback) {
              (callback || (callback = function () { }));

              var modalInstance = $modal.open({
                  templateUrl: '/content/checkout/add-customer-address-modal.html',
                  resolve: {
                  },
                  backdrop: 'static',
                  controller: [
                   '$scope', '$modalInstance', function ($scope, $modalInstance) {
                       $scope.working = false;
                       $scope.save = function (address) {
                           $scope.working = true;
                           CustomerAddressService.create(customerNo, address)
                           .then(function success(savedAddress) {
                               $modalInstance.close(savedAddress);
                           }, function failure(resp) {
                               $scope.error = resp;
                           })['finally'](function () {
                               $scope.working = false;
                           });
                       };
                   }
                  ]
              });

              modalInstance.result.then(function done(address) {
                  callback(null, address);
              }, function dismissed(err) {
                  callback();
              });
          }
      }
    ];

    var Item = function () {
        this.checkout = {};
        this.requiresShippingAddress = function () {
            return this.checkout.shippingMethod && this.checkout.shippingMethod.requiresShippingInfo;
        };

        this.isValid = function () {
            return this.checkout.shippingMethod &&
              (!this.checkout.shippingMethod.requiresShippingInfo ||
                (this.checkout.address && this.checkout.shippingAgentService));
        };

        this.isReadyForSubmission = function () {
            return this.isValid() && this.checkout.transportCost !== null;
        };

        this.total = function () {
            return this.amount.amount + (this.checkout.transportCost || 0);
        };
    };

    var Controller = function ($controller, $scope, $http, $location, $modal, $log, TransportService, ShippingServicesService,
      CheckoutService, reservations, customerNo, shipToAddresses, v5Addresses, shippingMethods, siteUrls) {
        if (!reservations || reservations.length == 0) {
            $location.path('/reservations');
            return;
        }

        $scope.financeManager = $controller(FinanceManager, { customerNo: customerNo });
        $scope.addressManager = $controller(CustomerAddressManager, { customerNo: customerNo });

        $scope.items = reservations.map(function (x) {
            return angular.extend(new Item(), x);
        });

        $scope.newShipToAddressAddedCallbackFactory = function (item, property) {
            return function (err, newAddress) {
                if (err) {
                    $log.warn(err || 'Ship to address not added', err);
                    return;
                }

                if (!newAddress) return;

                $scope.shipToAddresses.push(newAddress);
                $scope.v5Addresses.push(newAddress);
                item.checkout['address'] = newAddress;
            };
        };
        $scope.newV5AddressAddedCallbackFactory = function (item, property) {
            return function (err, newAddress) {
                if (err) {
                    $log.warn(err || 'V5 address not added', err);
                    return;
                }

                if (!newAddress) return;

                $scope.shipToAddresses.push(newAddress);
                $scope.v5Addresses.push(newAddress);
                item.checkout['v5Address'] = newAddress;
            };
        };

        $scope.shippingMethods = shippingMethods;
        $scope.shipToAddresses = shipToAddresses;
        $scope.v5Addresses = v5Addresses;

        var refreshShippingServicesFor = function (item) {
            item.shippingAgentServices = undefined;
            item.checkout.shippingAgentService = undefined;
            ShippingServicesService.find(item.checkout.shippingMethod.code, item.no)
            .then(function (res) {
                item.shippingAgentServices = res.data;
                if (item.shippingAgentServices.length == 1)
                    item.checkout.shippingAgentService = item.shippingAgentServices[0];
            });
        };

        $scope.isReadyForSubmission = function (items) {
            var areValid = items.map(function (item) { return item.isReadyForSubmission(); });

            for (var i = areValid.length - 1; i >= 0; i--) {
                if (!areValid[i]) return false;
            }

            return $scope.financeManager.isValid(items);
        };

        $scope.totalPurchasePrice = function (items) {
            return $scope.totalVehiclePrice(items) + $scope.totalTransportCost(items);
        };

        $scope.totalbuyersFee = function (items) {
            var fee = items.map(function (x) { return x.buyersFee; });

            var total = 0;
            for (var i = fee.length - 1; i >= 0; i--) {
                total += fee[i];
            }
            return total;
        };
        $scope.totalVehiclePrice = function (items) {
            var costs = items.map(function (x) { return x.amount.amount; });

            var total = 0;

            for (var i = costs.length - 1; i >= 0; i--) {
                total += costs[i];
            }

            return total;
        };

        $scope.totalTransportCost = function (items) {
            var costs = items
            .map(function (x) { return x.checkout.transportCost; });

            var total = 0;

            for (var i = costs.length - 1; i >= 0; i--) {
                total += costs[i];
            }

            return total;
        };

        var currentTransportP;
        var updateTransportCosts = function (items) {
            var thisTransportP;
            thisTransportP = currentTransportP = TransportService.quote({
                items: items
                  .filter(function (x) { return x.isValid(); })
                  .map(function (item) {
                      return {
                          id: item.no,
                          shippingMethod: item.checkout.shippingMethod.code,
                          shippingAgent: (item.checkout.shippingAgentService || {}).shippingAgentCode,
                          shippingAgentService: (item.checkout.shippingAgentService || {}).code,
                          from: {
                              vehicleRegistrationNo: item.registrationPlate
                          },
                          to: {
                              customerNo: customerNo,
                              addressCode: (item.checkout.address || {}).code
                          }
                      };
                  }), WithVat: true
            }).then(function success(resp) {
                if (currentTransportP != thisTransportP)
                    return;
                var predicate = function (x) { return x.id == item.no; };
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    var transport = resp.data.filter(predicate)[0];

                    item.checkout.transportCost = (transport === undefined) ? null : transport.cost;
                }
            }, function failure(resp) {
                if (currentTransportP != thisTransportP)
                    return;
                for (var i = 0; i < items.length; i++) {
                    items[i].checkout.transportCost = null;
                }
            });
        };

        $scope.$watch('items', function (newValue) {
            updateTransportCosts($scope.items);
        }, true);

        $scope.checkout = function (items) {

            if (!$scope.isReadyForSubmission(items)) return;

            $scope.submitting = true;
            $scope.error = null;
            CheckoutService.create({
                items: items
                  .map(function (item) {
                      return {
                          financeRequired: !!item.checkout.finance,
                          finance: (!item.checkout.finance ? null
                            : {
                                service: item.checkout.finance.service,
                                method: item.checkout.finance.method,
                                agent: item.checkout.finance.agent
                            }
                          ),
                          transport: {
                              price: item.checkout.transportCost
                          },
                          shipping: {
                              method: item.checkout.shippingMethod.code,
                              agent: (item.checkout.shippingAgentService || {}).shippingAgentCode,
                              service: (item.checkout.shippingAgentService || {}).code,
                              addressCode: (item.checkout.address || {}).code
                          },
                          basket: {
                              entryno: item.no
                          },
                          customerNo: customerNo,
                          price: item.amount.amount,
                          buyersFee: item.buyersFee,
                          v5: {
                              shipToAddressCode: (item.checkout.v5Address || {}).code
                          }
                      };
                  })
            }).then(function success(resp) {
                $log.info(resp);
                $location.path('/reservations/checkout/complete')
                  .search({
                      orderNumbers: (resp.data.orderNos || []).join(','),
                      failedBaskets: (resp.data.failed || []).join(',')
                  });
            }, function failure(resp) {
                $scope.error = resp.data;
                $scope.submitting = false;
            })['finally'](function () {
            });
        };

        (function () {
            if (v5Addresses.length !== 1) return;

            $scope.items.forEach(function (i) {
                i.checkout.v5Address = v5Addresses[0];
            });
        })();

        (function () {
            if ($scope.shippingMethods.length == 0) return;

            var defaultShippingMethod = $scope.shippingMethods.filter(function (m) { return m.code === 'STD' })[0];
            if (defaultShippingMethod === undefined) return;
            $scope.items.forEach(function (i) {
                i.checkout.shippingMethod = defaultShippingMethod;
                refreshShippingServicesFor(i);
            });
        })();

        var itemListener = function (n, o) {
            if ((n.checkout.shippingMethod || {}).code != (o.checkout.shippingMethod || {}).code) {
                if (!n.requiresShippingAddress())
                    n.checkout.address = undefined;
                else if (!o.requiresShippingAddress() && shipToAddresses.length == 1)
                    n.checkout.address = shipToAddresses[0];

                refreshShippingServicesFor(n);
            }
        };

        var attachItemWatches = function () {
            $scope.items.forEach(function (i) {
                $scope.$watch(function () { return i; }, itemListener, true);
            });
        };

        attachItemWatches();
    };
    module.controller('CheckoutOrderController', ['$controller', '$scope', '$http', '$location', '$modal', '$log',
      'TransportService', 'ShippingServicesService', 'CheckoutService',
      'reservations', 'customerNo', 'shipToAddresses', 'v5Addresses', 'shippingMethods', 'siteUrls',
      Controller]);
}(angular.module('app.Controllers')));
