﻿/// <reference path="./app.js"/>

(function (module) {
    var Controller = function ($scope, $http, $location, $timeout, $log, siteUrls, customers, selected) {
        $scope.reservations = [];
        $scope.selected = selected;
        $scope.waitingForNextStep = false;

        $scope.customers = customers;
        if (customers.length === 1)
            $scope.checkoutCustomer = customers[0];

        var getReservations = function () {
            $scope.busy = true;
            $scope.error = undefined;
            $scope.reservations = [];
            $http.get(siteUrls.path('api/basket'), { params: { "No": [], "cusNo": "" } })
                .then(function (res) {
                    $scope.reservations = res.data || [];
                    removeNonBasketSelections();
                    $scope.busy = false;
                }, function (res) {
                    $scope.busy = false;
                    $log.error(res);
                    $scope.error = res;
                });
        };
        getReservations();

        var removeNonBasketSelections = function () {
            var entryNos = $scope.reservations.map(function (v) { return v.no; });
            var selectedIds = $scope.selected.slice(0);

            for (var i = selectedIds.length; i >= 0; i--) {
                var no = selectedIds[i];
                if (entryNos.indexOf(no) == -1) {
                    $scope.selected.splice(i, 1);
                }
            }

            $location.search({ selected: $scope.selected });
        };

        $scope.continueWithSelection = function (customer, items) {
            $scope.waitingForNextStep = true;
            $location.path('/reservations/checkout').search({
                customer: customer.no,
                selected: items
            });
        };

        $scope.selectAll = function ($event) {
            var checkbox = $event.target;
            for (var i = 0; i < $scope.reservations.length; i++) {
                var entry = $scope.reservations[i];
                updateSelected(checkbox.checked, entry.no);
            }
            $location.search({ selected: $scope.selected });
        };

        $scope.updateSelection = function ($event, entry) {
            var selected;
            if ($event) {
                selected = $event.target.checked;
            } else {
                selected = !$scope.isSelected(entry);
            }
            updateSelected(selected, entry.no);
            $location.search({ selected: $scope.selected });
        };

        $scope.isSelected = function (entry) {
            return $scope.selected.indexOf(entry.no) >= 0;
        };

        $scope.isSelectedAll = function () {
            return $scope.selected.length === $scope.reservations.length;
        };

        $scope.isSelectedAll = function () {
            return $scope.selected.length === $scope.reservations.length;
        };
        $scope.removeFromBasket = function (item) {
            $http({
                method: 'POST',
                url: siteUrls.path('api/basket/' + item.no + '/destroy')
            })
                .then(function (res) {
                    var index = $scope.reservations.indexOf(item);
                    $scope.reservations.splice(index, 1);
                    var indexSelected = $scope.selected.indexOf(item.no);
                    $scope.selected.splice(indexSelected, 1);

                    $scope.busy = false;
                }, function (res) {
                    $scope.busy = false;
                    $log.error(res);
                });
        };

        var updateSelected = function (select, basketEntryNo) {
            if (select && $scope.selected.indexOf(basketEntryNo) === -1) {
                $scope.selected.push(basketEntryNo);
            }
            if (!select && $scope.selected.indexOf(basketEntryNo) !== -1) {
                $scope.selected.splice($scope.selected.indexOf(basketEntryNo), 1);
            }
        };
    };
    module.controller('ReservationsController', ['$scope', '$http', '$location', '$timeout', '$log', 'siteUrls', 'customers', 'selected', Controller]);
}(angular.module('app.Controllers')));
