﻿/// <reference path="./app.js"/>

(function (module) {
    var Controller = function ($scope, $http, siteUrls, sfsAuthService) {
        $scope.params = {};

        if (sfsAuthService.isAuthenticated)
            $scope.params.contact = {
                fullName: (sfsAuthService.currentUser.firstName + ' ' + sfsAuthService.currentUser.lastName),
                email: sfsAuthService.currentUser.emailAddress,
                telephone: sfsAuthService.currentUser.phoneNumber
            };


        var getValuations = function () {
            $scope.working = true;
            $scope.previousValuations = [];
            $http.get(siteUrls.path('api/partex'))
                .then(function (res) {
                    $scope.previousValuations = res.data;
                    $scope.working = false;
                },
                    function () {
                        $scope.working = false;
                    });
        };

        $scope.deleteValuation = function (valuation) {
            $http({ method: 'DELETE', url: siteUrls.path('api/partex/' + valuation.id) })
                .then(function () {
                    var index = $scope.previousValuations.indexOf(valuation);
                    if (index > -1)
                        $scope.previousValuations.splice(index, 1);
                    $scope.working = false;
                }, function () {
                    $scope.working = false;
                });
        };

        $scope.startOver = function () {
            $scope.vehicle = undefined;
            $scope.newValuation = undefined;
            getValuations();
        };

        $scope.lookup = function (registration) {
            $scope.vehicle = undefined;
            $scope.noFound = undefined;
            $scope.working = true;

            $http.get(siteUrls.path('api/partex?registration=' + registration))
                .then(function (res) {
                    $scope.vehicle = res.data;
                    $scope.working = false;
                }, function () {
                    $scope.noFound = true;
                    $scope.working = false;
                });
        };

        $scope.confirmVehicle = function () {
            $scope.vehicle.confirmed = true;
        };

        $scope.getValuation = function (registration, mileage, contact) {
            $scope.working = true;

            //contact.referrerURL = $.cookie('referrer') ? $.cookie('referrer') : "";

            $http({
                url: siteUrls.path('api/partex?registration=' + registration + '&mileage=' + mileage),
                method: 'POST',
                data: {
                    contact: contact
                }
            })
                .then(function (res) {
                    $scope.newValuation = res.data;
                    $scope.working = false;

                    if ($.cookie('referrer'))
                        $.removeCookie('referrer');

                }, function () {
                    $scope.newValuation = { error: true };
                    $scope.working = false;
                });
        };

        $scope.clearValuation = function () {
            $scope.newValuation = undefined;
        };

        getValuations();
    };
    module.controller('PartExchangeController', ['$scope', '$http', 'siteUrls', 'sfsAuthService', Controller]);
}(angular.module('app.Controllers')));