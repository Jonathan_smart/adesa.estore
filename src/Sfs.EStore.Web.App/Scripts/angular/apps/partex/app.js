﻿/// <reference path="~/angular"/>
/// <reference path="~/angular/route"/>
/// <reference path="~/angular/bootstrap"/>
/// <reference path="~/scripts/angular/services/"/>
/// <reference path="~/angular/angularytics"/>

(function () {
    "use strict";

    angular.module("app.Services", ['sfs-auth-service', 'ui.bootstrap', 'angularytics']);
    angular.module("app.Controllers", []);

    angular.module("app", ["app.Services", "app.Controllers", 'ngRoute', "urlFilters", "prettyDateFilters"]);
    var app = angular.module('app');

    app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    } ])
    .config(['AngularyticsProvider', function(AngularyticsProvider) {
      AngularyticsProvider.setEventHandlers(['Console', 'GoogleUniversal']);
    }]).run(['Angularytics', function(Angularytics) {
      Angularytics.init();
    }]);

    app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
        //$locationProvider.html5Mode(true);
        $routeProvider
            .when('/', {
                templateUrl: 'index.html',
                controller: 'PartExchangeController'
            })
            .otherwise({ redirectTo: '/' });
    } ]);

    app.run(['$rootScope', 'sfsAuthService', 'sfsLoginOnRequest', function ($rootScope, sfsAuthService, sfsLoginOnRequest) {
        $rootScope.appVersion = "0.0.0.0";

        $rootScope.auth = sfsAuthService;
    } ]);
} ());