﻿/// <reference path="~/Scripts/angular/components/search"/>
/// <reference path="~/angular/angularytics"/>
/// <reference path="~/scripts/angular/services/"/>
/// <reference path="~/scripts/angular/services/searches"/>
/// <reference path="~/angular/bootstrap"/>
/// <reference path="~/angular/cookies"/>
/// <reference path="~/angular/route"/>

(function (angular) {
    "use strict";

    angular.module("app.Services", ['ui.bootstrap', 'prettyDateFilters', 'urlFilters', 'http-auth-interceptor', 'lotWatchModule', 'ngCookies',
        'components.searches',
        'app.Services.SavedSearches', 'app.Services.SavedSearchService',
        'app.Services.SearchResultsDirective', 'angularytics']);
    angular.module('app.Controllers', ['app.ListingResultsController', 'app.ListingSearchController']);
    angular.module('app.Config', ['app.Config.Html5Mode', 'app.Config.RequestedWith']);
    angular.module('app.Run', ['app.Run.Setup']);

    angular.module('app', ['app.Services', 'app.Controllers', 'app.Config', 'app.Run', 'ngRoute']);

    var app = angular.module("app");


    app.config(['AngularyticsProvider', function(AngularyticsProvider) {
      AngularyticsProvider.setEventHandlers(['Console', 'GoogleUniversal']);
    }]).run(['Angularytics', function(Angularytics) {
      Angularytics.init();
    }]);

    app.config(['$routeProvider', 'siteUrlsProvider', function ($routeProvider, siteUrlsProvider) {
        var searchRoute = {
            templateUrl: 'lot-search.html',
            controller: 'ListingSearchController'
        };

        var siteUrls = siteUrlsProvider.$get();

        $routeProvider
            .when(siteUrls.path('search'), searchRoute)
            .otherwise({ redirectTo: siteUrls.path('search') });
    }]);
}).call(this, angular);