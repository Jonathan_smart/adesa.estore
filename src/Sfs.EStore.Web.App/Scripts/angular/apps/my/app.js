﻿/// <reference path="~/angular"/>
/// <reference path="~/angular/route"/>
/// <reference path="~/angular/bootstrap"/>
/// <reference path="~/scripts/angular/services/"/>

(function () {
    "use strict";

    // TODO: auth modal dialog

    angular.module("app.Services", ['urlFilters', 'prettyDateFilters', 'ui.bootstrap', 'http-auth-interceptor', 'lotWatchModule']);
    angular.module("app.Controllers", []);

    angular.module("app", ['ngRoute', "app.Services", "app.Controllers"]);
    var app = angular.module('app');

    app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    } ]);

    app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
        //$locationProvider.html5Mode(true);
        $routeProvider
            .when('/watches', {
                templateUrl: 'watchlist.html',
                controller: 'WatchListController',
                reloadOnSearch: false
            })
            .when('/bidding', {
                templateUrl: 'bidding.html',
                controller: 'BiddingController'
            })
            .when('/didnt-win', {
                templateUrl: 'didnt-win-auctions.html',
                controller: 'DidntWinAuctionsController'
            })
            .when('/purchase-history', {
                templateUrl: 'purchase-history.html',
                controller: 'PurchaseHistoryController'
            })
            .otherwise({ redirectTo: '/watches' });
    } ]);

        app.run(["$rootScope", 'sfsAuthService', 'sfsLoginOnRequest', function ($rootScope, sfsAuthService, sfsLoginOnRequest) {
            $rootScope.appVersion = "0.0.0.0";
            $rootScope.auth = sfsAuthService;
    } ]);
} ());