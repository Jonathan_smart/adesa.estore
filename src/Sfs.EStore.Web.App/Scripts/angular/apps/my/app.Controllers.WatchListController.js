﻿/// <reference path="~/angular"/>
/// <reference path="./app.js"/>

(function () {
    "use strict";

    var WatchListController = function ($scope, $http, $location, watches) {

        $scope.working = false;
        $scope.loaded = false;
        $scope.vehicles = [];
        $scope.search = $location.search() || {};
        $scope.watches = watches;

        $scope.pagination = {
            take: 20,
            maxSize: 7,
            currentPage: 1,
            noOfPages: 0
        };

        $scope.query = function () {
            $location.search($scope.search);
            executeQuery();
        };

        var executeQuery = function () {
            $scope.working = true;
            var skip = ($scope.pagination.currentPage - 1) * $scope.pagination.take;

            watches.find({
                params: {
                    q: $scope.search.q,
                    top: $scope.pagination.take,
                    skip: skip
                }
            }).then(function (res) {
                $scope.loaded = true;
                $scope.working = false;
                $scope.vehicles = res.data;

                $scope.totalCount = res.headers("X-TotalResults");
            }, function () {
                $scope.loaded = true;
                $scope.errorLoading = true;
                $scope.working = false;
            });
        };

        $scope.$watch('totalCount', function () {
            $scope.pagination.noOfPages = Math.ceil($scope.totalCount / $scope.pagination.take);
        });
        $scope.$watch('pagination.currentPage', function (newValue, oldValue) {
            if (newValue !== oldValue)
                $scope.query();
        });

        executeQuery();
    };

    angular.module('app.Controllers')
        .controller('WatchListController', ['$scope', '$http', '$location', 'watches', WatchListController]);
} ());