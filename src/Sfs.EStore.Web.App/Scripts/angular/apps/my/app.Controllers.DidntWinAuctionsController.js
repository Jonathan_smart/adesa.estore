﻿/// <reference path="~/angular"/>
/// <reference path="./app.js"/>

(function () {
    "use strict";

    var Controller = function ($scope, $http, $location, siteUrls) {

        $scope.working = false;
        $scope.loaded = false;
        $scope.listings = undefined;
        $scope.search = $location.search() || {};

        $scope.sortBy = '-endTime';

        $scope.pagination = {
            take: 20,
            currentPage: 1
        };

        $scope.query = function () {
            $location.search($scope.search);
            executeQuery();
        };

        $scope.setSortBy = function () {
            $scope.sortBy = $('#sortBy').val();
            executeQuery();
        };

        $scope.$watch('sortBy', function (newValue, oldValue) {
            if (newValue == oldValue) return;
            
            executeQuery();
        });

        var executeQuery = function () {
            $scope.working = true;

            $http.get(siteUrls.path('api/bids'), {
                params: {
                    status: 'lost',
                    sortBy: $scope.sortBy,
                    pagenumber: $scope.pagination.currentPage,
                    pagesize: $scope.pagination.take
                }
            })
                .then(function (res) {
                    $scope.loaded = true;
                    $scope.working = false;
                    $scope.listings = res.data;

                    $scope.pagination.totalCount = res.headers("X-TotalResults");
                }, function (res) {
                    $scope.loaded = true;
                    $scope.errorLoading = true;
                    $scope.working = false;
                });
        };

        $scope.$watch('totalCount', function () {
            $scope.pagination.noOfPages = Math.ceil($scope.totalCount / $scope.pagination.take);
        });
        $scope.$watch('pagination.currentPage', function (newValue, oldValue) {
            if (newValue !== oldValue)
                $scope.query();
        });

        executeQuery();
    };

    angular.module('app.Controllers')
        .controller('DidntWinAuctionsController', ['$scope', '$http', '$location', 'siteUrls', Controller]);
} ());