using System;
using System.Web;
using System.Web.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Sfs.EStore.Web.App.Code;

namespace Sfs.EStore.Web.App
{
    public static class HttpResponseBaseExtensions
    {
        public static void SetFormsAuthentication(this HttpResponseBase @this, UserIdentity identity)
        {
            var authTicket = new FormsAuthenticationTicket(version: 1,
                                                           name: identity.UserName,
                                                           issueDate: DateTime.Now,
                                                           expiration: DateTime.Now.AddMinutes(30),
                                                           isPersistent: false,
                                                           userData: JObject.FromObject(identity)
                                                               .ToString(Formatting.None));

            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName,
                                        FormsAuthentication.Encrypt(authTicket));
            @this.Cookies.Add(cookie);
        }
    }
}