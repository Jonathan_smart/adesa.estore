﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Policy;
using Sfs.Darker.CommandProcessor;
using Sfs.EStore.Ports.Listings;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Controllers.Api;
using Sfs.EStore.Web.App.Models;
using Sfs.EStore.Web.App.ThirdParties;

namespace Sfs.EStore.Web.App.Ports.Listings
{
    public class EStoreApiGetListing : RequestHandler<GetListingInfo>
    {
        private readonly AuthorizedHttpClient _client;
        private readonly ITranslateListingDetails _translator;
        private readonly SingleLotGetParams _singleLotGetParams;

        public EStoreApiGetListing(AuthorizedHttpClient apiProxy, IVehicleSearchHandler vehicleSearchHandler,
            ITranslateListingDetails translator, ILog logger)
            : base(logger)
        {
            _client = apiProxy;
            _translator = translator;
            _singleLotGetParams = new SingleLotGetParams();
            vehicleSearchHandler.HandlerSearchParams(_singleLotGetParams);
        }

        public override GetListingInfo Handle(GetListingInfo cmd)
        {
            var model = _client.Get("lots/" + cmd.ListingId + "?" + _singleLotGetParams.ToUrlParams())
                .AcceptJson().SendAsync()
                .ContinueWith(r =>
                {
                    if (r.Result.StatusCode == HttpStatusCode.NotFound)
                        return null;

                    r.Result.EnsureSuccessStatusCode();

                    return r.Result.Content.ReadAsAsync<ListingViewModel>().Result;
                }).Result;

            var listing = _translator.Translate(model);

            cmd.Result = listing;

            return cmd;
        }
    }

    public interface ITranslateListingDetails
    {
        ListingViewModel Translate(ListingDetail source);
        ListingDetail Translate(ListingViewModel source);
    }

    public class ListingDetailTranslator : ITranslateListingDetails
    {
        private const string ReservedSellingStatus = "Reserved";

        public ListingViewModel Translate(ListingDetail source)
        {
            var model = new ListingViewModel();

            model.AttentionGrabber = source.AttentionGrabber;
            //model.BiddingStatus = new ListingViewModel.BiddingInfoModel
            //model.DiscountPriceInfo = new ListingSummaryViewModel.DiscountPriceInfoModel
            model.ListingId = source.ListingId;
            model.ListingInfo = new ListingViewModel.FullListingInfoModel
            {
                BuyItNowAvailable = source.ListingInfo.BuyItNowAvailable,
                BuyItNowPrice = Convert(source.ListingInfo.BuyItNowPrice),
                EndTime = source.ListingInfo.EndAt,
                ListingType = source.ListingInfo.ListingType.ToString(),
                StartTime =  source.ListingInfo.StartAt,
                TimeRemaining = source.ListingInfo.TimeRemaining
            };
            model.MoreImages = (source.MoreImages ?? new ListingImage[0]).Select(x => new ImageModel
            {
                Container = x.Container,
                Name = x.Name,
                Url = x.Url == null ? null : x.Url.ToString()
            }).ToArray();
            model.PrimaryImage = source.PrimaryImage == null
                ? null
                : Convert(source.PrimaryImage);
            model.PricesIncludeVat = source.PricesIncludeVat;
            model.Programme = source.Programme;
            model.SellingStatus = new ListingViewModel.FullSellingStatusModel
            {
                CurrentPrice = Convert(source.SellingStatus.CurrentPrice),
                SellingState =
                    source.SellingStatus.HasBeenReserved ? ReservedSellingStatus : source.SellingStatus.SellingState.ToString(),
                TimeLeft = source.SellingStatus.TimeLeft
            };
            model.Title = source.Title;
            model.VehicleInfo = new ListingViewModel.FullVehicleInfoModel
            {
                BodyColour = source.VehicleInfo.BodyColour,
                CapId = source.VehicleInfo.CapId,
                Derivative = source.VehicleInfo.Derivative,
                EngineDescription = source.VehicleInfo.EngineDescription,
                FuelType = source.VehicleInfo.FuelType,
                Make = source.VehicleInfo.Make,
                Mileage = source.VehicleInfo.Mileage,
                Model = source.VehicleInfo.Model,
                ModelYear = source.VehicleInfo.ModelYear,
                Registration = Convert(source.VehicleInfo.Registration),
                Source = source.VehicleInfo.Source == null
                    ? null
                    : new ListingViewModel.FullVehicleInfoModel.SourceInfo
                    {
                        Code = source.VehicleInfo.Source.Code,
                        Name = source.VehicleInfo.Source.Name
                    },
                Specification = new ListingViewModel.FullVehicleInfoModel.SpecificationModel
                {
                    Co2 = source.VehicleInfo.Specification.Co2,
                    Dimensions = new ListingViewModel.DimensionsInfoModel
                    {
                        HeightMm = source.VehicleInfo.Specification.Dimensions.HeightMm,
                        KerbWeightMax = source.VehicleInfo.Specification.Dimensions.KerbWeightMax,
                        KerbWeightMin = source.VehicleInfo.Specification.Dimensions.KerbWeightMin,
                        LengthMm = source.VehicleInfo.Specification.Dimensions.LengthMm,
                        WheelbaseForVans = source.VehicleInfo.Specification.Dimensions.WheelbaseForVans,
                        WidthMm = source.VehicleInfo.Specification.Dimensions.WidthMm,
                    },
                    Economy = new ListingViewModel.EconomyInfoModel
                    {
                        Combined = source.VehicleInfo.Specification.Economy.Combined,
                        CostPerUnitDistanceTravelled =
                            Convert(source.VehicleInfo.Specification.Economy.CostPerUnitDistanceTravelled,
                                nullsAllowed: true),
                        ExtraUrban = source.VehicleInfo.Specification.Economy.ExtraUrban,
                        Urban = source.VehicleInfo.Specification.Economy.Urban
                    },
                    Engine = new ListingViewModel.EngineInfoModel
                    {
                        ArrangementOfCylinders = source.VehicleInfo.Specification.Engine.ArrangementOfCylinders,
                        CapacityCc = source.VehicleInfo.Specification.Engine.CapacityCc,
                        NumberOfCylinders = source.VehicleInfo.Specification.Engine.NumberOfCylinders,
                        NumberOfValvesPerCylinder =
                            source.VehicleInfo.Specification.Engine.NumberOfValvesPerCylinder
                    },
                    Equipment = new ListingViewModel.EquipmentInfoModel
                    {
                        Items = source.VehicleInfo.Specification.Equipment.Items
                            .Select(x => new ListingViewModel.EquipmentInfoModel.EquipmentItemModel
                            {
                                Category = x.Category,
                                Name = x.Name
                            }).ToArray()
                    },
                    InsuranceGroup = source.VehicleInfo.Specification.InsuranceGroup,
                    Performance = new ListingViewModel.PerformanceInfoModel
                    {
                        AccelerationTo100KphInSecs =
                            source.VehicleInfo.Specification.Performance.AccelerationTo100KphInSecs,
                        MaximumPowerAtRpm = source.VehicleInfo.Specification.Performance.MaximumPowerAtRpm,
                        MaximumPowerInBhp = source.VehicleInfo.Specification.Performance.MaximumPowerInBhp,
                        MaximumPowerInKw = source.VehicleInfo.Specification.Performance.MaximumPowerInKw,
                        MaximumSpeedMph = source.VehicleInfo.Specification.Performance.MaximumSpeedMph,
                        MaximumTorqueAtRpm = source.VehicleInfo.Specification.Performance.MaximumTorqueAtRpm,
                        MaximumTorqueLbFt = source.VehicleInfo.Specification.Performance.MaximumTorqueLbFt,
                        MaximumTorqueNm = source.VehicleInfo.Specification.Performance.MaximumTorqueNm
                    },
                    TaxRate = source.VehicleInfo.Specification.TaxRate == null
                        ? null
                        : new ListingViewModel.FullVehicleInfoModel.TaxRateModel
                        {
                            Band = source.VehicleInfo.Specification.TaxRate.Band,
                            RoadTax12Months =
                                Convert(source.VehicleInfo.Specification.TaxRate.RoadTax12Months, nullsAllowed: true)
                        },
                },
                Transmission = source.VehicleInfo.Transmission,
                Trim = source.VehicleInfo.Trim == null
                    ? null
                    : new ListingViewModel.FullVehicleInfoModel.TrimInfo
                    {
                        Code = source.VehicleInfo.Trim.Code,
                        Name = source.VehicleInfo.Trim.Name
                    },
                VatStatus = source.VehicleInfo.VatStatus,
                VehicleType = source.VehicleInfo.VehicleType,
            };

            return model;
        }

        public ListingDetail Translate(ListingViewModel source)
        {
            var detail = new ListingDetail
            {
                AttentionGrabber = source.AttentionGrabber,
                ListingId = source.ListingId,
                ListingInfo = new ListingDetail.ListingListing
                {
                    BuyItNowAvailable = source.ListingInfo.BuyItNowAvailable,
                    BuyItNowPrice = Convert(source.ListingInfo.BuyItNowPrice),
                    EndAt = source.ListingInfo.EndTime,
                    ListingType = (ListingType) Enum.Parse(typeof (ListingType), source.ListingInfo.ListingType),
                    StartAt =  source.ListingInfo.StartTime,
                    TimeRemaining = source.ListingInfo.TimeRemaining
                },
                MoreImages = source.MoreImages.Select(Convert).ToArray(),
                PricesIncludeVat = source.PricesIncludeVat,
                PrimaryImage = source.PrimaryImage == null ? null : Convert(source.PrimaryImage),
                Programme = source.Programme,
                SellingStatus = new ListingDetail.ListingSellingStatus
                {
                    SellingState = source.SellingStatus.SellingState == ReservedSellingStatus
                        ? ListingStatus.Active
                        : (ListingStatus) Enum.Parse(typeof (ListingStatus), source.SellingStatus.SellingState),
                    CurrentPrice = Convert(source.SellingStatus.CurrentPrice),
                    HasBeenReserved = source.SellingStatus.SellingState == ReservedSellingStatus,
                    TimeLeft = source.SellingStatus.TimeLeft
                },
                Title = source.Title,
                VehicleInfo = new ListingDetail.ListingVehicle
                {
                    BodyColour = source.VehicleInfo.BodyColour,
                    CapId = source.VehicleInfo.CapId,
                    Derivative = source.VehicleInfo.Derivative,
                    EngineDescription = source.VehicleInfo.EngineDescription,
                    FuelType = source.VehicleInfo.FuelType,
                    Make = source.VehicleInfo.Make,
                    Mileage = source.VehicleInfo.Mileage,
                    Model = source.VehicleInfo.Model,
                    ModelYear = source.VehicleInfo.ModelYear,
                    Registration = Convert(source.VehicleInfo.Registration),
                    Source = (source.VehicleInfo.Source == null)
                        ? null
                        : new ListingDetail.ListingVehicle.SourceInfo(source.VehicleInfo.Source.Code,
                            source.VehicleInfo.Source.Name),
                    Specification = new ListingDetail.ListingVehicle.VehicleSpecification
                    {
                        Co2 = source.VehicleInfo.Specification.Co2,
                        Dimensions = new ListingDetail.VehicleDimensions
                        {
                            HeightMm = source.VehicleInfo.Specification.Dimensions.HeightMm,
                            KerbWeightMax = source.VehicleInfo.Specification.Dimensions.KerbWeightMax,
                            KerbWeightMin = source.VehicleInfo.Specification.Dimensions.KerbWeightMin,
                            LengthMm = source.VehicleInfo.Specification.Dimensions.LengthMm,
                            WheelbaseForVans = source.VehicleInfo.Specification.Dimensions.WheelbaseForVans,
                            WidthMm = source.VehicleInfo.Specification.Dimensions.WidthMm,
                        },
                        Economy = new ListingDetail.VehicleEconomy
                        {
                            Combined = source.VehicleInfo.Specification.Economy.Combined,
                            CostPerUnitDistanceTravelled =
                                Convert(source.VehicleInfo.Specification.Economy.CostPerUnitDistanceTravelled,
                                    nullsAllowed: true),
                            ExtraUrban = source.VehicleInfo.Specification.Economy.ExtraUrban,
                            Urban = source.VehicleInfo.Specification.Economy.Urban,
                        },
                        Engine = new ListingDetail.VehicleEngine
                        {
                            ArrangementOfCylinders = source.VehicleInfo.Specification.Engine.ArrangementOfCylinders,
                            CapacityCc = source.VehicleInfo.Specification.Engine.CapacityCc,
                            NumberOfCylinders = source.VehicleInfo.Specification.Engine.NumberOfCylinders,
                            NumberOfValvesPerCylinder =
                                source.VehicleInfo.Specification.Engine.NumberOfValvesPerCylinder,
                        },
                        Equipment = new ListingDetail.VehicleEquipment
                        {
                            Items = source.VehicleInfo.Specification.Equipment.Items
                                .Select(x => new ListingDetail.VehicleEquipmentItem(x.Category, x.Name))
                                .ToArray()
                        },
                        InsuranceGroup = source.VehicleInfo.Specification.InsuranceGroup,
                        Performance = new ListingDetail.VehiclePerformance
                        {
                            AccelerationTo100KphInSecs =
                                source.VehicleInfo.Specification.Performance.AccelerationTo100KphInSecs,
                            MaximumPowerAtRpm = source.VehicleInfo.Specification.Performance.MaximumPowerAtRpm,
                            MaximumPowerInBhp = source.VehicleInfo.Specification.Performance.MaximumPowerInBhp,
                            MaximumPowerInKw = source.VehicleInfo.Specification.Performance.MaximumPowerInKw,
                            MaximumSpeedMph = source.VehicleInfo.Specification.Performance.MaximumSpeedMph,
                            MaximumTorqueAtRpm = source.VehicleInfo.Specification.Performance.MaximumTorqueAtRpm,
                            MaximumTorqueLbFt = source.VehicleInfo.Specification.Performance.MaximumTorqueLbFt,
                            MaximumTorqueNm = source.VehicleInfo.Specification.Performance.MaximumTorqueNm,
                        },
                        TaxRate = source.VehicleInfo.Specification.TaxRate == null
                            ? null
                            : new ListingDetail.ListingVehicle.VehicleTaxRate(
                                source.VehicleInfo.Specification.TaxRate.Band,
                                Convert(source.VehicleInfo.Specification.TaxRate.RoadTax12Months, nullsAllowed: true)),
                    },
                    Transmission = source.VehicleInfo.Transmission,
                    Trim = source.VehicleInfo.Trim == null
                        ? null
                        : new ListingDetail.ListingVehicle.TrimInfo(source.VehicleInfo.Trim.Code,
                            source.VehicleInfo.Trim.Name),
                    VatStatus = source.VehicleInfo.VatStatus,
                    VehicleType = source.VehicleInfo.VehicleType
                }
            };

            return detail;
        }

        private ListingDetail.ListingVehicle.VehicleRegistration Convert(ListingViewModel.FullVehicleInfoModel.RegistrationModel registration)
        {
            return new ListingDetail.ListingVehicle.VehicleRegistration(
                registration.Plate,
                registration.Date,
                registration.Yap);
        }

        private ListingViewModel.FullVehicleInfoModel.RegistrationModel Convert(ListingDetail.ListingVehicle.VehicleRegistration registration)
        {
            return new ListingViewModel.FullVehicleInfoModel.RegistrationModel
            {
                Plate = registration.Plate,
                Date = registration.Date,
                Yap = registration.Yap
            };
        }

        private ImageModel Convert(ListingImage image)
        {
            return new ImageModel
            {
                Container = image.Container,
                Name = image.Name,
                Url = image.Url == null ? null : image.Url.ToString()
            };
        }

        private ListingImage Convert(ImageModel image)
        {
            return new ListingImage(image.Container, image.Name,
                string.IsNullOrEmpty(image.Url) ? null : new Url(image.Url));
        }

        private static MoneyViewModel Convert(Money money, bool nullsAllowed = false)
        {
            if (nullsAllowed && money == null) return null;

            return new MoneyViewModel(money.Amount, money.Currency);
        }

        private static Money Convert(MoneyViewModel money, bool nullsAllowed = false)
        {
            if (nullsAllowed && money == null) return null;

            return new Money(money.Amount, money.Currency);
        }
    }
}
