namespace Sfs.EStore.Web.App
{
    public class CmsPagePermanameHelper
    {
        public static string Normalize(string permalink)
        {
            if (permalink.EndsWith("/"))
                permalink = permalink.Substring(0, permalink.Length - 1);

            return permalink;
        }
    }
}