using System.Linq;
using System.Web.Mvc;
using Sfs.EStore.Web.App.Code.Cms;
using Sfs.EStore.Web.App.Models;

namespace Sfs.EStore.Web.App.Controllers
{
    public class ManufacturerCmsPagesController : ApplicationController
    {
        [AllowAnonymous]
        public ActionResult Index(string vehicleType, string manufacturer)
        {
            var permaName = string.Format("used-{0}s/_make", vehicleType);

            var page = Session.Query<ManufacturerCmsPage>()
                .FirstOrDefault(x => x.VehicleTypeQueryTerm == vehicleType && x.ManufacturerSlug == manufacturer);

            if (page == null)
                Logger.WarnFormat("Cannot find manufacturer page for {0}/{1}.", vehicleType, manufacturer);

            var variables = new ManufacturerPageLiquidVariables(User.Identity, Request)
            {
                body = page != null ? page.Body : null,
                page_title = page != null ? page.PageTitle : null,
                image_url = page != null ? page.ImageUrl : null,
                manufacturer_query_term = page != null ? page.ManufacturerQueryTerm : manufacturer.Replace('-', ' ').ToUpperInvariant(),
                vehicle_type_query_term = page != null ? page.VehicleTypeQueryTerm : vehicleType.ToUpperInvariant()
            };

            return CmsManagedPage(permaName, variables, model =>
            {
                if (page == null) return;

                if (!string.IsNullOrWhiteSpace(page.MetaData.Title))
                    model.MetaData.Title = page.MetaData.Title;

                if (!string.IsNullOrWhiteSpace(page.MetaData.Description))
                    model.MetaData.Description = page.MetaData.Description;

                if (!string.IsNullOrWhiteSpace(page.MetaData.CanonicalUrl))
                    model.MetaData.CanonicalUrl = page.MetaData.CanonicalUrl;
            });
        }
    }
}