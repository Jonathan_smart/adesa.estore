﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Raven.Client;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.Mvc;
using Sfs.EStore.Web.App.Code.Security;

namespace Sfs.EStore.Web.App.Controllers
{
    [AuthoriseActivity("/Users/Guest/SignIn")]
    public class AffiliateController : ApplicationController
    {
        public ActionResult Index(string token, string redirectTo = "")
        {
            IGuestPassTicketDecrypted decryptedTicket;

            try
            {
                decryptedTicket = GuestPass.Decrypt(token);
            }
            catch (ArgumentException)
            {
                return new HttpUnauthorizedResult();
            }

            return new GuestPassTicketHandlerFactory(Response, Session, redirectTo).Create(decryptedTicket)
                .Handle(token, decryptedTicket);
        }
    }

    public class GuestPassTicketHandlerFactory
    {
        private readonly HttpResponseBase _response;
        private readonly IDocumentSession _session;
        private readonly string _redirectTo;

        public GuestPassTicketHandlerFactory(HttpResponseBase response, IDocumentSession session, string redirectTo)
        {
            _response = response;
            _session = session;
            _redirectTo = redirectTo;
        }

        public GuestPassTicketHandler Create(IGuestPassTicket decryptedTicket)
        {
            return string.IsNullOrEmpty(decryptedTicket.Id)
                       ? (GuestPassTicketHandler)new NonPersistentGuestPassTicketHandler(_redirectTo, _session, _response)
                       : new PersistentGuestPassTicketHandler(_session, _response);

        }
    }

    public abstract class GuestPassTicketHandler
    {
        public abstract ActionResult Handle(string token, IGuestPassTicketDecrypted decryptedTicket);
    }

    public class PersistentGuestPassTicketHandler : GuestPassTicketHandler
    {
        private readonly IDocumentSession _session;
        private readonly HttpResponseBase _response;

        public PersistentGuestPassTicketHandler(IDocumentSession session, HttpResponseBase response)
        {
            _session = session;
            _response = response;
        }

        public override ActionResult Handle(string token, IGuestPassTicketDecrypted decryptedTicket)
        {
            var ticket = _session.Load<GuestPassTicket>(decryptedTicket.Id);

            if (ticket == null)
                return new HttpUnauthorizedResult("Ticket is no-longer valid");

            if (ticket.Expired)
                return new HttpUnauthorizedResult("Ticket has been expired");

            _response.SetCookie(new HttpCookie(GuestPass.CookieName, token)
            {
                Expires = SystemTime.UtcNow + ticket.TimeToLive
            });

            var redirect = ticket.RedirectTo ?? "";

            if (!string.IsNullOrWhiteSpace(redirect))
                return new RedirectResult(redirect);

            return new RedirectToRouteResult(null,
                                             new RouteValueDictionary(
                                                 new
                                                     {
                                                         action = "index", 
                                                         controller = "cmspages", 
                                                         area = "", 
                                                         id = "home",
                                                         utm_source = ticket.Source,
                                                         utm_medium = "guestpass",
                                                         utm_campaign = ticket.Campaign
                                                     }));
        }
    }

    public class NonPersistentGuestPassTicketHandler : GuestPassTicketHandler
    {
        private readonly string _redirectTo;
        private readonly IDocumentSession _session;
        private readonly HttpResponseBase _response;

        public NonPersistentGuestPassTicketHandler(string redirectTo, IDocumentSession session, HttpResponseBase response)
        {
            _redirectTo = redirectTo;
            _session = session;
            _response = response;
        }

        public override ActionResult Handle(string token, IGuestPassTicketDecrypted decryptedTicket)
        {
            var source = decryptedTicket.Source ?? "";
            var campaign = decryptedTicket.Campaign ?? "";

            var ticket = _session.Query<GuestPassTicket>()
                .FirstOrDefault(x => x.Source == source && x.Campaign == campaign);

            if (ticket == null)
            {
                ticket = new GuestPassTicket(null, decryptedTicket.Source, decryptedTicket.TimeToLive,
                                             decryptedTicket.Campaign, _redirectTo);

                _session.Store(ticket);
            }

            var updatedToken = GuestPass.Encrypt(ticket);

            _response.SetCookie(new HttpCookie(GuestPass.CookieName, updatedToken)
            {
                Expires = SystemTime.UtcNow + ticket.TimeToLive
            });

            var redirect = ticket.RedirectTo ?? "";

            if (!string.IsNullOrWhiteSpace(redirect))
                return new RedirectResult(redirect);

            return new RedirectToRouteResult(null,
                                             new RouteValueDictionary(
                                                 new { action = "index", controller = "cmspages", area = "", id = "home" }));
        }
    }
}
