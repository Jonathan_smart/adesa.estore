using System;
using System.Web;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    public class FinanceApplyLinkBuilder
    {
        private readonly string _dealerName;
        private readonly Uri _returnUrl;

        public FinanceApplyLinkBuilder(string dealerName, Uri returnUrl)
        {
            if (returnUrl == null) throw new ArgumentNullException("returnUrl");

            _dealerName = dealerName;
            _returnUrl = returnUrl;
        }

        public string Build(Guid quoteId, string vrm)
        {
            return string.Format("https://finance-application.com/{0}/{1}/{2}/{3}/",
                _dealerName,
                quoteId,
                vrm,
                HttpUtility.UrlEncode(_returnUrl.ToString()));
        }
    }
}