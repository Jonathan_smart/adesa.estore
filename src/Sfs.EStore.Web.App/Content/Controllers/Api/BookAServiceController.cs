﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Sfs.EStore.Web.App.Code.Http;
using Sfs.EStore.Web.App.Models;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    [AllowAnonymous]
    public class BookAServiceController : ApiApplicationController
    {
        [ValidationFilter, ModelRequiredFilter]
        public Task<HttpResponseMessage> Post(BookAServicePostModel form)
        {
            return ApiProxy.PostAsJson("bookaservice", form)
                .AcceptJson().SendAsync()
                .ContinueWith(t => Request.CreateResponse(t.Result.IsSuccessStatusCode ? HttpStatusCode.OK : t.Result.StatusCode));
        }
    }
}