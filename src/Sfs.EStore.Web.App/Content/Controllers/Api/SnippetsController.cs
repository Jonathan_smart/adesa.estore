﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Raven.Client;
using Raven.Client.Linq;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.Http;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    [Authorise(Roles="content_editor")]
    public class SnippetsController : ApiApplicationController
    {
        public Task<HttpResponseMessage> Get()
        {
            return Task.Factory.StartNew(() =>
            {
                var list = Session.Query<CmsSnippet>()
                    .OrderBy(x => x.Id)
                    .ToListAsync().Result;

                return Request.CreateResponse(HttpStatusCode.OK,
                                              list.Select(x => new
                                                                   {
                                                                       x.Id,
                                                                       x.Key
                                                                   })
                                                  .ToArray());
            });
        }

        public Task<HttpResponseMessage> Get(string id)
        {
            return Task.Factory.StartNew(() =>
            {
                var snippet = Session.LoadAsync<CmsSnippet>(CmsSnippet.IdFromKey(id)).Result;
                if (snippet == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Cannot find snippet with id " + id);

                return Request.CreateResponse(HttpStatusCode.OK, snippet);
            });
        }

        public class ApiSnippetsPutModel
        {
            [Required]
            public string Id { get; set; }
            [Required]
            public string Body { get; set; }
        }

        public Task<HttpResponseMessage> Delete(string id)
        {
            return Task.Factory.StartNew(() =>
                                             {
                                                 var snippet =
                                                     Session.LoadAsync<CmsSnippet>(CmsSnippet.IdFromKey(id)).Result;

                                                 if (snippet == null)
                                                     return Request.CreateErrorResponse(HttpStatusCode.NotFound, "");

                                                 Session.Delete(snippet);
                                                 return Request.CreateResponse(HttpStatusCode.OK);
                                             });
        }

        public Task<HttpResponseMessage> Put(ApiSnippetsPutModel model)
        {
            return Task.Factory.StartNew(() => AddOrUpdateSnippet(model.Id, model.Body));
        }

        private HttpResponseMessage AddOrUpdateSnippet(string key, string body)
        {
            var snippet = Session.LoadAsync<CmsSnippet>(CmsSnippet.IdFromKey(key)).Result
                          ?? CreateNewSnippet(key);

            snippet.SetBody(body);

            return Request.CreateResponse(HttpStatusCode.OK, snippet);
        }

        private CmsSnippet CreateNewSnippet(string slug)
        {
            var n = new CmsSnippet(slug);
            Session.StoreAsync(n);
            return n;
        }
    }
}
