﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Raven.Client;
using Sfs.EStore.Web.App.Code.Http;
using Sfs.EStore.Web.App.Code.Security;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    [Authorise(Roles = "user_manager")]
    public class AffiliateController : ApiApplicationController
    {
        public class AffiliateGetModel
        {
            public string Q { get; set; }
            public bool? Expired { get; set; }
            private int _page = 1;
            public int Page
            {
                get { return _page; }
                set { _page = value; }
            }

            private int _take = 15;
            public int Take
            {
                get { return _take; }
                set { _take = value; }
            }
        }

        public Task<HttpResponseMessage> Get([FromUri]AffiliateGetModel filter)
        {
            filter = filter ?? new AffiliateGetModel();

            return Task.Factory.StartNew(() =>
                                             {
                                                 RavenQueryStatistics ravenQueryStatistics;
                                                 IQueryable<GuestPassTicket> query = Session.Query<GuestPassTicket>()
                                                     .Statistics(out ravenQueryStatistics);
                                                 
                                                 if (filter.Q != null)
                                                 {
                                                     query = query.Where(x =>
                                                                         x.Source.StartsWith(filter.Q) ||
                                                                         x.Campaign.StartsWith(filter.Q) ||
                                                                         x.CreatedBy.StartsWith(filter.Q));
                                                 }

                                                 if (filter.Expired.HasValue)
                                                 {
                                                     query = query.Where(x => x.Expired == filter.Expired);
                                                 }

                                                 query = query
                                                     .OrderByDescending(x => x.CreatedAt)
                                                     .Skip((filter.Page - 1)*filter.Take)
                                                     .Take(filter.Take);

                                                 var guestPasses = query.ToListAsync().Result;

                                                 var response = Request.CreateResponse(HttpStatusCode.OK, guestPasses);
                                                 response.Headers.Add("X-TotalResults", ravenQueryStatistics.TotalResults.ToString(CultureInfo.InvariantCulture));

                                                 return response;
                                             });
        }

        public Task<HttpResponseMessage> Get(string id)
        {
            return Task.Factory.StartNew(() =>
            {
                var guestPassTicket = Session.LoadAsync<GuestPassTicket>(id).Result;

                if (guestPassTicket == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                                                       "Cannot find guest pass " + id);

                return Request.CreateResponse(HttpStatusCode.OK,
                                              GuestPassTicketModel(guestPassTicket));
            });
        }

        public Task<HttpResponseMessage> Delete(string id)
        {
            return Task.Factory.StartNew(() =>
                                             {
                                                 var guestPassTicket = Session.LoadAsync<GuestPassTicket>(id).Result;

                                                 if (guestPassTicket == null)
                                                     return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                                                                                        "Cannot find guest pass " + id);

                                                 guestPassTicket.Expire(User.Identity.Name);

                                                 return Request.CreateResponse(HttpStatusCode.OK, guestPassTicket);
                                             });
        }

        public Task<HttpResponseMessage> Post(AffiliatePostModel ticket)
        {
            return Task.Factory.StartNew(() => CreateGuestPass(ticket));
        }

        private HttpResponseMessage CreateGuestPass(AffiliatePostModel ticket)
        {
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

            var guestPassTicket = new GuestPassTicket(User.Identity.Name, ticket.Source,
                                                      TimeSpan.FromHours(ticket.TimeToLive ?? 72),
                                                      ticket.Campaign,
                                                      ticket.RedirectTo);

            Session.StoreAsync(guestPassTicket).Wait();

            return Request.CreateResponse(HttpStatusCode.OK,
                                          GuestPassTicketModel(guestPassTicket));
        }

        private object GuestPassTicketModel(GuestPassTicket ticket)
        {
            var token = GuestPass.Encrypt(ticket);

            return new
                       {
                           Ticket = ticket,
                           Token = new
                                       {
                                           Value = token,
                                           Link = Url.Link("Affiliate:GuestPass",
                                                           new
                                                               {
                                                                   controller = "affiliate",
                                                                   action = "index",
                                                                   area = "",
                                                                   token
                                                               })
                                       }
                       };
        }

        public class AffiliatePostModel
        {
            [Required(AllowEmptyStrings = false)]
            public string Source { get; set; }

            [Range(1, 43800)]
            public int? TimeToLive { get; set; }

            public string Campaign { get; set; }

            public string RedirectTo { get; set; }
        }
    }
}
