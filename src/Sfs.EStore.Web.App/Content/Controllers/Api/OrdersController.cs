using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    public class OrdersController : ApiApplicationController
    {
        public Task<HttpResponseMessage> Get([FromUri] string[] ids)
        {
            return Task.Factory.StartNew(() => GetOrderHistory(ids));
        }

        public Task<HttpResponseMessage> Post(CreateOrdersRequest model)
        {
            return Task.Factory.StartNew(() => CompleteOrder(model));
        }

        private HttpResponseMessage GetOrderHistory(string[] ids)
        {
            var queryString = string.Empty;
            if (ids != null && ids.Any())
                queryString = "?" + string.Join("&", ids.Select(x => "ids=" + x));
            return ApiProxy.Get("orders" + queryString).AcceptJson().SendAsync().Result;
        }

        private HttpResponseMessage CompleteOrder(CreateOrdersRequest model)
        {
            return ApiProxy.PostAsJson("orders", model).AcceptJson().SendAsync().Result;
        }
    }

    public class CreateOrdersRequest
    {
        public CreateOrdersRequestSalesItem[] Items { get; set; }
    }

    public class CreateOrdersRequestSalesItem
    {
        public string CustomerNo { get; set; }
        public string SerialNo { get; set; }
        public bool FinanceRequired { get; set; }
        public CreateOrdersRequestFinance Finance { get; set; }
        public CreateOrdersRequestBasket Basket { get; set; }
        public CreateOrdersRequestTransport Transport { get; set; }
        public CreateOrdersRequestShipping Shipping { get; set; }
        public CreateOrdersRequestV5 V5 { get; set; }
        public decimal? Price { get; set; }
        public decimal? BuyersFee { get; set; }
    }

    public class CreateOrdersRequestFinance
    {
        public string Method { get; set; }
        public string Agent { get; set; }
        public string Service { get; set; }
    }

    public class CreateOrdersRequestBasket
    {
        public int EntryNo { get; set; }
    }

    public class CreateOrdersRequestTransport
    {
        public decimal Price { get; set; }
    }

    public class CreateOrdersRequestShipping
    {
        public string Method { get; set; }
        public string Agent { get; set; }
        public string Service { get; set; }
        public string AddressCode { get; set; }
    }

    public class CreateOrdersRequestV5
    {
        public string ShipToAddressCode { get; set; }
    }
}