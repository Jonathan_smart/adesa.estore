﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.Http;
using Sfs.EStore.Web.App.Models;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    [AllowAnonymous]
    public class CallbacksController : ApiApplicationController
    {
        [ValidationFilter, ModelRequiredFilter]
        public Task<HttpResponseMessage> Post(CallbacksPostModel model)
        {
            return Task.Factory.StartNew(() => Callback(model));
        }

        private HttpResponseMessage Callback(CallbacksPostModel model)
        {
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

            var visitorId = VisitorTrackingHttpModule.VisitorId(Request.GetHttpContext());

            SiteVisitor visitor = null;
            if (visitorId != null)
                visitor = Session.LoadAsync<SiteVisitor>(visitorId).Result;

            var success = ApiProxy.PostAsJson("callbacks", model.ToApi(visitor))
                .AcceptJson().SendAsync()
                .ContinueWith(t =>
                {
                    var r = t.Result.Content.ReadAsAsync<JObject>().Result.Value<bool>("success");

                    return r;
                }).Result;

            return success
                       ? Request.CreateResponse(HttpStatusCode.OK, true)
                       : Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Failed to complete callback request");
        }
    }
}
