using System.Net.Http;
using System.Threading.Tasks;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    public class CheckoutFinanceOptionsController : ApiApplicationController
    {
        public Task<HttpResponseMessage> Get()
        {
            return ApiProxy.Get("checkoutfinanceoptions")
                .AcceptJson()
                .SendAsync()
                .ContinueWith(t => t.Result);
        }
    }
}