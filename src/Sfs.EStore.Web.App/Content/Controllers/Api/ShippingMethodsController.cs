using System.Net.Http;
using System.Threading.Tasks;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    public class ShippingMethodsController : ApiApplicationController
    {
        public Task<HttpResponseMessage> Get()
        {
            return Task.Factory.StartNew(() => GetAll());
        }

        private HttpResponseMessage GetAll()
        {
            return ApiProxy.Get("shippingmethods").AcceptJson().SendAsync().Result;
        }
    }
}