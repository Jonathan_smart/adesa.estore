using System;
using System.Net;
using System.Net.Http;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Sfs.Darker.CommandProcessor;
using Sfs.EStore.Ports.Listings;
using Sfs.EStore.Web.App.ConsumerFinance.Ports;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    [AllowAnonymous]
    public class ConsumerFinanceController : ApiApplicationController
    {
        private readonly ICommandProcessor _commandProcessor;
        private readonly FinanceApplyLinkBuilder _financeApplyLinkBuilder;

        public ConsumerFinanceController(ICommandProcessor commandProcessor, FinanceApplyLinkBuilder financeApplyLinkBuilder)
        {
            _commandProcessor = commandProcessor;
            _financeApplyLinkBuilder = financeApplyLinkBuilder;
        }

        public Task<HttpResponseMessage> Get(int lotNumber, [FromUri]FinanceQuoteModel quote)
        {
            var listingCmd = new GetListingInfo(lotNumber);
            _commandProcessor.Send(listingCmd);

            if (listingCmd.Result == null) throw new HttpException(404, string.Format("Listing {0} cannot be found", lotNumber));

            var listing = listingCmd.Result;

            if (!listing.VehicleInfo.Mileage.HasValue ||
                !listing.VehicleInfo.Registration.Date.HasValue ||
                !listing.VehicleInfo.CapId.HasValue)
            {
                return Task.FromResult(Request.CreateResponse(HttpStatusCode.OK, new QuoteResultModel[0]));
            }
            
            return Task.Factory.StartNew(() =>
            {
                var quoteCmd =
                    new GetConsumerFinanceQuote(
                        new GetConsumerFinanceQuote.RequestParameters(quote.Term),
                        new GetConsumerFinanceQuote.RequestFigures(listing.SellingStatus.CurrentPrice.Amount,
                            quote.Deposit,
                            quote.AnnualMileage),
                        new GetConsumerFinanceQuote.RequestVehicle(listing.VehicleInfo.Mileage.Value,
                            listing.VehicleInfo.Registration.Date.Value,
                            listing.VehicleInfo.Registration.Plate,
                            listing.VehicleInfo.CapId.Value,
                            "CAR".Equals(listing.VehicleInfo.VehicleType, StringComparison.InvariantCultureIgnoreCase)
                                ? GetConsumerFinanceQuote.RequestVehicleClass.Car
                                : GetConsumerFinanceQuote.RequestVehicleClass.LCV));

                _commandProcessor.Send(quoteCmd);

                var mapper = new ConsumerFinanceQuoteMapper(listing, _financeApplyLinkBuilder);
                
                return Request.CreateResponse(HttpStatusCode.OK, mapper.Map(quoteCmd.Result));
            });
        }

        public class FinanceQuoteModel
        {
            public decimal Deposit { get; set; }
            public int AnnualMileage { get; set; }
            public int Term { get; set; }
            public string[] Product { get; set; }
        }

        public class QuoteResultModel
        {
            public int VehicleTermDistance { get; set; }
            public Guid Id { get; set; }
            public string FacilityType { get; set; }
            public decimal TotalCashPrice { get; set; }
            public decimal TotalDeposit { get; set; }
            public decimal Advance { get; set; }
            public decimal AcceptanceFee { get; set; }
            public decimal OptionToPurchaseFee { get; set; }
            public decimal TotalPayable { get; set; }
            public decimal FirstPayment { get; set; }
            public decimal RegularPayment { get; set; }
            public decimal FinalPayment { get; set; }
            public int NumberOfRegularPayments { get; set; }
            public int Term { get; set; }
            public decimal APR { get; set; }
            public decimal AER { get; set; }

            public string ApplyUrl { get; set; }
            public string VideoLinkUrl { get; set; }
        }

        internal class ConsumerFinanceQuoteMapper
        {
            private readonly ListingDetail _listing;
            private readonly FinanceApplyLinkBuilder _applyUrlBuilder;

            public ConsumerFinanceQuoteMapper(ListingDetail listing, FinanceApplyLinkBuilder applyUrlBuilder)
            {
                _listing = listing;
                _applyUrlBuilder = applyUrlBuilder;
            }

            public QuoteResultModel[] Map(GetConsumerFinanceQuote.QuoteResult quotes)
            {
                return quotes.Quotes
                    .Select(x => new QuoteResultModel
                    {
                        Id = x.Id,
                        FacilityType = x.FacilityType.ToString(),
                        TotalCashPrice = x.TotalCashPrice,
                        TotalDeposit = x.TotalDeposit,
                        Advance = x.Advance,
                        AcceptanceFee = x.AcceptanceFee,
                        OptionToPurchaseFee = x.OptionToPurchaseFee,
                        TotalPayable = x.TotalPayable,
                        FirstPayment = x.FirstPayment,
                        RegularPayment = x.RegularPayment,
                        FinalPayment = x.FinalPayment,
                        NumberOfRegularPayments = x.NumberOfRegularPayments,
                        Term = x.Term,
                        APR = x.APR,
                        AER = x.AER,
                        VehicleTermDistance = x.VehicleTermDistance,
                        ApplyUrl = _applyUrlBuilder.Build(x.Id, _listing.VehicleInfo.Registration.Plate),
                        VideoLinkUrl = string.Format("//video.ivendi.com/{0}/{1}", x.Asset.Class, x.FacilityType.ToString().ToLowerInvariant())
                    })
                    .ToArray();
            }
        }
    }
}