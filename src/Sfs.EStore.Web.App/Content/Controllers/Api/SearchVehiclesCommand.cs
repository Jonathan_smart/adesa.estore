using System.Net.Http;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Models;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    public class SearchVehiclesCommand : LotsGetParams
    {
        public ListingSummaryViewModel[] Result { get; set; }
    }

    public class SearchVehicles
    {
        private readonly AuthorizedHttpClient _apiProxy;

        public SearchVehicles(AuthorizedHttpClient apiProxy)
        {
            _apiProxy = apiProxy;
        }

        public void Execute(SearchVehiclesCommand cmd)
        {
            var listings = _apiProxy.PostAsJson("search", cmd)
                .AcceptJson().SendAsync()
                .ContinueWith(r =>
                {
                    if (r.Exception != null)
                        throw r.Exception;

                    return r.Result.Content.ReadAsAsync<ListingSummaryViewModel[]>().Result;
                });

            cmd.Result = listings.Result;
        }
    }
}