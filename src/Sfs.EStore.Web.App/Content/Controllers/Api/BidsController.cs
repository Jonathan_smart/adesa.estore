using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    public class BidsController : ApiApplicationController
    {
        public Task<HttpResponseMessage> Get([FromUri] GetModel query)
        {
            query = query ?? new GetModel();

            return ApiProxy.Get("bids?" + query.ToUrlParams())
                .AcceptJson()
                .SendAsync()
                .ContinueWith(r =>
                                  {
                                      var apiResult = r.Result;
                                      apiResult.EnsureSuccessStatusCode();

                                      return r.Result;
                                  });
        }

        public class GetModel
        {
            public string Status { get; set; }
            public string SortBy { get; set; }
            public int? PageNumber { get; set; }
            public int? PageSize { get; set; }
        }
    }
}