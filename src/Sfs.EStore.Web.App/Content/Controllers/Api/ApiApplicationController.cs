using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using Raven.Client;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.Security;
using Sfs.EStore.Web.App.Areas.Retail;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    public abstract class ApiApplicationController : ApiController
    {
        protected new IWebApplicationPrincipal User
        {
            get { return (base.User as IWebApplicationPrincipal); }
        }

        protected ApiApplicationController()
        {
            _apiProxy = new Lazy<AuthorizedHttpClient>(() => ApiProxyFactory.Create(User));

            var ctx = new HttpContextWrapper(HttpContext.Current);
            _tenant = ctx.CurrentTenant();
            _permissions = ctx.CurrentTenantPermissions();

            _retail = new Lazy<RetailView>(() => new RetailView(User));
        }

        private Lazy<AuthorizedHttpClient> _apiProxy;

        public AuthorizedHttpClient ApiProxy
        {
            get { return _apiProxy.Value; }
            set { _apiProxy = new Lazy<AuthorizedHttpClient>(() => value, true); }
        }

        private SiteTenant _tenant;
        public SiteTenant Tenant
        {
            get { return _tenant; }
            set { _tenant = value; }
        }

        public Lazy<RetailView> _retail { get; set; }

        public IAsyncDocumentSession Session { get; set; }

        private SiteTenantActivityPermissions _permissions;
        public SiteTenantActivityPermissions Permissions
        {
            get { return _permissions; }
            set { _permissions = value; }
        }

        public override Task<HttpResponseMessage> ExecuteAsync(HttpControllerContext controllerContext,
                                                               CancellationToken cancellationToken)
        {
            using (Session = WebApiApplication.GetTenantDocumentStoreFor(Tenant.DatabaseName).OpenAsyncSession())
            {
                var result = base.ExecuteAsync(controllerContext, cancellationToken);

                result.ContinueWith(x =>
                {
                    if (x.Exception == null)
                        Session.SaveChangesAsync();
                });

                return result;
            }
        }
    }
}