using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    public class WatchesController : ApiApplicationController
    {
        [HttpGet, Route("api/watches")]
        public Task<HttpResponseMessage> Get([FromUri]WatchGetModel query)
        {
            query = query ?? new WatchGetModel();

            return ApiProxy.Get("watches?"+query.ToUrlParams())
                .AcceptJson()
                .SendAsync()
                .ContinueWith(r =>
                                  {
                                      var apiResult = r.Result;
                                      apiResult.EnsureSuccessStatusCode();

                                      return new LotSummaryModelResponseMessageMapper(null, Request.CreateResponse)
                                          .Map(apiResult);
                                  });
        }

        [HttpGet, Route("api/listings/{listingId}/watches")]
        public Task<HttpResponseMessage> Get(int listingId)
        {
            return ApiProxy.Get(string.Format("lots/{0}/watches", listingId)).AcceptJson().SendAsync();
        }

        [HttpPost, Route("api/listings/{listingId}/actions/add_watch")]
        public Task Post(int listingId)
        {
            return ApiProxy.Put(string.Format("lots/{0}/watches", listingId), null).AcceptJson().SendAsync();
        }

        [HttpPost, Route("api/listings/{listingId}/actions/remove_watch")]
        public Task Delete(int listingId)
        {
            return ApiProxy.Delete(string.Format("lots/{0}/watches", listingId)).AcceptJson().SendAsync();
        }
    }

    public class WatchGetModel
    {
        public string Q { get; set; }
        public int? Top { get; set; }
        public int? Skip { get; set; }
    }
}