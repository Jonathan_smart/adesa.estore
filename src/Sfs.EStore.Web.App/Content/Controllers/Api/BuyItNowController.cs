using System.Net.Http;
using System.Threading.Tasks;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    public class BuyItNowController : ApiApplicationController
    {
        public class BuyItNowPostModel
        {
            public string CustomerId { get; set; }
            public string ShippingMethodId { get; set; }
            public string ShipToAddressId { get; set; }
        }

        public Task<HttpResponseMessage> Post(int lotNumber, BuyItNowPostModel model)
        {
            return ApiProxy.PostAsJson(string.Format("lots/{0}/buyitnow", lotNumber), model).AcceptJson().SendAsync();
        }
    }
}