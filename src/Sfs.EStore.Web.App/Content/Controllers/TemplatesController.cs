﻿using System.Web.Mvc;

namespace Sfs.EStore.Web.App.Controllers
{
    [AllowAnonymous]
    public class TemplatesController : Controller
    {
        public ActionResult Index(string id)
        {
            return View(id);
        }

    }
}
