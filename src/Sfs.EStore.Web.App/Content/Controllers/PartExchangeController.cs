using System.Web.Mvc;
using Sfs.EStore.Web.App.Code.Mvc;

namespace Sfs.EStore.Web.App.Controllers
{
    [AuthoriseActivity("/PartEx")]
    public class PartExchangeController : ApplicationController
    {
        public ActionResult Index()
        {
            return CmsManagedPage("part-exchange", "");
        }
    }
}