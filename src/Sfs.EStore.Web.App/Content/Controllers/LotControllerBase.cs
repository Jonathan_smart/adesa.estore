using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Controllers.Api;
using Sfs.EStore.Web.App.Models;
using Sfs.EStore.Web.App.ThirdParties;

namespace Sfs.EStore.Web.App.Controllers
{
    public abstract class LotControllerBase : ApplicationController
    {
        private readonly SingleLotGetParams _singleLotGetParams;

        protected LotControllerBase(IVehicleSearchHandler vehicleSearchHandler)
        {
            _singleLotGetParams = new SingleLotGetParams();
            vehicleSearchHandler.HandlerSearchParams(_singleLotGetParams);
        }

        public Task<ListingViewModel> GetLotAsync(int lotNumber)
        {
            return ApiProxy.Value.Get("lots/" + lotNumber + "?" + _singleLotGetParams.ToUrlParams())
                .AcceptJson().SendAsync()
                .ContinueWith(r =>
                                  {
                                      if (r.Result.StatusCode == HttpStatusCode.NotFound)
                                          return null;

                                      r.Result.EnsureSuccessStatusCode();

                                      return r.Result.Content.ReadAsAsync<ListingViewModel>().Result;
                                  });
        }

        public ListingViewModel GetLot(int lotNumber)
        {
            return GetLotAsync(lotNumber).Result;
        }
    }
}