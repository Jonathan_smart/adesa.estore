﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sfs.EStore.Web.App.Code.Cms;
using Sfs.EStore.Web.App.Code.Mvc;
using Sfs.EStore.Web.App.Models;

namespace Sfs.EStore.Web.App.Controllers
{
    internal static class PredefinedSearchTypes
    {
        public const string Auctions = "a";
        public const string FixedPrice = "f";
    }

    [AuthoriseActivity("/Lots/View")]
    public class SearchController : ApplicationController
    {
        public ActionResult ViewPage(string cmspostfix)
        {
            var rvd = new System.Web.Routing.RouteValueDictionary { { "pg", cmspostfix } };
            foreach (var key in HttpContext.Request.QueryString.AllKeys)
            {
                rvd.Add(key, HttpContext.Request.QueryString[key]);
            }
            return RedirectToAction("", rvd);
        }

        public ActionResult Index(string t, string pg = null)
        {
            SearchPageModel page = null;

            var cmsPermaName = PermanameFromPostfix(pg);

            if (PredefinedSearchTypes.Auctions.Equals(t, StringComparison.InvariantCultureIgnoreCase))
            {
                page = new SearchPageModel(GetPage(cmsPermaName ?? "search-auctions"))
                {
                    PredefinedQueryParams =
                    {
                        LtAuction = true
                    },
                    ShowAuction = true,
                    ShowSaleFilter = true,
                    AllowOrderByTimeEndingSoonest = true
                };
            }
            else if (PredefinedSearchTypes.FixedPrice.Equals(t, StringComparison.InvariantCultureIgnoreCase))
            {
                page = new SearchPageModel(GetPage(cmsPermaName ?? "search-fixedprice"))
                {
                    PredefinedQueryParams =
                    {
                        LtAuction = false,
                        LtBin = true
                    }
                };
            }

            page = page ?? DefaultPage(cmsPermaName);

            if (!page.PageFound())
            {
                throw new HttpException(404, "Search page of type '" + t + "'" + " not found");
            }

            if (!page.CanBeAccessedAs(User))
                return new HttpUnauthorizedResult();

            return View(page);
        }

        private string PermanameFromPostfix(string postfix)
        {
            if (postfix == null) return null;

            return string.Format("search-{0}", postfix);
        }

        private CmsPage GetPage(string permaName)
        {
            return Session.Query<CmsPage>()
                .FirstOrDefault(x => x.PermaName == permaName);
        }

        private SearchPageModel DefaultPage(string paramName)
        {
            return new SearchPageModel(GetPage(paramName ?? "search-all"));
        }
    }
}
