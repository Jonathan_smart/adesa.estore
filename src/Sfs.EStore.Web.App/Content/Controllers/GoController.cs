﻿using System.Web.Mvc;
using Sfs.EStore.Web.App.Areas.Vehicles;
using Sfs.EStore.Web.App.Controllers.Api;
using Sfs.EStore.Web.App.ThirdParties;
using Slugify;

namespace Sfs.EStore.Web.App.Controllers
{
    [AllowAnonymous]
    public class GoController : ApplicationController
    {
        public ActionResult Index(string reg)
        {
            if (string.IsNullOrEmpty(reg))
                return Redirect(Url.Content("~/"));

            var searchHandler = new SearchVehicles(ApiProxy.Value);
            var cmd = new SearchVehiclesCommand
            {
                Registration = reg
            };
            searchHandler.Execute(cmd);

            if (cmd.Result.Length == 0)
                return Redirect(Url.Content("~/"));

            var listing = cmd.Result[0];

            return RedirectToRoute(VehiclesAreaRegistration.VehicleListing,
                new {id = listing.ListingId, slug = new SlugHelper().GenerateSlug(listing.Title)});
        }
    }
}