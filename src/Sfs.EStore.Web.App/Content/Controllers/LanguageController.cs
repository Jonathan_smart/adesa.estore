using System.Threading;
using System.Web;
using System.Web.Mvc;
using Sfs.EStore.Web.App.Code;

namespace Sfs.EStore.Web.App.Controllers
{
    [AllowAnonymous]
    public class LanguageController : ApplicationController
    {
        [HttpGet]
        public ActionResult Index()
        {
            return Json(Thread.CurrentThread.CurrentUICulture.ToString(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Index(string id)
        {
            Response.Cookies.Set(new HttpCookie("language",
                                                id ?? Thread.CurrentThread.CurrentUICulture.ToString())
                                     {
                                         Expires = SystemTime.UtcNow.AddYears(10)
                                     });

            if (Request.IsAjaxRequest())
                return Json(id, JsonRequestBehavior.AllowGet);

            return Redirect("~/");
        }
    }
}