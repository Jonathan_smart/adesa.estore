jQuery(function($) {

// placeholder support
if (window.Modernizr && !Modernizr.input.placeholder) {
	$("input[placeholder]").each(function(i) {
		var $this = $(this);
		if (!$this.val()) {
			$this.val($this.attr("placeholder"));
		}
	}).focus(function(e) {
		var $this = $(this);
		if ($this.val() === $this.attr("placeholder")) {
			$this.val("");
		}
	}).blur(function(e) {
		var $this = $(this);
		if (!$this.val()) {
			$this.val($this.attr("placeholder"));
		}
	});
	
	$("form").submit(function(e) {
		$("input[placeholder]", this).each(function(i) {
			var $this = $(this);
			if ($this.val() === $this.attr("placeholder")) {
				$this.val("");
			}
		});
	});
}

// input autofocus support
if (window.Modernizr && !Modernizr.input.autofocus) {
	$("*:input[autofocus]").focus();
}

});