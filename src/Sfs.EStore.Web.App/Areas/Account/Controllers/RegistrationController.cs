using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using Sfs.EStore.Web.App.Areas.Account.Models;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.Mvc;
using Sfs.EStore.Web.App.Code.Widgets.Liquid.Drops;
using Sfs.EStore.Web.App.Controllers;
using Sfs.EStore.Web.App.Models;
using Sfs.EStore.Web.App.ThirdParties;

namespace Sfs.EStore.Web.App.Areas.Account.Controllers
{
    [AuthoriseActivity("/Users/Registration/New")]
    public class RegistrationController : ApplicationController
    {
        private readonly RegisterNewUsersThroughApiCommand _registerNewUsersCommand;

        public RegistrationController(RegisterNewUsersThroughApiCommand registerNewUsersCommand)
        {
            _registerNewUsersCommand = registerNewUsersCommand;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View(new RegistrationIndexModel());
        }

        [HttpPost]
        public ActionResult Index(RegistrationIndexModel model)
        {
            _registerNewUsersCommand.ModelState = ModelState;
            _registerNewUsersCommand.Request = model;
            ExecuteCommand(_registerNewUsersCommand);

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            return RedirectToAction("nextstep", "registration", new { model.EmailAddress, model.ReturnUrl });
        }

        [HttpGet]
        public ActionResult NextStep(string emailAddress)
        {
            return CmsManagedPage("account-registration-next-step", new RegistrationNextStepLiquidVariables(User.Identity, Request) {email_address = emailAddress});
        }

        [HttpGet, AllowAnonymous]
        public ActionResult Confirm(string token)
        {
            return ApiProxy.Value.Post("userregistration/confirm?token=" + HttpUtility.UrlEncode(token), null)
                .AcceptJson().SendAsync()
                .ContinueWith(x =>
                {
                    if (!x.Result.IsSuccessStatusCode)
                        return CmsManagedPage("account-registration-confirm-failed",
                            new CmsPageLiquidVariables(new UserDrop(User.Identity), new CmsPageDrop(Request)));

                    var identity = x.Result.Content.ReadAsAsync<UserIdentity>().Result;
                    Response.SetFormsAuthentication(identity);
                    return RedirectToAction("complete");
                })
                .Result;
        }

        [HttpGet]
        public ActionResult Complete()
        {
            return CmsManagedPage("account-registration-complete",
                new CmsPageLiquidVariables(new UserDrop(User.Identity), new CmsPageDrop(Request)));
        }
    }
}