using System;
using System.Web.Mvc;
using Sfs.EStore.Web.App.Areas.Account.Models;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.Mvc;
using Sfs.EStore.Web.App.Controllers;
using Sfs.EStore.Web.App.ThirdParties;

namespace Sfs.EStore.Web.App.Areas.Account.Controllers
{
    [AuthoriseActivity("/Users/Account/ResetPassword")]
    public class ResetPasswordController : ApplicationController
    {
        private readonly IValidatePasswordRulesCommand _validatePasswordRules;

        public ResetPasswordController(IValidatePasswordRulesCommand validatePasswordRules)
        {
            _validatePasswordRules = validatePasswordRules;
        }

        [HttpGet]
        public ActionResult Index(string token)
        {
            if (!ExecuteCommand(new ValidatePasswordResetToken(token)))
                return View("Index.InValid");

            return View(new ResetPasswordIndexModel { Token = token });
        }

        [HttpPost]
        public ActionResult Index(ResetPasswordIndexModel model)
        {
            _validatePasswordRules.ModelState = ModelState;
            _validatePasswordRules.Password = model.Password;

            if (ModelState.IsValid && ExecuteCommand(_validatePasswordRules) && model.Password != model.ConfirmPassword)
            {
                ModelState.AddModelError(model, x => x.ConfirmPassword,
                                         "The two password fields didn't match.");
            }

            if (!ModelState.IsValid)
                return View(model);

            try
            {
                var userIdentity = ExecuteCommand(new ResetPasswordCommand(model.Token, model.Password));
                Response.SetFormsAuthentication(userIdentity);
            }
            catch(AggregateException ex)
            {
                if (ex.GetBaseException() is ResetPasswordCommand.AuthException)
                    ModelState.AddModelError("AUTH", ex.GetBaseException().Message);
            }

            if (!ModelState.IsValid)
                return View(model);

            return RedirectToAction("complete");
        }

        public ActionResult Complete()
        {
            return View();
        }
    }
}