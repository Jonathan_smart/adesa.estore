using System;
using System.Net;
using System.Web.Mvc;
using Sfs.EStore.Web.App.Areas.Account.Models;
using Sfs.EStore.Web.App.Code.Mvc;
using Sfs.EStore.Web.App.Controllers;

namespace Sfs.EStore.Web.App.Areas.Account.Controllers
{
    [AuthoriseActivity("/Users/Account/ResetPassword")]
    public class ForgottenPasswordController : ApplicationController
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(ForgottenPasswordIndexModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            ApiProxy.Value.PostAsJson("forgottenpassword", new
            {
                model.Username
            })
                .AcceptJson().SendAsync()
                .ContinueWith(x =>
                {
                    if (x.Result.StatusCode == HttpStatusCode.NotFound)
                    {
                        ModelState.AddModelError(model, m => m.Username, "No record of user found.");
                        return;
                    }

                    if (x.Result.StatusCode == HttpStatusCode.Forbidden)
                    {
                        ModelState.AddModelError(model, m => m.Username, "User is locked out.");
                        return;
                    }

                    if (!x.Result.IsSuccessStatusCode)
                    {
                        ModelState.AddModelError("", "Failed to reset password.");
                        Logger.Error("api /forgottenpassword error: " + x.Result.StatusCode, new Exception(x.Result.Content.ReadAsStringAsync().Result));
                        return;
                    }
                })
                .Wait();

            if (!ModelState.IsValid)
                return View(model);

            return RedirectToAction("nextstep");
        }

        public ActionResult NextStep()
        {
            return View();
        }
    }
}