using System.ComponentModel.DataAnnotations;

namespace Sfs.EStore.Web.App.Areas.Account.Models
{
    public class ResetPasswordIndexModel
    {
        [Required(AllowEmptyStrings = false), Display(AutoGenerateField = false)]
        public string Token { get; set; }
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required(AllowEmptyStrings = false), DataType(DataType.Password), Display(Name="Confirm Password")]
        public string ConfirmPassword { get; set; }
    }
}