using System.ComponentModel.DataAnnotations;

namespace Sfs.EStore.Web.App.Areas.Account.Models
{
    public class ForgottenPasswordIndexModel
    {
        [Required(AllowEmptyStrings = false), Display(Name = "Username/Email")]
        public string Username { get; set; }
    }
}