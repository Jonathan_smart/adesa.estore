using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sfs.EStore.Web.App.Areas.Account.Models
{
    public class RegistrationIndexModel
    {
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password), Display(Name = "Confirm password")]
        public string ConfirmPassword { get; set; }

        [Display(Name="First name")]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [DataType(DataType.EmailAddress), Display(Name = "Email address")]
        public string EmailAddress { get; set; }
        
        [DataType(DataType.PhoneNumber),Display(Name="Phone number")]
        public string PhoneNumber { get; set; }

        public string ReturnUrl { get; set; }

        [Display(Name = "Company name")]
        public string CompanyName { get; set; }
        
        [Display(Name="Job title")]
        public string JobTitle { get; set; }
        
        public string Website { get; set; }
        
        private AddressModel _address = new AddressModel();
        public AddressModel Address
        {
            get { return _address; }
            set { _address = value; }
        }

        private Dictionary<string, string> _data = new Dictionary<string, string>();
        public Dictionary<string, string> Data
        {
            get { return _data; }
            set { _data = value; }
        }
    }

    public class AddressModel
    {
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string TownCity { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
    }
}