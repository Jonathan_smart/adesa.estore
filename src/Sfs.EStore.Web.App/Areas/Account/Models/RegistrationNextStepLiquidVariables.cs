using System.Web;
using Sfs.EStore.Web.App.Code.Security;
using Sfs.EStore.Web.App.Code.Widgets.Liquid.Drops;

namespace Sfs.EStore.Web.App.Areas.Account.Models
{
    public class RegistrationNextStepLiquidVariables : ILiquidVariables
    {
        public RegistrationNextStepLiquidVariables(IWebApplicationIdentity identity, HttpRequestBase httpRequest)
        {
            user = new UserDrop(identity);
            page = new CmsPageDrop(httpRequest);
        }

        // ReSharper disable InconsistentNaming
        public string email_address { get; set; }
        public UserDrop user { get; private set; }
        public CmsPageDrop page { get; private set; }
        // ReSharper restore InconsistentNaming
    }
}