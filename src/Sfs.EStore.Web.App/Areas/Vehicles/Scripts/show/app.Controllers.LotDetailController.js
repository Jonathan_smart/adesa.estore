﻿/// <reference path="./app.js"/>
/// <reference path="~/angular/bootstrap"/>
/// <reference path="~/scripts/jquery-royalslider/"/>
/// <reference path="~/scripts/jquery-360Slider/"/>
(function () {
    "use strict";

    var LotDetailController = function ($scope, $http, $timeout, $window, $log, $modal, watches, lotCallbacks, previousSearch, siteUrls, transportService) {

        $scope.openTransportCost = function (VehicleRegistrationNo) {

            $scope.registrationNo = VehicleRegistrationNo;

            var modalInstance = $modal.open({
                dialogFade: true,
                dialogClass: 'modal',
                backdrop: 'static',
                templateUrl: '/templates/_TransportCost-modal',
                controller: ['$scope', 'customers', 'shippingMethods', 'registrationNo', function ($scope, customers, shippingMethods, registrationNo) {
                    $scope.customers = customers;
                    $scope.shippingMethods = shippingMethods
                    $scope.item = {
                        "customer": customers[0],
                        // "shippingMethod": $scope.shippingMethods[0]
                    };

                    $scope.selectCustomer = function (item) {
                        alert("custoemr load");
                        if (customerNumber !== null)
                            transportService
                                     .shipToAddresses(item.customer.no)
                                     .then(function (response) {
                                         $scope.shipToAddresses = response.data;
                                     });
                    };
                    //Events
                    $scope.selectShippingMethod = function (item) {

                        transportService
                          .find(item.shippingMethod.code)
                              .then(function (response) {
                                  $scope.shippingAgentServices = response.data;

                                  if ($scope.shippingAgentServices[0] === undefined) {
                                      $scope.shippingAgentServices = undefined;
                                      $scope.shipToAddresses = undefined;
                                      $scope.transportCost = 0;
                                      return;
                                  }

                                  item.shippingAgentServices = response.data[0];

                                  if (item.shippingAgentServices.length == 1) {
                                      $scope.shippingAgentService = item.shippingAgentServices[0];
                                  }

                                  transportService
                                    .shipToAddresses(item.customer.no)
                                        .then(function (response) {
                                            $scope.shipToAddresses = response.data;
                                            item.address = response.data[0];
                                            updateTransportCosts(item);
                                        });
                              });
                    };

                    var updateTransportCosts = function (items) {
                        var transportdata = [];
                        var translateModel = {
                            "address": items.address,
                            "customer": items.customer,
                            "shippingAgentServices": items.shippingAgentServices,
                            "shippingMethod": items.shippingMethod,
                            "registrationNo": registrationNo

                        };
                        transportdata.push(translateModel);

                        var currentTransportP;
                        var thisTransportP;
                        thisTransportP = currentTransportP = transportService.quote({
                            items: transportdata.map(function (item) {
                                return {
                                    id: "1",//Only used on Basket Page
                                    shippingMethod: item.shippingMethod.code,
                                    shippingAgent: (item.shippingAgentServices || {}).shippingAgentCode,
                                    shippingAgentService: (item.shippingAgentServices || {}).code,
                                    from: {
                                        vehicleRegistrationNo: item.registrationNo
                                    },
                                    to: {
                                        customerNo: item.customer.no,
                                        addressCode: (item.address || {}).code
                                    }
                                };
                            }),
                            WithVat: false
                        }).then(function success(resp) {
                            $scope.transportCost = resp.data[0].cost;
                        }, function failure(resp) {
                            for (var i = 0; i < items.length; i++) {
                                $scope.transportCost = null;
                            }
                        });
                    };

                    $scope.close = function () {
                        modalInstance.close();
                    };

                }],
                resolve: {
                    customers: function () {
                        return transportService
                               .customer()
                               .then(function (response) {
                                   return response.data;
                               });
                    },
                    shippingMethods: function () {
                        return transportService
                            .shippingMethods()
                            .then(function (response) {
                                return response.data;
                            });
                    },
                    registrationNo: function () {
                        return $scope.registrationNo;
                    }
                }
            });
        };

        $scope.checkFor360 = function (listing) {
            var frameCount = 36;
            $scope.is360 = !angular.isUndefined(listing.images360) && listing.images360.length >= frameCount;
        };

        $scope.initSlider = function (listing) {

            $scope.checkFor360(listing);

            if ($scope.is360) {

                $('#images-phone.royalSlider').royalSlider({
                    loop: true,
                    controlNavigation: 'none'
                });
                $('#images.royalSlider').royalSlider({
                    fullscreen: {
                        enabled: true,
                        buttonFS: true,
                    },
                    loop: true,
                    controlNavigation: 'thumbnails',
                    globalCaption: false,
                    imgWidth: "0px",
                    imgHeight: "0px",
                    arrowsNav: false,
                    autoScaleSlider: true,
                    autoScaleSliderWidth: 3,
                    autoScaleSliderHeight: 1,
                });

                $(".rsFullscreenBtn").css('display', 'none');
                $(".rsOverflow").css('height', '0px');
                $(".royalSlider ").css('height', '90px');

                var slider = $("#images.royalSlider").data('royalSlider');

                slider.ev.on('rsAfterSlideChange', function (event) {
                    if (slider.isFullscreen) {

                    } else {
                        slider.enterFullscreen();
                    }
                });

                slider.ev.on('rsEnterFullscreen', function () {
                    $(".rsOverflow").css('display', 'block');
                    $(".rsFullscreenBtn").css('display', 'block');
                });

                slider.ev.on('rsExitFullscreen', function () {
                    $(".rsFullscreenBtn").css('display', 'none');
                    $(".rsOverflow").removeAttr('display');
                    $(".rsOverflow").css('height', '0px');
                    $(".royalSlider ").css('height', '90px')
                });

                //var _filePrefix = listing.images360.length > 0 ? listing.images360[0].name.slice(0, listing.images360[0].name.lastIndexOf('_') + 1) : "";

                $scope.ImageUrl = 'http://92a3fcafbf5440f0c8df-db976de5d021e8433af17c6655564ef1.r93.cf3.rackcdn.com/';

                $log.log("look in:" + $scope.ImageUr);
                // $log.log("file prefix:" + _filePrefix);

                $scope.car =
                        $('.car').ThreeSixty({
                            totalFrames: 36, // Total no. of image you have for 360 slider
                            endFrame: 36, // end frame for the auto spin animation
                            currentFrame: 1, // This the start frame for auto spin
                            imgList: '.threesixty_images', // selector for image list
                            progress: '.spinner', // selector to show the loading progress
                            imagePath: $scope.ImageUrl, // path of the image assets
                            //filePrefix: _filePrefix, // file prefix if any
                            ext: '.jpg', // extention for the assets
                            height: 331,
                            width: 449,
                            navigation: false,
                            responsive: true,
                            playSpeed: 190, autoplayDirection: 1
                        });

                $('.custom_previous').bind('click', function (e) {
                    $scope.car.previous();
                });

                $('.custom_next').bind('click', function (e) {
                    $scope.car.next();
                });

                $('.custom_play').bind('click', function (e) {
                    $scope.car.play();
                });

                $('.custom_stop').bind('click', function (e) {
                    $scope.car.stop();
                });
            } else {

                $('#images-phone.royalSlider').royalSlider({
                    loop: true,
                    controlNavigation: 'none'
                });

                $('#images.royalSlider').royalSlider({
                    fullscreen: {
                        enabled: true,
                        buttonFS: true
                    },
                    loop: true,
                    controlNavigation: 'thumbnails'
                });
                jQuery('.royalSlider').royalSlider('updateSliderSize', true);
            }
        };

        $scope.lotCallbacks = lotCallbacks;
        $scope.loaded = false;

        $scope.lotStatusChanged = false;

        $scope.lot = {};
        $scope.user = {};

        $scope.financeQuote = {};

        var user = ($scope.auth.currentUser || {});

        $scope.request = {
            offer: 0,
            firstName: user.firstName || '',
            lastName: user.lastName || '',
            email: user.emailAddress || '',
            telephoneNumber: user.phoneNumber || '',
            comment: ''
        };

        $scope.bind = function (value) {
            $scope.request.offer = value;
        };

        $scope.firstName = $scope.request.firstName;
        $scope.lastName = $scope.request.lastName;
        $scope.email = $scope.request.email;
        $scope.telephoneNumber = $scope.request.telephoneNumber == '' ? "Na" : $scope.request.telephoneNumber;

        $scope.close = function () {
            $scope.request.message = "";
            angular.element('#offerModal').modal('hide');
        };

        $scope.submit = function (request) {
            $scope.submitting = true;
            $scope.submitted = false;
            $scope.success = undefined;

            $http.post(siteUrls.path('api/makeoffer'),
                {
                    lotid: $scope.lot.listingId,
                    offer: $scope.request.offer,
                    comments: $scope.request.message,
                })
                .then(function (res) {
                    $scope.success = (res.data === "true");
                }, function () {
                    $scope.success = false;
                })['finally'](function () {
                    $scope.submitting = false;
                    $scope.submitted = true;
                    $scope.close();
                });
        };

        $scope.$on('finance-quotes-received', function (e, quotes) {
            var bestPriceQuote = quotes.reduce(function (previousValue, currentValue, index, array) {
                if (!previousValue) return currentValue;

                return previousValue.regularPayment > currentValue.regularPayment
                  ? currentValue
                  : previousValue;
            });

            $scope.bestPriceFinanceExample = bestPriceQuote;
        });

        $scope.previousSearch = previousSearch.url();

        $scope.watches = {
            isWatched: false,
            remove: function (listingId) {
                watches.remove(listingId).then(function (isWatched) { $scope.watches.isWatched = isWatched; });
            },
            add: function (listingId) {
                watches.add(listingId).then(function (isWatched) { $scope.watches.isWatched = isWatched; });
            }
        };
        watches.get($scope.id).then(function (isWatched) { $scope.watches.isWatched = isWatched; });

        $.get(siteUrls.path('vehicles/track/lotview/' + $scope.id));

        var load = function (include) {
            return $http.get(siteUrls.path('api/lots/' + $scope.id + '?include=' + (include || '')),
                {
                    params: {}
                })
                .then(function (res) {
                    $scope.loaded = true;
                    $scope.lot = res.data;
                    return $scope.lot;
                });
        };
        load().then(function (lot) {
            $scope.loading = true;

            $scope.listing = lot;
            $scope.initSlider(lot);

            $scope.loading = false;
        });

        $scope.min = function (model) {
            return (model - 100);
        };

        var timer = undefined;
        $scope.queueRefresh = function (interval) {
            var actualInterval = interval || 0;
            if (timer) {
                $timeout.cancel(timer);
            }

            return timer = $timeout(function () {
                return load('biddingstatus,sellingstatus');
            }, actualInterval);
        };

        var lastKnownStatus;
        $scope.queueRefreshCallback = function () {

            if (!$scope.lot.biddingStatus || $scope.lot.sellingStatus.sellingState != 'Active') {
                return;
            }
            $scope.Processing = false;

            if ($scope.lot.sellingStatus.timeLeft == 0 && $scope.lot.sellingStatus.sellingState == 'Active') {
                $scope.Processing = true;
            }

            if (lastKnownStatus && lastKnownStatus != $scope.lot.sellingStatus.sellingState) {
                $scope.Processing = false;
                $window.location.reload();
                return;
            }
            console.log("Refresh interval started");
            lastKnownStatus = $scope.lot.sellingStatus.sellingState;

            var timeLeft = $scope.lot.sellingStatus.timeLeft + 2000;

            var refreshInterval = 5000;// >1hr @ 30m

            if (timeLeft === 0) { }
            else if (timeLeft <= 5000) refreshInterval = 500; // <=1m @ 1s
            else if (timeLeft <= 60000) refreshInterval = 1000; // <=1m @ 1s
            else if (timeLeft <= 300000) refreshInterval = 5000; // <=5m @ 10s
            else if (timeLeft <= 3600000) refreshInterval = 5000; // <=1hr @ 1m
            else refreshInterval = 5000; // >1hr @ 30m

            if (refreshInterval >= 0)
                $scope.queueRefresh(refreshInterval).then($scope.queueRefreshCallback);
        };

        $scope.refresh = function () {
            $scope.queueRefresh().then($scope.queueRefreshCallback);
        };

        $scope.refresh();

        $scope.showSpec = function () {
            var tab = $('#specificationTab')[0];
            window.scrollTo(0, tab.offsetTop - 4);
            $timeout(function () {
                $('#specificationTab a').click();
            });
        };

        $scope.showFinance = function () {
            var tab = $('#financeTab')[0];
            window.scrollTo(0, tab.offsetTop - 80);
            $timeout(function () {
                $('#financeTab a').click();
            });
        };

        $scope.showBidHistory = function () {
            $scope.queueRefresh()
                .then(function () {
                    $modal.open({
                        templateUrl: 'bid-history-dialog.html',
                        resolve: {
                            lot: function () {
                                return $scope.lot;
                            }
                        },
                        controller: ['$scope', '$modalInstance', 'lot', function ($scope, $modalInstance, lot) {
                            $scope.lot = lot;
                            $scope.close = function () {
                                $modalInstance.dismiss();
                            };
                        }]
                    });
                });
        };
        //---Bidding       
        $scope.showMakeBid = function (bidPrice) {

            if (angular.isUndefined(bidPrice) || bidPrice == null) return;

            $scope.bidPrice = bidPrice;

            $scope.BuyItNowModal = $modal.open({
                templateUrl: 'bid-confirm-dialog.html',
                resolve: {

                    lot: function () {
                        return $scope.lot;
                    },
                    bid: function () {
                        return $http.get(siteUrls.path('vehicles/bids/makebid/'), {
                            params: {
                                "id": $scope.lot.listingId,
                                "maximumBid": bidPrice,
                            }
                        }).success(function (result) {
                            $scope.refresh();
                        });
                    }
                },
                controller: [
                    '$scope', '$modalInstance', 'lot', 'bid', function ($scope, $modalInstance, lot, bid) {

                        $scope.lot = lot;
                        $scope.bid = bid.data;
                        $scope.SelectedCustomer = "";

                        $scope.makeBid = function () {

                            $http.get(siteUrls.path('vehicles/bids/Confirm/'), {
                                params: {
                                    "id": $scope.lot.listingId,
                                    "maximumBid": $scope.bid.CurrentBid.Amount,
                                }
                            }).success(function (result) {
                                load('biddingstatus,sellingstatus');
                                $scope.close();
                            });
                        }

                        $scope.buyItNow = function () {

                            if ($scope.SelectedCustomer.length == 0) return;

                            return $http.post(siteUrls.path('vehicles/buyitnow/index/'), {
                                lotId: $scope.lot.listingId,
                                customerNo: $scope.SelectedCustomer
                            }).then(function (result) {

                                if (result.data) {
                                    $scope.bid.ErrorMessage = result.data;
                                } else {
                                    $timeout(function () {
                                        load('biddingstatus,sellingstatus');
                                    }, 1000).then(function () { });
                                    $scope.close();
                                }
                            });
                        }
                        $scope.changed = function (value) {
                            $scope.SelectedCustomer = value;
                        }

                        $scope.close = function () {
                            $modalInstance.dismiss();
                        };

                        $scope.refresh = function () {
                            $modalInstance.dismiss();
                            $window.location.reload();
                        };
                    }
                ]
            });

            $scope.refresh();
        }

        $scope.buyItNow = function () {

            var modalInstance = $modal.open({
                templateUrl: 'buyItNow-dialog.html',
                resolve: {

                    lot: function () {
                        return $scope.lot;
                    },
                    model: function () {
                        return $http.get(siteUrls.path('vehicles/buyitnow/index/'), {
                            params: {
                                "id": $scope.lot.listingId
                            }
                        }).success(function (result) {
                            return result;
                        });
                    }
                },
                controller: [
                    '$scope', '$modalInstance', 'lot', 'model', function ($scope, $modalInstance, lot, model) {

                        $scope.lot = lot;
                        $scope.model = model.data;

                        $scope.SelectedCustomer = "";

                        $scope.buyItNow = function () {

                            if ($scope.SelectedCustomer.length == 0) return;

                            return $http.post(siteUrls.path('vehicles/buyitnow/index/'), {
                                lotId: $scope.lot.listingId,
                                customerNo: $scope.SelectedCustomer
                            }).then(function (result) {
                                if (result.data) {
                                    $scope.model.ErrorMessage = result.data;
                                } else {
                                    $timeout(function () {
                                        load('biddingstatus,sellingstatus');
                                    }, 2000).then(function () {

                                    });

                                    $scope.close();
                                }
                            });
                        }

                        $scope.changed = function (value) {
                            $scope.SelectedCustomer = value;
                        }

                        $scope.close = function () {
                            $modalInstance.dismiss();
                        };

                        $scope.refresh = function () {
                            $modalInstance.dismiss();
                            window.location.href = $window.location;
                        };
                    }
                ]
            });
        }

        $scope.$on("$destroy", function () {
            if (timer) {
                $timeout.cancel(timer);
            }
        });
    };

    angular.module("app.Controllers", [])
        .controller('LotDetailController', ['$scope', '$http', '$timeout', '$window', '$log', '$modal', 'watches', 'lotCallbacks', 'previousSearch', 'siteUrls', LotDetailController])
        .service('transportService', transportService);

    function transportService($http, siteUrls) {
        this.customer = function () {
            return $http.get(siteUrls.path('api/customers'));
        }
        this.shippingMethods = function () {
            return $http.get(siteUrls.path('api/shippingmethods'));
        }
        this.shipToAddresses = function (customerNo) {
            if (!customerNo) return;
            return $http.get(siteUrls.path('api/customers/' + customerNo + '/ship-to-addresses?type=vehicle'));
        }
        this.find = function (shippingMethod) {
            return $http({
                method: 'GET',
                url: siteUrls.path('api/shippingservicesview'),
                params: {
                    method: shippingMethod
                }
            })
        };

        this.quote = function (data) {
            return $http({
                method: 'POST',
                url: siteUrls.path('api/transport'),
                data: data
            });
        };
    };
}());



