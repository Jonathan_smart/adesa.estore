/// <reference path="~/angular"/>
/// <reference path="./app.js"/>
(function () {
  "use strict"
  angular.module('app.Services')
    .directive('showFinanceVideo', [function() {
      return {
        restrict: 'A',
        scope: { showFinanceVideo: '@' },
        controller: ['$scope', '$modal', function($scope, $modal) {

          $scope.show = function(url) {
            var modalInstance = $modal.open({
                templateUrl: 'finance-video-dialog.html',
                controller: ['$scope', '$sce', 'videoUrl', function($scope, $sce, videoUrl) {
                  $scope.videoUrl = $sce.trustAsResourceUrl(videoUrl);
                }],
                resolve: {
                    videoUrl: function () {
                        return url;
                    }
                }
            });
          };
        }],
        link: function(scope, elem, attr) {
          $(elem).on('click', function(ev) {
            scope.show(attr.showFinanceVideo);
          });
        }
      };
    }]);
} ());
