﻿/// <reference path="~/scripts/angular/services/"/>
/// <reference path="~/angular/bootstrap"/>
/// <reference path="~/angular/route"/>
/// <reference path="~/angular/cookies"/>
/// <reference path="~/scripts/angular/services/searches"/>
/// <reference path="~/angular/angularytics"/>
/// <reference path="~/scripts/angular/components/"/>

(function () {
    "use strict";

    angular.module("app.Services", [
        'ui.bootstrap', 'angularytics', 'app.components',
        'prettyDateFilters', 'ngCookies', 'components.searches'
    ]);
    angular.module("app.Controllers", []);

    angular.module("app", ['ngRoute', "app.Services", "app.Controllers", 'lotCallbacksModule',
        'lotWatchModule', 'http-auth-interceptor', 'sfs-auth-service', 'lotsModule', 'urlFilters'])
    .config(['AngularyticsProvider', function (AngularyticsProvider) {
        AngularyticsProvider.setEventHandlers(['Console', 'GoogleUniversal']);
    }]).run(['Angularytics', function (Angularytics) {
        Angularytics.init();
    }]);
    var app = angular.module("app");


    app.filter('noFractionCurrency',
['$filter', '$locale',
function (filter, locale) {
    var currencyFilter = filter('currency');
    var formats = locale.NUMBER_FORMATS;
    return function (amount, currencySymbol) {
        var value = currencyFilter(amount, currencySymbol);
        var sep = value.indexOf(formats.DECIMAL_SEP);
        if (amount >= 0) {
            return value.substring(0, sep);
        }
        return value.substring(0, sep) + ')';
    };
}]);





    app.config(['$httpProvider', '$injector', function ($httpProvider, $injector) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
        $httpProvider.defaults.headers.common['Cache-Control'] = 'no-cache, no-store, must-revalidate';
        $httpProvider.defaults.headers.common['Pragma'] = 'no-cache';
        $httpProvider.defaults.headers.common['Expires'] = '0';

    }]);

    app.run(["$rootScope", "sfsLoginOnRequest", "sfsAuthService", function ($rootScope, sfsLoginOnRequest, sfsAuthService) {
        $rootScope.lotAppVersion = "0.0.0.0";
        $rootScope.auth = sfsAuthService;
    }]);

}());
