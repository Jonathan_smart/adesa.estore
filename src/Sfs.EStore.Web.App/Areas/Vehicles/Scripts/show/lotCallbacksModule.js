﻿/// <reference path="~/angular"/>

(function () {

    var lotCallbacks = function ($http, $modal, Angularytics) {
        var myController = function ($scope, $modalInstance, listingId, listing, siteUrls) {

            $scope.lots = listingId;
            $scope.listing = listing;

            (function() {
                var user = ($scope.auth.currentUser || {});

                $scope.request = {
                    firstName: user.firstName || '',
                    lastName: user.lastName || '',
                    email: user.emailAddress || '',
                    telephoneNumber: user.phoneNumber || '',
                    message: ''
                };
            })();

            $scope.setContactType = function (type) {
                $scope.contactType = type;
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

            $scope.close = function () {
                $modalInstance.close($scope.success);
            };

            $scope.submit = function (request) {
                $scope.submitting = true;
                $scope.submitted = false;
                $scope.success = undefined;

                $http.post(siteUrls.path('api/callbacks'),
                    {
                        lotids: $scope.lots,
                        message: $scope.request.message,
                        firstName: $scope.request.firstName,
                        lastName: $scope.request.lastName,
                        email: $scope.request.email,
                        telephoneNumber: $scope.request.telephoneNumber
                    })
                    .then(function (res) {
                        $scope.success = (res.data === "true");
                    }, function () {
                        $scope.success = false;
                    })['finally'](function() { 
                        $scope.submitting = false; 
                        $scope.submitted = true;
                    });;
            };
        };

        return {
            openForm: function (listingId, listing) {
                var modalInstance = $modal.open({
                    dialogFade: true,
                    dialogClass: 'modal modal-callbacks',
                    backdrop: 'static',
                    keyboard: false,
                    templateUrl: 'callbackform.html',
                    controller: ['$scope', '$modalInstance', 'listingId', 'listing', 'siteUrls', myController],
                    resolve: {
                        listingId: function() { return listingId; },
                        listing: function() { return listing; }
                    }
                });
                return modalInstance;
            }
        };
    };

    angular.module('lotCallbacksModule', [])
        .factory('lotCallbacks', ['$http', '$modal', 'Angularytics', lotCallbacks]);
} ());
