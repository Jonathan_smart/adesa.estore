﻿/// <reference path="./app.js"/>
/// <reference path="~/scripts/jquery-royalslider/"/>
/// <reference path="~/angular/bootstrap"/>

(function () {
    "use strict";

    angular.module('app.Services')
      .directive('reserveVehicle', ['$log', function ($log) {
          return {
              restrict: 'A',
              scope: {
                  listing: '=reserveVehicle',
                  // Options: 
                  // - 'confimation': Show a confirmation dialog
                  // - 'basket': [default] Go to the basket page
                  // - fn: Call the specified function
                  onReserved: '=',
                  markup: "=markUp",
                  dealer: "=dealer"
              },
              link: function (scope, elem, attr) {
                  $(elem).on('click', function (ev) {
                      scope.showReservation();
                  });
              },
              controller: ['$scope', '$window', '$modal', '$location', function ($scope, $window, $modal, $location) {
                  var gotoBasketOnReservedCallback = function (result) {
                      $window.location.href = '/reservations#/reservations?new=' + result.basket.id + '&selected=' + result.basket.id;
                  };
                  var showConfirmationOnReservedCallback = function (result) {
                      var modalInstance = $modal.open({
                          dialogFade: true,
                          templateUrl: 'reservation-confirmation.html',
                          controller: ['$scope', 'listing', function ($scope, listing) {
                              $scope.listing = listing
                          }],
                          resolve: {
                              listing: function () {
                                  return result.listing;
                              }
                          }
                      });
                  };
                  $scope.showReservation = function () {
                      var listing = $scope.listing;
                      $log.log('Showing reservation: %o', $scope.listing);
                      $log.log('Showing markup: %o', $scope.markup);
                      var modalInstance = $modal.open({
                          dialogFade: true,
                          backdrop: 'static',
                          keyboard: false,
                          templateUrl: 'reservation-form.html',
                          controller: ['$scope', '$http', '$timeout', '$log', '$modalInstance', 'listing', 'markup', 'siteUrls', '$window', ModalInstanceController],
                          resolve: {
                              listing: function () {
                                  return listing;
                              },
                              markup: function () {
                                  return $scope.markup;
                              }
                          }
                      });

                      function calculateCallback(onReserved) {
                          var defaultCallback = gotoBasketOnReservedCallback;

                          if (!onReserved) return defaultCallback;
                          if (angular.isFunction(onReserved)) return onReserved;

                          if (onReserved === 'confirmation') return showConfirmationOnReservedCallback;
                          if (onReserved === 'basket') return gotoBasketOnReservedCallback;

                          return defaultCallback
                      }

                      modalInstance.result.then(function success(basketId) {
                          var fn = calculateCallback($scope.onReserved);
                          fn({
                              listing: listing,
                              basket: {
                                  id: basketId
                              }
                          });
                      }, function dismissed() { });
                  };
              }]
          };
      }]);

    var ModalInstanceController = function ($scope, $http, $timeout, $log, $modalInstance, listing, markup, siteUrls, $window) {

        $scope.lot = listing;

        $scope.reserve = function () {
            $scope.reserving = true;
            $scope.errored = false;
            //console.log("modal" + markup);
            $http({
                method: 'POST',
                url: siteUrls.path('api/basket/actions/add_listing/'),
                data: {
                    listingId: listing.listingId,
                    extendedExpiry: $window.__sfs.isWithinRetail,
                    markup: markup
                }
            })
                .then(function (res) {
                    $log.info('Basket created: ', res.data);
                    $modalInstance.close(res.data.basket.id);
                }, function (res) {
                    $scope.errored = res.data.message;
                    $scope.reserving = false;
                    $log.error(res);
                })['finally'](function () {
                });
        };
    };
}());
