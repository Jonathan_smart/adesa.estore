﻿/// <reference path="./app.js"/>

(function () {
    "use strict";

    var LotFinanceController = function ($rootScope, $scope, $http, $modal, siteUrls) {

        $scope.requested = {};
        $scope.params = {};
        $scope.params.deposit = 1000;



        $scope.setDeposit = function (make, model, year) {
            $http.get(siteUrls.path('/Areas/Vehicles/Scripts/show/deposits.json'))
                      .success(function (data) {

                          if (data[0].Make === make && data[0].Model === model && year <= 1) $scope.params.deposit = data[0].Deposit;

                      });
        }


        $scope.params.term = 60;
        $scope.params.annualMileage = 10000;

        $scope.getQuote = function () {
            var success = function (res) {
                $scope.requested = angular.copy($scope.params);

                var quotes = res.data;

                $scope.financeQuote = quotes;

                $scope.error = !quotes || quotes.length == 0;

                if (quotes.length > 0)
                    $scope.$emit('finance-quotes-received', $scope.financeQuote);
            };

            var error = function () {
                $scope.error = true;
            };

            $http.get(siteUrls.path('api/lots/' + $scope.id + '/consumerfinance'),
                {
                    params: $scope.params
                })
                .then(success, error);
        };

        $scope.$watch('lot', function (newValue, oldValue) {
            if (!newValue || !newValue.sellingStatus || !newValue.sellingStatus.currentPrice) return;
            if (oldValue && oldValue.sellingStatus && newValue.sellingStatus.currentPrice === oldValue.sellingStatus.currentPrice) return;

            if ($scope.params.deposit > 0)
                $scope.getQuote();
        });
    };

    angular.module("app.Controllers")
        .controller('LotFinanceController', ['$rootScope', '$scope', '$http', '$modal', 'siteUrls', LotFinanceController]);

}());