﻿/// <reference path="./app.js"/>
/// <reference path="~/scripts/jquery-royalslider/"/>
/// <reference path="~/angular/bootstrap"/>

(function () {
    var ScheduledLotDetailController = function ($scope, $http, watches, lotCallbacks, siteUrls) {
        $scope.watches = watches;
        $scope.lotCallbacks = lotCallbacks;
        $scope.loaded = false;
        $scope.lotStatusChanged = false;

        $scope.watches.get($scope.id);

        var load = function () {
            return $http.get(siteUrls.path('api/lots/' + $scope.id),
                {
                    params: {}
                })
                .then(function (res) {
                    $scope.loaded = true;
                    $scope.lot = res.data;
                    var initialStatus = $scope.previousLotStatus;
                    var currentStatus = $scope.lot.status;
                    if (initialStatus && currentStatus !== initialStatus) {
                        $scope.lotStatusChanged = true;
                    }
                    $scope.previousLotStatus = $scope.lot.status;
                });
        };
        load();

        $('#images.royalSlider').royalSlider({
            fullscreen: {
                enabled: true
            },
            loop: true,
            controlNavigation: 'thumbnails',
            globalCaption: true
        });
    };

    angular.module("app.Controllers")
        .controller('ScheduledLotDetailController', ['$scope', '$http', 'watches', 'lotCallbacks', 'siteUrls', ScheduledLotDetailController]);
} ());