/// <reference path="~/angular"/>
/// <reference path="./app.js"/>
(function () {
  "use strict"
  angular.module('app.Services')
    .directive('usersAlsoViewed', [function() {
      return {
        restrict: 'EA',
        scope: { listingId: '@', equipmentItemsToInclude: '@', take: '@' },
        templateUrl: 'users-also-viewed.html',
        controller: ['$scope', '$http', '$log', 'siteUrls', function($scope, $http, $log, siteUrls) {
          $log.log('Users also viewed - Id: %s; equipment: %s; take: %s', $scope.listingId, $scope.equipmentItemsToInclude, $scope.take);
          $scope.working = true;
      
          if (!$scope.listingId) {
            $log.warn('Listing Id is not set. User also viewed listings will not be retrieved.');
            return;
          }
          var listingsPromise = $http.get(siteUrls.path('api/lots/' + $scope.listingId + '/usersalsoviewed'),
              {
                  params: {
                      top: ($scope.take || 4)
                  }
              })
              .then(function (res) {
                  $scope.listings = res.data;
                  $scope.working = false;
                  return $scope.listings;
              });

            listingsPromise.then(function(listings) {
              (function getListingEquipments() {
                  if (!$scope.equipmentItemsToInclude && !parseInt($scope.equipmentItemsToInclude)) return;
                  $scope.equipments = {};
                  var req = {
                      url: siteUrls.path('api/listings/vehicle-equipment'),
                      method: 'GET',
                      params: {
                          limit: $scope.equipmentItemsToInclude,
                          id: listings.map(function(l) {
                              return l.listingId;
                          })
                      }
                  };
                  $http(req).then(function success(resp) {
                      var results = resp.data;
                      var equipments = {};
                      results.forEach(function(vehicle) {
                          equipments[vehicle.listingId] = vehicle.equipment.items;
                      });
                      $scope.equipments = equipments;
                  });
              })();
            });
        }]
      };
    }]);
} ());
