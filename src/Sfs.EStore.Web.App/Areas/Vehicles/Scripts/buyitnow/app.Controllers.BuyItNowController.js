﻿/// <reference path="./app.js"/>

(function (module) {

    var Controller = function ($scope, $http, $location, $log, customersService, shippingService, siteUrls) {

        $scope.listings = [];
        var selected = '';
        if ($location.search().selected) {
            var selectedSearch = $location.search().selected.toString();

            if (typeof (selectedSearch) === 'string')
                selectedSearch = selectedSearch.split(',');
            selected = selectedSearch;
        }

        $scope.returnToUrl = $location.search().backtourl;

        if (selected) {
            $http.post(siteUrls.path('api/lots'), { lotIds: selected })
                .then(function (res) {
                    $log.log(res.data);
                    for (var i = 0; i < res.data.length; i++) {
                        $scope.listings.push(res.data[i]);
                    }
                }, function (res) {
                    $log.error(res);
                });
        }

        $scope.order = {};

        customersService.find().then(function (customers) {
            customers = customers.map(function (c) {
                c.idAndName = c.id + ': ' + c.name;
                return c;
            });
            customers.forEach(function (c) {
                c.deliveryAddresses.unshift({ id: null, name: '<confirm later>' });
            });
            $scope.customers = customers;

            if ($scope.customers.length == 1) {
                $scope.order.customer = $scope.customers[0];
            }
        });

        shippingService.find().then(function (shipping) {
            $scope.shipping = shipping;
        });

        $scope.$watch('order.shippingMethod', function (n) {
            $log.log('order.shippingMethod: ' + (n || { id: 'null' }).id);
            $scope.order.shipTo = undefined;
        });
        $scope.$watch('order.customer', function () {
            $scope.order.shipTo = undefined;
        });

        $scope.confirmOrder = function () {
            $scope.busy = true;
            var listing = $scope.listings[0];

            var data = {
                customerId: $scope.order.customer.id,
                shippingMethodId: $scope.order.shippingMethod.id,
                shipToAddressId: $scope.order.shipTo ? $scope.order.shipTo.id : undefined
            };

            $log.log(data);

            $scope.confirmHasErrored = false;
            $http.post(siteUrls.path('api/lots/' + listing.listingId + '/buyitnow'), data)
                .then(function(res) {
                    $log.info(res);
                    $scope.busy = false;
                    $location.path('/checkout/complete').search({ orderNumbers: res.data.salesOrderIds.join(','), failedLotIds: res.data.failedLotIds.join(',') });
                }, function(err) {
                    $scope.busy = false;
                    $log.error(err);
                    $scope.confirmHasErrored = true;
                });
        };

    };
    module.controller('BuyItNowController', ['$scope', '$http', '$location', '$log', 'customersService', 'shippingService', 'siteUrls', Controller]);
} (angular.module('app.Controllers')));