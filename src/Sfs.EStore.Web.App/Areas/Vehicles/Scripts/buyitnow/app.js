﻿/// <reference path="~/angular"/>
/// <reference path="~/angular/route"/>
/// <reference path="~/angular/bootstrap"/>
/// <reference path="~/scripts/angular/services/"/>

(function () {
    "use strict";

    angular.module("app.Services", ['sfs-auth-service', 'ui.bootstrap']);
    angular.module("app.Controllers", []);

    angular.module("app", ["app.Services", "app.Controllers", 'ngRoute', "urlFilters", "prettyDateFilters"]);
    var app = angular.module('app');

    app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    }]);

 

    app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
        $routeProvider
            .when('/checkout', {
                templateUrl: 'order-checkout.html',
                controller: 'BuyItNowController'
            })
            .when('/checkout/complete', {
                templateUrl: 'order-checkout-success.html',
                controller: 'CheckoutOrderSuccessController',
                resolve: basketOrderSuccessControllerResolver
            })
            .otherwise({ redirectTo: '/checkout' });
    }]);

    var basketOrderSuccessControllerResolver = {
        orders: ['$http', 'siteUrls', '$location', function ($http, siteUrls, $location) {
            return $http.get(siteUrls.path('api/orders') + '?' + ($location.search().orderNumbers || '').split(',')
                    .map(function (i) {
                        return 'ids=' + i;
                    })
                    .join('&'))
                .then(function (res) {
                    return res.data;
                });
        }],
        failedLots: ['$http', 'siteUrls', '$location', function ($http, siteUrls, $location) {
            return $http.post(siteUrls.path('api/lots'), { lotIds: ($location.search().failedLotIds || '').split(',') })
                .then(function (res) {
                    return res.data;
                });
        }]
    };

    app.factory('customersService', ['siteUrls', '$q', '$http', function (siteUrls, $q, $http) {
        return {
            find: function () {
                return $http.get(siteUrls.path('api/customers'))
                    .then(function (res) {
                        return res.data;
                    });
            }
        };
    }]);

    app.factory('shippingService', ['siteUrls', '$q', '$http', function (siteUrls, $q, $http) {
        return {
            find: function () {
                var deferred = $q.defer();
                $http.get(siteUrls.path('api/shipping'))
                    .then(function (res) {
                        deferred.resolve(res.data);
                    }, function (res) {
                        deferred.reject(res);
                    });

                return deferred.promise;
            }
        };
    }]);

    app.run(['$rootScope', '$log', 'sfsAuthService', 'sfsLoginOnRequest', function ($rootScope, $log, sfsAuthService, sfsLoginOnRequest) {
        $rootScope.appVersion = "0.0.0.0";

        $rootScope.auth = sfsAuthService;
        $rootScope.logger = $log;
    }]);
}());