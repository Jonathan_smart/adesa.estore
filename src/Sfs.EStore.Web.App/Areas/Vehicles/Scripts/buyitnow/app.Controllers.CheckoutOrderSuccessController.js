﻿/// <reference path="./app.js"/>

(function (module) {
    var Controller = function ($scope, orders, failedLots, $log) {
        $scope.orders = orders;
        $scope.failedLots = failedLots;

        $scope.linesAmountTotal = function (lines) {
            var total = 0;

            if (!lines) return undefined;

            for (var i = 0; i < (lines || []).length; i++) {
                total += lines[i].amount.amount;
            }

            return { amount: total, currency: 'GBP' };
        };

        $scope.linesAmountIncVatTotal = function (lines) {
            var total = 0;

            if (!lines) return undefined;

            for (var i = 0; i < (lines || []).length; i++) {
                total += lines[i].amountInclVat.amount;
            }

            return { amount: total, currency: 'GBP' };
        };

    };
    module.controller('CheckoutOrderSuccessController', ['$scope', 'orders', 'failedLots', '$log', Controller]);
} (angular.module('app.Controllers')));