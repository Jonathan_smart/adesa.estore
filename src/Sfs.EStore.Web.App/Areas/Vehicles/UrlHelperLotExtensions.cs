﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using Sfs.EStore.Web.App.Code;

namespace Sfs.EStore.Web.App.Areas.Vehicles
{
    public static class StringExtensions
    {
        public static string ToSlug(this string @this)
        {
            if (@this == null) return "";
            var noSpaces = new Regex(@"\s+").Replace(@this, "-");

            return new Regex(@"([^\w-])").Replace(noSpaces, "").ToLower();
        }
    }

    public static class ListingUrl
    {
        public static string Listing(object listingId, string slug = "")
        {
            return string.Format("/vehicles/{0}/{1}", listingId, slug);
        }

        public static string Retail(object listingId, string slug = "")
        {
            return string.Format("/retail/listings/{0}/{1}", listingId, slug);
        }
    }

    public static class UrlHelperLotExtensions
    {
        public static string Lot(this UrlHelper @this, object lotNumber, string slug = "")
        {
            return ListingUrl.Listing(lotNumber, slug);
        }

        public static string RetailLot(this UrlHelper @this, object lotNumber, string slug = "")
        {
            return ListingUrl.Retail(lotNumber, slug);
        }
    }

    public static class SearchExtensions
    {
        public static string Search(this UrlHelper @this, dynamic query = null)
        {
            return SearchUrlForArea(@this, query, area: "");
        }

        public static string AuctionSearch(this UrlHelper @this, dynamic query = null)
        {
            return SearchUrlForArea(@this, query, area: "");
        }

        public static string RetailSearch(this UrlHelper @this, dynamic query = null)
        {
            return SearchUrlForArea(@this, query, area: "retail");
        }

        private static string SearchUrlForArea(UrlHelper @this, dynamic query = null, string area = "")
        {
            if (query == null)
                return @this.Action("index", "search", new { area });

            var json = JObject.Parse(JsonConvert.SerializeObject(query, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            }));
            json["area"] = area;

            var routeValuesDictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(HttpUtility.UrlDecode(json.ToString()));

            var routeValues = new RouteValueDictionary(routeValuesDictionary);

            return @this.Action("index", "search", routeValues);
        }

        public static bool HasPreviousSearch(this HttpContextBase @this)
        {
            return null != PreviousSearch(@this);
        }

        public static string PreviousSearch(this UrlHelper @this, dynamic fallback = null)
        {
            var cookie = PreviousSearch(@this.RequestContext.HttpContext);
            if (cookie == null)
                return Search(@this, fallback);

            return Search(@this, JsonConvert.DeserializeObject(HttpUtility.UrlDecode(cookie.Value)));
        }

        public static string PreviousRetailSearch(this UrlHelper @this, dynamic fallback = null)
        {
            var cookie = PreviousSearch(@this.RequestContext.HttpContext);
            if (cookie == null)
                return Search(@this, fallback);

            return RetailSearch(@this, JsonConvert.DeserializeObject(HttpUtility.UrlDecode(cookie.Value)));
        }

        private const string PreviousSearchCookieName = "lastSearch";

        private static HttpCookie PreviousSearch(HttpContextBase context)
        {
            return context.Request.Cookies[PreviousSearchCookieName];
        }
    }

    public static class UrlHelperExtensions
    {
        public static string AbsolutePath(this UrlHelper @this, string content)
        {
            var path = content;
            var url = new Uri(HttpContext.Current.Request.Url, path);

            return url.AbsoluteUri;
        }

        public static string CurrentPathAndQuery(this UrlHelper @this)
        {
            return @this.RequestContext.HttpContext.Request.Url != null
                       ? @this.RequestContext.HttpContext.Request.Url.PathAndQuery
                       : @this.Content("Lots~/");
        }

        public static string ReturnTo(this UrlHelper @this)
        {
            return @this.RequestContext.HttpContext.Request.Params["BackToUrl"];
        }

        public static string LoginAndReturn(this UrlHelper @this)
        {
            return @this.Action("login", new {area="", controller = "sessions", returnurl = @this.CurrentPathAndQuery()});
        }

        public static string ThemeContent(this UrlHelper @this, string relativePath)
        {
            var themeName = new HttpContextWrapper(HttpContext.Current).CurrentTenant().ThemeName;
            return @this.Content(string.Format("~/content/themes/{0}/{1}", themeName, relativePath));
        }

        public static string UserContent(this UrlHelper @this, string relativePath)
        {
            var key = new HttpContextWrapper(HttpContext.Current).CurrentTenant().Key;
            return @this.Content(string.Format("~/content/usercontent/{0}/{1}", key, relativePath));
        }
    }

    public static class HtmlTimeExtensions
    {
        public static string TimeRemaining(this TimeSpan @this)
        {
            if (@this.TotalDays >= 1)
                return string.Format("{0}d {1}h {2}m", @this.Days, @this.Hours, @this.Minutes);

            if (@this.TotalHours >= 1)
                return string.Format("{0}h {1}m", @this.Hours, @this.Minutes);

            if (@this.TotalMinutes >= 1)
                return string.Format("{0}m {1}s", @this.Minutes, @this.Seconds);

            return @this.Seconds <= 0 ? "Ended" : string.Format("{0}s", @this.Seconds);
        }

        public static string TimeRemainingFromNow(this DateTime @this)
        {
            return (DateTime.Now - @this).TimeRemaining();
        }

        public static bool IsEndingSoon(this TimeSpan @this)
        {
            return (@this <= TimeSpan.Zero) || @this <= new TimeSpan(0, 3, 0, 0);
        }

        public static string ToLongDateAndTimeString(this DateTime @this)
        {
            return String.Format("{0} at {1}", @this.ToLongDateString(), @this.ToShortTimeString());
        }

        public static string ToFuzzyTimeFromNowString(this DateTime @this)
        {
            return FuzzyTimeToWait(@this - SystemTime.UtcNow);
        }

        private static string FuzzyTimeToWait(TimeSpan timeLeft)
        {
            return FuzzyTime(timeLeft) ?? "";
        }

        private static string FuzzyTime(TimeSpan timeLeft)
        {
            if (timeLeft.TotalDays >= 1)
            {
                return string.Format("{0}d {1}h {2}m", timeLeft.Days, timeLeft.Hours, timeLeft.Minutes);
            }

            if (timeLeft.TotalHours >= 1)
            {
                return string.Format("{0}h {1}m", timeLeft.Hours, timeLeft.Minutes);
            }

            if (timeLeft.TotalMinutes >= 1)
            {
                return string.Format("{0}m {1}s", timeLeft.Minutes, timeLeft.Seconds);
            }

            return string.Format("{0}s", (int)timeLeft.TotalSeconds);
        }
    }
}