﻿using System.Web.Mvc;

namespace Sfs.EStore.Web.App.Areas.Vehicles
{
    public class VehiclesAreaRegistration : AreaRegistration
    {
        internal const string VehicleListing = "Vehicles:Listing";

        public override string AreaName
        {
            get
            {
                return "vehicles";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Vehicles:Listing:Brochure",
                "vehicles/{id}.pdf/{*slug}",
                new { action = "brochure", controller = "lots", slug = UrlParameter.Optional },
                new { id = @"(_id_)|(\d*)" }
            );
            context.MapRoute(
                VehicleListing,
                "vehicles/{id}/{*slug}",
                new { action = "show", controller = "lots", slug = UrlParameter.Optional },
                new { id = @"(_id_)|(\d*)" }
            );
            context.MapRoute(
                "Vehicles:Default",
                "vehicles/{controller}/{action}/{id}",
                new { action = "index", controller = "lots", id = UrlParameter.Optional }
            );
        }
    }
}
