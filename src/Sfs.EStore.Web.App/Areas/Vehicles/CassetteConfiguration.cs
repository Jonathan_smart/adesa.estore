using Cassette;
using Cassette.Scripts;

namespace Sfs.EStore.Web.App.Areas.Vehicles
{
    public class CassetteBundleConfiguration : IConfiguration<BundleCollection>
    {
        public void Configure(BundleCollection bundles)
        {
            bundles.AddPerSubDirectory<ScriptBundle>("~/areas/vehicles/scripts");
        }
    }
}