﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Newtonsoft.Json;
using Sfs.Core;
using Sfs.EStore.Web.App.Code;

namespace Sfs.EStore.Web.App.Areas.Vehicles.Models
{
    public class ListingViewModel
    {
        public int ListingId { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AuctionSalesChannel { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AttentionGrabber { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Programme { get; set; }
        public double Markup { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ConditionModel Condition { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public BuyitNowSaleModel BuyitNowSale { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ImageModel PrimaryImage { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ImageModel[] MoreImages { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ImageModel[] Images360 { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ListingSummaryViewModel.DiscountPriceInfoModel DiscountPriceInfo { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public FullListingInfoModel ListingInfo { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public FullSellingStatusModel SellingStatus { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public BiddingInfoModel BiddingStatus { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public FullVehicleInfoModel VehicleInfo { get; set; }

        public bool PricesIncludeVat { get; set; }
        public bool AllowOffers { get; set; }

        public class ConditionModel
        {
            public int Id { get; set; }
        }
        public class BuyitNowSaleModel
        {
            public string User { get; set; }
            public DateTime? Datetime { get; set; }
            public bool IsBuyer { get; set; }
            public Money BuyItNowPrice { get; set; }
        }

        public class DimensionsInfoModel
        {
            public decimal? KerbWeightMin { get; set; }
            public decimal? KerbWeightMax { get; set; }
            public int? LengthMm { get; set; }
            public int? WidthMm { get; set; }
            public int? HeightMm { get; set; }
            public decimal? WheelbaseForVans { get; set; }
        }

        public class FullSellingStatusModel
        {
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int? BidCount { get; set; }
            public MoneyViewModel CurrentPrice { get; set; }
            public string SellingState { get; set; }
            public bool ReserveMet { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public MoneyViewModel BidIncrement { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public MoneyViewModel MinimumToBid { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public MoneyViewModel MaxBid { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public double? TimeLeft { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public IEnumerable<Bid> Bids { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public MoneyViewModel BuyItNowPrice { get; set; }
            public bool ShowBin { get; set; }

            public class Bid
            {
                public string User { get; set; }
                public DateTime Time { get; set; }
                public MoneyViewModel Amount { get; set; }
                public int Position { get; set; }
            }


            public bool IsAt(params LotStatus[] states)
            {
                var currentState = (LotStatus)Enum.Parse(typeof(LotStatus), SellingState, true);

                return states.Any(x => x == currentState);
            }
        }

        public class BiddingInfoModel
        {
            public MoneyViewModel MaximumBid { get; set; }
            public DateTime MaximumBidTime { get; set; }

            public string BiddingStatus { get; set; }

            public bool IsWinning()
            {
                return BiddingStatus == "Winning";
            }
        }

        public class FullListingInfoModel
        {
            public bool BuyItNowAvailable { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]

            public MoneyViewModel BuyItNowPrice { get; set; }
            public DateTime? EndTime { get; set; }
            public DateTime? StartTime { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public long? TimeRemaining { get; set; }

            public string ListingType { get; set; }
        }

        public class FullVehicleInfoModel
        {
            public string Make { get; set; }
            public string Model { get; set; }
            public string Derivative { get; set; }
            public int? CapId { get; set; }
            public string VatStatus { get; set; }
            public string FuelType { get; set; }
            public string Transmission { get; set; }
            public RegistrationModel Registration { get; set; }
            public int? Mileage { get; set; }
            public string VehicleType { get; set; }
            public SpecificationModel Specification { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public string ModelYear { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public TrimInfo Trim { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public SourceInfo Source { get; set; }

            public string EngineDescription { get; set; }
            public string BodyColour { get; set; }
            public string InteriorDescription { get; set; }

            public GradeInfo Grade { get; set; }

            public class GradeInfo
            {
                public Grade Actual { get; set; }

                public class Grade
                {
                    public string Name { get; set; }
                }
            }

            public class TrimInfo
            {
                public string Code { get; set; }
                public string Name { get; set; }
            }

            public class SourceInfo
            {
                public string Code { get; set; }
                public string Name { get; set; }
            }

            public class RegistrationModel
            {
                public string Plate { get; set; }
                public DateTime? Date { get; set; }
                public string Yap { get; set; }
            }

            public class SpecificationModel
            {
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public EconomyInfoModel Economy { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public decimal? Co2 { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public string InsuranceGroup { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public PerformanceInfoModel Performance { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public DimensionsInfoModel Dimensions { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public EngineInfoModel Engine { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public EquipmentInfoModel Equipment { get; set; }
                [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
                public TaxRateModel TaxRate { get; set; }
            }

            public class TaxRateModel
            {
                public char? Band { get; set; }
                public MoneyViewModel RoadTax12Months { get; set; }
            }
        }

        public class EquipmentInfoModel
        {
            public EquipmentItemModel[] Items { get; set; }

            public class EquipmentItemModel
            {
                public string Category { get; set; }
                public string Name { get; set; }
            }
        }

        public class EngineInfoModel
        {
            public int? NumberOfCylinders { get; set; }
            public int? NumberOfValvesPerCylinder { get; set; }
            public string ArrangementOfCylinders { get; set; }
            public int? CapacityCc { get; set; }
        }

        public class PerformanceInfoModel
        {
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public decimal? MaximumSpeedMph { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public decimal? AccelerationTo100KphInSecs { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public decimal? MaximumTorqueNm { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public decimal? MaximumTorqueLbFt { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public decimal? MaximumTorqueAtRpm { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public decimal? MaximumPowerInBhp { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public decimal? MaximumPowerInKw { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public decimal? MaximumPowerAtRpm { get; set; }
        }

        public class EconomyInfoModel
        {
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public decimal? Urban { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public decimal? ExtraUrban { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public decimal? Combined { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public MoneyViewModel CostPerUnitDistanceTravelled { get; set; }
        }

        public TimeSpan? TimeLeft()
        {
            if (!SellingStatus.TimeLeft.HasValue) return null;

            return SystemTime.UtcNow.AddMilliseconds(SellingStatus.TimeLeft.Value) - SystemTime.UtcNow;
        }
    }

    public enum LotStatus
    {
        Scheduled,
        Active,
        Reserved,
        EndedWithSales,
        EndedWithoutSales,
        Cancelled,
        Archived
    }

    public class BidsMakeBidModel : IHaveLotInformationForBids
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public ImageModel PrimaryImage { get; set; }
        public MoneyViewModel CurrentPrice { get; set; }
        public bool PricesIncludeVat { get; set; }
        public string VatStatus { get; set; }
        public DateTime? EndDate { get; set; }
        public TimeSpan? TimeLeft { get; set; }
        public bool showBin { get; set; }

        public MoneyViewModel MinimumBidRequired { get; set; }
        public MoneyViewModel UsersCurrentMaximumBid { get; set; }
        public MoneyViewModel BuyItNowPrice { get; set; }
        public bool HasBids { get; set; }
        public bool CommitBid { get; set; }
        public string[] ErrorMessage { get; set; }
        public MoneyViewModel CurrentBid { get; set; }
        public SelectListItem[] Customers { get; set; }
        public string BackToUrl { get; set; }
    }

    public interface IHaveLotStatusForUser
    {
        bool AuctionHasEnded { get; }
        bool UserIsWinning { get; }
        bool UserHasBid { get; }
        bool HasMetReserve { get; }
    }

    public interface IHaveLotInformationForBids
    {
        int Id { get; }
        string Title { get; }
        ImageModel PrimaryImage { get; }
        MoneyViewModel CurrentPrice { get; }
        bool PricesIncludeVat { get; }
        string VatStatus { get; }
        DateTime? EndDate { get; }
        TimeSpan? TimeLeft { get; }
    }

    public class BuyItNowConfirmPostModel
    {
        [Required]
        public string CustomerNo { get; set; }
        [Required]
        public int Id { get; set; }
        public string BackToUrl { get; set; }
    }

    public class BuyItNowConfirmModel : IHaveLotInformationForBids
    {
        public int Id { get; set; }
        public SelectListItem[] Customers { get; set; }
        public string CustomerNo { get; set; }
        public string Title { get; set; }
        public ImageModel PrimaryImage { get; set; }
        public MoneyViewModel CurrentPrice { get; set; }
        public bool PricesIncludeVat { get; set; }
        public string VatStatus { get; set; }
        public DateTime? EndDate { get; set; }
        public TimeSpan? TimeLeft { get; set; }

        public MoneyViewModel BuyItNowPrice { get; set; }

        public string BackToUrl { get; set; }

        public class CustomerDetail
        {
            public string Name { get; set; }
            public string No { get; set; }
            public string City { get; set; }
        }
    }

    public class BuyItNowSuccessModel : IHaveLotInformationForBids
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public ImageModel PrimaryImage { get; set; }
        public MoneyViewModel CurrentPrice { get; set; }
        public bool PricesIncludeVat { get; set; }
        public string VatStatus { get; set; }
        public DateTime? EndDate { get; set; }
        public TimeSpan? TimeLeft { get; set; }

        public MoneyViewModel BuyItNowPrice { get; set; }

        public string BackToUrl { get; set; }
    }

    public class BidsConfirmModel : IHaveLotInformationForBids
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public ImageModel PrimaryImage { get; set; }
        public MoneyViewModel CurrentPrice { get; set; }
        public bool PricesIncludeVat { get; set; }
        public string VatStatus { get; set; }
        public DateTime? EndDate { get; set; }
        public TimeSpan? TimeLeft { get; set; }
        public bool CommitBid { get; set; }
        public MoneyViewModel MaximumBid { get; set; }

        public bool Type { get; set; }

        public string ErrorMessage { get; set; }
        public string BackToUrl { get; set; }
    }

    public class BidsConfirmationBidModel : IHaveLotInformationForBids, IHaveLotStatusForUser
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public ImageModel PrimaryImage { get; set; }
        public MoneyViewModel CurrentPrice { get; set; }
        public bool PricesIncludeVat { get; set; }
        public string VatStatus { get; set; }
        public DateTime? EndDate { get; set; }
        public TimeSpan? TimeLeft { get; set; }

        public MoneyViewModel MinimumBidRequired { get; set; }
        public MoneyViewModel UsersCurrentMaximumBid { get; set; }

        public bool AuctionHasEnded { get; set; }
        public bool UserIsWinning { get; set; }
        public bool UserHasBid { get; set; }

        public bool HasMetReserve { get; set; }

        public bool HasBuyItNow { get; set; }
        public MoneyViewModel BuyItNowPrice { get; set; }

        public string BackToUrl { get; set; }
    }
}