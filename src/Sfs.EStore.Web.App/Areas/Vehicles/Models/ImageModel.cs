using System;
using Newtonsoft.Json;

namespace Sfs.EStore.Web.App.Areas.Vehicles.Models
{
    public class ImageModel
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Url { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Container { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                this._name = value;
            }
        }

        private string _name;

        public static string CloudFilesImageProxyUrl = "http://d3l2oke4gxqliw.cloudfront.net/";
        // local: "http://localhost/Sfs.CloudFiles.ImageProxy.App/"


        public string RemoveLeadingZero()
        {
            return this.Url.Replace("_0", "_");
        }

        public string AsUrl(string size, string overlay = null)
        {
            var width = size;
            var height = "";
            if (size.Contains("x"))
            {
                var parts = size.Split(new[] { 'x' });
                width = parts[0];
                height = parts.Length > 1 ? parts[1] : null;
            }

            if (!string.IsNullOrWhiteSpace(Container))
            {
                var options = "-";
                if (!string.IsNullOrWhiteSpace(width) || !string.IsNullOrWhiteSpace(height))
                {
                    //options = "c_lpad,b_white";
                    if (!string.IsNullOrWhiteSpace(width))
                    {
                        options += ",w_" + width;
                    }
                    if (!string.IsNullOrWhiteSpace(height))
                    {
                        options += ",h_" + height;
                    }
                }

                if (string.IsNullOrWhiteSpace(overlay))
                    return string.Format("{0}/{1}/{2}/{3}", CloudFilesImageProxyUrl.TrimEnd('/'), Container, options, Name);

                return string.Format("{0}/{1}/{2}/{4}/{3}", CloudFilesImageProxyUrl.TrimEnd('/'), Container, options, Name, overlay);
            }

            if (!string.IsNullOrEmpty(Url))
            {
                if (string.IsNullOrWhiteSpace(width) && string.IsNullOrWhiteSpace(height))
                    return Url;

                if (Url.Contains("?"))
                    return string.Format("{0}&width={1}&height={2}", Url, width, height);

                return string.Format("{0}?width={1}&height={2}", Url, width, height);
            }

            throw new InvalidOperationException(
                "Don't know how to handle image url. Model doesn't have a Url or Container");
        }
    }
}
