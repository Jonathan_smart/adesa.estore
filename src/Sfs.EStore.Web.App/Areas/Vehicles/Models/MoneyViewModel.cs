using System;
using Newtonsoft.Json;

namespace Sfs.EStore.Web.App.Areas.Vehicles.Models
{
    public class MoneyViewModel
    {
        private const int DecimalPlaces = 2; 
        [JsonConstructor]
        public MoneyViewModel(decimal amount, string currency)
        {
            Amount = amount;
            Currency = currency;
        }

        public decimal Amount { get; set; }
        public string Currency { get; set; }

        public override string ToString()
        {
            return string.Format("{0}{1}", Amount.ToString("n2"), Currency);
        }

        public int Units()
        {
            return (int)((double)Amount * Math.Pow(10, DecimalPlaces));
        }

        public static MoneyViewModel From(MoneyViewModel source)
        {
            return new MoneyViewModel(source.Amount, source.Currency);
        }
    }
}