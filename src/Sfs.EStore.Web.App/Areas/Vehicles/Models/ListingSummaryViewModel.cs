using System;
using System.Linq.Expressions;
using Newtonsoft.Json;
using Raven.Abstractions;

namespace Sfs.EStore.Web.App.Areas.Vehicles.Models
{
    public class CurrentOffer
    {
        public string Username { get; set; }
    }

    public class BidsListingSummaryViewModel : ListingSummaryViewModel
    {
        public BiddingInfoModel BiddingInfo { get; set; }

        public class BiddingInfoModel
        {
            public MoneyViewModel MaximumBid { get; set; }
            public string BiddingStatus { get; set; }
        }

        public enum BiddingStatus
        {
            Winning,
            Outbid
        }
    }

    public class EconomyInfoModel
    {
        public decimal? Urban { get; set; }
        public decimal? ExtraUrban { get; set; }
        public decimal? Combined { get; set; }
    }

    public class ListingSummaryViewModel
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DiscountPriceInfoModel DiscountPriceInfo { get; set; }

        public bool BuyItNowSale { get; set; }

        public ImageModel PrimaryImage { get; set; }
        public ImageModel[] MoreImages { get; set; }

        public int ListingId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ConditionModel Condition { get; set; }
        
        public ListingInfoModel ListingInfo { get; set; }

        public SellingStatusModel SellingStatus { get; set; }

        public VehicleInfoModel VehicleInfo { get; set; }

        public bool PricesIncludeVat { get; set; }

        public string Title { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AttentionGrabber { get; set; }

        public class ConditionModel
        {
            public int Id { get; set; }
        }

        public class VehicleInfoModel
        {
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public SourceInfo Source { get; set; }
            public string Make { get; set; }
            public string Model { get; set; }
            public string Derivative { get; set; }
            public string VatStatus { get; set; }
            public string FuelType { get; set; }
            public string Transmission { get; set; }
            public string ExteriorColour { get; set; }
            public RegistrationModel Registration { get; set; }
            public int? Mileage { get; set; }
            public string VehicleType { get; set; }
            public SpecificationModel Specification { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public TrimInfo Trim { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public EngineInfo Engine { get; set; }

            public class RegistrationModel
            {
                public string Plate { get; set; }
                public DateTime? Date { get; set; }
                public string Yap { get; set; }
            }

            public class SpecificationModel
            {
                public EconomyInfoModel Economy { get; set; }
                public decimal? Co2 { get; set; }
            }

            public class TrimInfo
            {
                public string Code { get; set; }
                public string Name { get; set; }
            }

            public class SourceInfo
            {
                public string Code { get; set; }
                public string Name { get; set; }
            }

            public class EngineInfo
            {
                public string Description { get; set; }
            }
        }

        public class SellingStatusModel
        {
            public int BidCount { get; set; }
            public MoneyViewModel CurrentPrice { get; set; }
            public string SellingState { get; set; }
            public bool ReserveMet { get; set; }

            public long? TimeLeft { get; set; }
        }

        public class ListingInfoModel
        {
            public bool BuyItNowAvailable { get; set; }
            public MoneyViewModel BuyItNowPrice { get; set; }
            private DateTime _currentTime = DateTime.Now;
            public DateTime CurrentTime
            {
                get { return _currentTime; }
                set
                {
                    _currentTime = value;
                }
            }

            private DateTime? _endTime;
            public DateTime? EndTime
            {
                get { return _endTime; }
                set
                {
                    _endTime = value;

                    if (value.HasValue)
                        _endTime = value.Value;
                }
            }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public long? TimeRemaining { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public long? TimeLeft { get; set; }

            private DateTime? _startTime;
            public DateTime? StartTime
            {
                get { return _startTime; }
                set
                {

                    _startTime = value;
                    if (value.HasValue)
                        _startTime =  value.Value;
                }
            }

            public string ListingType { get; set; }
        }

        public class DiscountPriceInfoModel
        {
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public MoneyViewModel RetailPrice { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public MoneyViewModel WasPrice { get; set; }

            public bool HasSavingOver(MoneyViewModel currentPrice,
                Expression<Func<DiscountPriceInfoModel, MoneyViewModel>> priceForComparisson)
            {
                if (currentPrice == null) return false;
                if (currentPrice.Amount == 0) return false;

                var comparissonPrice = priceForComparisson.Compile()(this);
                if (comparissonPrice == null) return false;

                return comparissonPrice.Amount > currentPrice.Amount;
            }

            public MoneyViewModel SavingOver(MoneyViewModel price,
                Expression<Func<DiscountPriceInfoModel, MoneyViewModel>> priceForComparisson)
            {
                if (price == null) return new MoneyViewModel(0, "ZZZ");
                if (price.Amount == 0) return new MoneyViewModel(0, price.Currency);

                var comparissonPrice = priceForComparisson.Compile()(this);

                var savingAmount = comparissonPrice.Amount - price.Amount;
                return new MoneyViewModel(savingAmount, price.Currency);
            }
        }


    }
}