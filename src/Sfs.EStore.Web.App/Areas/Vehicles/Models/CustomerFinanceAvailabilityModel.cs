namespace Sfs.EStore.Web.App.Areas.Vehicles.Models
{
    public class CustomerFinanceAvailabilityModel
    {
        public decimal Available { get; set; }
        public string Availability { get; set; }
        public string Message { get; set; }
        public CustomerDetail Customer { get; set; }

        public class CustomerDetail
        {
            public string Name { get; set; }
            public string No { get; set; }
            public string City { get; set; }
        }
    }
}