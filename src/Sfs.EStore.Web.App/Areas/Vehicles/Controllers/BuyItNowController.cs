using System.Linq;
using System.Net.Http;
using System.Web.Mvc;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Controllers;
using Sfs.EStore.Web.App.ThirdParties;
using Sfs.Customers.Adapters.DataAccess;
using Sfs.Core;

namespace Sfs.EStore.Web.App.Areas.Vehicles.Controllers
{
    public class BuyItNowController : LotControllerBase
    {
        private readonly SalesChannel _salesChannel;

        public BuyItNowController(IVehicleSearchHandler vehicleSearchHandler, SalesChannel SalesChannel)
            : base(vehicleSearchHandler)
        {
            this._salesChannel = SalesChannel;
        }

        [HttpGet]
        public JsonResult Index(int id)
        {
            var lot = GetLot(id);

            var customers = SelectListCustomers();

            return this.Json(new BuyItNowConfirmModel
            {
                Customers = customers,
                //  BackToUrl = backToUrl,
                BuyItNowPrice = MoneyViewModel.From(lot.ListingInfo.BuyItNowPrice),
                CurrentPrice = MoneyViewModel.From(lot.SellingStatus.CurrentPrice),
                EndDate = lot.ListingInfo.EndTime,
                Id = lot.ListingId,
                PricesIncludeVat = lot.PricesIncludeVat,
                PrimaryImage = lot.PrimaryImage,
                TimeLeft = lot.TimeLeft(),
                Title = lot.Title,
                VatStatus = lot.VehicleInfo.VatStatus
            }, JsonRequestBehavior.AllowGet);
        }

        private SelectListItem[] SelectListCustomers()
        {
            using (var dbContext = new NavisionContext())
            {
                var contact = new ContactUsernameQuery(_salesChannel, dbContext).Query(User.Identity.Name);

                if (contact == null)
                    return null;

                var customers = new ContactCustomersQuery(contact.ContactNo, contact.BusinessUnit, dbContext).Query();

                var list = customers.Select(x =>
                        new SelectListItem
                        {
                            Text = x.Name + "-" + x.City,
                            Value = x.No
                        });

                return list.ToArray();
            }
        }

        private SelectListItem[] SelectListCustomers(SalesChannel salesChannel)
        {
            using (var dbContext = new NavisionContext())
            {

                var contact = new ContactUsernameQuery(salesChannel, dbContext).Query(User.Identity.Name);

                if (contact == null)
                    return null;

                var customers = new ContactCustomersQuery(contact.ContactNo, contact.BusinessUnit, dbContext).Query();

                var list = customers.Select(x =>
                        new SelectListItem
                        {
                            Text = x.Name + "-" + x.City,
                            Value = x.No
                        });

                return list.ToArray();
            }
        }

        [HttpPost]
        public JsonResult Index(int lotId, string customerNo)
        {
            if (!ModelState.IsValid) return this.Json("ModelState Is Invalid", JsonRequestBehavior.AllowGet);

            ApiProxy.Value.PostAsJson("lots/" + lotId + "/buyitnow", new { CustomerNo = customerNo })
         .AcceptJson().SendAsync()
         .ContinueWith(t =>
         {
             if (!t.Result.IsSuccessStatusCode)
             {
                 Logger.Error(t.Result.Content.ReadAsStringAsync());
                 ModelState.AddModelError(t.Result.RequestMessage.Content.ToString(), t.Result.Content.ReadAsStringAsync().Result.Replace(System.Environment.NewLine, string.Empty).Replace("\\", "").Replace("\"", ""));
             }
         })
         .Wait();

            if (!ModelState.IsValid)
                return this.Json(ModelState.Keys.SelectMany(k => ModelState[k].Errors)
          .Select(m => m.ErrorMessage).ToArray(), JsonRequestBehavior.AllowGet);

            return this.Json(null, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Complete(int id, string backToUrl)
        {
            var lot = GetLot(id);

            if (lot == null)
                return HttpNotFound();

            return View("success", new BuyItNowSuccessModel
            {
                BackToUrl = backToUrl,
                BuyItNowPrice = MoneyViewModel.From(lot.ListingInfo.BuyItNowPrice),
                CurrentPrice = MoneyViewModel.From(lot.SellingStatus.CurrentPrice),
                EndDate = lot.ListingInfo.EndTime,
                Id = lot.ListingId,
                PricesIncludeVat = lot.PricesIncludeVat,
                PrimaryImage = lot.PrimaryImage,
                TimeLeft = lot.TimeLeft(),
                Title = lot.Title,
                VatStatus = lot.VehicleInfo.VatStatus
            });
        }

        private ActionResult LotNotAvailableResult(int id, string backToUrl = null, string flash = null)
        {
            if (!string.IsNullOrWhiteSpace(flash))
                this.Flash(flash);

            if (!string.IsNullOrEmpty(backToUrl))
                return Redirect(backToUrl);

            return RedirectToAction("show", new { controller = "lots", id });
        }
    }
}