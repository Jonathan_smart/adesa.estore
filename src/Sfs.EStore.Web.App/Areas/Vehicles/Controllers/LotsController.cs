﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.ListingViewModelVisitors;
using Sfs.EStore.Web.App.Code.Mvc;
using Sfs.EStore.Web.App.Controllers;
using Sfs.EStore.Web.App.Models;
using Sfs.EStore.Web.App.ThirdParties;
using Slugify;

namespace Sfs.EStore.Web.App.Areas.Vehicles.Controllers
{
    [AuthoriseActivity("/Lots/View")]
    public class LotsController : LotControllerBase
    {
        private readonly IListingViewModelVisitor _modelVisitor;

        public LotsController(IVehicleSearchHandler vehicleSearchHandler, IListingViewModelVisitor modelVisitor) : base(vehicleSearchHandler)
        {
            _modelVisitor = modelVisitor;
        }

        public ActionResult Index()
        {
            return RedirectToAction("Index", "search", new { area = string.Empty });
        }

        public ActionResult Brochure(int id)
        {
            var model = GetLot(id);

            if (model == null)
                throw new HttpException(404, "Lot " + id + " cannot be found");

            var templatePath = Tenant.Value.IntegrationSettings["ListingPdfBrochure/TemplateWithOriginalPricePath"];
            var templateWithoutOriginalPricePath = Tenant.Value.IntegrationSettings["ListingPdfBrochure/TemplateWithoutOriginalPricePath"];
            var brochureWriter =
                new PdfSalesBrochureWriter(templatePath, templateWithoutOriginalPricePath);

            using (var stream = new MemoryStream())
            {
                brochureWriter.WriteToStream(model, stream);
                return new FileContentResult(stream.ToArray(), "application/pdf");
            }
        }

        public ActionResult Show(int id, bool noredirect = false)
        {
            var model = GetLot(id);

            _modelVisitor.Visit(model);

            if (model == null)
                throw new HttpException(404, "Lot " + id + " cannot be found");

            if (model.SellingStatus.IsAt(LotStatus.Reserved, LotStatus.Active))
                return View("ShowActive", model);

            if (model.SellingStatus.IsAt(LotStatus.Scheduled))
                return View("ShowScheduled", model);

            if (model.SellingStatus.IsAt(LotStatus.Archived, LotStatus.EndedWithSales, LotStatus.EndedWithoutSales))
            {
                System.Threading.Thread.Sleep(5000);

                return ShowEndedListing(noredirect, model);
            }

            return View("ShowUnavailable", model);
        }

        private ActionResult ShowEndedListing(bool noredirect, ListingViewModel model)
        {
            Func<ViewResult> defaultResult =
                () => View(model.ListingInfo.ListingType == "FixedPrice" ? "ShowUnavailable" : "ShowEnded", model);

            if (noredirect)
                return defaultResult();

            if (model.ListingInfo.ListingType != "FixedPrice")
                return defaultResult();

            var activeListing =
                ApiProxy.Value
                .PostAsJson("search", new LotsGetParams { Registration = model.VehicleInfo.Registration.Plate })
                    .AcceptJson().SendAsync()
                    .ContinueWith(r =>
                                      {
                                          r.Result.EnsureSuccessStatusCode();

                                          return r.Result.Content
                                              .ReadAsAsync<ListingSummaryViewModel[]>().Result
                                              .FirstOrDefault(x => x.ListingId != model.ListingId);
                                      }).Result;

            if (activeListing == null)
                return defaultResult();

            return RedirectToAction("show",
                                    new
                                    {
                                        id = activeListing.ListingId,
                                        slug = new SlugHelper().GenerateSlug(model.Title)
                                    });
        }
    }
}
