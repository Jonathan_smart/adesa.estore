using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Sfs.Core;
using Sfs.Customers.Adapters.DataAccess;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Code.Mvc;
using Sfs.EStore.Web.App.Controllers;
using Sfs.EStore.Web.App.ThirdParties;

namespace Sfs.EStore.Web.App.Areas.Vehicles.Controllers
{
    public class BidsController : LotControllerBase
    {
        private readonly SalesChannel _salesChannel;

        public BidsController(IVehicleSearchHandler vehicleSearchHandler, SalesChannel SalesChannel)
            : base(vehicleSearchHandler)
        {
            this._salesChannel = SalesChannel;
        }

        [HttpGet]
        public Task<JsonResult> MakeBid(int id, decimal maximumBid, string backtoUrl)
        {
            return Task.Factory.StartNew(() => MakeBidProcess(id, maximumBid, backtoUrl));
        }

        private JsonResult MakeBidProcess(int id, decimal maximumBid, string backToUrl)
        {
            var lot = GetLot(id);

            if (!IsBiddable(lot)) return this.Json("Invalid Lot - no bids can be accepted", JsonRequestBehavior.AllowGet);

            ValidateMaximumBid(maximumBid, lot);

            var maxbid = lot.SellingStatus.Bids.Any() ? lot.SellingStatus.MaxBid : new MoneyViewModel(maximumBid, "GBP");

            var model = new BidsMakeBidModel
            {
                HasBids = lot.SellingStatus.BidCount > 0,
                Id = id,
                CurrentPrice = lot.SellingStatus.CurrentPrice,
                PricesIncludeVat = lot.PricesIncludeVat,
                VatStatus = lot.VehicleInfo.VatStatus,
                UsersCurrentMaximumBid = lot.BiddingStatus != null
                                             ? lot.BiddingStatus.MaximumBid
                                             : null,
                EndDate = lot.ListingInfo.EndTime,
                PrimaryImage = lot.PrimaryImage,
                Title = lot.Title,
                showBin = lot.SellingStatus.Bids.Any() ? lot.SellingStatus.BuyItNowPrice.Amount > 0 && maxbid.Amount < lot.ListingInfo.BuyItNowPrice.Amount && maximumBid >= lot.ListingInfo.BuyItNowPrice.Amount : (lot.ListingInfo.BuyItNowPrice.Amount > 0 && maximumBid > lot.ListingInfo.BuyItNowPrice.Amount),
                BackToUrl = backToUrl,
                BuyItNowPrice = lot.ListingInfo.BuyItNowPrice,
                TimeLeft = lot.TimeLeft(),
                ErrorMessage = ModelState.Keys.SelectMany(k => ModelState[k].Errors)
          .Select(m => m.ErrorMessage).ToArray(),
                CurrentBid = new MoneyViewModel(maximumBid, "GBP"),
                CommitBid = false,
                Customers = SelectListCustomers()
            };

            return this.Json(model, JsonRequestBehavior.AllowGet);
        }
        private SelectListItem[] SelectListCustomers()
        {
            using (var dbContext = new NavisionContext())
            {
                var contact = new ContactUsernameQuery(_salesChannel, dbContext).Query(User.Identity.Name);

                if (contact == null)
                    return null;

                var customers = new ContactCustomersQuery(contact.ContactNo, contact.BusinessUnit, dbContext).Query();

                var list = customers.Select(x =>
                        new SelectListItem
                        {
                            Text = x.Name + "-" + x.City,
                            Value = x.No
                        });

                return list.ToArray();
            }
        }

        [HttpGet]
        public Task<JsonResult> Confirm(int id, decimal maximumBid)
        {
            return Task.Factory.StartNew(() => ConfirmProcess(id, maximumBid));
        }

        private JsonResult ConfirmProcess(int id, decimal maximumBid)
        {
            var lot = GetLot(id);

            if (!IsBiddable(lot)) return this.Json("Invalid Lot - no bids can be accepted", JsonRequestBehavior.AllowGet);

            ValidateMaximumBid(maximumBid, lot);

            if (!ModelState.IsValid) return this.Json(ModelState.Keys.SelectMany(k => ModelState[k].Errors)
                          .Select(m => m.ErrorMessage).ToArray()

                , JsonRequestBehavior.AllowGet);

            if (!lot.SellingStatus.IsAt(LotStatus.Active))
                return this.Json("Lot has ended", JsonRequestBehavior.AllowGet);

            try
            {
                ApiProxy.Value.PostAsJson(string.Format("lots/{0}/bids", id),
                        new
                        {
                            Amount = maximumBid,
                            Currency = lot.SellingStatus.CurrentPrice.Currency
                        })
                        .AcceptJson().SendAsync()
                        .ContinueWith(t => t.Result.EnsureSuccessStatusCode())
                        .Wait();


            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                return this.Json("Failure adding bid", JsonRequestBehavior.AllowGet);
            }

            var model = new BidsConfirmationBidModel
            {
                Id = id,
                CurrentPrice = lot.SellingStatus.CurrentPrice,
                PricesIncludeVat = lot.PricesIncludeVat,
                VatStatus = lot.VehicleInfo.VatStatus,
                HasMetReserve = lot.SellingStatus.ReserveMet,
                EndDate = lot.ListingInfo.EndTime,
                PrimaryImage = lot.PrimaryImage,
                Title = lot.Title,
                TimeLeft = lot.TimeLeft(),
                AuctionHasEnded = !lot.SellingStatus.IsAt(LotStatus.Active),
                UserHasBid = true,
                MinimumBidRequired = lot.SellingStatus.MinimumToBid,
                HasBuyItNow = lot.ListingInfo.BuyItNowAvailable,
                BuyItNowPrice = lot.ListingInfo.BuyItNowPrice,
                //BackToUrl = backToUrl
            };
            return this.Json(model, JsonRequestBehavior.AllowGet);
        }


        #region Buy It Now Process

        [HttpPost, AuthoriseActivity("/Lots/Purchase/BuyItNow")]
        public Task<JsonResult> BuyItNow(int id)
        {
            return Task.Factory.StartNew(() => BuyItNowProcess(id));
        }

        private JsonResult BuyItNowProcess(int id)
        {
            var lot = GetLot(id);

            if (!CanBuyItNow(lot)) return this.Json("Lot does not have a Buy It Now option", JsonRequestBehavior.AllowGet);

            ApiProxy.Value.Post(string.Format("lots/{0}/buyitnow", id), null)
                .AcceptJson().SendAsync()
                .ContinueWith(t =>
                {
                    if (!t.Result.IsSuccessStatusCode)
                    {
                        Logger.Error(t.Result.Content.ReadAsStringAsync());
                        this.Alert("An error occured while attempting to purchase the lot");
                        ModelState.AddModelError("", "An error occured while attempting to purchase the lot");
                    }
                })
                .Wait();

            if (!ModelState.IsValid) return this.Json(string.Empty, JsonRequestBehavior.AllowGet);

            return this.Json(string.Empty, JsonRequestBehavior.AllowGet);
        }
        #endregion

        private const int MaximumBidAmountLimit = 100000;

        private void ValidateMaximumBid(decimal maximumBid, ListingViewModel auction)
        {
            var usersCurrentMaximumBidAmount = auction.BiddingStatus != null
                                                   ? auction.BiddingStatus.MaximumBid
                                                   : null;

            if (maximumBid < auction.SellingStatus.MinimumToBid.Amount)
            {
                ViewData.ModelState.AddModelError("maximumBid",
                                                  string.Format("Enter a minimum bid of {0}",
                                                                auction.SellingStatus.MinimumToBid.ToCurrencyString()));
            }
            else if (usersCurrentMaximumBidAmount != null &&
                     maximumBid <= usersCurrentMaximumBidAmount.Amount)
            {
                ViewData.ModelState.AddModelError("maximumBid",
                                                  "Lower bids can't be lowered once submitted");
            }
            else if (maximumBid > MaximumBidAmountLimit)
            {
                ViewData.ModelState.AddModelError("maximumBid",
                                                  "Enter a lower maximum bid");
            }
            else if (maximumBid % 1 != 0)
            {
                ViewData.ModelState.AddModelError("maximumBid",
                                                  "Maximum bids can be in whole pounds only");
            }
            else if (maximumBid % auction.SellingStatus.BidIncrement.Amount != 0)
            {
                ViewData.ModelState.AddModelError("maximumBid",
                                                  "Maximum bids can be in increments of " + auction.SellingStatus.BidIncrement.Amount + " pounds only");
            }
        }

        private static bool IsBiddable(ListingViewModel lot)
        {
            return LotExists(lot) && lot.SellingStatus.IsAt(LotStatus.Active) && lot.ListingInfo.ListingType.Contains("Auction");
        }

        private static bool CanBuyItNow(ListingViewModel lot)
        {
            return LotExists(lot) && lot.SellingStatus.IsAt(LotStatus.Active) &&
                   lot.ListingInfo.BuyItNowAvailable &&
                   lot.ListingInfo.ListingType.Equals("AuctionWithBin", StringComparison.InvariantCultureIgnoreCase);
        }

        private static bool LotExists(ListingViewModel lot)
        {
            return lot != null;
        }

        private ActionResult LotNotAvailableResult(int id, string backToUrl = null, string flash = null)
        {
            if (!string.IsNullOrWhiteSpace(flash))
                this.Flash(flash);

            if (!string.IsNullOrEmpty(backToUrl))
                return Redirect(backToUrl);

            return RedirectToAction("show", new { controller = "lots", id });
        }

        private ActionResult AuctionEndedResult(int id, string backToUrl = null)
        {
            return LotNotAvailableResult(id, backToUrl, "This lot is no longer active");
        }
    }
}
