﻿using System.Web.Mvc;
using Sfs.EStore.Web.App.Controllers;

namespace Sfs.EStore.Web.App.Areas.Vehicles.Controllers
{
    [AllowAnonymous]
    public class TrackController : ApplicationController
    {
        public HttpStatusCodeResult LotView(int id)
        {
            return ApiProxy.Value.Post(string.Format("lots/{0}/track?session={1}", id, UniqueSessionId.Value), null)
                .AcceptJson().SendAsync()
                .ContinueWith(t => new HttpStatusCodeResult((int) t.Result.StatusCode))
                .Result;
        }
    }
}
