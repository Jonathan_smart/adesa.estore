﻿/// <reference path="~/angular"/>
/// <reference path="~/scripts/angular/components"/>
/// <reference path="~/angular/bootstrap"/>
/// <reference path="~/scripts/angular/services/"/>
/// <reference path="~/jQuery"/>


(function () {
    "use strict";

    var app = angular.module('app', ['ui.bootstrap', 'prettyDateFilters', 'app.components']);

    app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    }]);

    app.controller('UsersCtrl', ['$scope', '$http', '$modal', 'siteUrls', function ($scope, $http, $modal, siteUrls) {
        $scope.users = [];
        $scope.isBusy = false;

        $scope.pagination = {
            currentPage: 1,
            maxSize: 7,
            take: 20
        };
        $scope.filters = {
            top: $scope.pagination.take
        };

        $scope.applyFilters = function () {
            $scope.pagination.currentPage = 1;
            $scope.findUsers();
        };

        $scope.findUsers = function () {
            $scope.isBusy = true;

            var params = angular.copy($scope.filters);
            params.sortBy = 'username';
            params.skip = ($scope.pagination.currentPage - 1) * $scope.pagination.take;
            $http.get(siteUrls.path('api/membership'), { params: params })
                .then(function (res) {
                    $scope.users = res.data.map(function (u) {
                        return { isInEdit: false, isBusy: false, user: u };
                    });
                    $scope.pagination.totalCount = res.headers("X-TotalResults");
                    $scope.isBusy = false;
                }, function () {
                    $scope.isBusy = false;
                });
        };
        $scope.$watch('pagination.currentPage', function (newValue, oldValue) {
            if (newValue !== oldValue) {
                $scope.findUsers();
            }
        });

        $scope.approveUser = function (u) {
            u.user.isApproved = true;
            updateUser(u);
        };

        $scope.beginUserEdit = function (u) {
            if (u.isInEdit)
                return;

            u.backup = angular.copy(u.user);

            var modalInstance = $modal.open({
                dialogFade: true,
                backdrop: 'static',
                keyboard: true,
                templateUrl: 'user-edit-modal.html',
                controller: ['$scope', '$http', '$modalInstance', '$timeout', '$log', 'u', 'update', ModalInstanceController],
                resolve: {
                    u: function () {
                        return u;
                    },
                    update: function () {
                        return update;
                    }
                }
            });
            return modalInstance;
        };
        $scope.saveUser = function (u) {
            updateUser(u);
        };

        var updateUser = function (u) {
            u.isBusy = true;

            update(u.user).then(function (res) {
                u.user = res.data;
                u.isBusy = false;
            }, function () {
                u.isBusy = false;
            });
        };

        var update = function (user) {
            return $http.put(siteUrls.path('api/membership'), user);
        };

        $scope.findUsers();
    }]);

    var ModalInstanceController = function ($scope, $http, $modalInstance, $timeout, $log, u, update) {

        $scope.u = u;
        $scope.complete = false;
        $scope.message;

        $scope.roles = [
               { name: 'User Manager', value: "user_manager" },
               { name: 'Content Editor', value: "content_editor" },
               { name: 'Stock Manager', value: "stock_manager" }
        ];

        $scope.selection = [];

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.dismiss = function () {
            $modalInstance.close({});
        };;

        $scope.toggleSelection = function toggleSelection(role) {

            var idx = $scope.u.user.roles.indexOf(role);

            // is currently selected
            if (idx > -1) {
                $scope.u.user.roles.splice(idx, 1);
            }

                // is newly selected
            else {
                $scope.u.user.roles.push(role);
            }

        };

        $scope.deselectAll = function (u) {
            u.user.roles = [];
        };

        $scope.selectAll = function (u) {

            var v = $scope.roles.map(function (a) { return a.value; });

            angular.forEach(v, function (value) {
                u.user.roles.push(value);
            });
        };

        $scope.saveRole = function (u) {
            if (u.user.roles.length > $scope.selection.length) {
                angular.extend(u.user.roles, $scope.selection);
            } else {
                u.user.roles = $scope.selection;
            }


            save();
            $scope.dismiss();
        };

        var save = function () {
            $scope.errored = false;
            $scope.saving = false;
            update(u.user)
                .then(function (res) {
                    u.user = res.data;
                    $scope.complete = true;
                    $scope.saving = false;
                    //$scope.dismiss();
                }, function (res) {
                    $scope.errored = res.data.message;
                    $scope.saving = false;
                    $log.error(res);
                });
        };

        $scope.approveUser = function () {
            u.user.isApproved = true;
            save();
        };
        $scope.unlockUser = function () {
            u.user.isLockedOut = false;
            save();
        };
        $scope.lockUser = function () {
            u.user.isLockedOut = true;
            save();
        };

        $scope.resetPassword = function (u) {
            $http.post('/users/SendPasswordReset/', { username: u.user.username })
                    .success(function (data) {

                        $scope.message = data;

                    })
            .error(function (data) {
                $scope.message = data;
            });
        };
    };
}());