﻿/// <reference path="./app.js"/>
/// <reference path="~/jquery"/>

(function () {
    "use strict";

    var CreateGuestPassController = function ($rootScope, $scope, $http, $location, $timeout, siteUrls) {
        $rootScope.pageid = 'new';
        $scope.params = {};
        $scope.params.source = '';
        $scope.params.ttlValue = 90;
        $scope.params.ttlUnits = 24;

        $scope.createGuestPass = function () {
            $scope.busy = true;
            var params = $.extend({ timeToLive: timeToLive() }, $scope.params);

            $http.post(siteUrls.path('api/affiliate'), params)
                .then(function (res) {
                    var guestPass = res.data;
                    $timeout(function () {
                        $scope.busy = false;
                        $location.path($scope.appUrl(guestPass.ticket.id));
                    }, 1000);
                }, function () {
                    $scope.busy = false;
                });
        };

        $scope.reset = function () {
            $scope.guestPass = null;
        };

        $scope.addUtmParameters = function () {

            var landingPage = $scope.params.redirectto || '';

            if (landingPage.indexOf('utm_medium=') != -1) return;

            if (landingPage.indexOf('?') == -1) landingPage += '?';
            else landingPage += '&';

            landingPage += 'utm_source=' + ($scope.params.source || '') + '&utm_medium=guestpass&utm_campaign=' + ($scope.params.campaign || '');
            $scope.params.redirectto = landingPage;
        };

        var timeToLive = function () {
            return $scope.params.ttlValue * $scope.params.ttlUnits;
        };
    };

    angular.module("app")
        .controller('CreateGuestPassController', ['$rootScope', '$scope', '$http', '$location', '$timeout', 'siteUrls', CreateGuestPassController]);

} ());