﻿/// <reference path="./app.js"/>
/// <reference path="~/jquery"/>

(function () {
    "use strict";

    var GuestPassController = function ($scope, $http, $routeParams, $log, siteUrls) {
        var id = $routeParams.type + '/' + $routeParams.id;

        var get = function () {
            $scope.busy = true;

            $http.get(siteUrls.path('api/affiliate?id=' + id), {
                params: $scope.params
            })
                .then(function (res) {
                    $scope.guestPass = res.data;
                    $scope.busy = false;
                }, function (res) {
                    $scope.busy = false;
                    $log.error(res);
                });
        };

        get();
    };

    angular.module("app")
        .controller('GuestPassController', ['$scope', '$http', '$routeParams', '$log', 'siteUrls', GuestPassController]);

} ());