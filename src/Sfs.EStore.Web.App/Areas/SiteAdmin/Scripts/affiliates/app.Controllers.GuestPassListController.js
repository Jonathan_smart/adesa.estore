﻿/// <reference path="./app.js"/>
/// <reference path="~/jquery"/>

(function () {
    "use strict";

    var GuestPassListController = function ($rootScope, $scope, $http, $location, $log, siteUrls) {
        $rootScope.pageid = 'list';
        $scope.filterParams = {};
        angular.extend($scope.filterParams, $location.search() || {});

        $scope.filterParams.page = $scope.filterParams.page || 1;

        $scope.pagination = {};
        $scope.pagination.take = $scope.filterParams.take || 15;
        $scope.pagination.totalItems = 999;

        var get = function (params) {
            $scope.busy = true;

            var filter = { take: $scope.pagination.take };
            angular.extend(filter, params);

            $http.get(siteUrls.path('api/affiliate'), {
                params: filter
            })
                .then(function (res) {
                    $scope.guestPasses = res.data;

                    var totalCount = res.headers("X-TotalResults");
                    $scope.pagination.totalItems = totalCount;

                    $scope.busy = false;
                }, function () {
                    $scope.busy = false;
                    $scope.pagination.totalItems = 0;
                });
        };

        $scope.expire = function (guestPass) {
            $scope.busy = true;

            $http({ method: 'DELETE', url: siteUrls.path('api/affiliate?id=' + guestPass.id) })
                .then(function (res) {
                    angular.copy(res.data, guestPass);
                    guestPass.expired = true;
                    $scope.busy = false;
                }, function (res) {
                    $log.error(res);
                    $scope.busy = false;
                });
        };

        $scope.show = function (guestPass) {
            $location.path($scope.appUrl(guestPass.id));
        };

        $scope.filter = function () {
            if ($scope.busy) return;

            var params = {};
            angular.extend(params, $scope.filterParams);
            params.page = $scope.filterParams.page = 1;

            $location.search(params);
            get(params);
        };

        $scope.gotoPage = function (page) {
            var params = {};
            angular.extend(params, $location.search());
            params.page = page;

            $location.search(params);
            get(params);
        };

        get($scope.filterParams);
    };

    angular.module("app")
        .controller('GuestPassListController', ['$rootScope', '$scope', '$http', '$location', '$log', 'siteUrls', GuestPassListController]);

} ());