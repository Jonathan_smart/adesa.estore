﻿/// <reference path="~/angular"/>
/// <reference path="~/scripts/angular/components"/>
/// <reference path="~/scripts/angular/services/"/>
/// <reference path="~/angular/bootstrap"/>
/// <reference path="~/angular/route"/>

(function () {
    "use strict";
    var URL_PREFIX = 'siteadmin/affiliates';
    var appUrl = function (siteUrls, path) {
        (path || (path = ''))
        if (path.indexOf('/') != 0) path = '/' + path;

        return siteUrls.path(URL_PREFIX + path);
    };

    angular.module("app.Services", ['prettyDateFilters', 'ui.bootstrap', 'http-auth-interceptor', 'sfs-auth-service', 'app.components']);
    angular.module("app.Controllers", []);

    angular.module("app", ["app.Services", "app.Controllers", 'ngRoute']);

    var app = angular.module("app");

    app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    } ]);

    angular.module("app")
        .config(['$routeProvider', '$locationProvider', 'siteUrlsProvider', function ($routeProvider, $locationProvider, siteUrlsProvider) {
            var siteUrls = siteUrlsProvider.$get();

            $locationProvider.html5Mode(false);
            $routeProvider
                .when(appUrl(siteUrls), {
                    templateUrl: 'guest-pass-list.html',
                    controller: 'GuestPassListController',
                    reloadOnSearch: true
                })
                .when(appUrl(siteUrls, '/list'), {
                    redirectTo: appUrl(siteUrls)
                })
                .when(appUrl(siteUrls, '/new'), {
                    templateUrl: 'create-guest-pass.html',
                    controller: 'CreateGuestPassController'
                })
                .when(appUrl(siteUrls, '/:type/:id'), {
                    templateUrl: 'guest-pass.html',
                    controller: 'GuestPassController'
                })
                .otherwise({ redirectTo: appUrl(siteUrls) });
        } ]);

    app.run(["$rootScope", 'sfsAuthService', 'sfsLoginOnRequest', 'siteUrls', function($rootScope, sfsAuthService, sfsLoginOnRequest, siteUrls) {
        $rootScope.appVersion = "0.0.0.0";
        $rootScope.auth = sfsAuthService;
        $rootScope.appUrl = function(path) {
            return appUrl(siteUrls, path);
        }
    }]);

} ());