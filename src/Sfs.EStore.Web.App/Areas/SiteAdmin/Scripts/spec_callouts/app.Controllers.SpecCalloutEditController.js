﻿/// <reference path="./app.js"/>

(function () {
    "use strict";
    var Controller = function (siteUrls, $scope, $log, $http, $filter, $timeout) {
        $scope.busy = false;

        var orderByFilter = $filter('orderBy');

        var get = function () {
            $scope.busy = true;
            $http.get(siteUrls.path('api/speccallouts'))
                .then(function sucess(res) {
                    $scope.callout = { id: res.data.id, keys: orderByFilter(res.data.keys, '-weight') };
                    $scope.busy = false;
                }, function failure(res) {
                    $log.error(res);
                    $scope.busy = false;
                });
        };
        get();

        $scope.addNew = function (newItem) {
            var existing = findItem(newItem);
            if (existing) {
                if (confirm('A callout with the same category and name already exists. Would you like to update it\'s weight to ' + newItem.weight + '?')) {
                    existing.weight = newItem.weight;
                    return;
                }
                return;
            }
            var copy = angular.copy(newItem);
            $scope.callout.keys.splice(0, 0, copy);
            $scope.newItem = undefined;
        };

        var findItem = function (match) {
            for (var i = 0; i < $scope.callout.keys.length; i++) {
                var item = $scope.callout.keys[i];
                if (item.category == match.category && item.name == match.name)
                    return item;
            }
            return undefined;
        };

        $scope.remove = function (key) {
            if (confirm('Really delete: ' + key.category + ' - ' + key.name + '?'))
                $scope.callout.keys.splice($scope.callout.keys.indexOf(key), 1);
        };

        $scope.saveKeyChanges = function () {
            $scope.busy = true;
            $scope.saved = false;
            $scope.saveError = undefined;
            $http.post(siteUrls.path('api/speccallouts'), {
                id: $scope.callout.id,
                keys: $scope.callout.keys
            })
                .then(function (res) {
                    $scope.saved = true;
                    $timeout(function () {
                        $scope.saved = false;
                    }, 2000);
                }, function (res) {
                    $log.error(res);
                    $scope.saveError = 'Failed to save';
                    $timeout(function () {
                        $scope.saveError = undefined;
                    }, 3000);
                });
        };
    };

        angular.module("app.Controllers")
        .controller('SpecCalloutEditController', ['siteUrls', '$scope', '$log', '$http', '$filter', '$timeout', Controller]);
} ());
