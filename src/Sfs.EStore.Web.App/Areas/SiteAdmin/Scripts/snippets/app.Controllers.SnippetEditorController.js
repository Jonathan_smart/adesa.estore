﻿/// <reference path="./app.js"/>
/// <reference path="./app.Controllers.SnippetListController.js"/>

(function () {
    "use strict";

    var SnippetEditorController = function ($rootScope, $scope, $log, $http, $timeout, siteUrls) {
        var editorOptionsList = {
            plaintext: {
                lineWrapping: true,
                lineNumbers: false,
                readOnly: false,
                viewportMargin: Infinity,
                mode: 'htmlmixed'
            },
            htmlmixed: {
                lineWrapping: true,
                lineNumbers: true,
                readOnly: false,
                viewportMargin: Infinity,
                mode: 'htmlmixed'
            },
            javascript: {
                lineWrapping: true,
                lineNumbers: true,
                readOnly: false,
                viewportMargin: Infinity,
                mode: 'javascript'
            }
        };

        $scope.editorOptions = editorOptionsList.htmlmixed;

        $scope.save = function () {
            $scope.busy = true;
            $scope.status = 'saving';
            $rootScope.$broadcast('event:SnippetManagerApp-snippetSaving');

            $http.put(siteUrls.path('api/snippets'),
                {
                    id: $scope.selectedSnippet.id,
                    body: $scope.selectedSnippet.body
                })
                .then(function (res) {
                    $scope.busy = false;
                    $scope.status = 'saved';
                    $timeout(function() { $scope.status = ''; }, 1500);
                    $rootScope.$broadcast('event:SnippetManagerApp.Editor.SnippetSaved');
                }, function (res) {
                    $scope.busy = false;
                    $scope.status = 'save failed';
                    $timeout(function () { $scope.status = ''; }, 1500);
                    $rootScope.$broadcast('event:SnippetManagerApp.Editor.SnippetSaved');
                    $log.error(res);
                });
        };

        $scope.$watch('busy', function (newValue) {
            if (newValue)
                $rootScope.$broadcast('event:SnippetManagerApp.Editor.IsBusy');
            else
                $rootScope.$broadcast('event:SnippetManagerApp.Editor.IsIdle');
        });

        $scope.$watch('selectedItem', function (newValue) {
            $scope.selectedSnippet = undefined;
            if (!newValue) return;

            $scope.busy = true;
            $scope.status = 'loading';

            $http.get(siteUrls.path('api/snippets?id=' + newValue.id))
                .then(function (res) {
                    $scope.selectedSnippet = res.data;
                    $scope.busy = false;
                    $scope.status = '';
                }, function (res) {
                    $scope.busy = false;
                    $scope.status = '';
                    $log.error(res);
                });
        });

        $scope.editorRefresh = true;
        $scope.$watch('selectedSnippet', function (newValue) {
            if (!newValue) return;

            $scope.editorOptions = editorOptionsList[newValue.expectedType] || editorOptionsList.htmlmixed;
            $scope.editorRefresh = !$scope.editorRefresh;
        });
    };

    angular.module("app.Controllers")
        .controller('SnippetEditorController', ['$rootScope', '$scope', '$log', '$http', '$timeout', 'siteUrls', SnippetEditorController]);
} ());