﻿/// <reference path="~/angular"/>
/// <reference path="~/scripts/angular/components"/>
/// <reference path="~/angular/route"/>
/// <reference path="~/areas/siteadmin/scripts/angular/codemirror/ui-codemirror.js"/>


(function () {
    "use strict";

    angular.module("app.Controllers", []);
    angular.module("app.Services", ['ui.codemirror', 'app.components']);
    angular.module("app", ["app.Controllers", "app.Services", 'ngRoute'])
        .config(['$routeProvider', '$locationProvider', function($routeProvider) {
            $routeProvider
                .otherwise({
                    templateUrl: 'index.html',
                    controller: 'SnippetListController'
                });
        }]);

            angular.module("app").config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    } ]);

    angular.module("app").run(["$rootScope", function ($rootScope) {
        $rootScope.snippetManagerAppVersion = "0.0.0.0";
    } ]);
} ());