﻿/// <reference path="./app.js"/>

(function () {
    "use strict";

    var SnippetListController = function ($rootScope, $scope, $log, $http, siteUrls) {

        var loadList = function () {
            $scope.busy = true;
            $http.get(siteUrls.path('api/snippets'))
                .then(function (res) {
                    $scope.snippets = res.data;
                    $scope.busy = false;
                }, function (res) {
                    $log.error(res);
                    $scope.busy = false;
                });
        };
        loadList();

        $rootScope.$on('event:SnippetManagerApp.Editor.IsBusy', function () {
            $scope.editorWorking = true;
        });
        $rootScope.$on('event:SnippetManagerApp.Editor.IsIdle', function () {
            $scope.editorWorking = false;
        });
    };

    angular.module("app.Controllers")
        .controller('SnippetListController', ['$rootScope', '$scope', '$log', '$http', 'siteUrls', SnippetListController]);
} ());