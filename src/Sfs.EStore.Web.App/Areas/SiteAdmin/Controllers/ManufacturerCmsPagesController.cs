﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Sfs.EStore.Web.App.Code.Cms;
using Sfs.EStore.Web.App.Code.Mvc;
using Sfs.EStore.Web.App.Controllers;
using Sfs.EStore.Web.App.Models;

namespace Sfs.EStore.Web.App.Areas.SiteAdmin.Controllers
{
    [Authorise(Roles = "content_editor")]
    public class ManufacturerCmsPagesController : ApplicationController
    {
        public ActionResult Index()
        {
            var pages = TenantPages().ToList();
            return View(new ManufacturerCmsPagesIndexModel(pages));
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View(new ManufacturerCmsPagesCreateModel());
        }

        [HttpPost]
        public ActionResult Create(ManufacturerCmsPagesCreateModel bound)
        {
            if (!ModelState.IsValid)
                return View(bound);

            var page = new ManufacturerCmsPage
            {
                ManufacturerQueryTerm = bound.ManufacturerQueryTerm,
                VehicleTypeQueryTerm = bound.VehicleTypeQueryTerm,
                PageTitle = bound.ManufacturerQueryTerm
            };

            Session.Store(page);

            return RedirectToAction("edit", new {editing = page.Id});
        }

        [HttpGet]
        public ActionResult Edit(string editing)
        {
            var page = Session.Load<ManufacturerCmsPage>(editing);

            if (page == null) return HttpNotFound("Cannot find page with id " + editing);

            return View(ManufacturerCmsPagesEditModel.CreateFrom(page));
        }

        [HttpPost]
        public ActionResult Edit(string editing, ManufacturerCmsPagesEditModel bound)
        {
            var page = Session.Load<ManufacturerCmsPage>(editing);

            if (page == null) return HttpNotFound("Cannot find page with id " + editing);

            if (bound.IsDeleteAction())
            {
                Session.Delete(page);
                this.Flash("'{0}' page has been permanently deleted", page.PageTitle);
                return RedirectToAction("index");
            }

            if (!ModelState.IsValid) return View(bound);

            bound.CopyTo(page);

            this.Flash("Page updated");
            return RedirectToAction("edit", new {editing});
        }

        private IEnumerable<ManufacturerCmsPage> TenantPages()
        {
            return
                Session.Query<ManufacturerCmsPage>()
                    .Customize(x => x.WaitForNonStaleResults())
                    .OrderBy(x => x.VehicleTypeQueryTerm)
                    .ThenBy(x => x.ManufacturerQueryTerm);
        }
    }

    public class ManufacturerCmsPagesIndexModel
    {
        public ManufacturerCmsPagesIndexModel(IEnumerable<ManufacturerCmsPage> pages)
        {
            Pages = pages.Select(x => new Page(x)).ToArray();
        }

        public IEnumerable<Page> Pages { get; private set; }

        public class Page
        {
            private readonly ManufacturerCmsPage _page;

            public Page(ManufacturerCmsPage page)
            {
                _page = page;
            }

            public string Id
            {
                get { return _page.Id; }
            }

            public string VehicleTypeQueryTerm
            {
                get { return _page.VehicleTypeQueryTerm; }
            }

            public string ManufacturerQueryTerm
            {
                get { return _page.ManufacturerQueryTerm; }
            }

            public string PageTitle
            {
                get { return _page.PageTitle; }
            }
        }
    }

    public class ManufacturerCmsPagesCreateModel
    {
        [Required]
        public string VehicleTypeQueryTerm { get; set; }
        [Required]
        public string ManufacturerQueryTerm { get; set; }
    }

    public class ManufacturerCmsPagesEditModel
    {
        public string Id { get; set; }

        [Required]
        public string VehicleTypeQueryTerm { get; set; }
        [Required]
        public string ManufacturerQueryTerm { get; set; }
        [Required]
        public string PageTitle { get; set; }
        [AllowHtml]
        public string Body { get; set; }
        public string ImageUrl { get; set; }

        public string MoveToTrash { get; set; }

        private MetaDataModel _metaData = new MetaDataModel();
        public MetaDataModel MetaData
        {
            get { return _metaData; }
            set { _metaData = value; }
        }

        public static ManufacturerCmsPagesEditModel CreateFrom(ManufacturerCmsPage page)
        {
            return new ManufacturerCmsPagesEditModel
            {
                Id = page.Id,
                VehicleTypeQueryTerm = page.VehicleTypeQueryTerm,
                ManufacturerQueryTerm = page.ManufacturerQueryTerm,
                MetaData = MetaDataModel.CreateFrom(page.MetaData),
                Body = page.Body,
                PageTitle = page.PageTitle,
                ImageUrl = page.ImageUrl
            };
        }

        public void CopyTo(ManufacturerCmsPage page)
        {
            page.Body = Body;
            page.ImageUrl = ImageUrl;
            page.ManufacturerQueryTerm = ManufacturerQueryTerm;
            MetaData.CopyTo(page.MetaData);
            page.PageTitle = PageTitle;
            page.VehicleTypeQueryTerm = VehicleTypeQueryTerm;
        }

        public bool IsDeleteAction()
        {
            return "delete".Equals(MoveToTrash);
        }
    }
}
