﻿using System.Web.Mvc;
using Sfs.EStore.Web.App.Controllers;

namespace Sfs.EStore.Web.App.Areas.SiteAdmin.Controllers
{
    public class DashboardController : ApplicationController
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}