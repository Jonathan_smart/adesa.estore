﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Sfs.EStore.Web.App.Code.Cms;
using Sfs.EStore.Web.App.Code.Mvc;
using Sfs.EStore.Web.App.Controllers;
using Sfs.EStore.Web.App.Models;

namespace Sfs.EStore.Web.App.Areas.SiteAdmin.Controllers
{
    [Authorise(Roles = "content_editor")]
    public class ManufacturerModelCmsPagesController : ApplicationController
    {
        public ActionResult Index()
        {
            var pages = TenantPages().ToList();
            return View(new ManufacturerModelCmsPagesIndexModel(pages));
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View(new ManufacturerModelCmsPagesCreateModel());
        }

        [HttpPost]
        public ActionResult Create(ManufacturerModelCmsPagesCreateModel bound)
        {
            if (!ModelState.IsValid)
                return View(bound);

            var page = new ManufacturerModelCmsPage
            {
                VehicleTypeQueryTerm = bound.VehicleTypeQueryTerm,
                ManufacturerQueryTerm = bound.ManufacturerQueryTerm,
                ModelQueryTerm = bound.ModelQueryTerm,
                PageTitle = string.Format("{0} {1}", bound.ManufacturerQueryTerm, bound.ModelQueryTerm)
            };

            Session.Store(page);

            return RedirectToAction("edit", new {editing = page.Id});
        }

        [HttpGet]
        public ActionResult Edit(string editing)
        {
            var page = Session.Load<ManufacturerModelCmsPage>(editing);

            if (page == null) return HttpNotFound("Cannot find page with id " + editing);

            return View(ManufacturerModelCmsPagesEditModel.CreateFrom(page));
        }

        [HttpPost]
        public ActionResult Edit(string editing, ManufacturerModelCmsPagesEditModel bound)
        {
            var page = Session.Load<ManufacturerModelCmsPage>(editing);

            if (page == null) return HttpNotFound("Cannot find page with id " + editing);

            if (bound.IsDeleteAction())
            {
                Session.Delete(page);
                this.Flash("'{0}' page has been permanently deleted", page.PageTitle);
                return RedirectToAction("index");
            }

            if (!ModelState.IsValid) return View(bound);

            bound.CopyTo(page);

            this.Flash("Page updated");
            return RedirectToAction("edit", new {editing});
        }

        private IEnumerable<ManufacturerModelCmsPage> TenantPages()
        {
            return
                Session.Query<ManufacturerModelCmsPage>()
                    .Customize(x => x.WaitForNonStaleResults())
                    .OrderBy(x => x.VehicleTypeQueryTerm)
                    .ThenBy(x => x.ManufacturerQueryTerm)
                    .ThenBy(x => x.ModelQueryTerm)
                    .Take(256);
        }
    }

    public class ManufacturerModelCmsPagesIndexModel
    {
        public ManufacturerModelCmsPagesIndexModel(IEnumerable<ManufacturerModelCmsPage> pages)
        {
            Pages = pages.Select(x => new Page(x)).ToArray();
        }

        public IEnumerable<Page> Pages { get; private set; }

        public class Page
        {
            private readonly ManufacturerModelCmsPage _page;

            public Page(ManufacturerModelCmsPage page)
            {
                _page = page;
            }

            public string Id
            {
                get { return _page.Id; }
            }

            public string VehicleTypeQueryTerm
            {
                get { return _page.VehicleTypeQueryTerm; }
            }

            public string ManufacturerQueryTerm
            {
                get { return _page.ManufacturerQueryTerm; }
            }

            public string ModelQueryTerm
            {
                get { return _page.ModelQueryTerm; }
            }

            public string PageTitle
            {
                get { return _page.PageTitle; }
            }
        }
    }

    public class ManufacturerModelCmsPagesCreateModel
    {
        [Required]
        public string VehicleTypeQueryTerm { get; set; }
        [Required]
        public string ManufacturerQueryTerm { get; set; }
        [Required]
        public string ModelQueryTerm { get; set; }
    }

    public class ManufacturerModelCmsPagesEditModel
    {
        public string Id { get; set; }

        [Required]
        public string VehicleTypeQueryTerm { get; set; }
        [Required]
        public string ManufacturerQueryTerm { get; set; }
        [Required]
        public string ModelQueryTerm { get; set; }
        [Required]
        public string PageTitle { get; set; }
        [AllowHtml]
        public string Body { get; set; }
        public string ImageUrl { get; set; }

        public string MoveToTrash { get; set; }

        private MetaDataModel _metaData = new MetaDataModel();
        public MetaDataModel MetaData
        {
            get { return _metaData; }
            set { _metaData = value; }
        }

        public static ManufacturerModelCmsPagesEditModel CreateFrom(ManufacturerModelCmsPage page)
        {
            return new ManufacturerModelCmsPagesEditModel
            {
                Id = page.Id,
                VehicleTypeQueryTerm = page.VehicleTypeQueryTerm,
                ManufacturerQueryTerm = page.ManufacturerQueryTerm,
                ModelQueryTerm = page.ModelQueryTerm,
                MetaData = MetaDataModel.CreateFrom(page.MetaData),
                Body = page.Body,
                PageTitle = page.PageTitle,
                ImageUrl = page.ImageUrl
            };
        }

        public void CopyTo(ManufacturerModelCmsPage page)
        {
            page.Body = Body;
            page.ImageUrl = ImageUrl;
            page.VehicleTypeQueryTerm = VehicleTypeQueryTerm;
            page.ManufacturerQueryTerm = ManufacturerQueryTerm;
            page.ModelQueryTerm = ModelQueryTerm;
            MetaData.CopyTo(page.MetaData);
            page.PageTitle = PageTitle;
        }

        public bool IsDeleteAction()
        {
            return "delete".Equals(MoveToTrash);
        }
    }
}
