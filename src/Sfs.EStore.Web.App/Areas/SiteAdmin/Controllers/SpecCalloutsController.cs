﻿using System.Web.Mvc;
using Sfs.EStore.Web.App.Code.Mvc;
using Sfs.EStore.Web.App.Controllers;

namespace Sfs.EStore.Web.App.Areas.SiteAdmin.Controllers
{
    [Authorise(Roles = "stock_manager")]
    public class SpecCalloutsController : ApplicationController
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
