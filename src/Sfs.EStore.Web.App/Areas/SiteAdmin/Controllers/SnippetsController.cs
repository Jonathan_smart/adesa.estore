﻿using System.Web.Mvc;
using Sfs.EStore.Web.App.Code.Mvc;

namespace Sfs.EStore.Web.App.Areas.SiteAdmin.Controllers
{
    [Authorise(Roles = "content_editor")]
    public class SnippetsController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

    }
}
