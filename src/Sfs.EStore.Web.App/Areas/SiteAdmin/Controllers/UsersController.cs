﻿using System.Web.Mvc;
using Sfs.EStore.Web.App.Code.Mvc;
using Sfs.EStore.Web.App.Controllers;
using System.Net;
using Sfs.EStore.Web.App.Models;

namespace Sfs.EStore.Web.App.Areas.SiteAdmin.Controllers
{
    [Authorise(Roles = "user_manager")]
    public class UsersController : ApplicationController
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult sendPasswordReset(string username)
        {
            string message = string.Empty;

            ApiProxy.Value.PostAsJson("forgottenpassword", new
            {
                username
            })
                .AcceptJson().SendAsync()
                .ContinueWith(x =>
                {
                    if (x.Result.StatusCode == HttpStatusCode.NotFound)
                    {
                        message = "No record of user found.";
                        return;
                    }

                    if (x.Result.StatusCode == HttpStatusCode.Forbidden)
                    {
                        message = "User is locked out.";
                        return;
                    }

                    if (!x.Result.IsSuccessStatusCode)
                    {
                        message = x.Result.Content.ReadAsStringAsync().Result;
                        return;
                    }
                    message = "Email sent successfully";
                    return;
                })
                .Wait();

            return Content(message);

        }


        [HttpPost]
        public ActionResult updateContent(CmsPageModel model)
        {
            return PartialView("_contentView", model);
        }
    }
}
