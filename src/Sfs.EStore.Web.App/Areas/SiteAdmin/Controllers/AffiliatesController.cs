﻿using System.Web.Mvc;
using Sfs.EStore.Web.App.Code.Mvc;

namespace Sfs.EStore.Web.App.Areas.SiteAdmin.Controllers
{
    [Authorise(Roles = "user_manager")]
    public class AffiliatesController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
