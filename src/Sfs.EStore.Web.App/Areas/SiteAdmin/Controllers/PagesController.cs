﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Sfs.EStore.Web.App.Code.Cms;
using Sfs.EStore.Web.App.Code.Mvc;
using Sfs.EStore.Web.App.Controllers;
using Sfs.EStore.Web.App.Models;
using Sfs.EStore.Web.App.Code.Widgets.Liquid.Drops;

namespace Sfs.EStore.Web.App.Areas.SiteAdmin.Controllers
{
    [Authorise(Roles = "content_editor")]
    public class PagesController : ApplicationController
    {
        public ActionResult Index()
        {
            var pages = TenantPages().ToList();
            return View(pages);
        }

        [HttpGet]
        public ActionResult Edit(string edit)
        {
            var page = TenantPages()
                .FirstOrDefault(x => x.PermaName == edit);

            if (page == null)
                return HttpNotFound();


            var model = PagesEditModel.From(page);

            var p = new PagePreviewModel();
            p.editModel = PagesEditModel.From(page);
            p.RenderModel = new CmsPageModel(page, new CmsPageLiquidVariables(new UserDrop(User.Identity), new CmsPageDrop(Request)));

            return View(p);
        }

        [HttpPost]
        public ActionResult Edit(string edit, PagePreviewModel updatedPage, string movetotrash)
        {
            var page = TenantPages()
                .FirstOrDefault(x => x.PermaName == edit);

            if (page == null)
                return HttpNotFound();

            if ("delete".Equals(movetotrash))
            {
                Session.Delete(page);
                this.Flash("{0} page has been permanently deleted", page.PermaName);
                return RedirectToAction("index");
            }

            updatedPage.editModel.CopyTo(page);

            this.Flash("{0} page updated", page.PermaName);

            return RedirectToAction("edit", new { edit = page.PermaName });
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View(new CmsPageCreateModel());
        }

        [HttpPost]
        public ActionResult Create(CmsPageCreateModel page)
        {
            if (string.IsNullOrWhiteSpace(page.Title))
                ModelState.AddModelError(page, x => x.Title, "Title is required");

            if (!ModelState.IsValid)
                return View(page);

            var cmsPage = new CmsPage
            {
                PageTitle = page.Title,
                AllowAccess = PageAccess.Deny
            }.PermaNameFrom(page.Title);

            Session.Store(cmsPage);

            return RedirectToAction("edit", new { edit = cmsPage.PermaName });
        }

        private IQueryable<CmsPage> TenantPages()
        {
            return Session.Query<CmsPage>().Customize(x => x.WaitForNonStaleResults()).OrderBy(x => x.PermaName);
        }
    }

    public class CmsPageCreateModel
    {
        public string Title { get; set; }
    }

    public class PagePreviewModel
    {
        public PagesEditModel editModel { get; set; }
        public CmsPageModel RenderModel { get; set; }
    }

    public class PagesEditModel
    {
        [Display(Name = "Page Title")]
        public string PageTitle { get; set; }
        [Display(Name = "URL Slug")]
        public string PermaName { get; set; }
        [Display(Name = "Body CSS Class")]
        public string BodyCssClass { get; set; }
        [AllowHtml]
        [Display(Name = "Body")]
        public string Body { get; set; }
        [Display(Name = "Allow Access")]
        public string AllowAccess { get; set; }
        public PageScriptsEditModel[] Scripts { get; set; }

        private MetaDataModel _metaData = new MetaDataModel();
        public MetaDataModel MetaData
        {
            get { return _metaData; }
            set { _metaData = value; }
        }

        public class PageScriptsEditModel
        {
            public string Value { get; set; }
            public string[] Dependencies { get; set; }
        }

        public void CopyTo(CmsPage page)
        {
            page.PageTitle = PageTitle;
            page.BodyCssClass = BodyCssClass;
            page.Body = Body;
            page.AllowAccess = AllowAccess;
            page.PermaNameFrom(PermaName);

            if (MetaData != null)
            {
                MetaData.CopyTo(page.MetaData);
            }
        }

        public static PagesEditModel From(CmsPage page)
        {
            if (page == null) return null;

            return new PagesEditModel
            {
                AllowAccess = page.AllowAccess,
                Body = page.Body,
                BodyCssClass = page.BodyCssClass,
                MetaData = new MetaDataModel
                {
                    CanonicalUrl = page.MetaData.CanonicalUrl,
                    Description = page.MetaData.Description,
                    Title = page.MetaData.Title
                },
                PageTitle = page.PageTitle,
                PermaName = page.PermaName
            };
        }
    }
}
