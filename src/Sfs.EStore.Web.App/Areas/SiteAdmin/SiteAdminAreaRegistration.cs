﻿using System.Web.Mvc;

namespace Sfs.EStore.Web.App.Areas.SiteAdmin
{
    public class SiteAdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SiteAdmin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "siteadmin_default",
                "siteadmin/{controller}/{action}/{id}",
                new { action = "index", controller="dashboard", id = UrlParameter.Optional }
            );
        }
    }
}
