using Cassette;
using Cassette.Scripts;
using Cassette.Stylesheets;

namespace Sfs.EStore.Web.App.Areas.SiteAdmin
{
    public class CassetteBundleConfiguration : IConfiguration<BundleCollection>
    {
        public void Configure(BundleCollection bundles)
        {
            bundles.AddPerSubDirectory<ScriptBundle>("~/areas/siteadmin/scripts");

            bundles.Add<StylesheetBundle>(
                "siteadmin/style",
                new[] { "~/areas/siteadmin/content/style.scss" });

            BundleCodeMirror(bundles);
        }

        public void BundleCodeMirror(BundleCollection bundles)
        {
            bundles.Add<ScriptBundle>(
                "siteadmin/codemirror/codemirror",
                new[]
                    {
                        "~/areas/siteadmin/content/codemirror/codemirror.js",
                         "~/areas/siteadmin/content/codemirror/AddOn/fullscreen.js",
                        "~/areas/siteadmin/content/codemirror/mode/css/css.js",
                        "~/areas/siteadmin/content/codemirror/mode/javascript/javascript.js",
                        "~/areas/siteadmin/content/codemirror/mode/htmlmixed/htmlmixed.js",
                        "~/areas/siteadmin/content/codemirror/mode/xml/xml.js"
                    });
            bundles.Add<StylesheetBundle>(
                "siteadmin/codemirror/codemirror",
                new[] { "~/areas/siteadmin/content/codemirror/codemirror.css", "~/areas/siteadmin/content/codemirror/AddOn/fullscreen.css" },
                bundle => bundle.AddReference("~/siteadmin/style"));
        }
    }
}