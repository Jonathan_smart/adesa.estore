﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Sfs.EStore.Web.App.Areas
{
    public class UrlBuilder
    {
        private readonly string _baseUrl;
        private readonly Dictionary<string, Collection<object>> _queryParams = new Dictionary<string, Collection<object>>();

        public UrlBuilder(string baseUrl)
        {
            _baseUrl = baseUrl;
        }

        public UrlBuilder AddQueryParam<T>(string key, params T[] values)
        {
            values.ToList().ForEach(x => AddQueryParam(key, () => x, () => true));
            return this;
        }

        public UrlBuilder AddQueryParam<T>(string key, T value, Func<T, bool> condition)
        {
            return AddQueryParam(key, () => value, () => condition(value));
        }

        public UrlBuilder AddQueryParam<T>(string key, Func<T> value, Func<bool> condition)
        {
            if (condition())
            {
                if (!_queryParams.ContainsKey(key))
                    _queryParams[key] = new Collection<object>();

                _queryParams[key].Add(value());
            }
            return this;
        }

        public string Build()
        {
            var queryString = "";
            if (_queryParams.Any())
                queryString = InitialQueryStringChar() +
                              string.Join("&", _queryParams.Select(kv => KeyToQuery(kv.Key, kv.Value.ToArray())).ToArray());

            return _baseUrl + queryString;
        }

        private string InitialQueryStringChar()
        {
            return _baseUrl.Contains("?") ? "&" : "?";
        }

        private static string KeyToQuery(string key, IEnumerable<object> values)
        {
            return string.Join("&", values.Select(kv => string.Format("{0}={1}", key, kv)).ToArray());
        }
    }
}