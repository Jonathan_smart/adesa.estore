using Sfs.EStore.Web.App.Code.Security;

namespace Sfs.EStore.Web.App.Areas.Retail
{
    public class RetailModeDealer
    {
        public RetailModeDealer(string code, string name)
        {
            Code = code;
            Name = name;
        }

        public string Code { get; private set; }
        public string Name { get; private set; }
    }

    public class RetailModeDealerStore
    {
        internal const string CodeKey = "RetailMode_Dealer_Code";
        internal const string NameKey = "RetailMode_Dealer_Name";

        private readonly IWebApplicationIdentity _identity;

        public RetailModeDealerStore(IWebApplicationPrincipal principal)
        {
            _identity = principal.Identity;
        }

        public RetailModeDealer Get()
        {
            if (!(_identity.Data.ContainsKey(CodeKey) && _identity.Data.ContainsKey(NameKey))) return null;

            var code = _identity.Data[CodeKey];
            var name = _identity.Data[NameKey];

            return new RetailModeDealer(code, name);

        }
        public RetailModeDealer Get(string code, string name)
        {
            return new RetailModeDealer(code, name);
        }
    }
}