﻿(function(angular) {
    angular.module('app.Filters.MarkupFilter', [])
        .filter('markup', ['$window', function ($window) {
            return function (value) {
                var markupConfig = $window.__sfs.markupTable;

                if (!markupConfig)
                    return value;

                var markupAdjustment = markupConfig.filter(function (i) {
                    return (!i.from || value.amount >= i.from) && (!i.to || value.amount <= i.to);
                });
                if (!markupAdjustment.length)
                    return value;

                var clone = angular.copy(value);
                clone.amount += markupAdjustment[0].markup;
                return clone;
            };
        }]);
}).call(this, angular);