﻿/// <reference path="~/scripts/angular/services/"/>
/// <reference path="~/angular/bootstrap"/>
/// <reference path="~/angular/angularytics"/>
/// <reference path="~/scripts/angular/components/"/>

(function () {
    "use strict";

    angular.module("app.Services", [
        'ui.bootstrap', 'angularytics'
    ]);
    angular.module("app.Controllers", []);

    angular.module("app", ["app.Services", "app.Controllers"])
    .config(['AngularyticsProvider', function (AngularyticsProvider) {
        AngularyticsProvider.setEventHandlers(['Console', 'GoogleUniversal']);
    }]).run(['Angularytics', function (Angularytics) {
        Angularytics.init();
    }]);

    var app = angular.module("app");

    app.controller('MarkupController', ['$scope', '$http', '$modal', function ($scope, $http, $modal) {

 

        //$scope.ShowCustomer = function () {
        //    var modalInstance = $modal.open({

        //        backdrop: 'static',
        //        keyboard: false,
        //        templateUrl: 'SelectBrand.html',
        //        resolve: {
        //            dealers: function () {
        //                return $http.get('/dealer/Get')
        //              .success(function (data) {
        //                  $scope.dealers = data;
        //              });
        //            }
        //        },
        //        controller: ['$scope', '$modalInstance', 'dealers', function ($scope, $modalInstance, dealers) {

        //            $scope.loadData = function (selected) {

        //                $http.get('/dealer/Get')
        //               .success(function (data) {
        //                   $scope.dealers = data;
        //               });

        //                $http.get('/dealer/getDealer', { params: { "No": selected.No } })
        //                    .success(function (data) {
        //                        $scope.links = data;
        //                    });
        //            }

        //            $scope.GetValue = function (selected) {
        //                $scope.loadData(selected);
        //            };

        //            $scope.dealers = dealers.data;
        //            $scope.selectedDealer = $scope.dealers[0];
        //            $scope.loadData($scope.dealers[0]);

        //            $scope.Submit = function (brand) {

        //                var dealer = {
        //                    No: $scope.links.No,
        //                    Name: $scope.links.Name,
        //                    Marquee: brand,

        //                };

        //                $http.post('/dealer/Setup/', { dealer: dealer })
        //                    .success(function (data) {

        //                        $modalInstance.dismiss();

        //                        window.location.reload();
        //                    });
        //            };

        //            $scope.close = function () {
        //                $modalInstance.dismiss();
        //            };
        //        }]
        //    });
        //}

        $scope.savestate = function (mode) {
            $scope.mode = mode;
            console.log($scope.mode);
        }

        $scope.Submit = function (dealer) {

            $http.post('/dealer/Setup/', { dealer: dealer })
                .success(function (data) {
                    $scope.submitdata.$setPristine();
                    window.location.reload();
                });
        };

    }]);


    var MarkupTable = function (data) {
        this._value = data;
        this.lines = (data || [])
            .map(function (line) {
                return new MarkupLine(line);
            });
    }
    MarkupTable.prototype.hasLines = function () {
        return this.lines.length > 0;
    }
    MarkupTable.prototype.min = function () {
        var markup = [];
        if (this.lines.length > 0) {
            angular.forEach(this.lines, function (value, index) {

                if (value.markup != null) {
                    markup.push(value.markup);
                };
            });
            return Math.min.apply(Math, markup);
        }
    }
    MarkupTable.prototype.lastLineIsEmpty = function () {
        var lastLine = this.lines[this.lines.length - 1];
        if (!lastLine) return undefined;

        return lastLine.isEmpty();
    }
    MarkupTable.prototype.addEmptyLine = function (data) {
        var initialValues = data || {};
        this.lines.push(new MarkupLine({
            from: (initialValues.from === undefined ? null : initialValues.from),
            to: (initialValues.to === undefined ? null : initialValues.to),
            markup: (initialValues.markup === undefined ? null : initialValues.markup)
        }));
    }

    var MarkupLine = function (data) {
        this._value = data;
        this.from = data.from;
        this.to = data.to;
        this.markup = data.markup;
    }
    MarkupLine.prototype.isEmpty = function () {
        return this.from === null &&
                this.to === null &&
                this.markup === null;
    }

    app.directive('editableMarkupTable', [function () {
        return {
            restrict: 'A',
            scope: {
                lines: '@'
            },
            templateUrl: 'editable-markup-table.html',
            controller: ['$scope', function ($scope) {


                $scope.table = new MarkupTable(
                    angular.fromJson($scope.lines)
                    .map(function (line) {
                        if ($(this).val() == $(this).attr('placeholder')) {
                            $(this).val('');
                        }
                        return {
                            from: line.From,
                            to: line.To,
                            markup: line.Markup
                        };
                    })
                );

                $scope.$watch('table', function (newValue) {

                    if ($scope.table.hasLines()) {
                        $scope.table.min();
                    }

                    if (!$scope.table.hasLines())
                        $scope.table.addEmptyLine({ from: 0 });
                    else if (!$scope.table.lastLineIsEmpty())
                        $scope.table.addEmptyLine();

                }, true);
            }]
        };
    }]);

    app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    }]);




}());