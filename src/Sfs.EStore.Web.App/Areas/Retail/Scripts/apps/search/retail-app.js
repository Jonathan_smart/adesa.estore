﻿/// <reference path="~/Scripts/angular/components/search"/>
/// <reference path="~/areas/retail/scripts/components/app.Filters.MarkupFilter.js"/>

/// <reference path="~/scripts/angular/services/"/>
/// <reference path="~/scripts/angular/services/searches"/>
/// <reference path="~/angular/bootstrap"/>
/// <reference path="~/angular/cookies"/>
/// <reference path="~/angular/route"/>

(function (angular) {
    "use strict";

    angular.module("app.Services", ['ui.bootstrap', 'prettyDateFilters', 'urlFilters', 'http-auth-interceptor', 'lotWatchModule', 'ngCookies',
        'components.searches',
        'app.Services.SavedSearches', 'app.Services.SavedSearchService',
        'app.Services.SearchResultsDirective']);
    angular.module('app.Controllers', ['app.ListingResultsController', 'app.ListingSearchController']);
    angular.module('app.Config', ['app.Config.Html5Mode', 'app.Config.RequestedWith']);
    angular.module('app.Run', ['app.Run.Setup']);
    angular.module('app.Filters', ['app.Filters.MarkupFilter']);

    angular.module('app', ['app.Services', 'app.Controllers', 'app.Filters', 'app.Config', 'app.Run', 'ngRoute']);

    var app = angular.module("app");

    app.config(['$routeProvider', 'siteUrlsProvider', function ($routeProvider, siteUrlsProvider) {
        var searchRoute = {
            templateUrl: 'lot-search.html',
            controller: 'ListingSearchController'
        };

        var siteUrls = siteUrlsProvider.$get();

        $routeProvider
            .when(siteUrls.path('retail/search'), searchRoute)
            .otherwise({ redirectTo: siteUrls.path('retail/search') });
    }]);
}).call(this, angular);