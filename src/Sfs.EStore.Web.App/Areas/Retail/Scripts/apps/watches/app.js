﻿/// <reference path="~/scripts/angular/services/"/>
/// <reference path="~/angular/bootstrap"/>
/// <reference path="~/angular/angularytics"/>
/// <reference path="~/scripts/angular/components/"/>
/// <reference path="~/angular/cookies"/>
(function () {
    "use strict";

    angular.module("app.Services", ['urlFilters', 'prettyDateFilters', 'ui.bootstrap', 'http-auth-interceptor', 'lotWatchModule']);
    angular.module("app.Controllers", []);

    angular.module("app", ["app.Services", "app.Controllers"]);
    var app = angular.module('app');

    app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    }]);

    var app = angular.module("app");

    app.controller('WatchListController', ['$scope', '$http', '$location', 'watches',function ($scope, $http, $location, watches) {

        $scope.working = false;
        $scope.loaded = false;
        $scope.vehicles = [];
        $scope.search = $location.search() || {};
        $scope.watches = watches;

        $scope.pagination = {
            take: 20,
            maxSize: 7,
            currentPage: 1,
            noOfPages: 0
        };

        $scope.query = function () {
            $location.search($scope.search);
            executeQuery();
        };

        var executeQuery = function () {
            $scope.working = true;
            var skip = ($scope.pagination.currentPage - 1) * $scope.pagination.take;

            watches.find({
                params: {
                    q: $scope.search.q,
                    top: $scope.pagination.take,
                    skip: skip
                }
            }).then(function (res) {
                $scope.loaded = true;
                $scope.working = false;
                $scope.vehicles = res.data;

                $scope.totalCount = res.headers("X-TotalResults");
            }, function () {
                $scope.loaded = true;
                $scope.errorLoading = true;
                $scope.working = false;
            });
        };

        $scope.$watch('totalCount', function () {
            $scope.pagination.noOfPages = Math.ceil($scope.totalCount / $scope.pagination.take);
        });
        $scope.$watch('pagination.currentPage', function (newValue, oldValue) {
            if (newValue !== oldValue)
                $scope.query();
        });

        executeQuery();
    }]);
}());
