﻿
(function () {
    "use strict";

    var app = angular.module("app");

    app.controller('retailMenuCtrl', ['$scope', '$http', '$location', '$modal', function ($scope, $http, $location, $modal) {

        $scope.dealers = {};

        $scope.init = function () {
            $http.get('/dealer/Get')
         .success(function (data) {
             $scope.dealers = data;
         });

            $http.get('/dealer/ReturnDealer')
                      .success(function (data) {
                          $scope.dealer = data;
                          $scope.backColor = $scope.dealer.Marquee == "select" ? "background-color#f5f5f5!important;" : "background-color:#f5f5f5!important;";
                      });
        };

        $scope.showCustomers = function (len) {
            return $location.absUrl().indexOf("/retail/markup") > -1 && len > 1;
        }

        $scope.showBrands = function (dealers) {
            return $location.absUrl().indexOf("/retail/home") > -1;
        }

        $scope.ShowBrandModal = function (data, customerNo) {
            console.log($scope.dealers);
            var modalInstance = $modal.open({
            	size: 'lg',
                templateUrl: '/Brand.html',
                resolve: {
                    items: function () {
                        return $scope.dealers;
                    }
                },
                scope: $scope,
                controller: [
                    '$scope', '$modalInstance', function ($scope, $modalInstance) {

                        $scope.loadData = function (selected) {

                            $http.get('/dealer/getDealer', { params: { "No": selected } })
                                .success(function (data) {
                                    $scope.links = data;
                                });
                        }

                        $scope.GetValue = function (selected) {
                            $scope.loadData(selected);
                        };

                        $scope.selectedDealer = {
                            "No": customerNo
                        };

                        $scope.loadData(customerNo);

                        $scope.Submit = function (brand) {

                            var dealer = {
                                No: $scope.links.No,
                                Name: $scope.links.Name,
                                Marquee: brand

                            };

                            $http.post('/dealer/Setup/', { dealer: dealer })
                                .success(function (data) {

                                    $modalInstance.dismiss();

                                    window.location.reload();

                                });
                        };

                        $scope.close = function () {
                            $modalInstance.dismiss();
                        };
                    }
                ]
            });

        };


    }]);

}());