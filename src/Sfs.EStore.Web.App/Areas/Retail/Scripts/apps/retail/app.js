﻿/// <reference path="~/scripts/angular/services/"/>
/// <reference path="~/angular/bootstrap"/>
/// <reference path="~/angular/angularytics"/>
/// <reference path="~/scripts/angular/components/"/>
/// <reference path="~/angular/cookies"/>
(function () {
    "use strict";

    angular.module("app.Services", [
        'ui.bootstrap', 'angularytics', 'ngCookies'
    ]);
    angular.module("app.Controllers", []);

    angular.module("app", ["app.Services", "app.Controllers"])
        .config([
            'AngularyticsProvider', function (AngularyticsProvider) {
                AngularyticsProvider.setEventHandlers(['Console', 'GoogleUniversal']);
            }
        ]).run([
            'Angularytics', function (Angularytics) {
                Angularytics.init();
            }
        ]);

    var app = angular.module("app");

    app.controller('retailController', ["$scope", "$http", "$modal", "$cookies", function ($scope, $http, $modal, $cookies) {

        $scope.Init = function (val) {
            $scope.username = val;
        };

        if ($cookies['_v'] == null) {

            $cookies['_v'] = '1';

            var modalInstance = $modal.open({
                size: 'lg',
                backdrop: 'static',
                keyboard: false,
                templateUrl: '/Brand.html',
                resolve: {
                    dealers: function () {
                        return $http.get('/dealer/Get')
                            .success(function (data) {
                                $scope.dealers = data;
                            });
                    }
                },
                controller: [
                    '$scope', '$modalInstance', 'dealers', function ($scope, $modalInstance, dealers) {
                        $scope.loadData = function (selected) {

                            $http.get('/dealer/Get')
                           .success(function (data) {
                               $scope.dealers = data;
                           });

                            $http.get('/dealer/getDealer', { params: { "No": selected } })
                                .success(function (data) {
                                    $scope.links = data;
                                });
                        }
                 
                        $scope.GetValue = function (selected) {
                            $scope.loadData(selected);
                        };

                        $scope.dealers = dealers.data;

                        $scope.selectedDealer = {
                            "No":$scope.dealers[0].No
                        };
                        $scope.loadData($scope.selectedDealer.No);

                        $scope.Submit = function (brand) {

                            var dealer = {
                                No: $scope.links.No,
                                Name: $scope.links.Name,
                                Marquee: brand,

                            };

                            $http.post('/dealer/Setup/', { dealer: dealer })
                                .success(function (data) {

                                    $modalInstance.dismiss();

                                    window.location.reload();
                                });
                        };

                        $scope.close = function () {
                            $modalInstance.dismiss();
                        };
                    }
                ]
            });
        }

        $scope.isEmpty = function (val) {
            return (typeof val === 'undefined' || val === null);
        }

        var url = function () {
            var arr = window.location.href.split("/");
            var result = arr[0] + "//" + arr[2];
            return result;
        }

        //$scope.ShowBrand = function (customerNo) {

        //    var modalInstance = $modal.open({

        //        templateUrl: '/Brand.html',
        //        resolve: {
             
        //        },
        //        controller: [
        //            '$scope', '$modalInstance', function ($scope, $modalInstance) {

        //                $scope.loadData = function (selected) {

        //                    $http.get('/dealer/Get')
        //                   .success(function (data) {
        //                       $scope.dealers = data;
        //                   });

        //                    $http.get('/dealer/getDealer', { params: { "No": selected } })
        //                        .success(function (data) {
        //                            $scope.links = data;
        //                        });
        //                }

        //                $scope.GetValue = function (selected) {
        //                    $scope.loadData(selected);
        //                };

        //                $scope.selectedDealer = {
        //                    "No": customerNo
        //                };

        //                $scope.loadData(customerNo);

        //                $scope.Submit = function (brand) {

        //                    var dealer = {
        //                        No: $scope.links.No,
        //                        Name: $scope.links.Name,
        //                        Marquee: brand

        //                    };

        //                    $http.post('/dealer/Setup/', { dealer: dealer })
        //                        .success(function (data) {

        //                            $modalInstance.dismiss();

        //                            window.location.reload();
 
        //                        });
        //                };

        //                $scope.close = function () {
        //                    $modalInstance.dismiss();
        //                };
        //            }
        //        ]
        //    });


        //    //modalInstance.result.then(function done(address) {

        //    //}, function dismissed(err) {

        //    //});
        //};

        $scope.Search = function () {
            $http.get('SearchMin')
                 .success(function (data) {

                     if (data == "false") {
                         alert("Pin not setup - please go to Retail Setup");
                     } else {
                         window.location.href = url() + '/retail/search';
                     }

                 }, function (error) {
                     console.log('CONTROLLER ERROR : app offline' + error);
                 });
        }

        $scope.Submit = function (tableName, pin) {
            if (!$scope.isEmpty(tableName) && $scope.isEmpty(pin) && tableName.length > 0) {
                $http.get('validateTable', { params: { "tableName": tableName } })
                    .success(function (data) {

                        if (data == "false") {
                            alert("Username is not correct - please go to Retail Setup");
                        } else {
                            window.location.href = url() + '/retail/search';
                        }

                    }, function (error) {
                        console.log('CONTROLLER ERROR : app offline' + error);
                    });
                return;
            }

            $http.get('ValidatePin', { params: { "pin": pin } })
           .success(function (data) {

               if (data == "false") {
                   alert("Pin not setup, please go to Retail Setup");
               } else {
                   window.location.href = url() + '/retail/search';
               }

           }, function (error) {
               console.log('CONTROLLER ERROR : app offline' + error);
           });
        }
    }]);
    app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
        $httpProvider.defaults.headers.common['Cache-Control'] = 'no-cache, no-store, must-revalidate';
        $httpProvider.defaults.headers.common['Pragma'] = 'no-cache';
        $httpProvider.defaults.headers.common['Expires'] = '0';
    }]);
}());
