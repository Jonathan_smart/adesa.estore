﻿/// <reference path="~/scripts/angular/services/"/>
/// <reference path="~/angular/bootstrap"/>
/// <reference path="~/angular/angularytics"/>
/// <reference path="~/scripts/angular/components/"/>
(function () {
    "use strict";

    angular.module("app.Services", [
        'ui.bootstrap', 'angularytics'
    ]);
    angular.module("app.Controllers", []);

    angular.module("app", ["app.Services", "app.Controllers"])
        .config([
            'AngularyticsProvider', function (AngularyticsProvider) {
                AngularyticsProvider.setEventHandlers(['Console', 'GoogleUniversal']);
            }
        ]).run([
            'Angularytics', function (Angularytics) {
                Angularytics.init();
            }
        ]);

    var app = angular.module("app");

    app.controller('retailController', ["$scope", "$http", function ($scope, $http) {

        $scope.Init = function (val) {
            $scope.username = val;
        };

        angular.isEmpty = function (val) {
            return angular.isUndefined(val) || val === null;
        }

        var url = function () {
            var arr = window.location.href.split("/");
            var result = arr[0] + "//" + arr[2];
            return result;
        }

        $scope.Search = function () {

            $http.get('SearchMin')
                 .success(function (data) {

                     if (data == "false") {
                         alert("Pin not setup - please go to Retail Setup");
                     } else {
                         window.location.href = url() + '/retail/search';
                     }

                 }, function (error) {
                     console.log('CONTROLLER ERROR : app offline' + error);
                 });
        }

        $scope.Submit = function (tableName, pin) {

            if (!angular.isEmpty(tableName) && angular.isEmpty(pin) && tableName.length > 0) {

                $http.get('validateTable', { params: { "tableName": tableName } })
                    .success(function (data) {

                        if (data == "false") {
                            alert("Username is not correct - please go to Retail Setup");
                        } else {
                            window.location.href = url() + '/retail/search';
                        }

                    }, function (error) {
                        console.log('CONTROLLER ERROR : app offline' + error);
                    });
                return;
            }

            //check if pin has value  >= minium Markup if not return message and exit.
            $http.get('ValidatePin', { params: { "pin": pin } })
           .success(function (data) {

               if (data == "false") {
                   alert("Pin not setup, please go to Retail Setup");
               } else {
                   window.location.href = url() + '/retail/search';
               }

           }, function (error) {
               console.log('CONTROLLER ERROR : app offline' + error);
           });
        }
    }]);
    app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
        $httpProvider.defaults.headers.common['Cache-Control'] = 'no-cache, no-store, must-revalidate';
        $httpProvider.defaults.headers.common['Pragma'] = 'no-cache';
        $httpProvider.defaults.headers.common['Expires'] = '0';
    }]);
}());
