﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using Sfs.Customers.Adapters.DataAccess.Entities;
using Sfs.EStore.Web.App.Areas.Retail.Models;
using Sfs.EStore.Web.App.Code.Security;

namespace Sfs.EStore.Web.App.Areas.Retail
{
    public class RetailView
    {
        private string _name;
        private string _no;
        private string[] _makes;
        private string _marque;
        #region Public Accessible Properties


        public string Title
        {
            get
            {
                return _no + "-" + _name;
            }
            //protected set
            //{
            //    Name = value;
            //}
        }

        public string Name
        {
            get
            {
                return _name;
            }
            protected set
            {
                Name = value;
            }
        }

        public string No
        {
            get
            {
                return _no;
            }
            protected set
            {
                No = value;
            }

        }

        public string[] Makes
        {
            get
            {
                return _makes;
            }
            protected set
            {
                Makes = value;
            }

        }

        public string Marque
        {
            get
            {
                return _marque;
            }
            protected set
            {
                Marque = value;
            }

        }


        #endregion

        private static NavisionContext _dbContext;
        private IWebApplicationIdentity identity;

        public RetailView(IWebApplicationPrincipal user)
        {
            _dbContext = new NavisionContext();

            Load(null, user);
        }

        public RetailView(IWebApplicationIdentity identity)
        {
            this.identity = identity;


        }


        public static DealerModel returnDealer(HttpContextBase context)
        {
            if (context != null)
            {
                bool cookieExists = context.Request.Cookies["_retail"] != null;

                if (cookieExists)
                {
                    return GetModel(context.Request.Cookies["_retail"]);
                }
            }

            return null;
        }

        public void Load(DealerModel dealer, IWebApplicationPrincipal principal)
        {
            if (principal == null) return;

            var user = principal.Identity;

            var context = HttpContext.Current;
            var cookie = context.Request.Cookies["_retail"];

            bool cookieExists = cookie != null;

            if (cookieExists)
            {
                var existingdealer = GetModel(cookie);

                bool requiresUpdate = dealer != null && (true & dealer.No != null && existingdealer.No != dealer.No
                                                           || existingdealer.Name != null && existingdealer.Name != dealer.Name
                                                           || existingdealer.Marquee != null && existingdealer.Marquee != dealer.Marquee);
                if (requiresUpdate)
                {
                    Reset();

                    dealer = Save(dealer, principal);

                    if (!user.Data.Any())
                    {
                        user.Data.Add("RetailMode_Dealer_Code", existingdealer.No);
                        user.Data.Add("RetailMode_Dealer_Name", existingdealer.Name);
                    }

                    if (dealer != null)
                    {
                        _name = dealer.Name;
                        _no = dealer.No;
                        _makes = dealer.Makes;
                        _marque = dealer.Marquee;
                    }
                }
                else
                {
                    if (!user.Data.Any())
                    {
                        user.Data.Add("RetailMode_Dealer_Code", existingdealer.No);
                        user.Data.Add("RetailMode_Dealer_Name", existingdealer.Name);
                    }

                    if (existingdealer != null)
                    {
                        _name = existingdealer.Name;
                        _no = existingdealer.No;
                        _makes = existingdealer.Makes;
                        _marque = existingdealer.Marquee;
                    }
                }
            }
            else
            {
                dealer = Save(dealer, principal);

                if (dealer != null)
                {
                    user.Data.Add("RetailMode_Dealer_Code", dealer.No);
                    user.Data.Add("RetailMode_Dealer_Name", dealer.Name);

                    _name = dealer.Name;
                    _no = dealer.No;
                    _makes = dealer.Makes;
                    _marque = dealer.Marquee;
                }
            }
        }

        private static DealerModel Save(DealerModel model, IWebApplicationPrincipal principal)
        {
            var foundFranchise = new string[] { "FIAT", "FIAT ABARTH", "FIAT PROFESSIONAL" };
            var selectFranchise = new string[] { "ALFA ROMEO", "JEEP" };

            if (model == null)
            {
                model = _dbContext.Set<ContactUsername>().Where(x => x.Username == principal.Identity.Name)
                     .Join(_dbContext.Set<ContactRelationship>(),
                         contactUser => contactUser.ContactNo,
                         contactRelation => contactRelation.ContactNo,
                         (contactRelation, contactUser) => contactUser).OrderBy(x => x.IsDefault)
                     .Join(_dbContext.Set<Customer>().Where(x => x.GlobalDimension1Code == "FGA"),
                         cus => cus.SourceNo,
                         contactUser => contactUser.No,
                         (cus, contactUser) => contactUser)

                     .Select(x => new DealerModel { Name = x.Name.ToUpper() + " - " + x.City.ToUpper(), No = x.No }).FirstOrDefault();

                if (model == null) return null;
            }

            if (model.Marquee == "selected")
            {
                var isSelect =
                  _dbContext.Set<Customer>().Where(x => x.No == model.No)
                      .Join(
                          _dbContext.Set<CustomerCampaign>()
                              .Where(x => x.Type == 1 & selectFranchise.Contains(x.FranchiseCode)),
                          campaign => campaign.No,
                          cus => cus.CustomerNo, (campaign, cus) => cus)
                      .Select(x => x.FranchiseCode).Distinct();

                if (isSelect.Any())
                {
                    model.Marquee = "select";
                    model.Makes = isSelect.ToArray();
                }
            }

            if (model.Marquee == "found")
            {
                var isFound =
          _dbContext.Set<Customer>().Where(x => x.No == model.No)
              .Join(
                  _dbContext.Set<CustomerCampaign>()
                      .Where(x => x.Type == 1 & foundFranchise.Contains(x.FranchiseCode)),
                  campaign => campaign.No,
                  cus => cus.CustomerNo, (campaign, cus) => cus)
              .Select(x => x.FranchiseCode).Distinct();

                if (isFound.Any())
                {
                    model.Marquee = "found";
                    model.Makes = isFound.ToArray();
                }
            }

            if (model.Marquee == null)
            {
                var isSelect =
                _dbContext.Set<Customer>().Where(x => x.No == model.No)
                    .Join(
                        _dbContext.Set<CustomerCampaign>()
                            .Where(x => x.Type == 1 & selectFranchise.Contains(x.FranchiseCode)),
                        campaign => campaign.No,
                        cus => cus.CustomerNo, (campaign, cus) => cus)
                    .Select(x => x.FranchiseCode).Distinct();

                if (isSelect.Any())
                {
                    model.Marquee = "select";
                    model.Makes = isSelect.ToArray();

                    AssignCookie(model);
                    return model;
                }

                var isFound =
                            _dbContext.Set<Customer>().Where(x => x.No == model.No)
                               .Join(
                                   _dbContext.Set<CustomerCampaign>()
                                       .Where(x => x.Type == 1 & foundFranchise.Contains(x.FranchiseCode)),
                                   campaign => campaign.No,
                                   cus => cus.CustomerNo, (campaign, cus) => cus)
                               .Select(x => x.FranchiseCode).Distinct();

                if (isFound.Any())
                {
                    model.Marquee = "found";
                    model.Makes = isFound.ToArray();

                    AssignCookie(model);
                    return model;
                }
            }


            AssignCookie(model);

            return model;
        }

        private static void AssignCookie(DealerModel model)
        {
            var context = HttpContext.Current;
            var data = new JavaScriptSerializer().Serialize(model);

            var cookie = new HttpCookie("_retail", Encrypt(data))
            {
                HttpOnly = true,
                Expires = DateTime.Now.AddDays(1)
            };

            context.Response.Cookies.Add(cookie);
        }

        public void Reset()
        {
            HttpContext context = HttpContext.Current;

            HttpCookie currentUserCookie = context.Request.Cookies["_retail"];

            if (currentUserCookie != null)
            {
                context.Response.Cookies.Remove("_retail");
                currentUserCookie.Expires = DateTime.Now.AddDays(-10);
                currentUserCookie.Value = null;
                context.Response.SetCookie(currentUserCookie);
            }
        }

        private static DealerModel GetModel(HttpCookie cookie)
        {
            var serializer1 = new JavaScriptSerializer();
            return serializer1.Deserialize<DealerModel>(Decrypt(cookie.Value));
        }

        private static string Decrypt(string value)
        {
            byte[] data = Convert.FromBase64String(value);
            return Encoding.UTF8.GetString(data);
        }

        private static string Encrypt(string value)
        {
            var buffer = Encoding.ASCII.GetBytes(value);
            return Convert.ToBase64String(buffer);
        }
    }
}