using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Sfs.EStore.Web.App.Code.Security;

namespace Sfs.EStore.Web.App.Areas.Retail
{
    public class RequiresDealerAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var principal = filterContext.HttpContext.User as IWebApplicationPrincipal;
            if (principal == null)
            {
                filterContext.Result = NoDealerResult();
                return;
            }

            var ctx = new HttpContextWrapper(HttpContext.Current);
            var _tenant = ctx.CurrentTenant();

            if (_tenant.Key == "networkq")
            {
                var dealer = new RetailModeDealerStore(principal).Get();

                if (dealer == null)
                {
                    filterContext.Result = NoDealerResult();
                    return;
                }
            }
            else
            {
                var _retail = new Lazy<RetailView>(() => new RetailView(principal.Identity));
                if (_retail == null)
                {
                    filterContext.Result = NoDealerResult();
                    return;
                }
            }
        }

        private static RedirectToRouteResult NoDealerResult()
        {
            return new RedirectToRouteResult(new RouteValueDictionary
            {
                {"action", "index"},
                {"controller", "DealerNotKnown"},
                {"area", "retail"}
            });
        }
    }
}