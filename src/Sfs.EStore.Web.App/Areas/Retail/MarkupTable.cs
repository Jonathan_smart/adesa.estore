﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sfs.EStore.Web.App.Areas.Retail
{
    public class MarkupTable
    {
        public readonly string TableName;
        private readonly List<Row> _rows;

        public MarkupTable(string tableName, params Row[] rows)
        {
            _rows = rows.ToList();
            TableName = tableName;
        }

        public double Markup(double value)
        {
            var line = _rows.Single(x => x.IsSatisfiedBy(value));

            return line.Markup(value);
        }

        public void Accept(IMarkupTableRowVisitor visitor)
        {
            _rows.ForEach(x => x.Accept(visitor));
        }

        public abstract class Row
        {
            public abstract bool IsSatisfiedBy(double value);
            public abstract double Markup(double value);

            public abstract void Accept(IMarkupTableRowVisitor visitor);
        }

        public class FixedMarkupWithin : Row
        {
            private readonly double _from;
            private readonly double? _to;
            private readonly double _markup;

            public FixedMarkupWithin(double from, double? to, double markup)
            {
                _from = @from;
                _to = to;
                _markup = markup;
            }

            public override bool IsSatisfiedBy(double value)
            {
                return _from <= value && (!_to.HasValue || _to.Value > value);
            }

            /// <exception cref="ArgumentOutOfRangeException">value</exception>
            public override double Markup(double value)
            {
                if (!IsSatisfiedBy(value))
                    throw new ArgumentOutOfRangeException("value", value,
                        string.Format("{0} is not satisfied. value must be between {1} and {2}",
                            value, _from, _to));

                return value + _markup;
            }

            public override void Accept(IMarkupTableRowVisitor visitor)
            {
                visitor.VisitFixed(_from, _to, _markup);
            }
        }
    }

    public interface IMarkupTableRowVisitor
    {
        void VisitFixed(double from, double? to, double markup);
    }

    public class JsonReadyMarkupTableRowVisitor : IMarkupTableRowVisitor
    {
        private readonly List<MarkupRange> _range = new List<MarkupRange>();

        public void VisitFixed(double from, double? to, double markup)
        {
            _range.Add(new MarkupRange
            {
                from = from,
                to = to,
                markup = markup
            });
        }

        public MarkupRange[] Result()
        {
            return _range.ToArray();
        }

        public class MarkupRange
        {
            // ReSharper disable InconsistentNaming
            public double from { get; set; }
            public double? to { get; set; }
            public double markup { get; set; }
            // ReSharper restore InconsistentNaming
        }
    }
}
