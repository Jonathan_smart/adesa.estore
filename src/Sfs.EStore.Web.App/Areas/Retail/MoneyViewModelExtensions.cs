using Sfs.EStore.Web.App.Areas.Vehicles.Models;

namespace Sfs.EStore.Web.App.Areas.Retail
{
    public static class MoneyViewModelExtensions
    {
        public static MoneyViewModel Markup(this MoneyViewModel @this, MarkupTable table)
        {
            return new MoneyViewModel((decimal)table.Markup((double)@this.Amount), @this.Currency);
            
        }
    }
}