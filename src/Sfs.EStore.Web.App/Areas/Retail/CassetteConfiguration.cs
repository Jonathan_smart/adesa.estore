using Cassette;
using Cassette.Scripts;

namespace Sfs.EStore.Web.App.Areas.Retail
{
    public class CassetteBundleConfiguration : IConfiguration<BundleCollection>
    {
        public void Configure(BundleCollection bundles)
        {
            bundles.AddPerSubDirectory<ScriptBundle>("~/areas/retail/scripts/apps");
            bundles.AddPerIndividualFile<ScriptBundle>("~/areas/retail/scripts/components");
        }
    }
}