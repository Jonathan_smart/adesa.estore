﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Sfs.EStore.Web.App.Areas.Retail
{
    public class RetailAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Retail";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Retail::menu",
                "retail/menu-action",
                new { action = "index", controller = "retailmenu" }
            );
            context.MapRoute(
                "Retail::Search",
                "retail/search",
                new { action = "index", controller = "search" }
            );
            context.MapRoute(
                "Retail::Listing",
                "retail/listings/{lotNumber}/{slug}",
                new { action = "index", controller = "listings", slug = UrlParameter.Optional }
            );
            context.MapRoute(
                "Retail::markup",
                "retail/markup/{Name}/{No}",
                new { action = "index", controller = "markup", Name = UrlParameter.Optional, No = UrlParameter.Optional }
            );

            context.MapRoute(
             "Retail::Dealer:return",
             "retail/dealer/returnMarkup/{dealer}",
             new { controller = "dealer", action = "returnMarkup", dealer = UrlParameter.Optional }
           );
            context.MapRoute(
            "Retail::get",
            "retail/dealer/get",
            new { action = "get", controller = "dealer" }
        );
            context.MapRoute(
              "Retail::getDealer",
              "retail/dealer/getDealer/{dealer}",
              new { action = "getDealer", controller = "dealer", dealer = UrlParameter.Optional }
          );
            context.MapRoute(
                "Retail::markup::Reset",
                "retail/markup/reset",
                new { action = "reset", controller = "markup" }
            );

            context.MapRoute(
                "Retail::DealerNotKnown",
                "retail/dealer-not-known",
                new { action = "index", controller = "dealernotknown" }
            );

            context.MapRoute(
      "Retail::home",
      "retail/home",
      new { action = "index", controller = "retail", TableName = UrlParameter.Optional }
     );

            context.MapRoute(
    "Retail::Validatetable",
    "retail/Validatetable",
    new { action = "validatetable", controller = "retail" }
   );
            context.MapRoute(
         "Retail::Validatepin",
        "retail/Validatepin",
        new { action = "validatepin", controller = "retail" }
        );
            context.MapRoute(
          "Retail::searchmin",
          "retail/SearchMin",
          new { action = "Searchmin", controller = "retail" }
      );

            context.MapRoute(
  "Retail::watches",
  "retail/watches",
  new { action = "index", controller = "watches" }
);

            context.MapRoute(
                "Retail::cmsPage",
                "retail/{*permaName}",
                new { action = "index", controller = "cmspage" }
            );
        }
    }
}