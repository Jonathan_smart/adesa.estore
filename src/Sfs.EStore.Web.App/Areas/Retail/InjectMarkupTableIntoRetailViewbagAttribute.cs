﻿using System;
using System.Web.Mvc;

namespace Sfs.EStore.Web.App.Areas.Retail
{
    public class InjectMarkupTableIntoRetailViewbagAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            var localPath = filterContext.HttpContext.Request.Url.LocalPath;
            if (!localPath.StartsWith("/retail", StringComparison.InvariantCultureIgnoreCase))
                return;

            var table = filterContext.HttpContext.Session[EnsureMarkupAttribute.SessionKey];

            filterContext.Controller.ViewBag.MarkupTable = table;
        }
    }
}