using System.Collections.Generic;
using Sfs.EStore.Web.App.Code;

namespace Sfs.EStore.Web.App.Areas.Retail.Models
{
    public class RetailMenuModel
    {
        public RetailModeDealer Dealer { get; set; }
        public IEnumerable<MenuItem> Items { get; set; }
    }
}