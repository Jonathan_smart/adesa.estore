using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Sfs.EStore.Web.App.Areas.Retail.Models
{
    public class MarkupTableForm : IValidatableObject
    {
        public PricingMode Mode { get; set; }
        public MarkupTableLine[] Lines { get; set; }
        public string CustomerName { get; set; }
        public string TableName { get; set; }
        public bool SaveTable { get; set; }
        public Guid? TableToEdit { get; set; }
        public Guid? TableToDelete { get; set; }
        public RetailView SelectedDealer { get; set; }
        public SavedMarkupTable[] SavedTables { get; set; }
        public double? MinMarkup { get; set; }

        public class SavedMarkupTable
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public SavedMarkupTableLine[] Lines { get; set; }
            public DateTime CreatedDate { get; set; }
        }

        public enum PricingMode
        {
            Undefined,
            Simple,
            Table
        }



        internal static MarkupTableForm Load(IEnumerable<SavedMarkupTable> savedTables, string dealer = "", double? minMarkup = null)
        {
            return new MarkupTableForm
            {
                Mode = PricingMode.Undefined,
                MinMarkup = minMarkup ?? 0,
                Lines = new MarkupTableLine[0],
                SavedTables = (savedTables ?? new SavedMarkupTable[0]).ToArray(),
                CustomerName = dealer
            };
        }
        internal static MarkupTableForm Empty(IEnumerable<SavedMarkupTable> savedTables, string dealer = "", double? minMarkup = null)
        {
            return new MarkupTableForm
            {
                Mode = PricingMode.Undefined,
                MinMarkup = minMarkup ?? 0,
                Lines = new MarkupTableLine[0],
                SavedTables = (savedTables ?? new SavedMarkupTable[0]).ToArray(),
                CustomerName = dealer
            };
        }

        internal static MarkupTableForm CloneForResubmission(MarkupTableForm submitted, IEnumerable<SavedMarkupTable> savedTables, double? minMarkup = null)
        {
            var form = Empty(savedTables);
            form.Mode = submitted.Mode;
            form.TableName = submitted.TableName;
            form.Lines = (submitted.Lines ?? new MarkupTableLine[0]).Where(x => !x.IsEmpty()).OrderBy(x => x.From).ToArray();
            form.MinMarkup = minMarkup;
            //form.TableToEdit = submitted.TableToEdit;
            //form.SelectedDealer = submitted.SelectedDealer;
            //form.CustomerName = submitted.SelectedDealer.No + "-" + submitted.SelectedDealer.Name;

            return form;
        }

        internal MarkupTableLine[] NewValueLines()
        {
            return Lines.Where(x => !x.IsEmpty()).OrderBy(x => x.From).ToArray();
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Lines == null || Lines.All(x => x.IsEmpty()))
            {
                yield return new ValidationResult("No markup specified", new[] { "Lines" });
                yield break;
            }

            var valueLines = Lines.Where(x => !x.IsEmpty()).ToArray();

            var zeroFrom = valueLines.Where(x => x.From == 0).ToArray();
            if (!zeroFrom.Any())
                yield return new ValidationResult("There must be a row with a \"From\" value of 0.", new[] { "Lines" });
            if (zeroFrom.Count() > 1)
                yield return new ValidationResult("Only one row can have a \"From\" value of 0.", new[] { "Lines" });

            var openTo = valueLines.Count(x => x.To == null);
            if (openTo > 1)
                yield return new ValidationResult("Only one row can have an open \"To\" (e.g. no value).", new[] { "Lines" });
            if (openTo == 0)
                yield return new ValidationResult("There must be a row with an open \"To\" (e.g. no value). You should either add a new row or remove the \"To\" value from the last row.", new[] { "Lines" });

            int? prevTo = -1;

            foreach (var x in valueLines.OrderBy(x => x.From))
            {
                if (x.From - 1 != prevTo)
                {
                    yield return
                        new ValidationResult(
                            string.Format("The \"From\" value ({1}) must be 1 more than the previous \"To\" value ({0})", prevTo, x.From),
                            new[] { "Lines" });
                }
                if (x.To != null && x.To <= x.From)
                {
                    yield return
                        new ValidationResult(
                            string.Format("The \"To\" value ({0}) must be more than the \"From\" value ({1})", x.To, x.From),
                            new[] { "Lines" });
                }

                prevTo = x.To;
            }
        }
    }
}