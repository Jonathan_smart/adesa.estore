﻿using System.Collections.Generic;


namespace Sfs.EStore.Web.App.Areas.Retail.Models
{
    public class DealerModel
    {
        public string No { get; set; }
        public string Name { get; set; }
        public string Marquee { get; set; }
        public List<string> FoundFranchises { get; set; }
        public List<string> SelectFranchises { get; set; }
        public string[] Makes { get; set; }
    }

    public class Dealer{
        public string No { get; set; }
        public string Name { get; set; }
    }
}