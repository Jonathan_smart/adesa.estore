using Sfs.EStore.Web.App.Areas.Vehicles.Models;

namespace Sfs.EStore.Web.App.Areas.Retail.Models
{
    public class ListingsIndexModel
    {
        public ListingViewModel Listing { get; set; }
        public MarkupTable MarkupTable { get; set; }
        public string Brand { get; set; }
    }
}