﻿

namespace Sfs.EStore.Web.App.Areas.Retail.Models
{
    public class MarkupBasket
    {
        public int Id { get; set; }
        public double Markup { get; set; }
        public string Customer { get; set; }
    }
}