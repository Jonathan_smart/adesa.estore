﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sfs.EStore.Web.App.Areas.Retail.Models
{
    public class MarkupTableLine : IValidatableObject
    {
        private const int MaximumMarkup = 99999;
        private const int MaximumVehicleValue = 999999;

        [DataType(DataType.Currency), Range(minimum: 0, maximum: MaximumVehicleValue)]
        public int? From { get; set; }

        [DataType(DataType.Currency), Range(minimum: 0, maximum: MaximumVehicleValue)]
        public int? To { get; set; }

        [DataType(DataType.Currency), Range(minimum: 0, maximum: MaximumMarkup)]
        public int? Markup { get; set; }

        public static MarkupTableLine Create(int from, int markupAmount, int? to = null)
        {
            return new MarkupTableLine
            {
                From = from,
                To = to,
                Markup = markupAmount
            };
        }

        public bool IsEmpty()
        {
            return !(From.HasValue || To.HasValue || Markup.HasValue);
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (IsEmpty()) yield break;

            if (!From.HasValue)
                yield return new ValidationResult("The field From must have a value.", new[] { "From" });
            if (!Markup.HasValue)
                yield return new ValidationResult("The field Markup must have a value.", new[] { "Markup" });
        }
    }
}