﻿namespace Sfs.EStore.Web.App.Areas.Retail.Models
{
    public class RetailHomeModel
    {
        public string MarkupTableName { get; set; }
        public int VehicleCount { get; set; }
        public double? MinMarkUp { get; set; }
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
    }
}