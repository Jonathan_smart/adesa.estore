namespace Sfs.EStore.Web.App.Areas.Retail.Models
{
    public class SavedMarkupTableLine
    {
        public int From { get; set; }
        public int? To { get; set; }
        public int Markup { get; set; }
    }
}