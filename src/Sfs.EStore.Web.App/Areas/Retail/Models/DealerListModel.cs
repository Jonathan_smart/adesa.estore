﻿using System;
using System.Collections.Generic;

namespace Sfs.EStore.Web.App.Areas.Retail.Models
{
    public class DealerListModel
    {
        public IEnumerable<DealerModel> Dealers { get; set; }
        public DealerModel Dealer { get; set; }
    }
}