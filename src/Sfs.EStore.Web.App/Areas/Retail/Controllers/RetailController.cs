﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ninject;
using Sfs.EStore.Web.App.App_Start;
using Sfs.EStore.Web.App.Areas.Retail.Models;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Controllers;
using Sfs.EStore.Web.App.Models;

namespace Sfs.EStore.Web.App.Areas.Retail.Controllers
{
    [RequiresDealer]
    public class RetailController : ApplicationController
    {
        private RetailModeDealerStore _retailModeDealerStore;

        public RetailController(RetailModeDealerStore retailModeDealerStore)
        {
            _retailModeDealerStore = retailModeDealerStore;
        }

        public ActionResult Index()
        {
            var data = new RetailHomeModel();

            switch (Tenant.Value.Key)
            {
                case "networkq":
                    data.VehicleCount = CountVehicle();
                    data.MarkupTableName = LastSavedTable().Name;

                    return View(data);

                default:

                    data.VehicleCount = CountVehicle();
                    data.MarkupTableName = string.Empty;
                    data.CustomerNo = Retail.No;
                    data.CustomerName = Retail.Name;

                    return View(data);
            }
        }

        [HttpPost]
        public ActionResult Index(string TableName)
        {
            var data = new RetailHomeModel();

            data.VehicleCount = CountVehicle();
            data.MarkupTableName = TableName;
            data.CustomerNo = Retail.No;
            data.CustomerName = Retail.Name;

            return View(data);
        }

        [HttpGet]
        public JsonResult ValidateTable(string tableName)
        {
            var customer = Customer();

            bool valid = customer != null && customer.Tables.Where(x => !x.Deleted && x.Name == tableName).Select(x => x.Name).FirstOrDefault() != null;

            if (valid)
            {
                var valueLines = customer.ActiveTables().Where(x => x.Name == tableName).Select(x => x.Lines).First();

                var markupTable = new MarkupTable(tableName, valueLines
                    .Select(x => new MarkupTable.FixedMarkupWithin(x.From, x.To, x.Markup))
                    .ToArray());
                new MarkupTableSessionStore(WebSession).Set(markupTable);
            }


            return this.Json(valid, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult ValidatePin(double? pin)
        {
            var customer = Customer();

            bool isValid = customer != null && customer.MinMarkUp > 0;

            if (pin != null)
                isValid = customer != null && customer.MinMarkUp > 0 && (customer.MinMarkUp <= pin);

            if (isValid)
                SavePin(pin ?? customer.MinMarkUp, string.Empty);

            return this.Json(isValid, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult SearchMin()
        {
            var customer = Customer();

            bool isValid = customer != null && customer.MinMarkUp > 0;

            if (isValid)
            {
                SavePin(customer.MinMarkUp, string.Empty);
            }

            return this.Json(isValid, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult VehicleCount()
        {
            return this.Json(CountVehicle(), JsonRequestBehavior.AllowGet);
        }

        private CustomerMarkupTables Customer()
        {
            _retailModeDealerStore = new RetailModeDealerStore(User);

            var customerCode = (Tenant.Value.Key == "networkq") ? _retailModeDealerStore.Get().Code : Retail.No;

            return Session.Load<CustomerMarkupTables>(CustomerMarkupTables.CreateIdFor(customerCode));
        }

        private CustomerMarkupTables.Table LastSavedTable()
        {
            var customerCode = (Tenant.Value.Key == "networkq") ? _retailModeDealerStore.Get().Code : Retail.No;

            return Session.Load<CustomerMarkupTables>(CustomerMarkupTables.CreateIdFor(customerCode)).Tables.OrderByDescending(x => x.CreatedDate).FirstOrDefault();
        }

        private int CountVehicle()
        {
            var proxy = NinjectWebCommon.Kernel.Get<AuthorizedHttpClient>();

            var param = new LotsGetParams { Makes = Retail.Makes };

            var data = proxy.PostAsJson("search?", param)
                 .AcceptJson().SendAsync()
                 .ContinueWith(x =>
                 {
                     x.Result.EnsureSuccessStatusCode();

                     return int.Parse(x.Result.Headers.GetValues("X-TotalResults").First());
                 });

            return data.Result;
        }

        private void SavePin(double pin, string tableName)
        {
            var valuelines = new List<MarkupTableLine> { MarkupTableLine.Create(0, (int)pin, null) };

            var markupTable = new MarkupTable(tableName, valuelines
                                    .Select(x => new MarkupTable.FixedMarkupWithin(x.From.Value, x.To, x.Markup.Value))
                                    .ToArray());

            new MarkupTableSessionStore(WebSession).Set(markupTable);
        }
    }
}