﻿using System.Web.Mvc;
using Sfs.EStore.Web.App.Code.Widgets.Liquid.Drops;
using Sfs.EStore.Web.App.Controllers;
using Sfs.EStore.Web.App.Models;

namespace Sfs.EStore.Web.App.Areas.Retail.Controllers
{
    public class DealerNotKnownController : ApplicationController
    {
        public ActionResult Index()
        {
            const string permalink = "retail-dealer-not-known";
            return CmsManagedPage(permalink, new CmsPageLiquidVariables(new UserDrop(User.Identity), new CmsPageDrop(Request)));
        }
    }
}