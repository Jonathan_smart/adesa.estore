using System.Diagnostics;
using System.Web;
using System.Web.Mvc;
using Sfs.EStore.Web.App.Areas.Retail.Models;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Controllers;

namespace Sfs.EStore.Web.App.Areas.Retail.Controllers
{
    [AllowAnonymous]
    public class RetailMenuController : ApplicationController
    {
        private readonly RetailModeDealerStore _retailModeDealerStore;

        public RetailMenuController(RetailModeDealerStore retailModeDealerStore)
        {
            _retailModeDealerStore = retailModeDealerStore;
        }

        public ActionResult Index()
        {
            var docId = IdToRavenId("navbar-retail");
            var menu = Session.Load<SiteMenu>(docId);

            Debug.Assert(menu != null, "menu != null");

            var items = menu.Items;

            var dealer = new RetailModeDealer("", "");

            if (Tenant.Value.Key == "fga")
            {
                if (Retail!=null)
                {
                    dealer = new RetailModeDealer(Retail.No, Retail.Name);
                }
            }
            else
            {
                dealer = _retailModeDealerStore.Get();
            }

            return PartialView("index", new RetailMenuModel
            {
                Dealer = dealer,
                Items = items
            });
        }

        private static string IdToRavenId(string id)
        {
            return string.Format("SiteMenus/{0}", id);
        }
    }
}