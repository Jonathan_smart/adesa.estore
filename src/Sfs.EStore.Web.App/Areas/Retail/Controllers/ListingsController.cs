﻿using System.Web;
using System.Web.Mvc;
using Sfs.Darker.CommandProcessor;
using Sfs.EStore.Ports.Listings;
using Sfs.EStore.Web.App.Areas.Retail.Models;
using Sfs.EStore.Web.App.Code.Mvc;
using Sfs.EStore.Web.App.Controllers;
using Sfs.EStore.Web.App.Ports.Listings;

namespace Sfs.EStore.Web.App.Areas.Retail.Controllers
{
    [AuthoriseActivity("/Lots/View")]
    [RequiresDealer]
    [EnsureMarkup]
    public class ListingsController : ApplicationController
    {
        private readonly ICommandProcessor _commandProcessor;
        private readonly ITranslateListingDetails _listingDetailTranslator;

        public ListingsController(ICommandProcessor commandProcessor,
            ITranslateListingDetails listingDetailTranslator)
        {
            _commandProcessor = commandProcessor;
            _listingDetailTranslator = listingDetailTranslator;
        }

        [HttpGet]
        public ActionResult Index(int lotNumber)
        {
            var cmd = new GetListingInfo(lotNumber);
            _commandProcessor.Send(cmd);

            if (cmd.Result == null)
                throw new HttpException(404, string.Format("Listing {0} cannot be found", lotNumber));
            // TODO: What to do if vehicle is not active?
            //if (cmd.Result.SellingStatus.SellingState != ListingStatus.Active)
            //    throw new HttpException(404, "Listing " + lotNumber + " cannot be found")
            var markupTable = new MarkupTableSessionStore(WebSession).Get();
            var model = _listingDetailTranslator.Translate(cmd.Result);
            model.Markup = markupTable.Markup((double)model.ListingInfo.BuyItNowPrice.Amount);

            return View(new ListingsIndexModel
            {
                Listing = model,
                MarkupTable = markupTable,
                Brand = Retail.Marque
            });
        }
    }
}
