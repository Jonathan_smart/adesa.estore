﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Raven.Client;
using Sfs.EStore.Web.App.Areas.Retail.Models;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Controllers;

namespace Sfs.EStore.Web.App.Areas.Retail.Controllers
{
    [RequiresDealer]
    public class MarkupController : ApplicationController
    {
        private readonly RetailModeDealerStore _retailModeDealerStore;

        public MarkupController(RetailModeDealerStore retailModeDealerStore)
        {
            _retailModeDealerStore = retailModeDealerStore;
        }

        public ActionResult Index()
        {
            if (Tenant.Value.Key == "networkq")
            {
                return View(MarkupTableForm.Empty(SavedTables(), string.Empty, GetDefaultMarkup(_retailModeDealerStore.Get().Code, Session)));
            }

            if (Retail.No == null) { return Redirect("~/purchasing-not-setup"); }

            var dealer = Retail;

            Tenant.Value.Settings.Marquee.Name = dealer.Marque == "found" ? SiteTenantSettings.MarqueeSettings.Type.found : SiteTenantSettings.MarqueeSettings.Type.select;

            return View(MarkupTableForm.Empty(SavedTables(), dealer.No + "-" + dealer.Name, GetDefaultMarkup(dealer.No, Session)));
        }

        [HttpGet]
        public ActionResult Reset()
        {
            new MarkupTableSessionStore(WebSession).Reset();
            HttpCookie expireCookie = new HttpCookie("_mk");
            expireCookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(expireCookie);

            return RedirectToAction("index");
        }


        [HttpPost]
        public ActionResult Index(MarkupTableForm form)
        {
            if (Tenant.Value.Key == "fga")
            {
                form.SelectedDealer = Retail;
            }

            if (form == null)
            {
                return View(MarkupTableForm.Empty(savedTables: SavedTables()));
            }

            if (form.TableToDelete.HasValue)
            {
                var customer = CustomerTables(0);
                customer.RemoveTable(form.TableToDelete.Value);
                return RedirectToAction("index");
            }

            if (form.TableToEdit.HasValue)
            {
                return View(MarkupTableForm.CloneForResubmission(form, SavedTables(), GetDefaultMarkup(form.SelectedDealer.No, Session)));
            }

            if (!ModelState.IsValid)
            {
                if (Tenant.Value.Key == "fga")
                {
                    return
               View(MarkupTableForm.CloneForResubmission(form, SavedTables(),
                   GetDefaultMarkup(form.SelectedDealer.No, Session)));
                }
                else
                {
                    return
                   View(MarkupTableForm.CloneForResubmission(form, SavedTables(),
                       GetDefaultMarkup(_retailModeDealerStore.Get().Code, Session)));
                }

            }


            var valueLines = form.NewValueLines();
            int minMarkup = valueLines[0].Markup ?? 0;

            if (form.Mode == MarkupTableForm.PricingMode.Simple)
            {
                CustomerTables(minMarkup);
            }

            var markupTable = new MarkupTable(form.TableName, valueLines
                .Select(x => new MarkupTable.FixedMarkupWithin(x.From.Value, x.To, minMarkup))
                .ToArray());
            new MarkupTableSessionStore(WebSession).Set(markupTable);

            if (form.SaveTable)
            {
                if (SavedTables().Where(x => x.Name.Trim() == form.TableName.Trim()).Any())
                {
                    var c = SavedTables().Where(x => x.Name.Trim() == form.TableName.Trim()).FirstOrDefault();

                    var customer = CustomerTables(0);
                    customer.RemoveTable(c.Id);

                    var newTable = new CustomerMarkupTableTranslator().Translate(form, User.Identity.Name);
                    customer.AddTable(newTable);

                }
                else
                {
                    var customer = CustomerTables(0);
                    var newTable = new CustomerMarkupTableTranslator().Translate(form, User.Identity.Name);
                    customer.AddTable(newTable);


                }
            }
            if (Tenant.Value.Key == "networkq")
                return Redirect(Url.Content("~/retail"));

            return RedirectToAction("index");

        }

        public CustomerMarkupTables CustomerTables(int minMarkup)
        {
            var customerCode = (Tenant.Value.Key == "fga") ? Retail.No : _retailModeDealerStore.Get().Code;

            var customer = Session.Load<CustomerMarkupTables>(CustomerMarkupTables.CreateIdFor(customerCode));

            if (customer == null)
            {
                customer = new CustomerMarkupTables(customerCode, minMarkup, new CustomerMarkupTables.Table[0]);
                Session.Store(customer);
            }

            if (minMarkup > 0) { SaveMinMarkup(customer, minMarkup); }

            return customer;
        }

        private IEnumerable<MarkupTableForm.SavedMarkupTable> SavedTables()
        {
            var customer = CustomerTables(0);

            if (customer == null) return new MarkupTableForm.SavedMarkupTable[0];

            var translator = new CustomerMarkupTableTranslator();

            return customer.ActiveTables().OrderByDescending(x => x.CreatedDate).Select(x => translator.Translate(x));
        }

        private CustomerMarkupTables SaveMinMarkup(CustomerMarkupTables data, int minMarkup)
        {
            data.MinMarkUp = minMarkup > 0 ? minMarkup : data.MinMarkUp;

            return data;
        }

        private static Double GetDefaultMarkup(string customerCode, IDocumentSession session)
        {
            var customer = session.Load<CustomerMarkupTables>(CustomerMarkupTables.CreateIdFor(customerCode));

            return customer != null ? customer.MinMarkUp : 0;
        }
    }
}