﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sfs.EStore.Web.App.Code.Cms;
using Sfs.EStore.Web.App.Code.Mvc;
using Sfs.EStore.Web.App.Controllers;
using Sfs.EStore.Web.App.Models;

namespace Sfs.EStore.Web.App.Areas.Retail.Controllers
{
    [AuthoriseActivity("/Lots/View")]
    [RequiresDealer]
    [EnsureMarkup]
    public class SearchController : ApplicationController
    {
        public ActionResult Index()
        {
            var makes = Retail.Makes;

            var page = new SearchPageModel(GetPage("retail-search-all"))
            {
                PredefinedQueryParams =
                {
                    Makes = makes,
                    LtAuction = false,
                    LtBin = true
                }
            };

            if (!page.PageFound())
            {
                throw new HttpException(404, "Retail search page not found");
            }

            if (!page.CanBeAccessedAs(User))
                return new HttpUnauthorizedResult();

            return View(page);
        }

        private CmsPage GetPage(string permaName)
        {
            return Session.Query<CmsPage>()
                .FirstOrDefault(x => x.PermaName == permaName);
        }
    }
}
