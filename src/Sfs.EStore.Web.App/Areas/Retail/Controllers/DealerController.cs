﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Sfs.Customers.Adapters.DataAccess.Entities;
using Sfs.EStore.Web.App.Areas.Retail.Models;
using Sfs.EStore.Web.App.Controllers;

namespace Sfs.EStore.Web.App.Areas.Retail.Controllers
{
    public class DealerController : ApplicationController
    {
        private readonly RetailModeDealerStore _retailModeDealerStore;
        private readonly NavisionContext _dbContext = new NavisionContext();

        public DealerController(RetailModeDealerStore retailModeDealerStore)
        {
            _retailModeDealerStore = retailModeDealerStore;
        }
        // GET: Retail/Dealer
        [HttpGet]
        public ActionResult Index()
        {
            Retail.Reset();

            return View();
        }

        [HttpGet]
        public Task<JsonResult> Get()
        {
            return Task.Factory.StartNew(() =>
            {
                using (_dbContext.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
                {
                    return this.Json(_dbContext.Set<ContactUsername>().Where(x => x.Username == User.Identity.Name)
                        .Join(_dbContext.Set<ContactRelationship>(),
                            contactUser => contactUser.ContactNo,
                            contactRelation => contactRelation.ContactNo,
                            (contactRelation, contactUser) => contactUser).OrderBy(x => x.IsDefault)
                        .Join(_dbContext.Set<Customer>().Where(x => x.GlobalDimension1Code == "FGA"),
                            cus => cus.SourceNo,
                            contactUser => contactUser.No,
                            (cus, contactUser) => contactUser)

                        .Select(x => new Dealer { Name = x.Name.ToUpper() + " - " + x.City.ToUpper(), No = x.No })
                        .Distinct().ToList(), JsonRequestBehavior.AllowGet);

                }
            });
        }

        [HttpGet]
        public Task<JsonResult> GetDealer(DealerModel dealer)
        {
            return Task.Factory.StartNew(() =>
          {
              var returnModel = new DealerModel();

              returnModel.No = dealer.No;

              var _found = new string[] { "FIAT", "FIAT ABARTH", "FIAT PROFESSIONAL" };
              var _selectedForYou = new string[] { "ALFA ROMEO", "JEEP" };
              var _foundFranchise = new List<string>();
              var _selectedForYouFranchise = new List<string>();

              using (_dbContext.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
              {
                  returnModel.Name = _dbContext.Set<Customer>().Where(x => x.No == dealer.No).Select(x => x.Name.ToUpper() + " - " + x.City.ToUpper()).FirstOrDefault();

                  _foundFranchise =
                     _dbContext.Set<Customer>().Where(x => x.No == dealer.No)
                         .Join(
                             _dbContext.Set<CustomerCampaign>()
                                 .Where(x => x.Type == 1 & _found.Contains(x.FranchiseCode)),
                             campaign => campaign.No,
                             cus => cus.CustomerNo, (campaign, cus) => cus)
                         .Select(x => x.FranchiseCode).Distinct().ToList();

                  _selectedForYouFranchise =
                   _dbContext.Set<Customer>().Where(x => x.No == dealer.No)
                          .Join(
                              _dbContext.Set<CustomerCampaign>()
                                  .Where(x => x.Type == 1 & _selectedForYou.Contains(x.FranchiseCode)),
                              campaign => campaign.No,
                              cus => cus.CustomerNo, (campaign, cus) => cus)
                          .Select(x => x.FranchiseCode).Distinct().ToList();

                  returnModel.FoundFranchises = _foundFranchise;
                  returnModel.SelectFranchises = _selectedForYouFranchise;
              }

              return this.Json(returnModel, JsonRequestBehavior.AllowGet);
          });
        }


        [HttpGet]
        public Task<JsonResult> ReturnDealer()
        {
            return Task.Factory.StartNew(() =>
            {
                return this.Json(RetailView.returnDealer(this.HttpContext), JsonRequestBehavior.AllowGet);

            });
        }
        
        [HttpPost]
        [Authorize]
        public void Setup(DealerModel dealer)
        {
            Retail.Load(dealer, User);
        }
    }
}