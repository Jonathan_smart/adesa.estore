﻿using System.Web.Mvc;
using Sfs.EStore.Web.App.Code.Widgets.Liquid.Drops;
using Sfs.EStore.Web.App.Controllers;
using Sfs.EStore.Web.App.Models;

namespace Sfs.EStore.Web.App.Areas.Retail.Controllers
{
    [RequiresDealer]
    [EnsureMarkup]
    public class CmsPageController : ApplicationController
    {
        [HttpGet]
        public ActionResult Index(string permaName = "home")
        {
            var permalink = CmsPagePermanameHelper.Normalize(string.Format("retail-{0}", permaName));

            return CmsManagedPage(permalink, new CmsPageLiquidVariables(new UserDrop(User.Identity), new CmsPageDrop(Request)));
        }
    }
}