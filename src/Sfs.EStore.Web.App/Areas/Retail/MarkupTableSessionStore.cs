﻿using System;
using System.Web;
using System.Web.SessionState;

namespace Sfs.EStore.Web.App.Areas.Retail
{
    public class MarkupTableSessionStore
    {
        private const string SessionKey = "MarkupTable";
        private readonly HttpSessionStateBase _session;
        private HttpSessionState session;

        /// <exception cref="ArgumentNullException"><paramref name="session"/> is <see langword="null" />.</exception>
        public MarkupTableSessionStore(HttpSessionStateBase session)
        {
            if (session == null) throw new ArgumentNullException("session");

            _session = session;
        }

        public MarkupTableSessionStore(HttpSessionState session)
        {
            this.session = session;
        }

        public void Reset()
        {
            _session.Remove(SessionKey);
        }

        public void Set(MarkupTable valueLines)
        {
            _session[SessionKey] = valueLines;
        }

        public MarkupTable Get()
        {
            return _session[SessionKey] as MarkupTable;
        }
    }
}