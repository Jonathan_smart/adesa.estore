﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Sfs.EStore.Web.App.Areas.Retail
{
    public class EnsureMarkupAttribute : ActionFilterAttribute
    {
        internal const string SessionKey = "MarkupTable";

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var table = filterContext.HttpContext.Session[SessionKey];

            if (table != null) return;

            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
            {
                {"action", "index"},
                {"controller", "markup"},
                {"area", "retail"}
            });
        }
    }
}