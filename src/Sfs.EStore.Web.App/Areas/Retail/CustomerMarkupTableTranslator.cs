using System;
using System.Linq;
using Sfs.EStore.Web.App.Areas.Retail.Models;
using Sfs.EStore.Web.App.Code;

namespace Sfs.EStore.Web.App.Areas.Retail
{
    public class CustomerMarkupTableTranslator
    {
        /// <exception cref="ArgumentNullException"><paramref name="table"/> is <see langword="null" />.</exception>
        public MarkupTableForm.SavedMarkupTable Translate(CustomerMarkupTables.Table table)
        {
            if (table == null) throw new ArgumentNullException("table");

            return new MarkupTableForm.SavedMarkupTable
            {
                Id = table.Id,
                Name = table.Name,
                CreatedDate = table.CreatedDate,
                Lines = table.Lines.Select(x => new SavedMarkupTableLine
                {
                    From = (int)x.From,
                    To = x.To.HasValue ? (int)x.To.Value : (int?)null,
                    Markup = (int)x.Markup
                }).ToArray()
            };
        }

        /// <exception cref="ArgumentNullException"><paramref name="table"/> is <see langword="null" />.</exception>
        public CustomerMarkupTables.Table Translate(MarkupTableForm table, string createdBy)
        {
            if (table == null) throw new ArgumentNullException("table");

            return new CustomerMarkupTables.Table(table.TableName, createdBy,
                table.NewValueLines().Select(x => new CustomerMarkupTables.TableLine(x.From.Value, x.To, x.Markup.Value)).ToArray());
        }

        //public CustomerMarkupTables.Table Translate(MarkupTableForm.SavedMarkupTable table, MarkupTableLine[] Lines, string createdBy)
        //{
        //    if (table == null) throw new ArgumentNullException("table");

        //    return new CustomerMarkupTables.Table(table, Lines.Select(x => new CustomerMarkupTables.TableLine((double)x.From, x.To, (double)x.Markup)).ToArray());

        //}
    }
}