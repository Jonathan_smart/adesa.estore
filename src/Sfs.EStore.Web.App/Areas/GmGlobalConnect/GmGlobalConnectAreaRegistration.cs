﻿using System.Web.Mvc;

namespace Sfs.EStore.Web.App.Areas.GmGlobalConnect
{
    public class GmGlobalConnectAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "GmGlobalConnect";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "GmGlobalConnect_default",
                "GmGlobalConnect/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}