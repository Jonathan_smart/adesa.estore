using Sfs.Darker.CommandProcessor;
using Sfs.EStore.Ports.GmGlobalConnect;

namespace Sfs.EStore.Web.App.Areas.GmGlobalConnect.Ports
{
    public class EStoreApiBuyerPermissions : ISetBuyerPermissions
    {
        private readonly ILog _logger;
        private readonly AuthorizedHttpClient _client;

        public EStoreApiBuyerPermissions(AuthorizedHttpClient apiProxy, ILog logger)
        {
            _client = apiProxy;
            _logger = logger;
        }

        public bool Allow(Buyer buyer)
        {
            _logger.Debug("Attempting to give buyer permissions to {user} of {dealer}", buyer.Username, buyer.DealerCode);

            var apiResponse = _client
                .Put(string.Format("membership/{0}/buying-status/{1}", buyer.Username, buyer.DealerCode), null)
                .SendAsync().Result;

            if (!apiResponse.IsSuccessStatusCode)
                _logger.Warning("Failed to give buyer permissions to {user} of {dealer}", buyer.Username, buyer.DealerCode);

            return apiResponse.IsSuccessStatusCode;
        }

        public bool Revoke(Buyer buyer)
        {
            _logger.Debug("Attempting to revoke buyer permissions to {user} of {dealer}", buyer.Username, buyer.DealerCode);

            var apiResponse = _client
                .Delete(string.Format("membership/{0}/buying-status/{1}", buyer.Username, buyer.DealerCode))
                .SendAsync().Result;

            if (!apiResponse.IsSuccessStatusCode)
                _logger.Warning("Failed to revoke buyer permissions to {user} of {dealer}", buyer.Username, buyer.DealerCode);

            return apiResponse.IsSuccessStatusCode;
        }
    }
}