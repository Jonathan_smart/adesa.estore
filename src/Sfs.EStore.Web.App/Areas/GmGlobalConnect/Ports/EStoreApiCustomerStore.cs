using System.Net;
using System.Net.Http;
using Sfs.Core;
using Sfs.Customers.Adapters.DataAccess;
using Sfs.EStore.Ports.GmGlobalConnect;

namespace Sfs.EStore.Web.App.Areas.GmGlobalConnect.Ports
{
    public class DatabaseCustomerStore : ICustomerStore
    {
        public KnownCustomer Get(string dealerCode)
        {
            using (var dbContext = new NavisionContext())
            {
                var customer = new CustomersQuery(dbContext).Query(dealerCode);
                return customer == null
                    ? null
                    : new KnownCustomer(customer.No, customer.Name);
            }
        }
    }
}