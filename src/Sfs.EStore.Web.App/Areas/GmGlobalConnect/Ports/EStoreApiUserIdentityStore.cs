using System.Net.Http;
using Sfs.EStore.Ports.GmGlobalConnect;
using Sfs.EStore.Web.App.Code;

namespace Sfs.EStore.Web.App.Areas.GmGlobalConnect.Ports
{
    public class EStoreApiUserIdentityStore : IUserIdentityStore
    {
        private readonly AuthorizedHttpClient _client;

        public EStoreApiUserIdentityStore(AuthorizedHttpClient apiProxy)
        {
            _client = apiProxy;
        }

        public AuthenticatedUser AuthenticateAs(User identity)
        {
            var apiResult = _client.PutAsJson(string.Format("membership/{0}/upsert", identity.Username), new
            {
                Email = identity.Email,
                FirstName = identity.Username,
                Roles = identity.Roles
            })
                .AcceptJson().SendAsync()
                .Result;

            apiResult.EnsureSuccessStatusCode();

            var apiAuthenticatedUser = apiResult.Content.ReadAsAsync<ApiValidator.AuthenticationResponse>().Result;

            return new AuthenticatedUser(identity.Username, apiAuthenticatedUser.Token, identity.Roles, identity.Email)
            {
                FirstName = apiAuthenticatedUser.FirstName
            };
        }
    }
}