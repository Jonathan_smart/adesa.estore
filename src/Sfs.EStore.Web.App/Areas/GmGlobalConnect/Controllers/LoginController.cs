﻿using System.Net;
using System.Web.Mvc;
using System.Web.Security;
using Sfs.Darker.CommandProcessor;
using Sfs.EStore.Ports.GmGlobalConnect;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Controllers;
using Sfs.EStore.Web.App.DomainEvents;

namespace Sfs.EStore.Web.App.Areas.GmGlobalConnect.Controllers
{
    [AllowAnonymous]
    public class LoginController : ApplicationController
    {
        private readonly ICommandProcessor _commandProcessor;

        public LoginController(ICommandProcessor commandProcessor)
        {
            _commandProcessor = commandProcessor;
        }

        public ActionResult Index(GlobalConnectDetails user)
        {
            if (user == null) return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            var cmd = new Authenticate
            {
                DealerCode = user.DealerCode,
                Email = user.Email,
                UserId = user.UserId,
                IvUser = Request.Headers["iv-user"],
                Role = user.Role,
                RemoteAddress = IPAddress.Parse(Request.ServerVariables["REMOTE_ADDR"])
            };

            _commandProcessor.Send(cmd);



            if (!cmd.Result.Authenticated)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            var authenticatedUser = cmd.Result.AuthenticatedUser;
            var identity = new UserIdentity(authenticatedUser.Username,
                authenticatedUser.Roles, authenticatedUser.Token)
            {
                FirstName = authenticatedUser.FirstName,
                EmailAddress = authenticatedUser.Email
            };
            identity["RetailMode_Dealer_Code"] = cmd.Result.KnownCustomer.Code;
            identity["RetailMode_Dealer_Name"] = cmd.Result.KnownCustomer.Name;

            Response.SetFormsAuthentication(identity);

            DomainEvent.Raise(new UserLoggedIn(identity, UniqueSessionId.Value));

            return cmd.Result.SetupForPurchasing
                ? (ActionResult)Redirect(FormsAuthentication.DefaultUrl)
                : RedirectToAction("index", "purchasingnotsetup", new { area = "gmglobalconnect" });
        }

        public class GlobalConnectDetails
        {
            public string UserId { get; set; }
            public string Email { get; set; }
            public string DealerCode { get; set; }
            public string Role { get; set; }
            public string Key { get; set; }
        }
    }
}