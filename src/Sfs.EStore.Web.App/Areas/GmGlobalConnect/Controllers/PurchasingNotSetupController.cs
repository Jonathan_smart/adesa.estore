﻿using System.Web.Mvc;
using Sfs.EStore.Web.App.Controllers;

namespace Sfs.EStore.Web.App.Areas.GmGlobalConnect.Controllers
{
    public class PurchasingNotSetupController : ApplicationController
    {
        public ActionResult Index()
        {
            return CmsManagedPage("purchasing-not-setup");
        }
    }
}