using System.Data.Entity;
using System.Reflection;
using Sfs.Customers.Adapters.DataAccess;

namespace Sfs.EStore.Web.App
{
    internal class NavisionContext : DbContext
    {
        public NavisionContext()
            : base("Name=NavisionContext")
        {
            Database.SetInitializer((IDatabaseInitializer<NavisionContext>)null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetExecutingAssembly());
            modelBuilder.Configurations.AddFromAssembly(typeof(ShipToAddressQuery).Assembly);
            base.OnModelCreating(modelBuilder);
        }
    }
}