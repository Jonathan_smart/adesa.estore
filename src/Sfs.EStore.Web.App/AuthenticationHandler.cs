using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.Security;

namespace Sfs.EStore.Web.App
{
    public class AuthenticationHandler
    {
        private readonly HttpContextWrapper _context;
        private readonly SiteTenant _tenant;

        public AuthenticationHandler(HttpContextWrapper context, SiteTenant tenant)
        {
            _tenant = tenant;
            _context = context;
        }

        public void Handle()
        {
            var authCookie = _context.Request.Cookies[FormsAuthentication.FormsCookieName];

            if (authCookie == null)
            {

                _context.User = new GenericWebApplicationPrincipal(_tenant.ApiAccessKeyId,
                    new GenericWebApplicationIdentity("",
                        data: null, guestPassId: GuestSource()));
                return;
            }

            FormsAuthenticationTicket authTicket;
            try
            {
                authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                FormsAuthentication.RenewTicketIfOld(authTicket);
            }
            catch (ArgumentException)
            {
                FormsAuthentication.SignOut();
                _context.User = new NullWebApplicationPrincipal(_tenant.ApiAccessKeyId);
                return;
            }

            UserIdentity authUser;
            try
            {
                authUser = JObject.Parse(authTicket.UserData).ToObject<UserIdentity>();
            }
            catch (JsonReaderException)
            {
                _context.User = new NullWebApplicationPrincipal(_tenant.ApiAccessKeyId);
                return;
            }

            _context.User = new GenericWebApplicationPrincipal(_tenant.ApiAccessKeyId,
                new GenericWebApplicationIdentity(authTicket.Name,
                    authUser.Data,
                    GuestSource())
                {
                    FirstName = authUser.FirstName,
                    LastName = authUser.LastName,
                    EmailAddress = authUser.EmailAddress,
                    PhoneNumber = authUser.PhoneNumber,
                },
                authUser.Roles.ToArray(), authUser.Token);
        }

        private string GuestSource()
        {
            var affiliateCookie = _context.Request.Cookies[GuestPass.CookieName];

            if (affiliateCookie == null || string.IsNullOrWhiteSpace(affiliateCookie.Value)) return "";

            try
            {
                return GuestPass.Decrypt(affiliateCookie.Value).Id ?? "";
            }
            catch(ArgumentException)
            {
                return "";
            }
        }
    }
}