using System.IO;
using System.Text.RegularExpressions;
using Cassette;
using Cassette.Stylesheets;

namespace Sfs.EStore.Web.App
{
    public class CassetteBundleThemeConfiguration : IConfiguration<BundleCollection>
    {
        public void Configure(BundleCollection bundles)
        {
            bundles.AddPerSubDirectory<StylesheetBundle>("content/themes",
                                                         new FileSearch
                                                             {
                                                                 Pattern = "*.css;*.scss",
                                                                 Exclude = new Regex("(print)|(_)", RegexOptions.IgnoreCase),
                                                                 SearchOption = SearchOption.AllDirectories
                                                             }, bundle =>
                                                                 {
                                                                     //bundle.Media = "screen";
                                                                 });
            bundles.AddPerSubDirectory<StylesheetBundle>("~/areas/retail/content");
        }
    }
}