using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;

namespace Sfs.EStore.Web.App
{
    public static class UrlImageExtensions
    {
        public static IHtmlString VehicleImage(this HtmlHelper @this, ImageModel image, string size,
            bool useMissingImage = false, object htmlAttributes = null, string overlay = null)
        {
            return VehicleImage(image, size, useMissingImage, htmlAttributes, overlay);
        }

        public static readonly Func<ImageModel> MissingImage = () =>
        {
            var tenant = new HttpContextWrapper(HttpContext.Current).CurrentTenant();
            return tenant.Settings.Images.MissingImage;
        };

    internal static string ImageUrl(ImageModel image, string size, string overlay)
        {
            return image != null
                ? image.AsUrl(size, overlay)
                : MissingImage().AsUrl(size);
        }

        private static IHtmlString VehicleImage(ImageModel image, string size, bool useMissingImage = false, object htmlAttributes = null, string overlay = null)
        {
            if (image == null && !useMissingImage) return MvcHtmlString.Empty;

            var tagBuilder = new TagBuilder("img");

            var attrs = (IDictionary<string, object>)HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
            tagBuilder.MergeAttributes(attrs);

            var imageUrl = ImageUrl(image, size, overlay);

            tagBuilder.MergeAttribute("src", imageUrl);

            return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.SelfClosing));
        }
    }
}