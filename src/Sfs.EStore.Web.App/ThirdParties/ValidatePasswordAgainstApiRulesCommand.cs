using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Sfs.EStore.Web.App.Areas.Account.Models;
using Sfs.EStore.Web.App.Commands;

namespace Sfs.EStore.Web.App.ThirdParties
{
    public class ValidatePasswordAgainstApiRulesCommand : Command, IValidatePasswordRulesCommand
    {
        public override void Execute()
        {
            ApiProxy.Post("userregistration/validatepassword?password=" + Password, null)
                .AcceptJson().SendAsync()
                .ContinueWith(t =>
                                  {
                                      if (t.Result.StatusCode == HttpStatusCode.OK)
                                      {
                                          Result = true;
                                          return;
                                      }

                                      t.Result.Content
                                          .ReadAsAsync<HttpError>()
                                          .ContinueWith(e =>
                                                            {
                                                                if (e.Result.ContainsKey("modelState"))
                                                                {
                                                                    var errors =
                                                                        ((JObject) e.Result["modelState"])
                                                                            .ToObject<Dictionary<string, IList<string>>>
                                                                            ();
                                                                    foreach (var err in errors)
                                                                        foreach (var msg in err.Value)
                                                                        {
                                                                            ModelState.AddModelError(err.Key, msg);
                                                                        }
                                                                }
                                                                else
                                                                    ModelState.AddModelError("",
                                                                                             "Failed to register user");

                                                            })
                                          .Wait();
                                  })
                .Wait();
        }

        public bool Result { get; private set; }

        public string Password { get; set; }
        public ModelStateDictionary ModelState { get; set; }

        public RegistrationIndexModel Request { get; set; }
    }
}