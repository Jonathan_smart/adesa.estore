using System;
using System.Net.Http;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Commands;

namespace Sfs.EStore.Web.App.ThirdParties
{
    public class ResetPasswordCommand : Command<UserIdentity>
    {
        private readonly string _token;
        private readonly string _newPassword;

        public ResetPasswordCommand(string token, string newPassword)
        {
            _token = token;
            _newPassword = newPassword;
        }

        public override void Execute()
        {
            ApiProxy.PostAsJson("resetpassword", new
                {
                    Token = _token,
                    NewPassword = _newPassword
                })
                .AcceptJson().SendAsync()
                .ContinueWith(x =>
                    {
                        if (!x.Result.IsSuccessStatusCode)
                            throw new InvalidOperationException("Failed to reset password.");

                        var res = x.Result.Content.ReadAsAsync<AuthenticationResponse>().Result;

                        if (!res.IsAuthenticated)
                            throw new AuthException(res.Message);

                        Result = new UserIdentity(res.Username, res.Roles, res.Token);
                    })
                .Wait();
        }

        private class AuthenticationResponse
        {
            public bool IsAuthenticated { get; set; }
            public string Message { get; set; }
            public string Username { get; set; }
            public string[] Roles { get; set; }
            public string Token { get; set; }
        }
        //[ISerializable]
        public class AuthException : Exception
        {
            public AuthException(string message)
                : base(message)
            {

            }
        }
    }
}