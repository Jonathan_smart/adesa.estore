using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Sfs.EStore.Web.App.Areas.Account.Models;
using Sfs.EStore.Web.App.Commands;
using Sfs.EStore.Web.App.DomainEvents;

namespace Sfs.EStore.Web.App.ThirdParties
{
    public class RegisterNewUsersThroughApiCommand : Command<bool>
    {
        public override void Execute()
        {
            if (!Validate())
            {
                Result = false;
                return;
            }

            var registrationRequest = new NewUserRegistrationRequest(Request);
            ApiProxy.PostAsJson("userregistration", registrationRequest)
                .AcceptJson().SendAsync()
                .ContinueWith(x =>
                {
                    Result = x.Result.IsSuccessStatusCode;
                    if (!Result)
                        ModelState.AddModelError("", "An error has occured. Please contact us.");
                })
                .Wait();

            if (Result)
                DomainEvent.Raise(new UserRegistered(registrationRequest.Username));
        }

        public ModelStateDictionary ModelState { get; set; }
        public RegistrationIndexModel Request { get; set; }

        private bool Validate()
        {
            var isValid = ApiProxy.PostAsJson("userregistration", new DryRunUserRegistrationRequest(Request))
                .AcceptJson().SendAsync()
                .ContinueWith(t =>
                                  {
                                      if (t.Result.StatusCode == HttpStatusCode.OK)
                                          return true;

                                      t.Result.Content
                                          .ReadAsAsync<HttpError>()
                                          .ContinueWith(e =>
                                                            {
                                                                if (e.Result.ContainsKey("modelState"))
                                                                {
                                                                    var errors =
                                                                        ((JObject) e.Result["modelState"])
                                                                            .ToObject<Dictionary<string, IList<string>>>();
                                                                    foreach (var err in errors)
                                                                        foreach (var msg in err.Value)
                                                                        {
                                                                            if (err.Key.Equals("username", StringComparison.InvariantCultureIgnoreCase))
                                                                                continue;
                                                                            ModelState.AddModelError(err.Key, msg);
                                                                        }
                                                                }
                                                                else
                                                                    ModelState.AddModelError("",
                                                                                             "Failed to register user");

                                                            })
                                          .Wait();


                                      return false;
                                  })
                .Result;

            if (!string.IsNullOrEmpty(Request.Password) && !Request.Password.Equals(Request.ConfirmPassword))
            {
                ModelState.AddModelError("Password", "Password confirmation does not match password");
                isValid = false;
            }

            return isValid;
        }

        private class NewUserRegistrationRequest
        {
            private readonly RegistrationIndexModel _request;

            public NewUserRegistrationRequest(RegistrationIndexModel request)
            {
                _request = request;
            }

            public string Username { get { return _request.EmailAddress; } }

            public string Password { get { return _request.Password; } }

            public string FirstName { get { return _request.FirstName; } }

            public string LastName { get { return _request.LastName; } }

            public string EmailAddress { get { return _request.EmailAddress; } }

            public string PhoneNumber { get { return _request.PhoneNumber; } }

            public string CompanyName { get { return _request.CompanyName; } }

            public string JobTitle { get { return _request.JobTitle; } }

            public string Website { get { return _request.Website; } }

            public AddressModel Address { get { return _request.Address; } }

            public Dictionary<string, string>  Data { get { return _request.Data; } }
        }

        private class DryRunUserRegistrationRequest : NewUserRegistrationRequest
        {
            public DryRunUserRegistrationRequest(RegistrationIndexModel request) : base(request)
            {
            }

            public bool DryRun { get { return true; } }
        }
    }
}