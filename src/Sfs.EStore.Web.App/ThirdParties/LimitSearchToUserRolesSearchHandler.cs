﻿using System.Collections.Generic;
using Sfs.EStore.Web.App.Code.Security;

namespace Sfs.EStore.Web.App.ThirdParties
{
    public class LimitSearchToUserRolesSearchHandler : IVehicleSearchHandler
    {
        private readonly IWebApplicationPrincipal _user;

        public LimitSearchToUserRolesSearchHandler(IWebApplicationPrincipal user)
        {
            _user = user;
        }

        public void HandlerSearchParams(ISecurable securable)
        {
            var roles = new List<string> { "public" };
            roles.AddRange(_user.Roles());

            securable.LimitToRoles = roles.ToArray();
        }
    }
}