using System.Net.Http;
using System.Web;
using Newtonsoft.Json.Linq;
using Sfs.EStore.Web.App.Commands;

namespace Sfs.EStore.Web.App.ThirdParties
{
    public class ValidatePasswordResetToken : Command<bool>
    {
        private readonly string _token;

        public ValidatePasswordResetToken(string token)
        {
            _token = token;
        }

        public override void Execute()
        {
            Result = ApiProxy.Get("resetpassword/validate?token=" + HttpUtility.UrlEncode(_token))
                .AcceptJson()
                .SendAsync()
                .ContinueWith(t => t.Result.IsSuccessStatusCode &&
                                   t.Result.Content.ReadAsAsync<JObject>()
                                       .Result.Value<bool>("isValid"))
                .Result;
        }
    }
}