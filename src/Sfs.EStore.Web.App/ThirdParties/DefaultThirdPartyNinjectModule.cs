namespace Sfs.EStore.Web.App.ThirdParties
{
    public class DefaultThirdPartyNinjectModule : ThirdPartyNinjectModule
    {
        public override void Load()
        {
            // Vehicle search handler
            Bind<IVehicleSearchHandler>().To<EmptyVehicleSearchHandler>();
            Bind<IVehicleSearchHandler>().To<LimitSearchToUserRolesSearchHandler>()
                .When(req => IntegratesWith("IVehicleSearchHandler", "Sfs.EStore.Web.App.ThirdParties.Grs.LimitSearchToUserRolesSearchHandler"));

            // User registration
            Bind<IValidatePasswordRulesCommand>().To<ValidatePasswordAgainstApiRulesCommand>();
        }
    }
}