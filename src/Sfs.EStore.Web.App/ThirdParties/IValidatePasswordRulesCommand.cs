using System.Web.Mvc;
using Sfs.EStore.Web.App.Commands;

namespace Sfs.EStore.Web.App.ThirdParties
{
    public interface IValidatePasswordRulesCommand : IQuery<bool>
    {
        string Password { get; set; }
        ModelStateDictionary ModelState { set; }
    }
}