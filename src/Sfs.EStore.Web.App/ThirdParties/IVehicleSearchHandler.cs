using System.Collections.Generic;
using Sfs.EStore.Ports.GmGlobalConnect;

namespace Sfs.EStore.Web.App.ThirdParties
{
    public interface IVehicleSearchHandler
    {
        void HandlerSearchParams(ISecurable securable);
    }

    public class EmptyVehicleSearchHandler : IVehicleSearchHandler
    {
        public void HandlerSearchParams(ISecurable securable)
        {
   
        }
    }

    public interface ISecurable
    {
        string[] LimitToRoles { get; set; }
    }
}