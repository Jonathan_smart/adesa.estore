﻿using System.Collections.Generic;
using Ninject;
using Ninject.Modules;
using Sfs.EStore.Web.App.Code;

namespace Sfs.EStore.Web.App.ThirdParties
{
    public abstract class ThirdPartyNinjectModule : NinjectModule
    {
        protected bool IntegratesWith(string settingName, string expectedValue)
        {
            var settings = CurrentIntegrationSettings();

            if (settings == null) return false;

            string actualValue;
            return settings.TryGetValue(settingName, out actualValue) && actualValue == expectedValue;
        }

        private IDictionary<string, string> CurrentIntegrationSettings()
        {
            var tenant = Kernel.Get<SiteTenant>();
            
            return tenant.IntegrationSettings;
        }
    }
}