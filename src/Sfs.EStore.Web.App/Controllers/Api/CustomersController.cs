using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Sfs.Core;
using Sfs.Customers.Adapters.DataAccess;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    [RoutePrefix("customers")]
    public class CustomersController : ApiApplicationController
    {
        private readonly SalesChannel _salesChannel;

        public CustomersController(SalesChannel salesChannel)
        {
            _salesChannel = salesChannel;
        }

        public Task<HttpResponseMessage> Get()
        {
            return Task.Factory.StartNew(() =>
            {
                using (var dbContext = new NavisionContext())
                {
                    var contact = new ContactUsernameQuery(_salesChannel, dbContext).Query(User.Identity.Name);
                    if (contact == null)
                        return Request.CreateResponse(HttpStatusCode.OK, new CustomerGetResult[0]);

                    var customers = new ContactCustomersQuery(contact.ContactNo, contact.BusinessUnit, dbContext).Query();

                    return Request.CreateResponse(HttpStatusCode.OK,
                        customers.Select(x => new CustomerGetResult
                        {
                            No = x.No,
                            City = x.City,
                            Name = x.Name
                        }));
                }
            });
        }

        public class CustomerGetResult
        {
            public string No { get; set; }
            public string Name { get; set; }
            public string City { get; set; }
        }
    }
}