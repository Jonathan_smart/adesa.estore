﻿using Sfs.EStore.Web.App.Code.Http;
using Sfs.EStore.Web.App.Models;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    public class MakeOfferController : ApiApplicationController
    {
        [ValidationFilter, ModelRequiredFilter]
        public Task Post(MakeOfferPostModel model)
        {
            return Task.Factory.StartNew(() => postMakeOffer(model));
        }
        private HttpResponseMessage postMakeOffer(MakeOfferPostModel model)
        {
            return ApiProxy.PostAsJson(string.Format("listings/{0}/makeoffer", model.LotId), model)
                .AcceptJson().SendAsync()
                .ContinueWith(
                    t =>
                        Request.CreateResponse(t.Result.IsSuccessStatusCode
                            ? HttpStatusCode.OK
                            : t.Result.StatusCode))
                .Result;

        }
    }
}
