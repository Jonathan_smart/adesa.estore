﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.Http;
using Sfs.EStore.Web.App.Models;
using log4net;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    [AuthoriseActivity("/PartEx")]
    public class PartExController : ApiApplicationController
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(PartExController));

        public Task<HttpResponseMessage> Get()
        {
            return Task.Factory.StartNew(() =>
                Request.CreateResponse(HttpStatusCode.OK,
                    ApiProxy.Get("valuations")
                        .AcceptJson()
                        .SendAsync()
                        .Result.Content.ReadAsAsync<PreviousValuationModel[]>().Result));
        }

        public Task<HttpResponseMessage> Delete(Guid id)
        {
            return Task.Factory.StartNew(() => DeleteValuation(id));
        }

        [NonAction]
        private HttpResponseMessage DeleteValuation(Guid id)
        {
            var result = ApiProxy.Delete("valuations?id=" + id).AcceptJson().SendAsync().Result;
            if (!result.IsSuccessStatusCode)
            {
                Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error deleting valuation");
            }
            return Request.CreateResponse(HttpStatusCode.NoContent);
        }

        public Task<HttpResponseMessage> Get(string registration)
        {
            return Task.Factory.StartNew(() => GetVrm(registration));
        }

        public Task<HttpResponseMessage> Post([FromUri]string registration, [FromUri]int mileage, PartExValuationModel form)
        {
            return Task.Factory.StartNew(() => GetValuation(registration, mileage, form));
        }

        private HttpResponseMessage GetVrm(string registration)
        {
            if (string.IsNullOrWhiteSpace(registration))
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                                                   "registration parameter required");

            var apiResponse = ApiProxy.Get("vrmlookup?vrm=" + registration)
                .AcceptJson().SendAsync()
                .Result;

            if (!apiResponse.IsSuccessStatusCode)
            {
                Logger.ErrorFormat("VRM ({0}) lookup failed with status {1}",
                                   registration, apiResponse.StatusCode);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                                                   "Could not retrieve details for registration " + registration);
            }

            return Request.CreateResponse(HttpStatusCode.OK, apiResponse.Content.ReadAsAsync<PartExVrmLookupModel>().Result);
        }

        private HttpResponseMessage GetValuation(string registration, int? mileage, PartExValuationModel form)
        {
            var visitorId = VisitorTrackingHttpModule.VisitorId(Request.GetHttpContext());
            SiteVisitor visitor = null;
            if (visitorId != null)
                visitor = Session.LoadAsync<SiteVisitor>(visitorId).Result;

            form.TenantEnrichment = new PartExValuationModel.TenantEnrichmentInfo();
            EnrichModelWithVisitor(form.TenantEnrichment, visitor);


            if (string.IsNullOrWhiteSpace(registration))
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                                                   "registration parameter required");

            if (!mileage.HasValue)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                                                   "mileage parameter required");

            var apiResponse = ApiProxy.PostAsJson(string.Format("valuations?vrm={0}&mileage={1}", registration, mileage), form)
                .AcceptJson()
                .SendAsync()
                .Result;

            if (!apiResponse.IsSuccessStatusCode)
            {
                Logger.ErrorFormat("Valuation failed for {0} with status {1}",
                                   registration, apiResponse.StatusCode);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                                                   "Could not retrieve valuation for registration " + registration);
            }

            return Request.CreateResponse(HttpStatusCode.OK, apiResponse.Content.ReadAsAsync<PreviousValuationModel>().Result);
        }
        private void EnrichModelWithVisitor(PartExValuationModel.TenantEnrichmentInfo info, SiteVisitor visitor)
        {
            if (info == null) throw new ArgumentNullException("info");
            if (visitor == null) return;

            info.OriginalVisitType = visitor.OriginalVisit.Source.Type.ToString();
            info.OriginalVisitSource = visitor.OriginalVisit.Source.Value;
            info.OriginalVisitAt = visitor.OriginalVisit.StartedAt;

            info.LatestVisitType = visitor.LastVisit.Source.Type.ToString();
            info.LatestVisitSource = visitor.LastVisit.Source.Value;
            info.LatestVisitAt = visitor.LastVisit.StartedAt;
        }
        public class PartExValuationModel
        {
            public ContactDetail Contact { get; set; }

            public class ContactDetail
            {
                public string FullName { get; set; }
                public string PostCode { get; set; }
                public string Telephone { get; set; }
                public string Email { get; set; }
                public string ReferrerURL { get; set; }
            }
            public TenantEnrichmentInfo TenantEnrichment { get; set; }

            public class TenantEnrichmentInfo
            {
                public string IpAddress { get; set; }

                public string OriginalVisitType { get; set; }
                public string OriginalVisitSource { get; set; }
                public DateTime? OriginalVisitAt { get; set; }
                public string LatestVisitType { get; set; }
                public string LatestVisitSource { get; set; }
                public DateTime? LatestVisitAt { get; set; }
            }
        }
    }
}
