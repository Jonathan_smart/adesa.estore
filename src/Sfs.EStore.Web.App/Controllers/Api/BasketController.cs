﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Code.Http;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    [RoutePrefix("api/basket")]
    public class BasketController : ApiApplicationController
    {
        [HttpGet, Route]
        public Task<HttpResponseMessage> Get([FromUri]int[] no, string cusNo)
        {
            return Task.Factory.StartNew(() => GetUserReservations(no, cusNo));
        }
        [HttpGet, Route]
        public Task<HttpResponseMessage> Get([FromUri]int[] no)
        {
            return Task.Factory.StartNew(() => GetUserReservations(no, string.Empty));
        }

        [ValidationFilter, ModelRequiredFilter]
        [HttpPost, Route("actions/add_listing")]
        public Task<HttpResponseMessage> Create(BasketPostModel bound)
        {
            if (bound.Markup > 0)
            {
                var Customer = _retail.Value.Name;
                return ApiProxy.Post(string.Format("lots/{0}/basket/retail/{1}/{2}", bound.ListingId, bound.Markup, Customer), null).AcceptJson().SendAsync();
            }

            if (bound.ExtendedExpiry)
                return ApiProxy.Post(string.Format("lots/{0}/basket/retail", bound.ListingId), null).AcceptJson().SendAsync();

            return ApiProxy.Post(string.Format("lots/{0}/basket", bound.ListingId), null).AcceptJson().SendAsync();
        }

        [HttpPost, Route("{entryNo}/destroy")]
        public Task<HttpResponseMessage> Delete(int entryNo)
        {
            return ApiProxy.Post(string.Format("basket/{0}", entryNo), null).AcceptJson().SendAsync();
        }

        private HttpResponseMessage GetUserReservations(int[] no, string cusNo)
        {
            var containsNegative = false;
            if (no != null)
                containsNegative = no.Any(i => i < 1);

            var qsParams = !containsNegative ? string.Join("&", (no ?? new int[0]).Select(x => string.Format("no={0}", x))) : string.Empty;
            qsParams = !containsNegative & qsParams.Length > 0 ? qsParams + "&cusNo=" + cusNo : string.Empty;

            return ApiProxy.Get("basket?" + qsParams)
                .AcceptJson().SendAsync()
                .ContinueWith(x =>
                {
                    var apiResult = x.Result;

                    apiResult.EnsureSuccessStatusCode();

                    var responseValue = apiResult.Content.ReadAsAsync<JArray>().Result
                        .Select(
                            r =>
                            {
                                var model = r.ToObject<BasketGetModel>();
                                if (model.ExpiresAt.HasValue)
                                    model.ExpiresAt = model.ExpiresAt.Value.ToLocalTime();



                                return model;
                            })
                        .ToArray();

                    return Request.CreateResponse(HttpStatusCode.OK, responseValue);
                })
                .Result;
        }
    }

    public class BasketPostModel
    {
        public int ListingId { get; set; }
        public bool ExtendedExpiry { get; set; }
        public string CustomerId { get; set; }
        public double Markup { get; set; }
    }

    public class BasketGetModel
    {
        public int No { get; set; }
        public ImageModel Image { get; set; }
        public string Title { get; set; }
        public string RegistrationPlate { get; set; }
        public string RegistrationYap { get; set; }
        public MoneyViewModel Amount { get; set; }
        public decimal BuyersFee { get; set; }
        public bool PricesIncludeVat { get; set; }
        public DateTime? ExpiresAt { get; set; }
        public int? ListingId { get; set; }
        public int Mileage { get; set; }
        public string Type { get; set; }
        public bool HasBuyitNow { get; set; }
        public MoneyViewModel MarkUp { get; set; }
        public string Dealer { get; set; }
    }
}
