﻿
using System.Data.Entity.ModelConfiguration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using Ionic.Zlib;
using Raven.Client.Connection;
using Telerik.Reporting;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    public class TelerikController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage ExportToPdf(string registrationNo)
        {
            var serialNo = "";

            using (var ctx = new NavisionContext())
            {
                 serialNo =
                    ctx.Set<SerialNoInformation>()
                        .Where(b => b.RegistrationNo == registrationNo & b.ItemNo == "VEHICLE")
                        .Select(n => n.SerialNo).FirstOrDefault();
            }

            var instanceReportSource =new Telerik.Reporting.InstanceReportSource();

            // Assigning the Report object to the InstanceReportSource
            instanceReportSource.ReportDocument = new GRS_Telerik_Reporting.Sales_Appraisal();
            instanceReportSource.Parameters.Add(new Telerik.Reporting.Parameter("serial_no", serialNo));

            var reportProcess = new Telerik.Reporting.Processing.ReportProcessor().RenderReport("PDF", instanceReportSource, null);

            var result = new HttpResponseMessage(HttpStatusCode.OK);

            result.Content = new ByteArrayContent(reportProcess.DocumentBytes, 0, reportProcess.DocumentBytes.Length);

            string fileName = reportProcess.DocumentName + "." + reportProcess.Extension;

            result.Content.Headers.ContentLength = reportProcess.DocumentBytes.Length;
            result.Content.Headers.Add("Content-Disposition",
                               string.Format("{0};FileName=\"{1}\"",
                                             "attachment",
                                             fileName));

            return result;

        }

        private static byte[] Compress(byte[] input)
        {
            using (var compressStream = new MemoryStream())
            {
                using (var compressor = new GZipStream(compressStream, CompressionMode.Compress))
                {
                    compressor.Write(input, 0, input.Length);
                    compressor.Close();
                    return compressStream.ToArray();
                }
            }
        }
    }

    internal class SerialNoInformation
    {
        public string ItemNo { get; set; }
        public string VariantCode { get; set; }
        public string SerialNo { get; set; }
        public string BrandCode { get; set; }
        public string CarlineCode { get; set; }
        public string RegistrationNo { get; set; }

    }

    internal class SerialNoInformationMapper : EntityTypeConfiguration<SerialNoInformation>
    {
        public SerialNoInformationMapper()
        {
            ToTable("SFS$Serial No_ Information");
            HasKey(e => new { e.ItemNo, e.VariantCode, e.SerialNo });
            Property(e => e.ItemNo).HasColumnName("Item No_");
            Property(e => e.SerialNo).HasColumnName("Serial No_");
            Property(e => e.RegistrationNo).HasColumnName("Registration No_");
        }
    }

}