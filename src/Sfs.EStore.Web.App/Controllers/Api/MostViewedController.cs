﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.Http;
using Sfs.EStore.Web.App.DomainEvents;
using Sfs.EStore.Web.App.Models;
using Sfs.EStore.Web.App.ThirdParties;
using System.Linq;
using Sfs.EStore.Web.App.Code.ListingViewModelVisitors;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    [AuthoriseActivity("/Lots/View")]
    public class MostViewedController : ApiApplicationController
    {
        private readonly IListingSummaryViewModelVisitor _modelVisitor;
        private readonly IVehicleSearchHandler _vehicleSearchHandler;

        public MostViewedController(IVehicleSearchHandler vehicleSearchHandler, IListingSummaryViewModelVisitor modelVisitor)
        {
            _vehicleSearchHandler = vehicleSearchHandler;
            _modelVisitor = modelVisitor;
        }

        public Task<HttpResponseMessage> Post(LotsGetParams search)
        {
            if (search == null)
                return Task.Factory.StartNew(() => Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Search parameters must be specified"));

            _vehicleSearchHandler.HandlerSearchParams(search);

            DomainEvent.Raise(new UserSearched(User.Identity, search));

            new LotPriceSearchSanitiser(Permissions, User).RemovePriceFilterIfRequired(search);

            return ApiProxy.PostAsJson("mostviewed", search)
                .AcceptJson().SendAsync()
                .ContinueWith(r =>
                {
                    var apiResult = r.Result;
                    apiResult.EnsureSuccessStatusCode();

                    return new LotSummaryModelResponseMessageMapper(_modelVisitor, Request.CreateResponse)
                        .Map(apiResult);
                });
        }
    }

    //todo: tf- move this into an Extensions class
    public static class ObjectExtensions
    {
        public static string ToUrlParams(this object source)
        {
            var properties = source.GetType().GetProperties();

            var allProps = properties
                .Select(prop => new KeyValuePair<string, object>(prop.Name, prop.GetValue(source, null)))
                .Where(x => x.Value != null)
                .ToList();
            var objProps = allProps
                .Where(x => !x.Value.GetType().IsArray)
                .Select(prop => string.Format("{0}={1}", prop.Key.ToLowerInvariant(), prop.Value))
                .ToList();
            var enumProps = allProps
                .Where(x => x.Value.GetType().IsArray)
                .Select(x => new KeyValuePair<string, IEnumerable<object>>(x.Key, x.Value as IEnumerable<object>))
                .SelectMany(x => x.Value.Select(y => new KeyValuePair<string, object>(x.Key, y)))
                .Select(prop => string.Format("{0}={1}", prop.Key.ToLowerInvariant(), prop.Value))
                .ToList();

            return string.Join("&", objProps.Union(enumProps));
        
        }
    }
}
