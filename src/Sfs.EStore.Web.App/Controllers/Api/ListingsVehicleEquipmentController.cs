using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    [RoutePrefix("api")]
    public class ListingsVehicleEquipmentController : ApiApplicationController
    {
        [Route("listings/vehicle-equipment")]
        public Task<HttpResponseMessage> Get([FromUri] int[] id, int limit = 10)
        {
            return ApiProxy.Get(string.Format("listings/vehicle-equipment?limit={0}&id={1}",
                limit, string.Join("&id=", id)))
                .AcceptJson()
                .SendAsync()
                .ContinueWith(r =>
                {
                    var apiResult = r.Result;
                    apiResult.EnsureSuccessStatusCode();
                    return apiResult;
                });
        }
    }
}