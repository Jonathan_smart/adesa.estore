﻿using Ionic.Zip;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    public class ImageController : ApiController
    {
        public Task<HttpResponseMessage> Post(SaveImages model)
        {
            return Task.Factory.StartNew(() => ReturnImages(model));
        }
        private static bool IsValidUrl(string url)
        {
            WebRequest webRequest = WebRequest.Create(url);
            WebResponse webResponse;
            try
            {
                webResponse = webRequest.GetResponse();
            }
            catch
            {
                return false;
            }
            return true;
        }
        private HttpResponseMessage ReturnImages(SaveImages imagePaths)
        {
            var streamContent = new PushStreamContent((outputStream, httpContext, transportContent) =>
            {
                try
                {
                    using (var zipFile = new ZipFile())
                    {
                        foreach (string i in imagePaths.imagesPaths)
                        {
                            var wc = new WebClient();
                            var imageStream = new MemoryStream(wc.DownloadData(i));
                            zipFile.CompressionLevel = Ionic.Zlib.CompressionLevel.None;
                            zipFile.AddEntry(i.Substring(i.LastIndexOf("/") + 1), imageStream);
                        }
                        zipFile.Save(outputStream); //Null Reference Exception
                    }
                }

                finally
                {
                    outputStream.Close();
                }
            });
            streamContent.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            streamContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            streamContent.Headers.ContentDisposition.FileName = "images.zip";

            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = streamContent
            };
            return response;
        }


    }
    public class SaveImages
    {
        public string[] imagesPaths { get; set; }
    }
}
