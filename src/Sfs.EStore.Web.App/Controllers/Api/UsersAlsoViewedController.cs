﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.ListingViewModelVisitors;
using Sfs.EStore.Web.App.ThirdParties;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    [AllowAnonymous]
    public class UsersAlsoViewedController : ApiApplicationController
    {
        private readonly IListingSummaryViewModelVisitor _modelVisitor; 
        private readonly IVehicleSearchHandler _vehicleSearchHandler;

        public UsersAlsoViewedController(IVehicleSearchHandler vehicleSearchHandler, IListingSummaryViewModelVisitor modelVisitor)
        {
            _vehicleSearchHandler = vehicleSearchHandler;
            _modelVisitor = modelVisitor;
        }

        public class UsersAlsoViewedGetModel
        {
            public int LotNumber { get; set; }
            public int? Top { get; set; }
        }

        public Task<HttpResponseMessage> Get([FromUri]UsersAlsoViewedGetModel model)
        {
            if (!Permissions["/Lots/View"].IsAllowedFor(User))
                return Task.Factory.StartNew(() => Request.CreateResponse(HttpStatusCode.OK, new ListingSummaryViewModel[0]));

            var alsoViewedParams = new AlsoViewedParams { Top = model.Top };
            _vehicleSearchHandler.HandlerSearchParams(alsoViewedParams);

            return ApiProxy.Get(string.Format("lots/{0}/usersalsoviewed?{1}", model.LotNumber, alsoViewedParams.ToUrlParams()))
                .AcceptJson().SendAsync()
                .ContinueWith(r =>
                {
                    var apiResult = r.Result;
                    apiResult.EnsureSuccessStatusCode();

                    return new LotSummaryModelResponseMessageMapper(_modelVisitor, Request.CreateResponse)
                        .Map(apiResult);
                });
        }

        //todo: tf- move into models?
        public class AlsoViewedParams : ISecurable
        {
            public int? Top { get; set; }
            public string[] LimitToRoles { get; set; }
        }
    }
}
