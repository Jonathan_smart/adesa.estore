using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.Http;
using Sfs.EStore.Web.App.Models;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    [AllowAnonymous]
    public class MakeAnOfferController : ApiApplicationController
    {
        [ValidationFilter, ModelRequiredFilter]
        public Task<HttpResponseMessage> Post([FromUri]int listingId, MakeAnOfferPostModel form)
        {
            return Task.Factory.StartNew(() =>
            {
                var visitorId = VisitorTrackingHttpModule.VisitorId(Request.GetHttpContext());
                SiteVisitor visitor = null;
                if (visitorId != null)
                    visitor = Session.LoadAsync<SiteVisitor>(visitorId).Result;

                form.TenantEnrichment = new MakeAnOfferPostModel.TenantEnrichmentInfo();
                EnrichModelWithVisitor(form.TenantEnrichment, visitor);

                return ApiProxy.PostAsJson(string.Format("listings/{0}/makeanoffer", listingId), form)
                    .AcceptJson().SendAsync()
                    .ContinueWith(
                        t =>
                            Request.CreateResponse(t.Result.IsSuccessStatusCode
                                ? HttpStatusCode.OK
                                : t.Result.StatusCode))
                    .Result;
            });
        }

        private void EnrichModelWithVisitor(MakeAnOfferPostModel.TenantEnrichmentInfo info, SiteVisitor visitor)
        {
            if (info == null) throw new ArgumentNullException("info");
            if (visitor == null) return;

            info.OriginalVisitType = visitor.OriginalVisit.Source.Type.ToString();
            info.OriginalVisitSource = visitor.OriginalVisit.Source.Value;
            info.OriginalVisitAt = visitor.OriginalVisit.StartedAt;
            
            info.LatestVisitType = visitor.LastVisit.Source.Type.ToString();
            info.LatestVisitSource = visitor.LastVisit.Source.Value;
            info.LatestVisitAt = visitor.LastVisit.StartedAt;
        }
    }
}