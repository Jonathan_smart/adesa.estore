using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Sfs.Customers.Adapters.DataAccess;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    [RoutePrefix("api/customers/{customerId}/ship-to-addresses")]
    public class CustomerAddressesController : ApiApplicationController
    {
        private readonly ShipToAddressQueryFactory _queryFactory;

        public CustomerAddressesController(ShipToAddressQueryFactory queryFactory)
        {
            _queryFactory = queryFactory;
        }

        [HttpGet, Route]
        public Task<HttpResponseMessage> Get(string customerId, string type)
        {
            return Task.Factory.StartNew(() =>
            {
                // TODO: Ensure access to the customer

                using (var dbContext = new NavisionContext())
                {
                    QueryParams.ShippedItemType shippedItemType;
                    if (!Enum.TryParse(type, ignoreCase: true, result: out shippedItemType))
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                            string.Format("{0} is not a valid shipped item type", type));
                    }

                    var addresses = _queryFactory.Create(customerId, dbContext).Query(new QueryParams(shippedItemType));

                    return Request.CreateResponse(HttpStatusCode.OK,
                        addresses.Select(x => new GetAddressResult
                        {
                            Code = x.Code,
                            Address1 = x.Line1,
                            Address2 = x.Line2,
                            City = x.City,
                            County = x.County,
                            Name = x.Name,
                            PostCode = x.PostCode
                        }));
                }
            });
        }

        [HttpPost, Route]
        public Task<HttpResponseMessage> Post(string customerId, CreateAddress form)
        {
            return ApiProxy.PostAsJson("customeraddresses/"+ customerId, form).AcceptJson().SendAsync();
        }

        public class GetAddressResult
        {
            public string Code { get; set; }
            public string Name { get; set; }
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string City { get; set; }
            public string County { get; set; }
            public string PostCode { get; set; }
        }

        public class CreateAddress
        {
            public string Name { get; set; }
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string City { get; set; }
            public string County { get; set; }
            public string PostCode { get; set; }
        }
    }
}