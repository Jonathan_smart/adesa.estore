﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.Http;
using Sfs.EStore.Web.App.Code.ListingViewModelVisitors;
using Sfs.EStore.Web.App.DomainEvents;
using Sfs.EStore.Web.App.Models;
using Sfs.EStore.Web.App.ThirdParties;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    [AuthoriseActivity("/Lots/View")]
    public class LotsController : ApiApplicationController
    {
        private readonly IListingViewModelVisitor _modelVisitor;
        private readonly IListingSummaryViewModelVisitor _summaryModelVisitor;
        private readonly IVehicleSearchHandler _vehicleSearchHandler;

        public LotsController(IVehicleSearchHandler handler, IListingViewModelVisitor modelVisitor, IListingSummaryViewModelVisitor summaryModelVisitor)
        {
            _vehicleSearchHandler = handler;
            _modelVisitor = modelVisitor;
            _summaryModelVisitor = summaryModelVisitor;
        }

        public class GetOptions
        {
            public string Include { get; set; }
        }

        public Task<HttpResponseMessage> Get(int id, [FromUri]GetOptions options = null)
        {
            var model = new SingleLotGetParams { Include = (options ?? new GetOptions()).Include };
            _vehicleSearchHandler.HandlerSearchParams(model);

            return ApiProxy.Get("lots/" + id + "?" + model.ToUrlParams())
                .AcceptJson().SendAsync()
                .ContinueWith(r =>
                                  {
                                      r.Result.EnsureSuccessStatusCode();

                                      return r.Result.Content.ReadAsAsync<ListingViewModel>()
                                          .ContinueWith(lotTask =>
                                                            {
                                                                var lot = lotTask.Result;

                                                                _modelVisitor.Visit(lot);

                                                                return Request.CreateResponse(HttpStatusCode.OK, lot);
                                                            })
                                          .Result;
                                  });
        }

        public Task<HttpResponseMessage> Post(LotsGetParams search)
        {
            _vehicleSearchHandler.HandlerSearchParams(search);

            DomainEvent.Raise(new UserSearched(User.Identity, search));

            new LotPriceSearchSanitiser(Permissions, User).RemovePriceFilterIfRequired(search);

            return ApiProxy.PostAsJson("search", search)
                .AcceptJson().SendAsync()
                .ContinueWith(r =>
                                  {
                                      var apiResult = r.Result;
                                      apiResult.EnsureSuccessStatusCode();

                                      return new LotSummaryModelResponseMessageMapper(_summaryModelVisitor, Request.CreateResponse)
                                          .Map(apiResult);
                                  });
        }

    }
}
