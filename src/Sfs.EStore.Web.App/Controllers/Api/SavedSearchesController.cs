using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    public class SavedSearchesPostModel
    {
        public string Name { get; set; }

        public SavedSearchesPostModelCriteriaAlertStatus AlertStatus { get; set; }

        public SavedSearchesPostModelCriteria Criteria { get; set; }
    }

    public class SavedSearchesPostModelCriteria
    {
        public string Q { get; set; }
        public string VehicleType { get; set; }
        public string[] VehicleTypes { get; set; }
        public string Category { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Derivative { get; set; }
        public int? MaxMileage { get; set; }
        public string Mileage { get; set; }
        public int? MaxAge { get; set; }
        public string Transmission { get; set; }
        public string FuelType { get; set; }
        public int? MaxPrice { get; set; }
        public int? MinPrice { get; set; }
    }

    public class SavedSearchesPostModelCriteriaAlertStatus
    {
        public bool Enabled { get; set; }
    }

    public class SavedSearchedSavedSearchModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public DateTime SavedOn { get; set; }
        public Dictionary<string, object> Criteria { get; set; }
        public SavedSearchedSavedSearchAlertStatusModel AlertStatus { get; set; }
    }

    public class SavedSearchedSavedSearchAlertStatusModel
    {
        public bool Enabled { get; set; }
        public DateTime? LastNotifiedAt { get; set; }
    }

    [RoutePrefix("api/saved-searches")]
    public class SavedSearchesController : ApiApplicationController
    {
        [HttpGet, Route]
        public Task<HttpResponseMessage> Get()
        {
            return ApiProxy.Get("savedsearches")
                .AcceptJson().SendAsync()
                .ContinueWith(r =>
                                  {
                                      r.Result.EnsureSuccessStatusCode();
                                      var result = r.Result.Content.ReadAsAsync<SavedSearchedSavedSearchModel[]>().Result;

                                      return Request.CreateResponse(HttpStatusCode.OK, result);
                                  });
        }

        [HttpPost, Route]
        public Task<HttpResponseMessage> Create(SavedSearchesPostModel search)
        {
            return ApiProxy.PostAsJson("savedsearches", search)
                .AcceptJson().SendAsync()
                .ContinueWith(r =>
                {
                    return r.Result.IsSuccessStatusCode
                               ? Request.CreateResponse(HttpStatusCode.OK, r.Result.Content.ReadAsAsync<SavedSearchedSavedSearchModel>().Result)
                               : Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "");
                });
        }

        [HttpPost, Route("{id}")]
        public Task<HttpResponseMessage> Update(Guid id, SavedSearchesPostModel search)
        {
            return ApiProxy.PutAsJson("savedsearches/" + id, search)
                .AcceptJson().SendAsync()
                .ContinueWith(r => r.Result.IsSuccessStatusCode
                    ? Request.CreateResponse(HttpStatusCode.OK)
                    : Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ""));
        }

        [HttpPost, Route("{id}/destroy")]
        public Task<HttpResponseMessage> Delete(Guid id)
        {
            return ApiProxy.Delete("savedsearches/" + id)
                .AcceptJson().SendAsync()
                .ContinueWith(r => r.Result.IsSuccessStatusCode
                    ? Request.CreateResponse(HttpStatusCode.OK)
                    : Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ""));
        }
    }
}