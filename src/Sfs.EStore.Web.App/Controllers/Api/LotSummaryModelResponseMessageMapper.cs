using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Code.ListingViewModelVisitors;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    public class LotSummaryModelResponseMessageMapper
    {
        private readonly IListingSummaryViewModelVisitor _modelVisitor;
        private readonly Func<HttpStatusCode, ListingSummaryViewModel[], HttpResponseMessage> _createReponse;

        public LotSummaryModelResponseMessageMapper(IListingSummaryViewModelVisitor modelVisitor,
                                                    Func<HttpStatusCode, ListingSummaryViewModel[], HttpResponseMessage>
                                                        createReponse)
        {
            _modelVisitor = modelVisitor;
            _createReponse = createReponse;
        }

        public HttpResponseMessage Map(HttpResponseMessage source)
        {
            return source.Content.ReadAsAsync<ListingSummaryViewModel[]>()
                .ContinueWith(
                    lotsTask =>
                    {
                        if (_modelVisitor != null)
                            lotsTask.Result.ToList().ForEach(_modelVisitor.Visit);

                        var response = _createReponse(HttpStatusCode.OK, lotsTask.Result);
                        var totalResultsHeader = source.Headers.FirstOrDefault(x => x.Key == "X-TotalResults");
                        if (totalResultsHeader.Key != null)
                            response.Headers.Add("X-TotalResults", totalResultsHeader.Value);
                        return response;
                    })
                .Result;
        }
    }
}