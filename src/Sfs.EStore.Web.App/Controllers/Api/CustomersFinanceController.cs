using System.Net.Http;
using System.Threading.Tasks;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    public class CustomersFinanceController : ApiApplicationController
    {
        public Task<HttpResponseMessage> Get(string id)
        {
            return ApiProxy.Get("customersfinance/" + id)
                .AcceptJson()
                .SendAsync()
                .ContinueWith(t => t.Result);
        }
        public Task<HttpResponseMessage> Get()
        {
            return ApiProxy.Get("customersfinance")
                .AcceptJson()
                .SendAsync()
                .ContinueWith(t => t.Result);
        }
    }
}