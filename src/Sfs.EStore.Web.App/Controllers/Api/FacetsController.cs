using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Models;
using Sfs.EStore.Web.App.ThirdParties;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    [AllowAnonymous]
    public class FacetsController : ApiApplicationController
    {
        private readonly IVehicleSearchHandler _vehicleSearchHandler;

        public FacetsController(IVehicleSearchHandler handler)
        {
            _vehicleSearchHandler = handler;
        }

        public Task<HttpResponseMessage> Post(FacetsGetParams search)
        {
            search = search ?? new FacetsGetParams();

            _vehicleSearchHandler.HandlerSearchParams(search);

            new LotPriceSearchSanitiser(Permissions, User).RemovePriceFilterIfRequired(search);

            return ApiProxy.PostAsJson("facets?", search).AcceptJson().SendAsync();
        }
    }

    public class FacetsGetParams : LotsGetParams
    {
        public string Facet { get; set; }
    }
}