﻿using System.Net.Http;
using System.Threading.Tasks;
using Sfs.EStore.Web.App.Code.Http;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    [Authorise(Roles = "user_manager")]
    public class MembershipController : ApiApplicationController
    {
        public Task<HttpResponseMessage> Get()
        {
            return ApiProxy.Get("membership" + Request.RequestUri.Query).AcceptJson().SendAsync();
        }

        public Task<HttpResponseMessage> Put(MembershipUser membershipUser)
        {
            return ApiProxy.PutAsJson("membership", membershipUser).AcceptJson().SendAsync();
        }
    }

    public class MembershipUser
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public bool IsLockedOut { get; set; }
        public bool IsApproved { get; set; }
        public string[] Roles { get; set; }

    }
}
