﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Raven.Client;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.Http;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    public class SpecCalloutsController : ApiApplicationController
    {
        public Task<HttpResponseMessage> Get()
        {
            return Task.Factory.StartNew(() =>
            {
                var callout =
                    Session.Query<LotSpecificationCallout>()
                    .ToListAsync()
                    .Result.First();

                return Request.CreateResponse(HttpStatusCode.OK, callout);
            });
        }

        [AuthoriseActivity("/Lots/View")]
        public Task<HttpResponseMessage> Get(int lotNumber, int limit)
        {
            return Task.Factory.StartNew(() => GetSpecCalloutsForLot(lotNumber, limit));
        }

        [Authorise(Roles = "stock_manager")]
        public Task<HttpResponseMessage> Post(LotSpecificationCalloutModel model)
        {
            return Task.Factory.StartNew(
                () =>
                    {
                        var callout = Session.LoadAsync<LotSpecificationCallout>(model.Id).Result;
                        callout.Keys =
                            model.Keys.Select(x => new LotSpecificationCalloutItem(x.Category, x.Name, x.Weight, x.Emphasise)).ToArray();
                        Session.StoreAsync(callout).Wait();

                        return Request.CreateResponse(HttpStatusCode.OK, true);
                    });
        }

        private HttpResponseMessage GetSpecCalloutsForLot(int lotNumber, int limit)
        {
            limit = Math.Min(limit, 30);
            var lot = ApiProxy.Get("lots/" + lotNumber)
                .AcceptJson().SendAsync()
                .Result.Content.ReadAsAsync<ListingViewModel>().Result;

            var callout = Session.Query<LotSpecificationCallout>()
                .ToListAsync()
                .Result.First();

            var selected = callout.Keys.OrderByDescending(x => x.Weight)
                .Where(x => lot.VehicleInfo.Specification.Equipment.Items.Any(i => i.Category == x.Category && i.Name == x.Name))
                .Take(limit)
                .ToArray();


            return Request.CreateResponse(HttpStatusCode.OK, selected);
        }
    }

    public class LotSpecificationCalloutModel
    {
        public string Id { get; set; }
        public LotSpecificationCalloutItemModel[] Keys { get; set; }
    }

    public class LotSpecificationCalloutItemModel
    {
        public string Category { get; set; }
        public string Name { get; set; }
        public int Weight { get; set; }
        public bool Emphasise { get; set; }
    }
}
