﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    public class ShippingServicesViewController : ApiApplicationController
    {
        public Task<HttpResponseMessage> Get([FromUri]Form form)
        {
            return Task.Factory.StartNew(() => GetAll(form));
        }

        private HttpResponseMessage GetAll(Form form)
        {
            var qsParams = string.Format("?method={0}", form.Method);
            return ApiProxy.Get("ShippingServicesView" + qsParams).AcceptJson().SendAsync().Result;
        }

        public class Form
        {
            public string Method { get; set; }
        }
    }
}