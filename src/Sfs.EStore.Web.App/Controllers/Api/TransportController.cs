using System.Net.Http;
using System.Threading.Tasks;

namespace Sfs.EStore.Web.App.Controllers.Api
{
    public class TransportController : ApiApplicationController
    {
        public Task<HttpResponseMessage> Post(TransportRequestModel form)
        {
            return ApiProxy.PostAsJson("transport", form).AcceptJson().SendAsync();
        }

        public class TransportRequestModel
        {
            public TransportRequestItemModel[] Items { get; set; }
            public bool WithVat { get; set; }
        }

        public class TransportRequestItemModel
        {
            public string Id { get; set; }
            public string ShippingMethod { get; set; }
            public string ShippingAgent { get; set; }
            public string ShippingAgentService { get; set; }
            public TransportItemFromModel From { get; set; }
            public TransportItemToModel To { get; set; }
        }

        public class TransportItemFromModel
        {
            public string VehicleRegistrationNo { get; set; }
        }

        public class TransportItemToModel
        {
            public string CustomerNo { get; set; }
            public string AddressCode { get; set; }
        }
    }
}