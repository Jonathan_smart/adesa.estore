using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web;
using System.Web.Mvc;
using Sfs.EStore.Web.App.Code;
using MenuItem = Sfs.EStore.Web.App.Code.MenuItem;

namespace Sfs.EStore.Web.App.Controllers
{
    [AllowAnonymous]
    public class MenuController : ApplicationController
    {
        public ActionResult Index()
        {
            var docId = IdToRavenId("navbar");
            var menu = Session.Load<SiteMenu>(docId);

            Debug.Assert(menu != null, "menu != null");

            var items = menu.Items;

            return PartialView("index", items);
        }

        private static string IdToRavenId(string id)
        {
            return string.Format("SiteMenus/{0}", id);
        }
        private void RemoveRetailToken()
        {
            var context = new HttpContextWrapper(System.Web.HttpContext.Current);

            HttpCookie token = context.Request.Cookies["_retail"];

            if (token != null)
            {
                Response.Cookies.Remove("_retail");
                token.Expires = DateTime.Now.AddDays(-10);
                token.Value = null;
                Response.SetCookie(token);
            }
        }
        /// <summary>
        /// Builder Dealer dynamically
        /// </summary>
        private static MenuItem BuildDealer()
        {
            var items = new List<MenuItem>();

            MenuItem customMenu = new CustomLinkMenuItem
            {
                Text = "Test",
                Url = "~/Url"
            };


            MenuItem customMenu1 = new CustomLinkMenuItem
            {
                Text = "Test",
                Url = "~/Url"
            };

            items.Add(customMenu);
            items.Add(customMenu1);

            var menu = new MenuItem[]
                            {
                                new SecureMenuItem
                                {
                                    Roles = null,
                                    Authenticated = new DropDownAuthMenuItem()
                                    {
                                          Text = "Retail",
                                          Items =  items
                                    },
                                    
                                },
                            };

            return menu[0];
        }
    }
}