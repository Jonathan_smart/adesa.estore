﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LukeSkywalker.IPNetwork;
using Raven.Client;
using Sfs.EStore.Web.App.Areas.Retail;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.Cms;
using Sfs.EStore.Web.App.Code.Security;
using Sfs.EStore.Web.App.Commands;
using Sfs.EStore.Web.App.Models;
using log4net;

namespace Sfs.EStore.Web.App.Controllers
{
    public abstract class ApplicationController : Controller
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(ApplicationController));

        protected ApplicationController()
        {
            Logger = LogManager.GetLogger(GetType());

            ApiProxy = new Lazy<AuthorizedHttpClient>(() => ApiProxyFactory.Create(User));
            var ctx = new HttpContextWrapper(System.Web.HttpContext.Current);
            var tenant = ctx.CurrentTenant();
            Tenant = new Lazy<SiteTenant>(() => tenant);

            _user = new Lazy<IWebApplicationPrincipal>(() => (base.User as IWebApplicationPrincipal));

            var permissions = ctx.CurrentTenantPermissions();
            Permissions = new Lazy<SiteTenantActivityPermissions>(() => permissions);

            //_retail = new Lazy<RetailView>(() => new RetailView(User));

            //CheckIp(ctx.CurrentTenant().AccessIP);

        }

        private void CheckIp(IList<string> allowedIpRange)
        {
            string currentIp = System.Web.HttpContext.Current.Request.UserHostAddress;

            if (System.Web.HttpContext.Current.Request.RawUrl.Contains("/siteadmin"))
            {
                if (allowedIpRange != null && allowedIpRange.Any())
                {
                    bool isAllowed = IsValidIpAddress(currentIp, allowedIpRange);

                    if (isAllowed)
                    {
                        _logger.Error(System.Web.HttpContext.Current.Request.Url + "-Permission Granted:" + currentIp);
                    }
                    else
                    {
                        _logger.Error(System.Web.HttpContext.Current.Request.Url + "-Permission Denied:" + currentIp);

                        System.Web.HttpContext.Current.Response.StatusCode = (int)System.Net.HttpStatusCode.Forbidden;
                        System.Web.HttpContext.Current.Response.End();
                    }
                }
                else
                {
                    _logger.Error(System.Web.HttpContext.Current.Request.Url + "-Permission Denied:" + currentIp);

                    System.Web.HttpContext.Current.Response.StatusCode = (int)System.Net.HttpStatusCode.Forbidden;
                    System.Web.HttpContext.Current.Response.End();
                }
            }
        }

        private bool IsValidIpAddress(string ip, IList<string> ipRange)
        {
            IPAddress incomingIp = IPAddress.Parse(ip);

#if !DEBUG

            foreach (var ipAddress in ipRange)
            {
                IPNetwork network = IPNetwork.Parse(ipAddress);

                if (IPNetwork.Contains(network, incomingIp))
                    return true;

            }
            return false;
#endif

            return true;
        }

        private Lazy<IWebApplicationPrincipal> _user;

        public new IWebApplicationPrincipal User
        {
            get { return _user.Value; }
            set { }
        }

        protected new RetailView Retail
        {
            get { return null; }//_retail.Value; }
            set { }
        }

        public Lazy<SiteTenant> Tenant { get; set; }

        private Lazy<RetailView> _retail { get; set; }

        public ILog Logger { get; set; }

        public Lazy<AuthorizedHttpClient> ApiProxy { get; set; }

        public HttpSessionStateBase WebSession { get { return base.Session; } }

        public new IDocumentSession Session { get; set; }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Session = WebApiApplication.GetTenantDocumentStoreFor(Tenant.Value.DatabaseName).OpenSession();
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            using (Session)
            {
                if (filterContext.Exception != null)
                    return;

                if (Session != null)
                    Session.SaveChanges();
            }
        }

        public Lazy<SiteTenantActivityPermissions> Permissions { get; set; }

        public Action<ICommand> AlternativeExecuteCommand { get; set; }
        public Func<ICommand, object> AlternativeExecuteCommandWithResult { get; set; }

        public void ExecuteCommand(ICommand cmd)
        {
            if (AlternativeExecuteCommand != null)
                AlternativeExecuteCommand(cmd);
            else
                DefaultExecuteCommand(cmd);
        }

        protected TResult ExecuteCommand<TResult>(ICommand<TResult> cmd)
        {
            if (AlternativeExecuteCommandWithResult != null)
                return (TResult)AlternativeExecuteCommandWithResult(cmd);
            return DefaultExecuteCommand(cmd);
        }

        protected void DefaultExecuteCommand(ICommand cmd)
        {
            cmd.Session = Session;
            cmd.ApiProxy = ApiProxy.Value;

            cmd.Execute();
        }

        protected TResult DefaultExecuteCommand<TResult>(ICommand<TResult> cmd)
        {
            ExecuteCommand((ICommand)cmd);
            return cmd.Result;
        }

        protected ActionResult CmsManagedPage(string permaName, string viewName, ILiquidVariables variables = null, Action<CmsPageModel> preResultAction = null)
        {
            var page = Session.Query<CmsPage>().FirstOrDefault(x => x.PermaName == permaName);

            if (page == null)
                throw new HttpException(404, "'" + permaName + "'" + " page not found");

            if (!page.CanAccessPageAs(User)) return new HttpUnauthorizedResult();

            var viewModel = new CmsPageModel(page, variables);

            if (preResultAction != null) preResultAction(viewModel);

            return View(viewName, viewModel);
        }

        protected ActionResult CmsManagedPage(string permaName, ILiquidVariables variables = null, Action<CmsPageModel> preResultAction = null)
        {
            return CmsManagedPage(permaName, "cmspage", variables, preResultAction);
        }

        public string AssignMarque()
        {
            HttpCookie cookie = Request.Cookies["_retail"];
            bool cookieExists = cookie != null && cookie.HasKeys;
            string marque = string.Empty;
            if (cookieExists)
            {
                marque = cookie.Values["MarqueName"];
            }
            return marque;
        }
    }
}
