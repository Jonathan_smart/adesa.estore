using System.Net;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.WebPages;
using Jurassic;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.DomainEvents;
using Sfs.EStore.Web.App.Models;

namespace Sfs.EStore.Web.App.Controllers
{
    [AllowAnonymous]
    public class SessionsController : ApplicationController
    {
        private readonly IUserValidator _userValidator;

        public SessionsController(IUserValidator userValidator)
        {
            _userValidator = userValidator;
        }

        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            return View("login", new SessionsLoginModel
                                     {
                                         ReturnUrl = returnUrl
                                     });
        }

        [HttpPost]
        public ActionResult Login(SessionsLoginModel login)
        {
            if (!ModelState.IsValid)
            {
                if (login.ReturnUrl != null)
                    return RedirectToAction("index", "CmsPages"); //--on all calls to View(), get RedirectToAction() based on Request.UrlReferrer

                return Request.IsAjaxRequest()
                            ? (ActionResult)new HttpStatusCodeResult(HttpStatusCode.Unauthorized)
                            : View("login", login);
            }

            var validationResult = _userValidator.Validate(login.Username, login.Password);

            if (!validationResult.IsAuthenticated)
            {
                ModelState.AddModelError("", validationResult.Reason);

                if (Request.IsAjaxRequest())
                {
                    HttpContext.Items["Unauthorized-Json-Response"] = validationResult.Reason;
                    return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
                }
                if (login.ReturnUrl != null)
                    return RedirectToAction("index", "CmsPages");

                return View("login", login);
            }

            Response.SetFormsAuthentication(validationResult.UserIdentity);

            DomainEvent.Raise(new UserLoggedIn(validationResult.UserIdentity, UniqueSessionId.Value));

            if (Request.IsAjaxRequest())
                return Json(new { validationResult.UserIdentity.FirstName, validationResult.UserIdentity.LastName, validationResult.UserIdentity.UserName, validationResult.UserIdentity.EmailAddress, validationResult.UserIdentity.PhoneNumber });

            return string.IsNullOrWhiteSpace(login.ReturnUrl) || !Url.IsLocalUrl(login.ReturnUrl)
                       ? Redirect(FormsAuthentication.DefaultUrl)
                       : Redirect(login.ReturnUrl);
        }

        [HttpGet]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            WebSession.Abandon();
            return RedirectToRoute(new { controller = "cmspages", action = "index" });
        }
    }
}