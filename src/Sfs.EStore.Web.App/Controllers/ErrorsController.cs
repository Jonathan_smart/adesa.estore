﻿using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.Cms;
using Sfs.EStore.Web.App.Code.Security;
using Sfs.EStore.Web.App.Code.Widgets.Liquid.Drops;
using Sfs.EStore.Web.App.Models;

namespace Sfs.EStore.Web.App.Controllers
{
    [AllowAnonymous]
    public class ErrorsController : ApplicationController
    {
        public ActionResult Index()
        {
            return View("500");
        }

        [ActionName("404")]
        public ActionResult NotFound(string aspxerrorpath)
        {
            var redirect = LoadPreConfiguredRedirect(aspxerrorpath);

            if (redirect != null)
            {
                Response.Redirect(redirect.RedirectTo, false);
                Response.StatusCode = (int)System.Net.HttpStatusCode.MovedPermanently;
                Response.End();
            }

            const string permaName = "404";
            var page = Session.Query<CmsPage>()
                .FirstOrDefault(x => x.PermaName == permaName) ?? new CmsPage
                {
                    AllowAccess = PageAccess.Everyone,
                    Body = "",
                    PageTitle = "Page not found",
                    PermaName = permaName
                };

            Response.StatusCode = 404;
            return View("cmspage", new CmsPageModel(page, new Errors404LiquidVariables(User.Identity, Request)
                                                              {
                                                                  aspx_error_path = aspxerrorpath
                                                              }));
        }

        private UrlRedirect LoadPreConfiguredRedirect(string aspxerrorpath)
        {
            var redirects = Session.Load<UrlRedirects>("UrlRedirects");

            if (redirects == null) return null;

            return redirects.List.FirstOrDefault(x => Regex.IsMatch(x.OldPath, aspxerrorpath));
        }

        [ActionName("500")]
        public ActionResult InternalServerError()
        {
            return View("500");
        }
    }

    public class Errors404LiquidVariables : ILiquidVariables
    {
        public Errors404LiquidVariables(IWebApplicationIdentity identity, HttpRequestBase httpRequest)
        {
            user = new UserDrop(identity);
            page = new CmsPageDrop(httpRequest);
        }

        // ReSharper disable InconsistentNaming
        public string aspx_error_path { get; set; }
        // ReSharper restore InconsistentNaming

        public UserDrop user { get; private set; }
        public CmsPageDrop page { get; private set; }
    }
}
