﻿using System.Web.Mvc;

namespace Sfs.EStore.Web.App.Controllers
{
    [AllowAnonymous]
    public class NotSupportedController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

    }
}
