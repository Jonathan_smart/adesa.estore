using System.Web.Mvc;
using Sfs.EStore.Web.App.Code.Widgets.Liquid.Drops;
using Sfs.EStore.Web.App.Models;

namespace Sfs.EStore.Web.App.Controllers
{
    public class CmsPagesController : ApplicationController
    {
        [AllowAnonymous]
        public ActionResult Index(string permalink = "home")
        {
            permalink = CmsPagePermanameHelper.Normalize(permalink);

            return CmsManagedPage(permalink, new CmsPageLiquidVariables(new UserDrop(User.Identity), new CmsPageDrop(Request)));
        }
    }
}