using System.Linq;
using System.Web.Mvc;
using Sfs.EStore.Web.App.Code.Cms;
using Sfs.EStore.Web.App.Models;

namespace Sfs.EStore.Web.App.Controllers
{
    public class ManufacturerModelCmsPagesController : ApplicationController
    {
        [AllowAnonymous]
        public ActionResult Index(string vehicleType, string manufacturer, string model)
        {
            var permaName = string.Format("used-{0}s/_make/_model", vehicleType);

            var page = Session.Query<ManufacturerModelCmsPage>()
                .FirstOrDefault(x => x.VehicleTypeQueryTerm == vehicleType &&
                                     x.ManufacturerSlug == manufacturer &&
                                     x.ModelSlug == model);

            if (page == null)
                Logger.WarnFormat("Cannot find manufacturer page for {0}/{1}.", vehicleType, manufacturer);

            var variables = new ManufacturerModelPageLiquidVariables(User.Identity, Request)
            {
                body = page != null ? page.Body : null,
                page_title = page != null ? page.PageTitle : null,
                image_url = page != null ? page.ImageUrl : null,
                vehicle_type_query_term = page != null ? page.VehicleTypeQueryTerm : vehicleType.ToUpperInvariant(),
                manufacturer_query_term = page != null ? page.ManufacturerQueryTerm : manufacturer.Replace('-', ' ').ToUpperInvariant(),
                model_query_term = page != null ? page.ModelQueryTerm : model.Replace('-', ' ').ToUpperInvariant()
            };

            return CmsManagedPage(permaName, variables, vm =>
            {
                if (page == null) return;

                if (!string.IsNullOrWhiteSpace(page.MetaData.Title))
                    vm.MetaData.Title = page.MetaData.Title;

                if (!string.IsNullOrWhiteSpace(page.MetaData.Description))
                    vm.MetaData.Description = page.MetaData.Description;

                if (!string.IsNullOrWhiteSpace(page.MetaData.CanonicalUrl))
                    vm.MetaData.CanonicalUrl = page.MetaData.CanonicalUrl;
            });
        }
    }
}