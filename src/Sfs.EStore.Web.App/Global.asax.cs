﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.WebPages;
using Mindscape.Raygun4Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Raven.Client;
using Sfs.EStore.Web.App.App_Start;
using Sfs.EStore.Web.App.Code;
using log4net;
using log4net.Config;
using Raven.Abstractions.Extensions;
using Raven.Client.Document;
using Serilog;
using Sfs.EStore.Web.App.Code.Security;

namespace Sfs.EStore.Web.App
{
    public class WebApiApplication : HttpApplication
    {
        private readonly static ConcurrentDictionary<string, Lazy<IDocumentStore>> Stores = new ConcurrentDictionary<string, Lazy<IDocumentStore>>();
        private static readonly LazyTenantDocumentStoreFactory TenantStores =
            new LazyTenantDocumentStoreFactory(Stores, ConfigurationManager.AppSettings["Raven/Url"]);

        protected WebApiApplication()
        {
            AuthenticateRequest += OnAuthenticateRequest;
            BeginRequest += (sender, args) =>
                                {
                                    var lang = Context.Request.Cookies["language"];
                                    if (lang == null || string.IsNullOrEmpty(lang.Value))
                                        return;

                                    System.Threading.Thread.CurrentThread.CurrentUICulture =
                                        new CultureInfo(lang.Value);
                                };
            EndRequest += (sender, args) =>
                              {
                                  var context = new HttpContextWrapper(Context);

                                  if (FormsAuthentication.IsEnabled && context.Response.StatusCode == 302
                                      && context.Request.IsAjaxRequest())
                                  {
                                      context.Response.Clear();
                                      context.Response.TrySkipIisCustomErrors = true;
                                      context.Response.AddHeader("Content-Type", "application/json");
                                      System.Web.Helpers.Json.Write(context.Items["Unauthorized-Json-Response"], context.Response.Output);
                                      context.Response.StatusCode = 401;
                                  }
                              };

            Error += (sender, args) =>
            {
                var expection = HttpContext.Current.Error;
                if (expection == null) return;

                if (expection is HttpException) return;

                Log.Error(expection.Message, expection);

                var currentPrincipal = (HttpContext.Current == null
                    ? null
                    : HttpContext.Current.User as IWebApplicationPrincipal) ?? new NullWebApplicationPrincipal();

                var client = new RaygunClient {User = currentPrincipal.Identity.Name};
                client.IgnoreFormFieldNames("Password", "ConfirmPassword");
                client.IgnoreHeaderNames("Authorization");

                client.Send(expection, null, new Hashtable
                {
                    {"current_principal", currentPrincipal},
                    {"logged_from", "WebApiApplication.Error event"}
                });
            };
        }

        private static readonly ILog Log = LogManager.GetLogger(typeof (WebApiApplication));

        private static IDocumentStore _webDocumentStore;
        internal static IDocumentStore WebAppDocumentStore { get { return _webDocumentStore; } }

        internal static IDocumentStore GetTenantDocumentStoreFor(string databaseName)
        {
            return TenantStores.GetTenantDocumentStoreFor(databaseName);
        }

        private static void InitDocumentStore()
        {
            const string databaseName = "eStore.webApp";
            _webDocumentStore = new DocumentStore
            {
                Url = ConfigurationManager.AppSettings["Raven/Url"],
                DefaultDatabase = databaseName
            }.Initialize();
        }

        protected void Application_Start()
        {
            Log.Debug("Application Start");

            XmlConfigurator.ConfigureAndWatch(
                new FileInfo(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log4net.config")));

            Serilog.Log.Logger = new LoggerConfiguration()
                .Destructure.ByTransforming<System.Net.IPAddress>(x => x.ToString())
                .WriteTo.Log4Net()
                .CreateLogger();

            var formatters = GlobalConfiguration.Configuration.Formatters;
            var jsonFormatter = formatters.JsonFormatter;
            var settings = jsonFormatter.SerializerSettings;
            settings.Formatting = Formatting.Indented;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            RouteTable.Routes.MapMvcAttributeRoutes();
            AreaRegistration.RegisterAllAreas();

            GlobalConfiguration.Configure(WebApiConfig.Bootstrap);
            MvcConfig.Bootstrap();
            LiquidConfig.Bootstrap();

            InitDocumentStore();

            using (var session = WebAppDocumentStore.OpenSession())
            {
                session.Query<SiteTenant>().Select(x => x.Key).Distinct()
                    .ForEach(key =>
                    {
                        DisplayModeProvider.Instance.Modes.Insert(1, new DefaultDisplayMode(key)
                        {
                            ContextCondition = ctx =>
                            {
                                var tenant = new HttpContextWrapper(HttpContext.Current).CurrentTenant();

                                return tenant != null && tenant.Key == key;
                            }
                        });
                    });
            }
        }
   
        private static void OnAuthenticateRequest(object sender, EventArgs e)
        {
            var context = new HttpContextWrapper(HttpContext.Current);
            var tenant = context.CurrentTenant();
            
            new AuthenticationHandler(context, tenant).Handle();
        }
    }
}