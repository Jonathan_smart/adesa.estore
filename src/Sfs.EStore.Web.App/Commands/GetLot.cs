﻿using System.Net;
using System.Net.Http;
using Raven.Client;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Controllers.Api;
using Sfs.EStore.Web.App.Models;

namespace Sfs.EStore.Web.App.Commands
{
    public class GetLot : IQuery<ListingViewModel>
    {
        private readonly int _lotNumber;

        public GetLot(int lotNumber, AuthorizedHttpClient apiProxy)
        {
            _lotNumber = lotNumber;
            ApiProxy = apiProxy;
        }

        public IDocumentSession Session { get; set; }
        public AuthorizedHttpClient ApiProxy { get; set; }
        public void Execute()
        {
            var singleLotParams = new SingleLotGetParams();
            var result = ApiProxy.Get("lots/" + _lotNumber + "?" + singleLotParams.ToUrlParams())
                .AcceptJson().SendAsync()
                .ContinueWith(r =>
                {
                    if (r.Result.StatusCode == HttpStatusCode.NotFound)
                        return null;

                    r.Result.EnsureSuccessStatusCode();

                    return r.Result.Content.ReadAsAsync<ListingViewModel>().Result;
                }).Result;

            Result = result;
        }

        public ListingViewModel Result { get; private set; }
    }
}