﻿using System.Net.Http;
using Raven.Client;

namespace Sfs.EStore.Web.App.Commands
{
    public interface ICommand
    {
        IDocumentSession Session { set; }
        AuthorizedHttpClient ApiProxy { set; }

        void Execute();
    }

    public abstract class Command : ICommand
    {
        public IDocumentSession Session { get; set; }
        public AuthorizedHttpClient ApiProxy { get; set; }

        public abstract void Execute();

        protected virtual TResult Query<TResult>(IQuery<TResult> query)
        {
            query.ApiProxy = ApiProxy;
            query.Session = Session;

            query.Execute();
            return query.Result;
        }
    }

    public abstract class Command<TResult> : Command, ICommand<TResult>
    {
        public TResult Result { get; set; }
    }

    public interface ICommand<out TResult> : ICommand
    {
        TResult Result { get; }
    }

    public interface IQuery<out TResult> : ICommand<TResult>
    {
    }
}