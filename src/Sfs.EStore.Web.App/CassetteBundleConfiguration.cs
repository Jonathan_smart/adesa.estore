using Cassette;
using Cassette.Scripts;
using Cassette.Stylesheets;

namespace Sfs.EStore.Web.App
{
    public class CassetteBundleConfiguration : IConfiguration<BundleCollection>
    {
        public void Configure(BundleCollection bundles)
        {
            bundles.AddPerSubDirectory<ScriptBundle>("~/scripts/modernizr");
            bundles.AddUrlWithAlias<ScriptBundle>("https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js",
                                                  "jquery",
                                                  bundle => bundle.PageLocation = "head");
            bundles.AddUrlWithAlias<ScriptBundle>("https://code.jquery.com/ui/1.10.3/jquery-ui.js",
                                                  "jquery-ui",
                                                  bundle => bundle.AddReference("~/jquery"));
            bundles.AddUrlWithAlias<ScriptBundle>("https://html5shim.googlecode.com/svn/trunk/html5.js",
                                                  "html5shim",
                                                  bundle => { bundle.PageLocation = "lt ie9"; });
            bundles
                .AddUrlWithAlias<ScriptBundle>("https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.6.0/moment.min.js",
                                               "momentjs",
                                               bundle => bundle.AddReference("~/jquery"));

            bundles.Add<ScriptBundle>("jquery/scrollTo",
                new[]
                {
                    "~/scripts/jquery.scrollTo.min.js"
                },
                bundle => bundle.AddReference("~/jquery"));

            BundleAngular(bundles);

            BundleJqueryFileUpload(bundles);

            BundleRoyalSlider(bundles);

            Bundle360Slider(bundles);

            BundleBootstrap(bundles);
        }

        private static void Bundle360Slider(BundleCollection bundles)
        {
            bundles.AddPerSubDirectory<ScriptBundle>("~/scripts/jquery-360Slider", (ScriptBundle bundle) => bundle.AddReference("~/content/360Silder"), false);
            bundles.AddPerSubDirectory<StylesheetBundle>("~/content/360Silder", false);
        }

        private static void BundleRoyalSlider(BundleCollection bundles)
        {
            bundles.AddPerSubDirectory<ScriptBundle>("~/scripts/jquery-royalslider", bundle => bundle.AddReference("~/content/royalslider"));
            bundles.AddPerSubDirectory<StylesheetBundle>("~/content/royalslider");
        }

        private static void BundleAngular(BundleCollection bundles)
        {
            bundles
                .AddUrlWithAlias<ScriptBundle>("https://ajax.googleapis.com/ajax/libs/angularjs/1.2.27/angular.js",
                                               "angular",
                                               bundle => bundle.AddReference("~/jquery"));
            bundles
                .AddUrlWithAlias<ScriptBundle>("https://ajax.googleapis.com/ajax/libs/angularjs/1.2.27/angular-route.js",
                                               "angular/route",
                                               bundle => bundle.AddReference("~/angular"));
            bundles
                .AddUrlWithAlias<ScriptBundle>("https://ajax.googleapis.com/ajax/libs/angularjs/1.2.27/angular-cookies.js",
                                               "angular/cookies",
                                               bundle => bundle.AddReference("~/angular"));

            // http://code.angularjs.org/1.0.7/i18n/angular-locale_en-gb.js
            bundles.AddPerIndividualFile<ScriptBundle>("~/scripts/angular/locale", null,
                                                       bundle => bundle.AddReference("~/angular"));

            bundles.Add<ScriptBundle>("angular/bootstrap", new[] { "~/scripts/angular/ui-bootstrap-tpls-0.11.0.min.js" });
            bundles.Add<ScriptBundle>("angular/angularytics", new[] { "~/scripts/angular/angularytics.min.js" });

            bundles.AddPerSubDirectory<ScriptBundle>("~/scripts/angular/components");
            bundles.AddPerSubDirectory<ScriptBundle>("~/scripts/angular/services");
            bundles.AddPerSubDirectory<ScriptBundle>("~/scripts/angular/apps");
        }

        private static void BundleJqueryFileUpload(BundleCollection bundles)
        {
            bundles.Add<ScriptBundle>("jquery/fileupload",
                                      new[]
                                          {
                                              "~/scripts/jquery-fileupload/jquery.iframe-transport.js",
                                              "~/scripts/jquery-fileupload/vendor/jquery.ui.widget-1.9.1.js",
                                              "~/scripts/jquery-fileupload/jquery.fileupload.js"
                                          },
                                      bundle => bundle.AddReference("~/jquery"));
        }

        private static void BundleBootstrap(BundleCollection bundles)
        {
            bundles.AddUrlWithAlias<StylesheetBundle>(
                "https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css",
                "font-awsome");

            bundles.Add<ScriptBundle>("respondjs",
                                      new[]
                                          {
                                              "~/scripts/respond.js"
                                          },
                                      bundle => bundle.PageLocation = "head");

            bundles.AddUrlWithAlias<StylesheetBundle>(
                "https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css",
                "bootstrap/css", bundle => bundle.AddReference("~/html5shim"));
            bundles.AddUrlWithAlias<ScriptBundle>(
                "https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js",
                "bootstrap/js", bundle =>
                                 {
                                     bundle.AddReference("~/html5shim");
                                     bundle.AddReference("~/jquery");
                                 });
        }
    }
}