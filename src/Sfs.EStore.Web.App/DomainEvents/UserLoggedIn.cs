﻿using Sfs.EStore.Web.App.Code;

namespace Sfs.EStore.Web.App.DomainEvents
{
    public class UserLoggedIn : IDomainEvent
    {
        private readonly UserIdentity _identity;
        private readonly string _sessionId;

        public UserLoggedIn(UserIdentity identity, string sessionId)
        {
            _identity = identity;
            _sessionId = sessionId;
        }

        public UserIdentity Identity { get { return _identity; } }
        public string UniqueSessionId { get { return _sessionId; } }
    }
}