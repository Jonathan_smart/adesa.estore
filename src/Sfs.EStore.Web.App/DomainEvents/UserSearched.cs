using Sfs.EStore.Web.App.Code.Security;
using Sfs.EStore.Web.App.Models;

namespace Sfs.EStore.Web.App.DomainEvents
{
    public class UserSearched : IDomainEvent
    {
        private readonly IWebApplicationIdentity _identity;
        private readonly LotsGetParams _searchPerformed;

        public UserSearched(IWebApplicationIdentity identity, LotsGetParams searchPerformed)
        {
            _identity = identity;
            _searchPerformed = searchPerformed;
        }

        public IWebApplicationIdentity Identity { get { return _identity; } }
        public LotsGetParams SearchPerformed { get { return _searchPerformed; } }
    }
}