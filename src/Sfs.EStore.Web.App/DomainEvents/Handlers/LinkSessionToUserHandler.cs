namespace Sfs.EStore.Web.App.DomainEvents.Handlers
{
    public class LinkSessionToUserHandler : IHandles<UserLoggedIn>
    {
        private readonly AuthorizedHttpClient _apiProxy;

        public LinkSessionToUserHandler(AuthorizedHttpClient apiProxy)
        {
            _apiProxy = apiProxy;
        }

        public void Handle(UserLoggedIn args)
        {
            _apiProxy
                .Put(string.Format("track?session={0}&usertoken={1}", args.UniqueSessionId, args.Identity.Token), null)
                .AcceptJson().SendAsync()
                .Wait();
        }
    }
}