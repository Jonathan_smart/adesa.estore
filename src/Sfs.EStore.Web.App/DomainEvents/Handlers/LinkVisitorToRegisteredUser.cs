using System.Web;
using Sfs.EStore.Web.App.Code;

namespace Sfs.EStore.Web.App.DomainEvents.Handlers
{
    public class LinkVisitorToRegisteredUser : IHandles<UserRegistered>
    {
        public void Handle(UserRegistered args)
        {
            var visitorId = VisitorTrackingHttpModule.VisitorId(new HttpContextWrapper(HttpContext.Current));

            if (visitorId == null) return;

            var visitor = CmsHttpModule.CurrentTenantSession().Load<SiteVisitor>(visitorId);

            if (visitor == null) return;

            visitor.IsKnownAsUser(args.Username);
        }
    }

    public class LinkLoggedInUserToVisitor : IHandles<UserLoggedIn>
    {
        public void Handle(UserLoggedIn args)
        {
            var visitorId = VisitorTrackingHttpModule.VisitorId(new HttpContextWrapper(HttpContext.Current));

            if (visitorId == null) return;

            var visitor = CmsHttpModule.CurrentTenantSession().Load<SiteVisitor>(visitorId);

            if (visitor == null) return;

            visitor.IsKnownAsUser(args.Identity.UserName);
        }
    }
}