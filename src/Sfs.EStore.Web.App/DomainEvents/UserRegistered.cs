namespace Sfs.EStore.Web.App.DomainEvents
{
    public class UserRegistered : IDomainEvent
    {
        public UserRegistered(string username)
        {
            Username = username;
        }

        public string Username { get; private set; }
    }
}