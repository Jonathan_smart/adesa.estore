using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;

namespace Sfs.EStore.Web.App.Code
{
    public class PdfSalesBrochureWriter
    {
        private const int NumberOfImagePlaceholders = 5;
        private readonly string _templateWithOriginalPricePath;
        private readonly string _templateWithoutOriginalPricePath;

        public PdfSalesBrochureWriter(string templateWithOriginalPricePath, string templateWithoutOriginalPricePath)
        {
            _templateWithOriginalPricePath = templateWithOriginalPricePath;
            _templateWithoutOriginalPricePath = templateWithoutOriginalPricePath;
        }

        public void WriteToStream(ListingViewModel listing, Stream writeStream)
        {
            var templatePath = HasBeenDiscounted(listing)
                ? _templateWithOriginalPricePath
                : _templateWithoutOriginalPricePath;

            using (var existingFileStream = new FileStream(templatePath, FileMode.Open))
            using (var pdfReader = new PdfReader(existingFileStream))
            using (var stamper = new PdfStamper(pdfReader, writeStream) { FormFlattening = true })
            {
                var form = stamper.AcroFields;

                form.SetField("CURRENT_TIME", DateTime.UtcNow.ToString("s"));

                form.SetField("TITLE", listing.Title);
                form.SetField("MAKE", listing.VehicleInfo.Make);
                form.SetField("MODEL", listing.VehicleInfo.Model);
                form.SetField("REGISTRATION_PLATE", listing.VehicleInfo.Registration.Plate);
                form.SetField("REGISTRATION_YEAR", listing.VehicleInfo.Registration.Date.HasValue
                    ? listing.VehicleInfo.Registration.Date.Value.ToString("Y")
                    : "");
                form.SetField("MILEAGE", listing.VehicleInfo.Mileage.HasValue
                    ? listing.VehicleInfo.Mileage.Value.ToString("n0")
                    : "");
                form.SetField("COLOUR", listing.VehicleInfo.BodyColour);
                form.SetField("TRANSMISSION", listing.VehicleInfo.Transmission);
                form.SetField("FUEL_TYPE", listing.VehicleInfo.FuelType);

                Func<string> pricingPostFix = () => listing.PricesIncludeVat ? "" : " +VAT";

                if (HasBeenDiscounted(listing))
                    form.SetField("RETAIL_PRICE", listing.DiscountPriceInfo.WasPrice.ToCurrencyString() + pricingPostFix());
                else
                    form.SetField("RETAIL_PRICE", "");

                form.SetField("CURRENT_PRICE", listing.SellingStatus.CurrentPrice.ToCurrencyString() + pricingPostFix());

                var spec = string.Join(", ", listing.VehicleInfo.Specification.Equipment.Items.Select(x => x.Name));
                form.SetField("SPECIFICATION", spec);
                
                AddEcoToPdf(listing, form);

                AddImagesToPdf(listing, stamper);
            }

        }

        private static bool HasBeenDiscounted(ListingViewModel listing)
        {
            return listing.DiscountPriceInfo != null && listing.DiscountPriceInfo.WasPrice != null &&
                   listing.DiscountPriceInfo.WasPrice.Amount > listing.SellingStatus.CurrentPrice.Amount;
        }

        private static void AddEcoToPdf(ListingViewModel listing, AcroFields form)
        {
            var ecos = new List<KeyValuePair<string, string>>();
            var vehicleTaxRate = listing.VehicleInfo.Specification.TaxRate;
            if (listing.VehicleInfo.Specification.Co2.HasValue && vehicleTaxRate != null)
            {
                ecos.Add(new KeyValuePair<string, string>("CO2", string.Format("{0} g/km (Band {1})",
                    listing.VehicleInfo.Specification.Co2.Value.ToString("n0"),
                    (vehicleTaxRate.Band ?? '-'))));
            }
            if (listing.VehicleInfo.Specification.Economy.Combined.HasValue &&
                listing.VehicleInfo.Specification.Economy.Combined > 0)
            {
                ecos.Add(new KeyValuePair<string, string>("Fuel Efficiency", string.Format("{0} mpg (combined)",
                    listing.VehicleInfo.Specification.Economy.Combined.Value.ToString("n1"))));

                var pencePerMile = listing.VehicleInfo.Specification.Economy.CostPerUnitDistanceTravelled == null
                    ? (decimal?) null
                    : listing.VehicleInfo.Specification.Economy.CostPerUnitDistanceTravelled.Amount*100;

                if (pencePerMile != null)
                    ecos.Add(new KeyValuePair<string, string>("Pence Per Mile",
                        string.Format("{0:n0}p", pencePerMile)));
            }
            if (vehicleTaxRate != null && vehicleTaxRate.RoadTax12Months != null)
            {
                ecos.Add(new KeyValuePair<string, string>("Road Tax", vehicleTaxRate.RoadTax12Months.ToCurrencyString()));
            }

            form.SetField("ECO", string.Join(", ", ecos.Select(x => string.Format("{0}: {1}", x.Key, x.Value))));
        }

        private static void AddImagesToPdf(ListingViewModel listing, PdfStamper stamper)
        {
            var imageFiles = new[] {listing.PrimaryImage}.Concat(listing.MoreImages)
                .Where(x => x != null)
                .Take(NumberOfImagePlaceholders).ToArray();
            for (var i = 0; i < NumberOfImagePlaceholders; i++)
            {
                var fieldName = string.Format("IMAGE_{0}", i + 1);
                var fieldPosition = stamper.AcroFields.GetFieldPositions(fieldName)[0];

                if (i < imageFiles.Count())
                {
                    var imageFile = imageFiles[i];
                    var imageField = new PushbuttonField(stamper.Writer, fieldPosition.position, fieldName)
                    {
                        Layout = PushbuttonField.LAYOUT_ICON_ONLY,
                        Image = Image.GetInstance(imageFile.AsUrl("640x480")), //.ToUrl(640, 480)),
                        ScaleIcon = PushbuttonField.SCALE_ICON_IS_TOO_BIG,
                        ProportionalIcon = true,
                        Options = BaseField.READ_ONLY
                    };
                    stamper.AddAnnotation(imageField.Field, fieldPosition.page);
                }

                stamper.AcroFields.RemoveField(fieldName);
            }
        }
    }
}