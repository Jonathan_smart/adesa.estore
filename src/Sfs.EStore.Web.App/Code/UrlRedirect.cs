using Newtonsoft.Json;

namespace Sfs.EStore.Web.App.Code
{
    public class UrlRedirects
    {
        private UrlRedirect[] _list = new UrlRedirect[0];
        public UrlRedirect[] List
        {
            get { return _list; }
            private set { _list = value; }
        }
    }

    public class UrlRedirect
    {
        [JsonConstructor]
        public UrlRedirect(string oldPath, string redirectTo)
        {
            RedirectTo = redirectTo;
            OldPath = oldPath;
        }

        public string OldPath { get; private set; }
        public string RedirectTo { get; private set; }
    }
}