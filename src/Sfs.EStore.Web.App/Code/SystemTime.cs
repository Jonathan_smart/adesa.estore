using System;

namespace Sfs.EStore.Web.App.Code
{
    public static class SystemTime
    {
        public static Func<DateTime> UtcDateTime;

        public static DateTime UtcNow
        {
            get
            {
                var func = UtcDateTime;
                return func != null ? func() : DateTime.UtcNow;
            }
        }
        public static DateTime CheckForTimeDiff(DateTime date)
        {
            TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");
            if (!tz.IsDaylightSavingTime(date)) return date;

            DateTime convertedDateTime = TimeZoneInfo.ConvertTimeFromUtc(date, tz);
            return convertedDateTime;

        }
    }
}