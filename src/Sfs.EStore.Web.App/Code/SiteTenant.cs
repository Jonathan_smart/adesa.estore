using System;
using System.Collections.Generic;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Code.Security;

namespace Sfs.EStore.Web.App.Code
{
    public class SiteTenant
    {
        public SiteTenant()
        {
            IntegrationSettings = new Dictionary<string, string>();
            Settings = new SiteTenantSettings();
        }

        public string Id { get; set; }
        public string Key { get; set; }
        public string ApiAccessKeyId { get; set; }
        public string Name { get; set; }
        public string[] Hosts { get; set; }
        public string ThemeName { get; set; }
        public string DatabaseName { get; set; }

        public Dictionary<string, string> IntegrationSettings { get; private set; }
        public SiteTenantSettings Settings { get; private set; }
        public IList<string> AccessIP { get; set; }
    }

    public class SiteTenantActivityPermissions
    {
        public SiteTenantActivityPermissions()
        {
            Activities = new Dictionary<string, string>();
        }

        public string Id { get; protected set; }
        public Dictionary<string, string> Activities { get; set; }

        public virtual ActivityPermission this[string activity]
        {
            get
            {
                string permission;
                if (Activities.TryGetValue(activity, out permission))
                    return new ActivityPermission(permission);

                return new NullActivityPermission();
            }
        }

        public class ActivityPermission
        {
            private readonly PrincipalUserRestrictedAccess _restrictedAccess;

            public ActivityPermission(string value)
            {
                _restrictedAccess = new PrincipalUserRestrictedAccess(value);
            }

            public virtual bool IsAllowedFor(IWebApplicationPrincipal user)
            {
                return _restrictedAccess.IsAllowedFor(user);
            }
        }

        private class NullActivityPermission : ActivityPermission
        {
            public NullActivityPermission()
                : base("")
            {
            }

            public override bool IsAllowedFor(IWebApplicationPrincipal user)
            {
                return false;
            }
        }

        public SiteTenantActivityPermissions SetIdFromTenant(SiteTenant tenant)
        {
            Id = IdFromTenant(tenant);
            return this;
        }

        public SiteTenantActivityPermissions AddActivity(string activity, string access)
        {
            Activities.Add(activity, access);
            return this;
        }

        public static string IdFromTenant(SiteTenant tenant)
        {
            if (tenant == null) throw new ArgumentNullException("tenant");
            if (string.IsNullOrEmpty(tenant.Id)) throw new ArgumentException("tenant must have an Id", "tenant");

            return string.Format("{0}/ActivityPermissions", tenant.Id);
        }
    }

    public class SiteTenantSettings
    {
        public SiteTenantSettings()
        {
            Images = new ImageSettings();
            Navbar = new NavbarSettings();
            Finance = new FinanceSettings();
            Marquee = new MarqueeSettings();
            Javascript = new JavascriptSettings();
        }

        public ImageSettings Images { get; private set; }
        public NavbarSettings Navbar { get; private set; }
        public FinanceSettings Finance { get; private set; }
        public MarqueeSettings Marquee { get; set; }
        public JavascriptSettings Javascript { get; set; }

        public class ImageSettings
        {
            public ImageSettings()
            {
                Dimensions = new Dictionary<string, string>();
                Overlays = new Dictionary<string, string>();
                MissingImage = new ImageModel
                {
                    Container = "estore-assets",
                    Name = "missing_image.jpg"
                };
            }

            public ImageModel MissingImage { get; set; }
            public Dictionary<string, string> Dimensions { get; private set; }
            public Dictionary<string, string> Overlays { get; private set; }

            public string Size(string key)
            {
                string value;
                Dimensions.TryGetValue(key, out value);

                return value;
            }

            public string Overlay(string key)
            {
                string value;
                Overlays.TryGetValue(key, out value);

                return value;
            }
        }

        public class NavbarSettings
        {
            public string LogoUrl { get; set; }
        }

        public class MarqueeSettings
        {
            public enum Type
            {
                none,
                found,
                select
            }
            public Type Name { get; set; }
        }

        public class FinanceSettings
        {
            public FinanceSettings()
            {
                VideoUrls = new Dictionary<string, string>();
            }

            public Dictionary<string, string> VideoUrls { get; set; }

            public string Video(string financeTypeKey)
            {
                string url;
                VideoUrls.TryGetValue(financeTypeKey, out url);

                return url;
            }
          
        }

    public class JavascriptSettings
    {
        public string NgRoutePrefix
        {
            get;
            set;
        }

        public JavascriptSettings()
        {
        }
    }
}

}