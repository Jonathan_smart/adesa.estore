using Sfs.EStore.Web.App.Code.Security;
using Sfs.EStore.Web.App.Models;

namespace Sfs.EStore.Web.App.Code
{
    public class LotPriceSearchSanitiser
    {
        private readonly bool _canShowPrices;

        public LotPriceSearchSanitiser(SiteTenantActivityPermissions permissions, IWebApplicationPrincipal user)
        {
            _canShowPrices = permissions["/Lots/View/Prices"].IsAllowedFor(user);
        }

        public void RemovePriceFilterIfRequired(LotsGetParams search)
        {
            if (_canShowPrices)
                return;

            search.MinPrice = null;
            search.MaxPrice = null;
            if (search.SortBy != null && search.SortBy.Contains("offerprice"))
                search.SortBy = null;
        }
    }
}