using System.Collections;
using System.Threading;
using System.Web.Http.Filters;
using log4net;
using Mindscape.Raygun4Net;
using Sfs.EStore.Web.App.Code.Security;

namespace Sfs.EStore.Web.App.Code
{
    public class ExceptionLoggingAttribute : ExceptionFilterAttribute
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ExceptionLoggingAttribute));

        public override void OnException(HttpActionExecutedContext context)
        {
            Log.Error(context.Exception.Message, context.Exception);

            var client = new RaygunClient {User = Thread.CurrentPrincipal.Identity.Name};
            client.IgnoreFormFieldNames("Password", "ConfirmPassword");
            client.IgnoreHeaderNames("Authorization");

            client.SendInBackground(context.Exception, null, new Hashtable
            {
                {"current_principal", Thread.CurrentPrincipal as IWebApplicationPrincipal},
                {"logged_from", "ExceptionFilter"}
            });
        }
    }
}