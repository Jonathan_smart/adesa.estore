using System.Web;
using System.Web.Mvc;
using Sfs.EStore.Web.App.Code.Security;

namespace Sfs.EStore.Web.App.Code.Mvc
{
    public class AuthoriseActivityAttribute : AuthoriseAttribute
    {
        public AuthoriseActivityAttribute(string activity)
        {
            Activity = activity;
        }

        public string Activity { get; set; }

        protected override bool IsAuthorised(HttpContextBase httpContext)
        {
            var user = httpContext.User as IWebApplicationPrincipal;

            if (user == null) return false;

            var permissions = new HttpContextWrapper(HttpContext.Current).CurrentTenantPermissions();

            return permissions[Activity].IsAllowedFor(user);
        }

        protected override bool SkipAuthorization(AuthorizationContext filterContext)
        {
            return filterContext.HasWithinContext(typeof (AllowAnonymousAttribute));
        }
    }
}