using System;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Sfs.EStore.Web.App.Code.Mvc
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class AuthoriseAttribute : FilterAttribute, IAuthorizationFilter
    {
        private string[] _rolesSplit = new string[0];
        private string _roles;

        public string Roles
        {
            get { return _roles ?? string.Empty; }
            set
            {
                _roles = value;
                _rolesSplit = SplitString(value);
            }
        }

        protected virtual bool IsAuthorised(HttpContextBase httpContext)
        {
            if (httpContext == null)
                throw new ArgumentNullException("httpContext");
            var user = httpContext.User;
            
            if (!user.Identity.IsAuthenticated || (_rolesSplit.Length > 0 && !_rolesSplit.Any(user.IsInRole)))
                return false;
            
            return true;
        }

        private bool IsForbidden(HttpContextBase httpContext)
        {
            if (httpContext == null)
                throw new ArgumentNullException("httpContext");
            var user = httpContext.User;
            return _rolesSplit.Length > 0 && !_rolesSplit.Any(user.IsInRole);
        }

        private void CacheValidateHandler(HttpContext context, object data, ref HttpValidationStatus validationStatus)
        {
            validationStatus = OnCacheAuthorization(new HttpContextWrapper(context));
        }

        public virtual void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
                throw new ArgumentNullException("filterContext");
            if (OutputCacheAttribute.IsChildActionCacheActive(filterContext))
                throw new InvalidOperationException("Cannot Use Within Child Action Cache");

            if (SkipAuthorization(filterContext))
                return;
            
            if (IsAuthorised(filterContext.HttpContext) && !IsForbidden(filterContext.HttpContext))
            {
                HttpCachePolicyBase cache = filterContext.HttpContext.Response.Cache;
                cache.SetProxyMaxAge(new TimeSpan(0L));
                cache.AddValidationCallback(CacheValidateHandler, null);
                return;
            }

            if (IsForbidden(filterContext.HttpContext))
                HandleForbiddenRequest(filterContext);
            else
                HandleUnauthorizedRequest(filterContext);
        }

        protected virtual bool SkipAuthorization(AuthorizationContext filterContext)
        {
            return filterContext.HasWithinContext(typeof (AllowAnonymousAttribute),
                                                  typeof (AuthoriseActivityAttribute));
        }

        private void HandleForbiddenRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        protected virtual void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new HttpUnauthorizedResult();
        }

        protected virtual HttpValidationStatus OnCacheAuthorization(HttpContextBase httpContext)
        {
            if (httpContext == null)
                throw new ArgumentNullException("httpContext");
            
            if (!IsAuthorised(httpContext))
                return HttpValidationStatus.IgnoreThisRequest;
            
            return HttpValidationStatus.Valid;
        }

        internal static string[] SplitString(string original)
        {
            if (string.IsNullOrEmpty(original))
                return new string[0];

            return original.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}