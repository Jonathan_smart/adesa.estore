using System;
using System.Linq;
using System.Web.Mvc;

namespace Sfs.EStore.Web.App.Code.Mvc
{
    public static class AuthorizationContextExtensions
    {
        public static bool HasWithinContext(this AuthorizationContext @this, params Type[] attributes)
        {
            var skipChecks = attributes
                .SelectMany(attr => new Func<bool>[]
                                        {
                                            () => @this.ActionDescriptor
                                                      .IsDefined(attr, true),
                                            () => @this.ActionDescriptor.ControllerDescriptor
                                                      .IsDefined(attr, true)
                                        });

            return skipChecks.Any(x => x());
        }
    }
}