﻿using System.Collections.Generic;

namespace Sfs.EStore.Web.App.Code
{
    public class SiteMenu
    {
        public IList<MenuItem> Items { get; set; }
    }

    public abstract class MenuItem { }

    public class EmptyMenuItem : MenuItem { }

    public class PageMenuItem : MenuItem
    {
        public string Text { get; set; }
        public string PageId { get; set; }
    }

    public class CustomLinkMenuItem : MenuItem
    {
        public string Text { get; set; }
        public string Url { get; set; }
        public bool? Blank { get; set; }
    }

    public class DropDownMenuItem : MenuItem
    {
        public string Text { get; set; }
        public IEnumerable<MenuItem> Items { get; set; }
    }

    public class DropDownAuthMenuItem : MenuItem
    {
        public string Text { get; set; }
        public List<MenuItem> Items { get; set; }
    }

    public class SecureDropDownMenuItem : MenuItem
    {
        public string Roles { get; set; }
        public DropDownMenuItem AuthenticatedMenu { get; set; }
        public DropDownMenuItem GuestMenu { get; set; }
        public DropDownMenuItem AnonymousMenu { get; set; }
    }

    public class SecureMenuItem : MenuItem
    {
        public string Roles { get; set; }
        public MenuItem Authenticated { get; set; }
        public MenuItem Guest { get; set; }
        public MenuItem Default { get; set; }
    }
}