using System.Linq;
using Newtonsoft.Json;

namespace Sfs.EStore.Web.App.Code
{
    public class LotSpecificationCallout
    {
        public string Id { get; protected set; }

        public LotSpecificationCalloutItem[] Keys { get; set; }

        public static LotSpecificationCallout CreateFor(params LotSpecificationCalloutItem[] keys)
        {
            return new LotSpecificationCallout
            {
                Keys = keys ?? new LotSpecificationCalloutItem[0]
            };
        }
    }

    public class LotSpecificationCalloutItem
    {
        [JsonConstructor]
        public LotSpecificationCalloutItem(string category, string name, int weight, bool emphasise = false)
        {
            Category = category;
            Name = name;
            Weight = weight;
            Emphasise = emphasise;
        }

        public string Category { get; private set; }
        public string Name { get; private set; }
        public int Weight { get; private set; }
        public bool Emphasise { get; private set; }
    }
}