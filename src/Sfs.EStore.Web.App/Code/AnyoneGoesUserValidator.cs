namespace Sfs.EStore.Web.App.Code
{
    public class AnyoneGoesUserValidator : IUserValidator
    {
        public UserIdentityValidation Validate(string username, string password)
        {
            if (!string.IsNullOrEmpty(username))
            {
                return new UserIdentityValidation();
            }
                
            return null;
        }
    }
}