using System.IO;
using DotLiquid;

namespace Sfs.EStore.Web.App.Code.Widgets
{
    public class NgViewTag : Tag
    {
        public override void Render(Context context, TextWriter result)
        {
            result.Write("<div ng-view></div>");
        }
    }
}