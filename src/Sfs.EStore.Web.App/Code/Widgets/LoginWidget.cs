﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Cassette.Views;

namespace Sfs.EStore.Web.App.Code.Widgets
{
    public class LoginWidget : Widget
    {
        public class Result : WidgetResult
        {
            public string RedirectUrl { get; set; }
            public override void WriteTo(HtmlHelper htmlHelper)
            {
                htmlHelper.RenderPartial("_Login", RedirectUrl);
            }
        }

        public string RedirectUrl { get; set; }

        public override WidgetResult Execute()
        {
            return new Result { Success = true, RedirectUrl = RedirectUrl};
        }
    }
}