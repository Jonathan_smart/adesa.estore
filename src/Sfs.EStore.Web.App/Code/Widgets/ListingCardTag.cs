using System.Collections.Generic;
using System.IO;
using DotLiquid;
using DotLiquid.Tags;
using Sfs.EStore.Web.App.Code.Widgets.Liquid.Drops;

namespace Sfs.EStore.Web.App.Code.Widgets
{
    /// <example>
    /// {% assign listing = my_listing_value %}
    /// {% listing_card %}
    /// </example>
    public class ListingCardTag : Tag
    {
        private const string Key = "templates/listings/card.liquid";
        private readonly Include _include = new Include();
        private string _prices;

        public override void Initialize(string tagName, string markup, List<string> tokens)
        {
            var parsedMarkup = Liquid.TagParamsParser.Parse(markup);
            var prices = "";
            if (parsedMarkup.TryGetValue("prices", out prices))
            {
                _prices = prices;
            }
            _include.Initialize("include", Key, tokens);
        }

        public override void Render(Context context, TextWriter result)
        {
            var showPrices = true;
            var prices = (_prices ?? "").ToLowerInvariant();

            if (prices.Equals("never")) showPrices = false;
            else if (prices.Equals("authenticated") && !((UserDrop)context["user"]).IsAuthenticated) showPrices = false;

                context.Stack(Hash.FromAnonymousObject(new
                {
                    show_prices = showPrices
                }), () => _include.Render(context, result));
        }
    }
}