using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using DotLiquid;
using Sfs.EStore.Web.App.Code.Widgets.Liquid;
using Sfs.EStore.Web.App.Models;

namespace Sfs.EStore.Web.App.Code.Widgets
{
    /// <example>
    /// {% quicksearch %}
    /// </example>
    public class QuickSearchWidget : Tag
    {
        private QuickSearchTagModel _model;
        private const string HideVehicleTypeSelectionKey = "hidevt";
        private const string HideAdvanced = "hideAdv";

        public override void Initialize(string tagName, string markup, List<string> tokens)
        {
            _model = new QuickSearchTagModel();

            var args = TagParamsParser.Parse(markup);
            if (args.ContainsKey(HideVehicleTypeSelectionKey))
            {
                args.Remove(HideVehicleTypeSelectionKey);
                _model.HideVehicleTypeSelection = true;
            }

            if (args.ContainsKey(HideAdvanced))
            {
                _model.HideAdvanced = true;
            }

            if (args.ContainsKey("vehicleType"))
                _model.PredefinedQueryParams.VehicleType = args["vehicleType"];
            if (args.ContainsKey("make"))
                _model.PredefinedQueryParams.Make = args["make"];
            if (args.ContainsKey("model"))
                _model.PredefinedQueryParams.Model = args["model"];

            base.Initialize(tagName, markup, tokens);
        }

        public override void Render(Context context, TextWriter result)
        {
            SetPredefinedQueryParamsFromContextIfNotAlreadyExplicitlySet(context);
            
            var htmlHelper = context.Registers.Get<HtmlHelper>("Helper");
            htmlHelper.RenderPartial("_QuickSearchWidget", _model);
            base.Render(context, result);
        }

        private void SetPredefinedQueryParamsFromContextIfNotAlreadyExplicitlySet(Context context)
        {
            if (string.IsNullOrEmpty(_model.PredefinedQueryParams.VehicleType) && context["vehicle_type_query_term"] != null)
                _model.PredefinedQueryParams.VehicleType = context["vehicle_type_query_term"].ToString();

            if (string.IsNullOrEmpty(_model.PredefinedQueryParams.Make) && context["manufacturer_query_term"] != null)
                _model.PredefinedQueryParams.Make = context["manufacturer_query_term"].ToString();

            if (string.IsNullOrEmpty(_model.PredefinedQueryParams.Model) && context["model_query_term"] != null)
                _model.PredefinedQueryParams.Model = context["model_query_term"].ToString();
        }
    }
}