using System.IO;
using DotLiquid;
using Ninject;
using Sfs.EStore.Web.App.App_Start;

namespace Sfs.EStore.Web.App.Code.Widgets.Liquid
{
    public class WidgetBlock<T> : Block where T : Widget
    {
        public WidgetBlock()
        {
            Widget = NinjectWebCommon.Kernel.Get<T>();
        }

        protected T Widget { get; private set; }
        protected string VariableName { get; set; }

        public override void Render(Context context, TextWriter result)
        {
            var widgetResult = Widget.Execute();

            context.Stack(() =>
                {
                    context[VariableName ?? "this"] = widgetResult.ToLiquid();
                    context.Registers.Add("WidgetResult", widgetResult);
                    RenderAll(NodeList, context, result);
                    context.Registers.Remove("WidgetResult");
                });
        }
    }
}