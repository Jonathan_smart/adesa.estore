using System.IO;
using System.Web.Mvc;
using DotLiquid;
using Ninject;
using Sfs.EStore.Web.App.App_Start;

namespace Sfs.EStore.Web.App.Code.Widgets.Liquid
{
    public class WidgetTag<T> : Tag where T : Widget
    {
        public WidgetTag()
        {
            Widget = NinjectWebCommon.Kernel.Get<T>();
        }

        protected T Widget { get; private set; }

        public override void Render(Context context, TextWriter result)
        {
            var helper = context.Registers.Get<HtmlHelper>("Helper");
            var widgetResult = Widget.Execute();
            if (widgetResult.Success)
                widgetResult.WriteTo(helper);
        }
    }
}