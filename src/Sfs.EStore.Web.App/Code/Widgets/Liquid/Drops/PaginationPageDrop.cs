using System;
using System.Globalization;
using DotLiquid;

namespace Sfs.EStore.Web.App.Code.Widgets.Liquid.Drops
{
    public class PaginationPageDrop : Drop
    {
        public PaginationPageDrop(int number, Func<int, string> urlBuilder, Func<int, bool> isActive, bool disabled = false)
        {
            Number = number;
            Text = number.ToString(CultureInfo.InvariantCulture);
            Active = isActive(number);
            Disabled = disabled;
            if (!disabled)
                Url = urlBuilder(number);
        }

        public int Number { get; private set; }
        public string Url { get; private set; }
        public bool Active { get; private set; }
        public bool Disabled { get; private set; }
        public string Text { get; set; }
    }
}