using DotLiquid;
using Sfs.EStore.Web.App.Code.Security;

namespace Sfs.EStore.Web.App.Code.Widgets.Liquid.Drops
{
    public class UserDrop : Drop
    {
        private readonly IWebApplicationIdentity _identity;

        public UserDrop(IWebApplicationIdentity identity)
        {
            _identity = identity;
        }

        public bool IsAnonymous { get { return !_identity.IsAuthenticated && !_identity.IsGuest; }}
        public bool IsGuest { get { return _identity.IsGuest; } }
        public bool IsAuthenticated { get { return _identity.IsAuthenticated; } }
        public string Name { get { return _identity.Name; } }
        public string Email { get { return _identity.EmailAddress; } }
        public string PhoneNumber { get { return _identity.PhoneNumber; } }
        public string FirstName { get { return _identity.FirstName; } }
        public string LastName { get { return _identity.LastName; } }
    }
}