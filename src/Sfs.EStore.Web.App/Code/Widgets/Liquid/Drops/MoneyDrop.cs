using DotLiquid;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;

namespace Sfs.EStore.Web.App.Code.Widgets.Liquid.Drops
{
    public class MoneyDrop : Drop
    {
        public MoneyDrop(MoneyViewModel model)
        {
            Amount = model.Amount;
            Currency = model.Currency;
        }

        public decimal Amount { get; private set; }
        public string Currency { get; private set; }

        public override string ToString()
        {
            return string.Format("{0}{1}", Amount.ToString("n2"), Currency);
        }
    }
}