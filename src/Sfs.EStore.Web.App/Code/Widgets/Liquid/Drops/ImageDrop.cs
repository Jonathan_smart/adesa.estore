﻿using DotLiquid;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;

namespace Sfs.EStore.Web.App.Code.Widgets.Liquid.Drops
{
    public class ImageDrop : Drop
    {
        private readonly ImageModel _model;

        public ImageDrop(ImageModel model)
        {
            _model = model;
        }

        public ImageModel Model()
        {
            return _model;
        }
    }
}