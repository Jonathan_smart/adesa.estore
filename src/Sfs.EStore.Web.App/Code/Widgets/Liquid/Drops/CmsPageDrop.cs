using System;
using System.Web;
using DotLiquid;

namespace Sfs.EStore.Web.App.Code.Widgets.Liquid.Drops
{
    public class CmsPageDrop : Drop
    {
        public CmsPageDrop(HttpRequestBase httpRequest)
        {
            if (httpRequest == null) throw new ArgumentNullException("httpRequest");

            Url = httpRequest.Url.AbsolutePath;
        }

        public string Url { get; private set; }
    }
}