using System;
using System.Linq;
using DotLiquid;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;

namespace Sfs.EStore.Web.App.Code.Widgets.Liquid.Drops
{
    public class ListingSummaryDrop : Drop
    {
        private readonly ListingSummaryViewModel _model;

        public ListingSummaryDrop(ListingSummaryViewModel model)
        {
            _model = model;
            
            if (_model.VehicleInfo != null)
                VehicleInfo = new VehicleInfoDrop(_model.VehicleInfo);
            
            if (_model.SellingStatus != null)
                SellingStatus = new SellingStatusDrop(_model.SellingStatus);

            if (_model.ListingInfo != null)
                ListingInfo = new ListingInfoDrop(_model.ListingInfo);

            if (_model.DiscountPriceInfo != null)
                DiscountPriceInfo = new DiscountPriceInfoDrop(_model.DiscountPriceInfo);

            if (model.PrimaryImage != null) PrimaryImage = new ImageDrop(model.PrimaryImage);

            MoreImages = (model.MoreImages ?? new ImageModel[0]).Select(x => new ImageDrop(x)).ToArray();
        }

        public DiscountPriceInfoDrop DiscountPriceInfo { get; private set; }

        public ImageDrop PrimaryImage { get; set; }
        public ImageDrop[] MoreImages { get; set; }

        public int ListingId { get { return _model.ListingId; }}

        public ListingInfoDrop ListingInfo { get; private set; }

        public SellingStatusDrop SellingStatus { get; private set; }

        public VehicleInfoDrop VehicleInfo { get; private set; }

        public bool PricesIncludeVat { get { return _model.PricesIncludeVat; } }

        public string Title { get { return _model.Title; } }
        public string AttentionGrabber { get { return _model.AttentionGrabber; } }


        public class VehicleInfoDrop : Drop
        {
            private readonly ListingSummaryViewModel.VehicleInfoModel _model;

            public VehicleInfoDrop(ListingSummaryViewModel.VehicleInfoModel model)
            {
                _model = model;
                Registration = new RegistrationDrop(model.Registration);
                Specification = new SpecificationDrop(model.Specification);
            }

            public string Make { get { return _model.Make; } }
            public string Model { get { return _model.Model; } }
            public string Derivative { get { return _model.Derivative; } }
            public string VatStatus { get { return _model.VatStatus; } }
            public string FuelType { get { return _model.FuelType; } }
            public string Transmission { get { return _model.Transmission; } }
            public RegistrationDrop Registration { get; private set; }
            public int? Mileage { get { return _model.Mileage; } }
            public string VehicleType { get { return _model.VehicleType; } }
            public SpecificationDrop Specification { get; set; }

            public class RegistrationDrop : Drop
            {
                private readonly ListingSummaryViewModel.VehicleInfoModel.RegistrationModel _model;

                public RegistrationDrop(ListingSummaryViewModel.VehicleInfoModel.RegistrationModel model)
                {
                    _model = model;
                }

                public string Plate { get { return _model.Plate; } }
                public DateTime? Date { get { return _model.Date; } }
                public string Yap { get { return _model.Yap; } }
            }

            public class SpecificationDrop : Drop
            {
                private readonly ListingSummaryViewModel.VehicleInfoModel.SpecificationModel _model;

                public SpecificationDrop(ListingSummaryViewModel.VehicleInfoModel.SpecificationModel specification)
                {
                    _model = specification;
                    Economy = new EconomyInfoDrop(_model.Economy);
                }

                public EconomyInfoDrop Economy { get; private set; }
                public decimal? Co2 { get { return _model.Co2; } }
            }
        }

        public class SellingStatusDrop : Drop
        {
            private readonly ListingSummaryViewModel.SellingStatusModel _model;

            public SellingStatusDrop(ListingSummaryViewModel.SellingStatusModel model)
            {
                _model = model;
                CurrentPrice = new MoneyDrop(model.CurrentPrice);
            }

            public int BidCount { get { return _model.BidCount; } }
            public MoneyDrop CurrentPrice { get; private set; }
            public string SellingState { get { return _model.SellingState; } }
            public bool ReserveMet { get { return _model.ReserveMet; } }
            public double? TimeLeft { get { return _model.TimeLeft; } }
        }

        public class ListingInfoDrop : Drop
        {
            private readonly ListingSummaryViewModel.ListingInfoModel _model;

            public ListingInfoDrop(ListingSummaryViewModel.ListingInfoModel model)
            {
                _model = model;
                BuyItNowPrice = new MoneyDrop(model.BuyItNowPrice);
            }

            public bool BuyItNowAvailable { get { return _model.BuyItNowAvailable; } }
            public MoneyDrop BuyItNowPrice { get; private set; }
            public DateTime? EndTime { get { return _model.EndTime; } }

            public DateTime? StartTime { get { return _model.StartTime; } }

            public string ListingType { get { return _model.ListingType; } }
        }

        public class DiscountPriceInfoDrop : Drop
        {
            public DiscountPriceInfoDrop(ListingSummaryViewModel.DiscountPriceInfoModel model)
            {
                if (model == null) return;

                RetailPrice = model.RetailPrice == null ? null : new MoneyDrop(model.RetailPrice);
                WasPrice = model.WasPrice == null ? null : new MoneyDrop(model.WasPrice);
            }

            public MoneyDrop RetailPrice { get; private set; }
            public MoneyDrop WasPrice { get; private set; }
        }
    }
}