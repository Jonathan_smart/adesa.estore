using DotLiquid;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;

namespace Sfs.EStore.Web.App.Code.Widgets.Liquid.Drops
{
    public class EconomyInfoDrop : Drop
    {
        private readonly EconomyInfoModel _model;

        public EconomyInfoDrop(EconomyInfoModel model)
        {
            _model = model;
        }

        public decimal? Urban { get { return _model.Urban; } }
        public decimal? ExtraUrban { get { return _model.ExtraUrban; } }
        public decimal? Combined { get { return _model.Combined; } }
    }
}