using System.Collections.Generic;

namespace Sfs.EStore.Web.App.Code.Widgets.Liquid
{
    public class ViewLotBlock : WidgetBlock<ViewLotWidget>
    {
        public override void Initialize(string tagName, string markup, List<string> tokens)
        {
            int lotId;
            if (int.TryParse(markup.Trim(), out lotId))
                Widget.LotId = lotId;
            else
                Widget.LotId = null;

            base.Initialize(tagName, markup, tokens);
        }
    }
}