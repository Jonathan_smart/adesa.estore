using System.IO;
using System.Web.Mvc;
using DotLiquid;

namespace Sfs.EStore.Web.App.Code.Widgets.Liquid
{
    public class DisplayWidgetTag : Tag
    {
        public override void Render(Context context, TextWriter result)
        {
            var helper = context.Registers.Get<HtmlHelper>("Helper");
            var widgetResult = (WidgetResult)context.Registers["WidgetResult"];
            widgetResult.WriteTo(helper);
            base.Render(context, result);
        }
    }
}