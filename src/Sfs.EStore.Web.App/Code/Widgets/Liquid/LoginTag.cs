namespace Sfs.EStore.Web.App.Code.Widgets.Liquid
{
    public class LoginTag : WidgetTag<LoginWidget>
    {
        public override void Initialize(string tagName, string markup, System.Collections.Generic.List<string> tokens)
        {
            var redirectUrl = markup.Trim();
            Widget.RedirectUrl = redirectUrl;
            base.Initialize(tagName, markup, tokens);
        }
    }
}