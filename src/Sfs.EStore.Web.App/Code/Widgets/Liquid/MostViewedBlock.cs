﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DotLiquid;
using Ninject;
using Sfs.EStore.Web.App.App_Start;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Code.Widgets.Liquid.Drops;

namespace Sfs.EStore.Web.App.Code.Widgets.Liquid
{
    /// <example>
    /// {% most_viewed vehicleType:'CAR' make:'AUDI' %}
    ///   {% for listing in results %} /* ListingSummaryDrop[] */
    ///     {% listing_card %}
    ///   {% endfor %}
    /// {% endmost_viewed %}
    /// </example>
    public class MostViewedBlock : Block
    {
        private Task<SearchModel> _model;
        private IDictionary<string, string> _query;

        public override void Initialize(string tagName, string parametersMarkup, List<string> tokens)
        {
            base.Initialize(tagName, parametersMarkup, tokens);

            _query = TagParamsParser.Parse(parametersMarkup);
            if (!_query.ContainsKey("top")) _query["top"] = "4";

            var proxy = NinjectWebCommon.Kernel.Get<AuthorizedHttpClient>();
            _model = proxy.PostAsJson("mostviewed?", _query)
                .AcceptJson().SendAsync()
                .ContinueWith(x =>
                {
                    x.Result.EnsureSuccessStatusCode();

                    return new SearchModel
                    {
                        Results = x.Result.Content.ReadAsAsync<ListingSummaryViewModel[]>().Result
                            .Select(m => new ListingSummaryDrop(m)).ToArray()
                    };
                });
        }

        private class SearchModel
        {
            public ListingSummaryDrop[] Results { get; set; }
        }

        public override void Render(Context context, TextWriter result)
        {
            context.Stack(Hash.FromAnonymousObject(new
            {
                results = _model.Result.Results,
                has_results = _model.Result.Results.Any(),
                query = _query
            }), () => base.Render(context, result));
        }
    }
}