﻿
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using DotLiquid;
using Ninject;
using Sfs.EStore.Web.App.App_Start;
using Sfs.EStore.Web.App.Controllers.Api;
using Sfs.EStore.Web.App.ThirdParties;

namespace Sfs.EStore.Web.App.Code.Widgets.Liquid
{
    /// <example>
    /// Display Car Lots within Allow Preview
    /// {% Scheduled_Auctions %}
    /// {% for sale in sales %}
    ///   Id: {{sale.id}}
    ///   Title: {{sale.title}}
    ///   Total count: {{sale.count}}
    ///   Available now: {{sale.available}}
    /// {% endfor %}
    /// {% endScheduled_Auctions %}
    /// </example>
    public class ScheduledAuctionsBlock : Block
    {
        private Task<Auctions[]> _auction;

        public override void Initialize(string tagName, string markup, System.Collections.Generic.List<string> tokens)
        {
            base.Initialize(tagName, markup, tokens);

            var proxy = NinjectWebCommon.Kernel.Get<AuthorizedHttpClient>();
            var searchHandler = NinjectWebCommon.Kernel.Get<IVehicleSearchHandler>();

            var getParams = new CurrentSalesBlock.GetParams();
            searchHandler.HandlerSearchParams(getParams);

            getParams.Status = "Preview";

            proxy.Get("sales?" + getParams.ToUrlParams())
                .AcceptJson().SendAsync()
                .ContinueWith(x =>
                {
                    x.Result.EnsureSuccessStatusCode();

                    _auction = x.Result.Content.ReadAsAsync<Auctions[]>();
                })
                .Wait();
        }

        public class Auctions : Drop
        {
            public int Count { get; set; }
            public int Available { get; set; }
            public string Id { get; set; }
            public string Title { get; set; }
            public string Scheduled { get; set; }
            public string StartDate { get; set; }
        }

        public override void Render(Context context, TextWriter result)
        {
            context.Stack(Hash.FromAnonymousObject(new
            {
                auctions = _auction.Result
            }), () => base.Render(context, result));
        }

        public class GetParams : ISecurable
        {
            public string[] LimitToRoles { get; set; }
            public string Status { get; set; }

        }
    }

}