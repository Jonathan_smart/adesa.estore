﻿using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using DotLiquid;
using Sfs.EStore.Web.App.Models;


namespace Sfs.EStore.Web.App.Code.Widgets.Liquid
{
    /// <example>
    //{% DealerWidget selectImage:'' %}
    /// </example>
    public class DealerWidget : Block
    {
        private DealerTagModel _model;

        private const string SelectImage = "selectImage";
        private const string FoundImage = "foundImage";

        public override void Initialize(string tagName, string markup, List<string> tokens)
        {
            var args = TagParamsParser.Parse(markup);
            _model = new DealerTagModel();

            if (args.ContainsKey(SelectImage))
            {            
                _model.SelectImageUrl = args[SelectImage];
            }

            if (args.ContainsKey(FoundImage))
            {
                _model.FoundImageUrl = args[FoundImage];
            }

            base.Initialize(tagName, markup, tokens);
        }


        public override void Render(Context context, TextWriter result)
        {
            var htmlHelper = context.Registers.Get<HtmlHelper>("Helper");
            htmlHelper.RenderPartial("_DealerWidget", _model);
                     
            base.Render(context, result);
        }
    }
}