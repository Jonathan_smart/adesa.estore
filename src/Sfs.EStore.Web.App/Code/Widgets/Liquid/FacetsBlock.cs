﻿using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using DotLiquid;
using Ninject;
using Sfs.EStore.Web.App.App_Start;
using System.Collections.Generic;

namespace Sfs.EStore.Web.App.Code.Widgets.Liquid
{
    /// <example>
    /// {% facets facet:'makes' vehicleType:'CAR' %}
    ///   {% for result in results.make.values %}
    ///     {{result.range}} ({{result.hits}})
    ///   {% endfor %}
    /// {% endfacets %}
    /// </example>
    public class FacetsBlock : Block
    {
        private Task<FacetResults> _fasetResults;
        private IDictionary<string, string> _query;
        private AuthorizedHttpClient _proxy;

        public override void Initialize(string tagName, string parametersMarkup, List<string> tokens)
        {
            base.Initialize(tagName, parametersMarkup, tokens);

            _query = TagParamsParser.Parse(parametersMarkup);

            _proxy = NinjectWebCommon.Kernel.Get<AuthorizedHttpClient>();
        }

        public override void Render(Context context, TextWriter result)
        {
            if (!_query.ContainsKey("vehicleType") && context["vehicle_type_query_term"] != null)
                _query["vehicleType"] = context["vehicle_type_query_term"].ToString();

            if (_query["facet"] != "makes" && !_query.ContainsKey("make") && context["manufacturer_query_term"] != null)
                _query["make"] = context["manufacturer_query_term"].ToString();

            if (_query["facet"] != "models" && !_query.ContainsKey("model") && context["model_query_term"] != null)
                _query["model"] = context["model_query_term"].ToString();


            _proxy.PostAsJson("facets?", _query)
                .AcceptJson().SendAsync()
                .ContinueWith(x =>
                {
                    x.Result.EnsureSuccessStatusCode();

                    _fasetResults = x.Result.Content.ReadAsAsync<FacetResults>();
                })
                .Wait();

            context.Merge(Hash.FromAnonymousObject(new
            {
                results = _fasetResults.Result.Results,
                query = _query
            }));
            base.Render(context, result);
        }

        public class FacetResults : Drop
        {
            public Dictionary<string, FacetResult> Results { get; set; }
        }

        public class FacetResult : Drop
        {
            public IEnumerable<FacetValue> Values { get; set; }
        }

        public class FacetValue : Drop
        {
            public int Hits { get; set; }
            public string Range { get; set; }
        }
    }
}