using System;
using System.Collections.Generic;
using System.Linq;

namespace Sfs.EStore.Web.App.Code.Widgets.Liquid
{
    public class SingleMostViewedBlock : WidgetBlock<SingleMostViewedWidget>
    {
        public override void Initialize(string tagName, string markup, List<string> tokens)
        {
            var pairs = markup.Trim().Split(' ').Where(x => x.Contains(':')).Select(x => new Tuple<string, string>(x.Split(':')[0], x.Split(':')[1])).ToList();
            var vehicleTypePair = pairs.FirstOrDefault(x => x.Item1.Equals("vehicletype", StringComparison.InvariantCultureIgnoreCase));
            if (vehicleTypePair != null)
                Widget.Model.VehicleType = vehicleTypePair.Item2;

            base.Initialize(tagName, markup, tokens);
        }
    }
}