using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DotLiquid;
using Sfs.EStore.Web.App.Code.Widgets.Liquid.Drops;

namespace Sfs.EStore.Web.App.Code.Widgets.Liquid
{
    /// <example>
    /// {% pagination previous_text:'Previous' next_text:'Next' max_size:'7' %}
    /// </example>
    public class PaginationTag : Tag
    {
        private int _maxSize = 5;
        private int _paginationHalfwayPoint;

        private string _previousText = "&lt;";
        private string _nextText = "&gt;";

        public override void Initialize(string tagName, string markup, List<string> tokens)
        {
            var @params = TagParamsParser.Parse(markup);

            if (@params.ContainsKey("previous_text"))
                _previousText = @params["previous_text"];

            if (@params.ContainsKey("next_text"))
                _nextText = @params["next_text"];

            if (@params.ContainsKey("max_size"))
                _maxSize = int.Parse(@params["max_size"]);

            _paginationHalfwayPoint = (int)Math.Ceiling(_maxSize / 2m);

            base.Initialize(tagName, markup, tokens);
        }

        public override void Render(Context context, TextWriter result)
        {
            var totalItems = (int)(context["total_items"] ?? 0);

            if (totalItems == 0) return;

            var currentPage = (int)(context[PaginatedPageContextTag.CurrentPageKey] ?? 1);
            var itemsPerPage = (int)(context[PaginatedPageContextTag.ItemsPerPageKey] ?? PaginatedPageContextTag.DefaultItemsPerPage);

            var numberOfPages = (int)Math.Ceiling(totalItems / (decimal)itemsPerPage);
            currentPage = Math.Min(currentPage, numberOfPages);

            int paginationStart;

            if (currentPage <= _paginationHalfwayPoint)
                paginationStart = 1;
            else if (currentPage >= (numberOfPages - _paginationHalfwayPoint))
                paginationStart = (numberOfPages - Math.Min(numberOfPages, _maxSize)) + 1;
            else
                paginationStart = (currentPage - _paginationHalfwayPoint) + 1;

            Func<int, string> urlBuilder = x => string.Format("?{0}={1}", PaginatedPageContextTag.DefaultParamName, x);
            Func<int, bool> isActive = x => x == currentPage;

            var pages = new List<PaginationPageDrop>();

            pages.AddRange(Enumerable.Range(paginationStart, Math.Min(_maxSize, numberOfPages))
                .Select(x => new PaginationPageDrop(x, urlBuilder, isActive)));

            pages.Insert(0, new PaginationPageDrop(currentPage - 1, urlBuilder, isActive, currentPage == 1) { Text = _previousText });
            pages.Add(new PaginationPageDrop(currentPage + 1, urlBuilder, isActive, currentPage >= numberOfPages) { Text = _nextText });

            result.WriteLine("<ul class=\"pagination\">");
            foreach(var page in pages)
            {
                var cssClasses = "";
                if (page.Disabled)
                    cssClasses += "disabled ";
                if (page.Active)
                    cssClasses += "active";

                result.WriteLine("<li class=\"{0}\"><a href=\"{1}\">{2}</a></li>", cssClasses, page.Url, page.Text);
            }
            result.WriteLine("</ul>");
        }
    }
}