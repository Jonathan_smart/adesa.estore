using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using DotLiquid;
using Ninject;
using Sfs.EStore.Web.App.App_Start;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Code.Widgets.Liquid.Drops;

namespace Sfs.EStore.Web.App.Code.Widgets.Liquid
{
    /// <example>
    /// {% available_finance %}
    ///   {% for customer in results %} /* CustomerAvailableFinanceDrop[] */
    ///     customer.name: {{customer.name}}
    ///     customer.no: {{customer.no}}
    ///     available_finace {{ customer.available_finance | format_as_money }}
    ///   {% endfor %}
    /// {% endavailable_finance %}
    /// </example>
    public class AvailableFinanceBlock : Block
    {
        public override void Initialize(string tagName, string parametersMarkup, List<string> tokens)
        {
            base.Initialize(tagName, parametersMarkup, tokens);
        }

        private class ResultModel
        {
            public CustomerAvailableFinanceDrop[] Results { get; set; }
            public bool Success { get; set; }
        }

        public class CustomerAvailableFinanceDrop : Drop
        {
            public string Name { get; set; }
            public string No { get; set; }
            public string City { get; set; }
            public MoneyDrop AvailableFinance { get; set; }
            public string Message { get; set; }
        }

        public override void Render(Context context, TextWriter result)
        {
            var proxy = NinjectWebCommon.Kernel.Get<AuthorizedHttpClient>();
            var model = proxy.Get("customersfinance")
                .AcceptJson().SendAsync()
                .ContinueWith(x =>
                {
                    if (!x.Result.IsSuccessStatusCode)
                        return new ResultModel
                        {
                            Success = false,
                            Results = new CustomerAvailableFinanceDrop[0]
                        };

                    return new ResultModel
                    {
                        Success = true,
                        Results = x.Result.Content.ReadAsAsync<CustomerFinanceAvailabilityModel[]>().Result
                            .Select(m => new CustomerAvailableFinanceDrop
                            {
                                AvailableFinance = new MoneyDrop(new MoneyViewModel(m.Available, "GBP")),
                                Name = m.Customer.Name,
                                No = m.Customer.No,
                                City = m.Customer.City,
                                Message = m.Message
                            }).ToArray()
                    };
                })
                .Result;

            context.Stack(Hash.FromAnonymousObject(new
            {
                success = model.Success,
                results = model.Results
            }), () => base.Render(context, result));
        }
    }
}