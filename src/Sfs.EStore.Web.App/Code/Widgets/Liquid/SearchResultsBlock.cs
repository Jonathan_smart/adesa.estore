using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DotLiquid;
using Ninject;
using Sfs.EStore.Web.App.App_Start;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Code.Widgets.Liquid.Drops;

namespace Sfs.EStore.Web.App.Code.Widgets.Liquid
{
    /// <example>
    /// {% search vehicleType:'CAR' make:'AUDI' %}
    ///   {% for listing in results %} /* ListingSummaryDrop[] */
    ///     listing_id: {{listing.listing_id }}
    ///     title: {{listing.title }}
    ///     attention_grabber: {{listing.attention_grabber }}
    ///     prices_include_vat: {{listing.prices_include_vat }}
    ///     primary_image: {{listing.primary_image | image_url}}
    ///     more_images: {% for image in listing.more_images %}{{image | image_url-}}{%endfor%}
    ///     listing_info
    ///       .buy_it_now_available: {{ listing.listing_info.buy_it_now_available }}
    ///       .buy_it_now_price: {{ listing.listing_info.buy_it_now_price | format_as_money }}
    ///         .amount: {{ listing.listing_info.buy_it_now_price.amount }}
    ///         .currency: {{ listing.listing_info.buy_it_now_price.currency }}
    ///       .end_time: {{ listing.listing_info.end_time }}
    ///       .start_time: {{ listing.listing_info.start_time }}
    ///       .listing_type: {{ listing.listing_info.listing_type }}
    ///     selling_status
    ///       .bid_count: {{ listing.selling_status.bid_count }}
    ///       .current_price: {{ listing.selling_status.current_price | format_as_money }}
    ///       .selling_state: {{ listing.selling_status.selling_state }}
    ///       .reserve_met: {{ listing.selling_status.reserve_met }}
    ///       .time_left: {{ listing.selling_status.time_left }}
    ///     vehicle_info
    ///       .make: {{ listing.vehicle_info.make }}
    ///       .model: {{ listing.vehicle_info.model }}
    ///       .derivative: {{ listing.vehicle_info.derivative }}
    ///       .vat_status: {{ listing.vehicle_info.vat_status }}
    ///       .fuel_type: {{ listing.vehicle_info.fuel_type }}
    ///       .transmission: {{ listing.vehicle_info.transmission }}
    ///       .registration: {{ listing.vehicle_info.registration }}
    ///         .plate: {{ listing.vehicle_info.registration.plate }}
    ///         .date: {{ listing.vehicle_info.registration.date }}
    ///         .yap: {{ listing.vehicle_info.registration.yap }}
    ///       .mileage: {{ listing.vehicle_info.mileage }}
    ///       .vehicle_type: {{ listing.vehicle_info.vehicle_type }}
    ///       .specification: {{ listing.vehicle_info.specification }}
    ///         .co2: {{ listing.vehicle_info.specification.co2 }}
    ///         .economy: {{ listing.vehicle_info.specification.economy }}
    ///           .urban: {{ listing.vehicle_info.specification.urban }}
    ///           .extra_urban: {{ listing.vehicle_info.specification.extra_urban }}
    ///           .combined: {{ listing.vehicle_info.specification.combined }}
    ///     discount_price_info
    ///       .retail_price: {{ listing.discount_price_info.retail_price | format_as_money }}
    ///   {% endfor %}
    /// {% endsearch %}
    /// </example>
    public class SearchResultsBlock : Block
    {
        private Task<SearchModel> _model;
        private IDictionary<string, string> _query;

        public override void Initialize(string tagName, string parametersMarkup, List<string> tokens)
        {
            base.Initialize(tagName, parametersMarkup, tokens);

            _query = TagParamsParser.Parse(parametersMarkup);
        }

        private class SearchModel
        {
            public ListingSummaryDrop[] Results { get; set; }
            public int Count { get; set; }
        }

        public override void Render(Context context, TextWriter result)
        {
            var currentPage = (int)(context[PaginatedPageContextTag.CurrentPageKey] ?? 1);
            var itemsPerPage = (int)(context[PaginatedPageContextTag.ItemsPerPageKey] ?? PaginatedPageContextTag.DefaultItemsPerPage);
            
            if (!_query.ContainsKey("top")) _query["top"] = itemsPerPage.ToString(CultureInfo.InvariantCulture);
            if (!_query.ContainsKey("skip")) _query["skip"] = (itemsPerPage * (currentPage - 1)).ToString(CultureInfo.InvariantCulture);

            if (!_query.ContainsKey("vehicleType") && context["vehicle_type_query_term"] != null)
                _query["vehicleType"] = context["vehicle_type_query_term"].ToString();

            if (!_query.ContainsKey("make") && context["manufacturer_query_term"] != null)
                _query["make"] = context["manufacturer_query_term"].ToString();

            if (!_query.ContainsKey("model") && context["model_query_term"] != null)
                _query["model"] = context["model_query_term"].ToString();

            var proxy = NinjectWebCommon.Kernel.Get<AuthorizedHttpClient>();
            _model = proxy.PostAsJson("search?", _query)
                .AcceptJson().SendAsync()
                .ContinueWith(x =>
                {
                    x.Result.EnsureSuccessStatusCode();

                    return new SearchModel
                    {
                        Results = x.Result.Content.ReadAsAsync<ListingSummaryViewModel[]>().Result
                            .Select(m => new ListingSummaryDrop(m)).ToArray(),
                        Count = int.Parse(x.Result.Headers.GetValues("X-TotalResults").First())
                    };
                });

            context.Stack(Hash.FromAnonymousObject(new
            {
                results = _model.Result.Results,
                total_items = _model.Result.Count,
                query = _query
            }), () => base.Render(context, result));
        }
    }
}