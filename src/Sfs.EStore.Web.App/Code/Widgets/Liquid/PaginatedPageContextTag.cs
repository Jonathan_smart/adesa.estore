using System.Collections.Generic;
using System.IO;
using System.Web;
using DotLiquid;

namespace Sfs.EStore.Web.App.Code.Widgets.Liquid
{
    /// <example>
    /// {% paginated_page_context items_per_page:'6' %}
    /// </example>
    public class PaginatedPageContextTag : Tag
    {
        public const string ItemsPerPageKey = "items_per_page";
        public const string CurrentPageKey = "current_page";

        public const int DefaultItemsPerPage = 20;
        public const string DefaultParamName = "currentPage";

        private int? _itemsPerPage;

        public override void Initialize(string tagName, string markup, List<string> tokens)
        {
            var @params = TagParamsParser.Parse(markup);

            int itemsPerPage;
            if (@params.ContainsKey(ItemsPerPageKey) && int.TryParse(@params[ItemsPerPageKey] ?? "", out itemsPerPage))
                _itemsPerPage = itemsPerPage;
        }

        public override void Render(Context context, TextWriter result)
        {
            if (context[CurrentPageKey] != null) return;

            int page;
            if (!int.TryParse(HttpContext.Current.Request.QueryString[DefaultParamName] ?? "1", out page))
                page = 1;

            context[CurrentPageKey] = page;

            context[ItemsPerPageKey] = (int) (_itemsPerPage ?? context[ItemsPerPageKey] ?? DefaultItemsPerPage);
        }
    }
}