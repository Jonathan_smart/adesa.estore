﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Sfs.EStore.Web.App.Code.Widgets.Liquid
{
    internal static class TagParamsParser
    {
        private static readonly Regex ParametersRegex = new Regex(@"((?<key>[\w]+)\:'(?<value>([^']*))')");

        public static IDictionary<string, string> Parse(string markup)
        {
            var matches = ParametersRegex.Matches((markup ?? "").Trim());

            return matches.Cast<Match>().ToDictionary(match => match.Groups["key"].Value, match => match.Groups["value"].Value);
        } 
    }
}