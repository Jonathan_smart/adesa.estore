﻿using System.Linq;
using System.Net.Http;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Code.ListingViewModelVisitors;
using Sfs.EStore.Web.App.Controllers.Api;
using Sfs.EStore.Web.App.ThirdParties;

namespace Sfs.EStore.Web.App.Code.Widgets
{
    public class SingleMostViewedWidget : Widget
    {
        public class Result : WidgetResult
        {
            public ListingSummaryViewModel Summary { get; set; }

            public override void WriteTo(HtmlHelper htmlHelper)
            {
                htmlHelper.RenderPartial("_viewlotcarouselitem", Summary ?? new ListingSummaryViewModel());
            }
        }

        //todo: tf- move into models?
        public class LotsGetModel : ISecurable
        {
            public int Top { get; set; }
            public string VehicleType { get; set; }
            public string[] LimitToRoles { get; set; }
        }

        public LotsGetModel Model { get; private set; }

        private readonly IVehicleSearchHandler _vehicleSearchHandler;
        private readonly SiteTenantActivityPermissions _permissions;
        private readonly IListingSummaryViewModelVisitor _summaryModelVisitor;

        public SingleMostViewedWidget(IVehicleSearchHandler vehicleSearchHandler, SiteTenantActivityPermissions permissions, IListingSummaryViewModelVisitor summaryModelVisitor)
        {
            _vehicleSearchHandler = vehicleSearchHandler;
            _permissions = permissions;
            _summaryModelVisitor = summaryModelVisitor;
            Model = new LotsGetModel { Top = 1 };
        }

        public override WidgetResult Execute()
        {
            if (!_permissions["/Lots/View"].IsAllowedFor(User))
                return new Result { Success = false };

            _vehicleSearchHandler.HandlerSearchParams(Model);

            var result = ApiProxy.Get("mostviewed/?" + Model.ToUrlParams()).AcceptJson().SendAsync().Result;
            
            if (!result.IsSuccessStatusCode)
                return new Result { Success = false };

            var summaries = result.Content.ReadAsAsync<ListingSummaryViewModel[]>()
                .Result;

            if (summaries == null)
                return new Result { Success = false };

            var summary = summaries.FirstOrDefault();
            if (summary == null)
                return new Result { Success = false };

            _summaryModelVisitor.Visit(summary);

            return new Result { Success = true, Summary = summary };
        }
    }
}