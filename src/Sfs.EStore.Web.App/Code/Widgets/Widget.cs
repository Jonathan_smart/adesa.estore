using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Web.Mvc;
using Sfs.EStore.Web.App.Code.Security;

namespace Sfs.EStore.Web.App.Code.Widgets
{
    public abstract class Widget
    {
        private AuthorizedHttpClient _apiProxy;

        protected IWebApplicationPrincipal User
        {
            get { return (System.Threading.Thread.CurrentPrincipal as IWebApplicationPrincipal); }
        }

        protected AuthorizedHttpClient ApiProxy
        {
            get { return _apiProxy ?? (_apiProxy = ApiProxyFactory.Create(User)); }
        }

        public abstract WidgetResult Execute();
    }

    public abstract class WidgetResult : DotLiquid.ILiquidizable
    {
        public bool Success { get; set; }

        public abstract void WriteTo(HtmlHelper htmlHelper);

        protected virtual dynamic CreateLiquidObject()
        {
            dynamic d = new DynamicDictionary();
            d.success = Success;
            return d;
        }

        public object ToLiquid()
        {
            var obj = CreateLiquidObject();
            return ((DynamicDictionary)obj).Dictionary;
        }

        private class DynamicDictionary : DynamicObject
        {
            public Dictionary<string, object> Dictionary { get; private set; }

            public DynamicDictionary()
            {
                Dictionary = new Dictionary<string, object>(StringComparer.InvariantCultureIgnoreCase);
            }

            public override bool TrySetMember(SetMemberBinder binder, object value)
            {
                if (Dictionary.ContainsKey(binder.Name))
                    Dictionary.Remove(binder.Name);
                Dictionary.Add(binder.Name, value);
                return true;
            }

            public override bool TryGetMember(GetMemberBinder binder, out object result)
            {
                return Dictionary.TryGetValue(binder.Name, out result);
            }
        }
    }
}