using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using DotLiquid;
using Sfs.EStore.Web.App.Code.Security;

namespace Sfs.EStore.Web.App.Code.Widgets
{
    /// <summary>Secure block content by user roles</summary>
    /// <example>
    /// {% if_in_roles foo,bar %}
    ///     Only shown if user is in 'foo' OR 'bar' role
    /// {% endif_in_roles %}
    /// </example>
    public class IfUserIsInRolesBlock : Block
    {
        private string[] _roles = new string[0];

        public override void Initialize(string tagName, string markup, List<string> tokens)
        {
            _roles = markup.Replace("'", "").Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => x.Trim())
                .ToArray();

            base.Initialize(tagName, markup, tokens);
        }

        public override void Render(Context context, TextWriter result)
        {
            var user = Thread.CurrentPrincipal as IWebApplicationPrincipal;

            if (user == null) return;

            if (_roles.Any(user.IsInRole))
                base.Render(context, result);
        }
    }
}