﻿using System.Linq;
using System.Net.Http;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Code.ListingViewModelVisitors;
using Sfs.EStore.Web.App.ThirdParties;

namespace Sfs.EStore.Web.App.Code.Widgets
{
    public class ViewLotWidget : Widget
    {
        public class Result : WidgetResult
        {
            public ListingSummaryViewModel Summary { get; set; }

            public override void WriteTo(HtmlHelper htmlHelper)
            {
                htmlHelper.RenderPartial("_viewlotcarouselitem", Summary ?? new ListingSummaryViewModel());
            }
        }

        //todo: tf- move into models?
        private class ViewLotGetModel : ISecurable
        {
            public string[] LimitToRoles { get; set; }
        }

        private readonly IVehicleSearchHandler _vehicleSearchHandler;
        private readonly SiteTenantActivityPermissions _permissions;
        private readonly IListingSummaryViewModelVisitor _summaryModelVisitor;

        public int? LotId { get; set; }

        public ViewLotWidget(IVehicleSearchHandler vehicleSearchHandler, SiteTenantActivityPermissions permissions, IListingSummaryViewModelVisitor summaryModelVisitor)
        {
            _vehicleSearchHandler = vehicleSearchHandler;
            _permissions = permissions;
            _summaryModelVisitor = summaryModelVisitor;
        }

        public override WidgetResult Execute()
        {
            if (LotId == null)
                return new Result { Success = false };

            if (!_permissions["/Lots/View"].IsAllowedFor(User))
                return new Result { Success = false };

            var model = new ViewLotGetModel();
            _vehicleSearchHandler.HandlerSearchParams(model);

            var result = ApiProxy.PostAsJson("search", new {q = string.Format("lots/{0}", LotId)})
                .AcceptJson().SendAsync().Result;

            if (!result.IsSuccessStatusCode) return new Result { Success = false };

            var summary = result.Content.ReadAsAsync<ListingSummaryViewModel[]>().Result.FirstOrDefault();

            if (summary == null) return new Result {Success = false};

            _summaryModelVisitor.Visit(summary);

            return new Result { Success = true, Summary = summary };
        }
    }
}