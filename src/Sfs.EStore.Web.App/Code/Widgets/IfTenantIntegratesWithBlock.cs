using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using DotLiquid;
using Block = DotLiquid.Block;

namespace Sfs.EStore.Web.App.Code.Widgets
{
    public class IfTenantIntegratesWithBlock : Block
    {
        private string _settingKey;

        private readonly Lazy<ViewTenant> _tenant = new Lazy<ViewTenant>(
            () =>
            {
                var ctx = new HttpContextWrapper(HttpContext.Current);
                return new ViewTenant(ctx.CurrentTenant(), ctx.CurrentTenantPermissions());
            });

        public override void Initialize(string tagName, string markup, List<string> tokens)
        {
            _settingKey = (markup ?? "").Trim();
            base.Initialize(tagName, markup, tokens);
        }

        public override void Render(Context context, TextWriter result)
        {
            if (_tenant.Value.IntegratesWith(_settingKey))
                base.Render(context, result);
        }
    }
}