using System.Web.Mvc;

namespace Sfs.EStore.Web.App.Code.Widgets
{
    public class CmsBodyRenderer
    {
        private readonly string _source;
        private readonly ILiquidVariables _model;

        public CmsBodyRenderer(string source, ILiquidVariables model = null)
        {
            _source = source;
            _model = model;
        }

        public void Render(HtmlHelper htmlHelper)
        {
            new LiquidTemplateRenderer(htmlHelper, htmlHelper.ViewContext.Writer)
                .Render(_source, _model);
        }
    }
}