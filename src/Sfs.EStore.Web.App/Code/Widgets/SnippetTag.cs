using System.Collections.Generic;
using System.IO;
using DotLiquid;
using DotLiquid.Tags;

namespace Sfs.EStore.Web.App.Code.Widgets
{
    /// <example>
    /// {% snippet my/snippet/key %}
    /// </example>
    public class SnippetTag : Tag
    {
        private readonly Include _include = new Include();

        public override void Initialize(string tagName, string markup, List<string> tokens)
        {
            _include.Initialize("include", markup, tokens);
        }

        public override void Render(Context context, TextWriter result)
        {
            _include.Render(context, result);
        }
    }
}