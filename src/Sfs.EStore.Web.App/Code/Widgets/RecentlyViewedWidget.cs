﻿using System.Linq;
using System.Net.Http;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Code.ListingViewModelVisitors;
using Sfs.EStore.Web.App.Controllers.Api;
using Sfs.EStore.Web.App.ThirdParties;

namespace Sfs.EStore.Web.App.Code.Widgets
{
    public class RecentlyViewedWidget : Widget
    {
        public class Result : WidgetResult
        {
            public ListingSummaryViewModel Summary { get; set; }

            public override void WriteTo(HtmlHelper htmlHelper)
            {
                htmlHelper.RenderPartial("_viewlotcarouselitem", Summary);
            }
        }

        private readonly IVehicleSearchHandler _vehicleSearchHandler;
        private readonly SiteTenantActivityPermissions _permissions;
        private readonly IListingSummaryViewModelVisitor _summaryModelVisitor;

        private class RecentlyViewedGetModel : ISecurable
        {
            public string SessionId { get; set; }
            public int? Top { get; set; }
            public string[] LimitToRoles { get; set; }
        }

        public RecentlyViewedWidget(IVehicleSearchHandler vehicleSearchHandler, SiteTenantActivityPermissions permissions, IListingSummaryViewModelVisitor summaryModelVisitor)
        {
            _vehicleSearchHandler = vehicleSearchHandler;
            _permissions = permissions;
            _summaryModelVisitor = summaryModelVisitor;
        }

        public override WidgetResult Execute()
        {
            var model = new RecentlyViewedGetModel
            {
                SessionId = UniqueSessionId.Value, 
                Top = 1
            };

            _vehicleSearchHandler.HandlerSearchParams(model);

            var result = ApiProxy.Get("recentlyviewed?" + model.ToUrlParams())
                .AcceptJson().SendAsync()
                .Result;

            if (!result.IsSuccessStatusCode)
                return new Result { Success = false };

            var summaries = result.Content.ReadAsAsync<ListingSummaryViewModel[]>().Result.ToList();

            var summary = summaries.FirstOrDefault();
            if (summary == null)
                return new Result { Success = false };

            _summaryModelVisitor.Visit(summary);

            return new Result { Success = true, Summary = summary };
        }
    }
}