using System.Collections.Generic;

namespace Sfs.EStore.Web.App.Code
{
    public interface IUserValidator
    {
        UserIdentityValidation Validate(string username, string password);
    }

    public class UserIdentityValidation
    {
        public UserIdentity UserIdentity { get; set; }
        public bool IsAuthenticated { get; set; }
        public string Reason { get; set; }
    }

    public class UserIdentity
    {
        public UserIdentity(string userName, IEnumerable<string> roles, string token)
        {
            UserName = userName;
            Roles = (roles ?? new string[0]);
            Token = token;

            Data = new Dictionary<string, string>();
        }

        public string UserName { get; set; }
        public IEnumerable<string> Roles { get; set; }
        public Dictionary<string, string> Data { get; private set; }
        public string Token { get; set; }

        private string _firstName;
        public string FirstName
        {
            get { return _firstName ?? ""; }
            set { _firstName = value; }
        }

        private string _lastName;
        public string LastName
        {
            get { return _lastName ?? ""; }
            set { _lastName = value; }
        }

        private string _phoneNumber;
        public string PhoneNumber
        {
            get { return _phoneNumber ?? ""; }
            set { _phoneNumber = value; }
        }

        private string _emailAddress;
        public string EmailAddress
        {
            get { return _emailAddress ?? ""; }
            set { _emailAddress = value; }
        }

        public string this[string key]
        {
            get { return Data[key]; }
            set { Data[key] = value; }
        }
    }
}