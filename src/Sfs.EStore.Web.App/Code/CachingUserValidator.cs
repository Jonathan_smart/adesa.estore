//using System;
//using System.Collections.Concurrent;

//namespace Sfs.EStore.Web.App.Code
//{
//    public class CachingUserValidator : IUserValidator
//    {
//        private class CachedIdentity
//        {
//            public CachedIdentity(IUserIdentity identity)
//            {
//                Identity = identity;
//                Time = SystemTime.UtcNow;
//            }
            
//            public IUserIdentity Identity { get; private set; }
//            public DateTime Time { get; private set; }
//        }

//        private readonly ConcurrentDictionary<string, CachedIdentity> _cache =
//            new ConcurrentDictionary<string, CachedIdentity>();
//        private readonly Func<IUserValidator> _proxy;
//        private readonly int _cacheForSeconds;

//        public CachingUserValidator(Func<IUserValidator> proxy, int cacheForSeconds)
//        {
//            _proxy = proxy;
//            _cacheForSeconds = cacheForSeconds;
//        }

//        public IUserIdentity Validate(string username, string password)
//        {
//            CachedIdentity cachedRequest;
//            if (!_cache.TryGetValue(username, out cachedRequest))
//                return ReQueryProxyAndCacheResult(username, password);

//            return HasExpired(cachedRequest) ? ReQueryProxyAndCacheResult(username, password) : cachedRequest.Identity;
//        }

//        private IUserIdentity ReQueryProxyAndCacheResult(string username, string password)
//        {
//            var identity = _proxy().Validate(username, password);

//            if (identity != null)
//                _cache.AddOrUpdate(username, u => new CachedIdentity(identity), (u, i) => new CachedIdentity(null));
            
//            return identity;
//        }

//        private bool HasExpired(CachedIdentity request)
//        {
//            return (SystemTime.UtcNow - request.Time).TotalSeconds > _cacheForSeconds;
//        }
//    }
//}