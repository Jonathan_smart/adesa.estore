using Sfs.EStore.Web.App.Areas.Vehicles.Models;

namespace Sfs.EStore.Web.App.Code.ListingViewModelVisitors
{
    public interface IListingSummaryViewModelVisitor
    {
        void Visit(ListingSummaryViewModel model);
    }
}