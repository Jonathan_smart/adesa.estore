using Sfs.EStore.Web.App.Areas.Vehicles.Models;

namespace Sfs.EStore.Web.App.Code.ListingViewModelVisitors
{
    public interface IListingViewModelVisitor
    {
        void Visit(ListingViewModel model);
    }
}