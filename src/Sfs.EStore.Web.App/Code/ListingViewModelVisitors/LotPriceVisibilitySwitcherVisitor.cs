﻿using System.Linq;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Code.Security;

namespace Sfs.EStore.Web.App.Code.ListingViewModelVisitors
{
    public class LotPriceVisibilitySwitcherVisitor : IListingViewModelVisitor, IListingSummaryViewModelVisitor
    {
        private readonly bool _canShowPrices;

        public LotPriceVisibilitySwitcherVisitor(SiteTenantActivityPermissions permissions, IWebApplicationPrincipal user)
        {
            _canShowPrices = permissions["/Lots/View/Prices"].IsAllowedFor(user);
        }

        public void Visit(ListingViewModel model)
        {
            if (_canShowPrices) return;

            if (model.SellingStatus != null)
            {
                model.SellingStatus.CurrentPrice = null;
                model.SellingStatus.MinimumToBid = null;

                if (model.SellingStatus.Bids != null)
                {
                    model.SellingStatus.Bids.ToList().ForEach(x => x.Amount = null);
                }
            }

            if (model.ListingInfo != null)
                model.ListingInfo.BuyItNowPrice = null;

            if (model.DiscountPriceInfo != null)
                model.DiscountPriceInfo.WasPrice = null;
        }

        public void Visit(ListingSummaryViewModel model)
        {
            if (_canShowPrices) return;

            model.ListingInfo.BuyItNowPrice = null;
            model.SellingStatus.CurrentPrice = null;

            if (model.DiscountPriceInfo != null)
                model.DiscountPriceInfo.WasPrice = null;
        }
    }
}