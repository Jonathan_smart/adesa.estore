﻿using Sfs.EStore.Web.App.Areas.Vehicles.Models;

namespace Sfs.EStore.Web.App.Code.ListingViewModelVisitors
{
    public class CompositeListingModelVisitor : IListingSummaryViewModelVisitor, IListingViewModelVisitor
    {
        private readonly IListingSummaryViewModelVisitor[] _summaryVisitors;
        private readonly IListingViewModelVisitor[] _modelVisitors;

        public CompositeListingModelVisitor(IListingSummaryViewModelVisitor[] summaryVisitors, IListingViewModelVisitor[] modelVisitors)
        {
            _summaryVisitors = summaryVisitors;
            _modelVisitors = modelVisitors;
        }

        public void Visit(ListingSummaryViewModel model)
        {
            foreach (var visitor in _summaryVisitors)
            {
                visitor.Visit(model);
            } 
        }

        public void Visit(ListingViewModel model)
        {
            foreach (var visitor in _modelVisitors)
            {
                visitor.Visit(model);
            };
        }
    }
}