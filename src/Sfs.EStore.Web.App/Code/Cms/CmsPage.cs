using System.Collections.Generic;
using Sfs.EStore.Web.App.Code.Security;
using Slugify;

namespace Sfs.EStore.Web.App.Code.Cms
{
    public interface ICmsPage
    {
        string PageTitle { get; }
        string Body { get; }
        MetaData MetaData { get; }
    }

    public class ManufacturerCmsPage : ICmsPage
    {
        private string _body = "";
        private MetaData _metaData = new MetaData();
        private string _manufacturerQueryTerm;

        public string Id { get; protected set; }

        public string PageTitle { get; set; }

        public string Body
        {
            get { return _body; }
            set { _body = value; }
        }

        public MetaData MetaData
        {
            get { return _metaData; }
            set { _metaData = value; }
        }

        public string VehicleTypeQueryTerm { get; set; }
        public string ManufacturerQueryTerm
        {
            get { return _manufacturerQueryTerm; }
            set
            {
                _manufacturerQueryTerm = value;
                ManufacturerSlug = new SlugHelper().GenerateSlug(value ?? "");
            }
        }

        public string ManufacturerSlug { get; set; }

        public string ImageUrl { get; set; }
    }

    public class ManufacturerModelCmsPage : ICmsPage
    {
        private string _body = "";
        private MetaData _metaData = new MetaData();
        private string _manufacturerQueryTerm;
        private string _modelQueryTerm;

        public string Id { get; protected set; }

        public string PageTitle { get; set; }

        public string Body
        {
            get { return _body; }
            set { _body = value; }
        }

        public MetaData MetaData
        {
            get { return _metaData; }
            set { _metaData = value; }
        }

        public string VehicleTypeQueryTerm { get; set; }

        public string ManufacturerQueryTerm
        {
            get { return _manufacturerQueryTerm; }
            set
            {
                _manufacturerQueryTerm = value;
                ManufacturerSlug = new SlugHelper().GenerateSlug(value ?? "");
            }
        }

        public string ModelQueryTerm
        {
            get { return _modelQueryTerm; }
            set
            {
                _modelQueryTerm = value;
                ModelSlug = new SlugHelper().GenerateSlug(value ?? "");
            }
        }

        public string ManufacturerSlug { get; set; }
        public string ModelSlug { get; set; }

        public string ImageUrl { get; set; }
    }

    public class CmsPage : ICmsPage
    {
        public CmsPage()
        {
            Scripts = new CmsScript[0];
        }

        public string PermaName { get; set; }
        public string PageTitle { get; set; }
        public string Body { get; set; }
        public string BodyCssClass { get; set; }

        private string _allowAccess = PageAccess.Authenticated;
        public string AllowAccess
        {
            get { return _allowAccess ?? PageAccess.Authenticated; }
            set { _allowAccess = value; }
        }

        private MetaData _metaData = new MetaData();
        public MetaData MetaData
        {
            get { return _metaData; }
            set { _metaData = value; }
        }

        public CmsScript[] Scripts { get; set; }

        public CmsPage PermaNameFrom(string value)
        {
            PermaName = new SlugHelper(new SlugHelper.Config {DeniedCharactersRegex = @"[^a-zA-Z0-9\-\._\/]"})
                .GenerateSlug(value);

            return this;
        }

        public bool CanAccessPageAs(IWebApplicationPrincipal user)
        {
            return new PrincipalUserRestrictedAccess(AllowAccess).IsAllowedFor(user);
        }
    }

    public class MetaData
    {
        public MetaData()
        {
            Description = Title = CanonicalUrl = "";
        }

        public string Description { get; set; }
        public string Title { get; set; }
        public string CanonicalUrl { get; set; }
    }

    public class CmsScript
    {
        public string Value { get; set; }
        public string[] Dependencies { get; set; }
    }

    public static class PageAccess
    {
        public const string Everyone = "*";
        public const string Guest = "guest";
        public const string Authenticated = "";
        public const string Deny = "deny";

        private static readonly Dictionary<string, string> List = new Dictionary<string, string>{
                {"Everyone", Everyone},
                {"Guest", Guest},
                {"Authenticated", Authenticated},
                {"Deny", Deny},
            };

        public static IEnumerable<KeyValuePair<string, string>> ToList()
        {
            return List;
        }
    }
}