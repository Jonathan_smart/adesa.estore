using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Sfs.EStore.Web.App.Code.Http
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class AuthoriseAttribute : AuthorizationFilterAttribute
    {
        private string[] _rolesSplit = new string[0];
        private string _roles;

        public string Roles
        {
            get { return _roles ?? string.Empty; }
            set
            {
                _roles = value;
                _rolesSplit = SplitString(value);
            }
        }

        protected virtual bool IsAuthorized(HttpActionContext actionContext)
        {
            if (actionContext == null)
                throw new ArgumentNullException("actionContext");

            var currentPrincipal = Thread.CurrentPrincipal;

            return currentPrincipal != null && currentPrincipal.Identity.IsAuthenticated;
        }

        private bool IsForbidden(HttpActionContext actionContext)
        {
            if (actionContext == null)
                throw new ArgumentNullException("actionContext");

            var currentPrincipal = Thread.CurrentPrincipal;
            return _rolesSplit.Length > 0 && !_rolesSplit.Any(currentPrincipal.IsInRole);
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (actionContext == null)
                throw new ArgumentNullException("actionContext");

            if (SkipAuthorization(actionContext) || (IsAuthorized(actionContext) && !IsForbidden(actionContext)))
                return;
            if (IsForbidden(actionContext))
                HandleForbiddenRequest(actionContext);
            else
                HandleUnauthorizedRequest(actionContext);
        }

        protected virtual bool SkipAuthorization(HttpActionContext actionContext)
        {
            var skipChecks = new Func<bool>[]
                                 {
                                     () =>
                                     actionContext.ActionDescriptor
                                         .GetCustomAttributes<AllowAnonymousAttribute>().Any(),
                                     () =>
                                     actionContext.ControllerContext.ControllerDescriptor
                                         .GetCustomAttributes<AllowAnonymousAttribute>().Any(),
                                     () =>
                                     actionContext.ActionDescriptor
                                         .GetCustomAttributes<AuthoriseActivityAttribute>().Any(),
                                     () =>
                                     actionContext.ControllerContext.ControllerDescriptor
                                         .GetCustomAttributes<AuthoriseActivityAttribute>().Any()
                                 };

            return skipChecks.Any(x => x());
        }

        protected virtual void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            if (actionContext == null) throw new ArgumentNullException("actionContext");
            actionContext.Response =
                actionContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized,
                                                          "Authorization has been denied for this request.");
        }

        protected virtual void HandleForbiddenRequest(HttpActionContext actionContext)
        {
            if (actionContext == null) throw new ArgumentNullException("actionContext");
            actionContext.Response =
                actionContext.Request.CreateErrorResponse(HttpStatusCode.Forbidden,
                                                          "Permission has been denied for this request.");
        }

        internal static string[] SplitString(string original)
        {
            if (string.IsNullOrEmpty(original))
                return new string[0];

            return original.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}