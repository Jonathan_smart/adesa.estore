using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Sfs.EStore.Web.App.Code.Http
{
    [AttributeUsage(AttributeTargets.Method, Inherited = true)]
    public class ModelRequiredFilterAttribute : ActionFilterAttribute
    {
        private readonly Func<Dictionary<string, object>, bool> _validate;

        public ModelRequiredFilterAttribute()
            : this(arguments => arguments.ContainsValue(null))
        {
        }

        public ModelRequiredFilterAttribute(Func<Dictionary<string, object>, bool> checkCondition)
        {
            _validate = checkCondition;
        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (_validate(actionContext.ActionArguments))
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(
                    HttpStatusCode.BadRequest, "The request is invalid. The argument cannot be null");
            }
        }
    }
}