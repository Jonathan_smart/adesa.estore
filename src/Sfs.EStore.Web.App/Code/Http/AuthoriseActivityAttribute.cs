using System;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using Sfs.EStore.Web.App.Code.Security;

namespace Sfs.EStore.Web.App.Code.Http
{
    public class AuthoriseActivityAttribute : AuthoriseAttribute
    {
        public AuthoriseActivityAttribute(string activity)
        {
            Activity = activity;
        }

        public string Activity { get; set; }

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            var user = Thread.CurrentPrincipal as IWebApplicationPrincipal;

            if (user == null) return false;

            var permissions = new HttpContextWrapper(HttpContext.Current).CurrentTenantPermissions();

            return permissions[Activity].IsAllowedFor(user);
        }

        protected override bool SkipAuthorization(HttpActionContext actionContext)
        {
            var skipChecks = new Func<bool>[]
                                 {
                                     () =>
                                     actionContext.ActionDescriptor
                                         .GetCustomAttributes<AllowAnonymousAttribute>().Any(),
                                     () =>
                                     actionContext.ControllerContext.ControllerDescriptor
                                         .GetCustomAttributes<AllowAnonymousAttribute>().Any()
                                 };

            return skipChecks.Any(x => x());
        }
    }
}