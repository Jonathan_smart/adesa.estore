﻿using System;
using System.Collections.Generic;

namespace Sfs.EStore.Web.App.Code
{
    public class SiteVisitor
    {
        public string Id { get; protected set; }

        public SiteVisit OriginalVisit { get; protected set; }
        public SiteVisit LastVisit { get; protected set; }

        private List<string> _knownAsUsers = new List<string>();
        public string[] KnownAsUsers
        {
            get { return _knownAsUsers.ToArray(); }
            protected set { _knownAsUsers = new List<string>(value); }
        }

        private DateTime _createdAt = SystemTime.UtcNow;
        public DateTime CreatedAt
        {
            get { return _createdAt; }
            protected set { _createdAt = value; }
        }

        public void IsKnownAsUser(string registeredUser)
        {
            if (!_knownAsUsers.Contains(registeredUser))
                _knownAsUsers.Add(registeredUser);
        }

        public void HasNewVisit(SiteVisit latestVisit)
        {
            if (OriginalVisit == null) OriginalVisit = latestVisit;

            LastVisit = latestVisit;
        }
    }

    public class SiteVisit
    {
        public SiteVisit(SiteVisitSource source, string requestUrl)
        {
            Source = source;
            RequestUrl = requestUrl;
        }

        public SiteVisitSource Source { get; protected set; }
        public string RequestUrl { get; protected set; }

        private DateTime _startedAt = SystemTime.UtcNow;
        public DateTime StartedAt
        {
            get { return _startedAt; }
            protected set { _startedAt = value; }
        }
    }

    public class SiteVisitSource
    {
        public SiteVisitSource(SiteVisitSourceType type, string value = "")
        {
            Type = type;
            Value = value;
        }

        public SiteVisitSourceType Type { get; protected set; }
        public string Value { get; protected set; }

        public enum SiteVisitSourceType
        {
            Direct,
            Referral,
            GuestPass
        }
    }
}