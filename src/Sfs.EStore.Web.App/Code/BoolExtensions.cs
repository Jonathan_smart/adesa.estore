namespace Sfs.EStore.Web.App.Code
{
    public static class BoolExtensions
    {
        public static string ToYesNo(this bool? source)
        {
            return source.HasValue && source.Value ? "Yes" : "No";
        }
    }
}