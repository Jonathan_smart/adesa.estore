//using System.Collections.Generic;
//using System.Linq;
//using System.ServiceModel;
//using System.ServiceModel.Channels;
//using Sfs.EStore.Web.App.GlobalMembershipService;
//using log4net;

//namespace Sfs.EStore.Web.App.Code
//{
//    public class GlobalMembershipUserValidator : IUserValidator
//    {
//        private readonly SiteTenant _tenant;
//        private const int ApplicationId = 31;// "EAuction_MVC"
//        private readonly ILog _logger = LogManager.GetLogger(typeof(GlobalMembershipUserValidator));

//        public GlobalMembershipUserValidator(SiteTenant tenant)
//        {
//            _tenant = tenant;
//        }

//        public IUserIdentity Validate(string username, string password)
//        {
//            string token;

//            if (!EnsureHasAccess(username, password, out token)) return null;

//            return GetUser(token, username);
//        }

//        public IUserIdentity GetUser(string token, string username)
//        {
//            using (var client = new GlobalMembershipServiceClient())
//            {
//                using (new OperationContextScope(client.InnerChannel))
//                {
//                    var authenticationBehaviourHeader = OperationContext.Current.OutgoingMessageHeaders;
//                    authenticationBehaviourHeader.Add(MessageHeader.CreateHeader("securitytoken", "", token));

//                    var membershipUser = client.GetMembershipUser(username);

//                    if (membershipUser == null)
//                    {
//                        _logger.DebugFormat("{0}: Cannot get membership user", username);
//                        return null;
//                    }

//                    var siteCompanyId = _tenant.Company.Id;

//                    var companyUserId = client.GetUsersCompanies(membershipUser.ID).ToList()
//                        .Where(x => x.CompanyID == siteCompanyId)
//                        .Select(x => x.Id).FirstOrDefault();

//                    var companyUserUserRoles = client.GetCompanyUserUserRoles(companyUserId);

//                    return new UserIdentity(username, companyUserUserRoles.Select(x => x.UserRole.Description));
//                }
//            }
//        }

//        private bool EnsureHasAccess(string username, string password, out string token)
//        {
//            using (var client = new GlobalMembershipServiceClient())
//            {
//                using (new OperationContextScope(client.InnerChannel))
//                {
//                    var authenticationBehaviourHeader = OperationContext.Current.OutgoingMessageHeaders;
//                    authenticationBehaviourHeader.Add(MessageHeader.CreateHeader("username", "", username));
//                    authenticationBehaviourHeader.Add(MessageHeader.CreateHeader("password", "", password));

//                    token = client.GetSecurityToken();

//                    _logger.DebugFormat("{0}: Security token is {1}", username, token);

//                    var hasAccess = client.CheckAppAccessForUser(username, password, new AppData
//                                                                                         {
//                                                                                             ApplicationId =
//                                                                                                 ApplicationId
//                                                                                         }, _tenant.Company.Name)
//                        .IsAuthenticated;

//                    if (!hasAccess)
//                    {
//                        _logger.DebugFormat("{0}: Does not have access", username);
//                        return false;
//                    }
//                }
//            }
//            return true;
//        }

//        public class UserIdentity : IUserIdentity
//        {
//            public UserIdentity(string userName, IEnumerable<string> roles)
//            {
//                UserName = userName;
//                Claims = (roles ?? new string[0]).ToArray();
//            }

//            public string UserName { get; set; }

//            public IEnumerable<string> Claims { get; set; }
//        }
//    }
//}