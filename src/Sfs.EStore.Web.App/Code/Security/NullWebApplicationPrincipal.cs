using System.Collections.Generic;
using System.Security.Principal;

namespace Sfs.EStore.Web.App.Code.Security
{
    public class NullWebApplicationPrincipal : IWebApplicationPrincipal
    {
        public NullWebApplicationPrincipal(string apiAccessKeyId = "")
        {
            ApiAccessKeyId = apiAccessKeyId ?? "";
            Identity = new NullWebApplicationIdentity();
            UserAuthorizationToken = "";
            Data = new Dictionary<string, string>();
        }

        public bool IsInRole(string role) { return false; }

        public IWebApplicationIdentity Identity { get; private set; }
        public IEnumerable<string> Roles()
        {
            return new string[0];
        }

        IIdentity IPrincipal.Identity { get { return Identity; } }

        public string ApiAccessKeyId { get; private set; }
        public string UserAuthorizationToken { get; private set; }

        public Dictionary<string, string> Data { get; private set; }
    }
}