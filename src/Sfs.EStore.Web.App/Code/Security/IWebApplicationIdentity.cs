using System.Collections.Generic;
using System.Security.Principal;

namespace Sfs.EStore.Web.App.Code.Security
{
    public interface IWebApplicationIdentity : IIdentity
    {
        bool IsGuest { get; }
        string GuestPassId { get; }

        string FirstName { get; }
        string LastName { get; }
        string PhoneNumber { get; }
        string EmailAddress { get; }

        Dictionary<string, string> Data { get; } 
    }
}