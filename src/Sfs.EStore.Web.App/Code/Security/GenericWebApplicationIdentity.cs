using System;
using System.Collections.Generic;

namespace Sfs.EStore.Web.App.Code.Security
{
    public class GenericWebApplicationIdentity : IWebApplicationIdentity
    {
        public GenericWebApplicationIdentity(string name, Dictionary<string, string> data, string guestPassId = "") 
        {
            Name = name ?? "";
            if (guestPassId == null) throw new ArgumentNullException("guestPassId");

            Data = data;
            GuestPassId = guestPassId;
        }

        public string Name { get; private set; }
        public string AuthenticationType { get { return null; } }
        public bool IsAuthenticated { get { return Name != ""; } }

        public bool IsGuest { get { return !IsAuthenticated && !string.IsNullOrEmpty(GuestPassId); } }

        public string GuestPassId { get; private set; }

        private string _firstName;
        public string FirstName
        {
            get { return _firstName ?? ""; }
            set { _firstName = value; }
        }

        private string _lastName;
        public string LastName
        {
            get { return _lastName ?? ""; }
            set { _lastName = value; }
        }

        private string _phoneNumber;
        public string PhoneNumber
        {
            get { return _phoneNumber ?? ""; }
            set { _phoneNumber = value; }
        }

        private string _emailAddress;
        public string EmailAddress
        {
            get { return _emailAddress ?? ""; }
            set { _emailAddress = value; }
        }

        public Dictionary<string, string> Data { get; private set; }
    }
}