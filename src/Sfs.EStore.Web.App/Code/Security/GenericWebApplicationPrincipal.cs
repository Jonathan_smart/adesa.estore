using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;

namespace Sfs.EStore.Web.App.Code.Security
{
    public class GenericWebApplicationPrincipal : IWebApplicationPrincipal
    {
        private readonly string[] _roles;

        public GenericWebApplicationPrincipal(string apiAccessKeyId, IWebApplicationIdentity identity, string[] roles = null, string userToken = "")
        {
            _roles = roles;
            if (identity == null) throw new ArgumentNullException("identity");

            Identity = identity;
            ApiAccessKeyId = apiAccessKeyId;
            UserAuthorizationToken = userToken;
        }

        public string ApiAccessKeyId { get; private set; }
        public string UserAuthorizationToken { get; private set; }

        public bool IsInRole(string role)
        {
            if (role == null || _roles == null) return false;

            return _roles.Any(x => String.Compare(role, x, StringComparison.OrdinalIgnoreCase) == 0);
        }

        IIdentity IPrincipal.Identity { get { return Identity; } }
        public IEnumerable<string> Roles()
        {
            return (_roles ?? new string[0]).ToArray();
        }

        public IWebApplicationIdentity Identity { get; private set; }
    }
}