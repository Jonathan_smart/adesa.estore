using System;
using Newtonsoft.Json;

namespace Sfs.EStore.Web.App.Code.Security
{
    public class GuestPassTicket : IGuestPassTicket
    {
        private readonly string _source;
        private readonly TimeSpan _timeToLive;
        private readonly string _campaign;

        [JsonConstructor]
        public GuestPassTicket(string createdBy, string source, TimeSpan timeToLive, string campaign, string redirectTo = "")
        {
            CreatedBy = createdBy;
            RedirectTo = redirectTo ?? "";
            _source = source;
            _timeToLive = timeToLive;
            _campaign = campaign ?? "";
        }

        public string CreatedBy { get; protected set; }

        private DateTime _createdAt = SystemTime.UtcNow;
        public DateTime CreatedAt
        {
            get { return _createdAt; }
            protected set { _createdAt = value; }
        }

        public string Id { get; protected set; }
        public string Source { get { return _source; } }
        public TimeSpan TimeToLive { get { return _timeToLive; } }
        public string Campaign { get { return _campaign; } }
        public string RedirectTo { get; protected set; }

        public bool Expired { get; protected set; }
        public DateTime? ExpiredAt { get; protected set; }
        public string ExpiredBy { get; protected set; }

        public void Expire(string expiredBy)
        {
            Expired = true;
            ExpiredAt = SystemTime.UtcNow;
            ExpiredBy = expiredBy ?? "";
        }
    }

    public interface IGuestPassTicket
    {
        string Id { get; }
    }

    public interface IGuestPassTicketDecrypted : IGuestPassTicket
    {
        string Source { get; }
        string Campaign { get; }
        TimeSpan TimeToLive { get; }
    }
}