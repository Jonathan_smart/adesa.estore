using System.Collections.Generic;

namespace Sfs.EStore.Web.App.Code.Security
{
    public class NullWebApplicationIdentity : IWebApplicationIdentity
    {
        public NullWebApplicationIdentity()
        {
            Data = new Dictionary<string, string>();
        }

        public string Name { get { return ""; } }

        public string AuthenticationType { get { return ""; } }

        public bool IsAuthenticated { get { return false; } }

        public bool IsGuest { get { return false; } }

        public string GuestPassId { get { return ""; } }

        public string FirstName { get { return ""; } }
        public string LastName { get { return ""; } }
        public string PhoneNumber { get { return ""; } }
        public string EmailAddress { get { return ""; } }

        public Dictionary<string, string> Data { get; private set; }
    }
}