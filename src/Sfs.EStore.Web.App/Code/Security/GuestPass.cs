using System;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Sfs.EStore.Web.App.Code.Security
{
    public static class GuestPass
    {
        static GuestPass()
        {
            CryptoServiceProvider = new TripleDESCryptoServiceProvider();
        }

        public static byte[] Key
        {
            get { return CryptoServiceProvider.Key; }
            set { CryptoServiceProvider.Key = value; }
        }

        public static byte[] IV
        {
            get { return CryptoServiceProvider.IV; }
            set { CryptoServiceProvider.IV = value; }
        }

        public static string CookieName { get { return "AffiliateAuth"; } }

        private static readonly TripleDESCryptoServiceProvider CryptoServiceProvider;

        public static IGuestPassTicketDecrypted Decrypt(string token)
        {
            const string exceptionMessage = "encryptedTicket is null.- or -encryptedTicket is an empty string (\"\").- or -encryptedTicket is of an invalid format.";

            if (token == null) throw new ArgumentNullException("token");

            try
            {
                var buffer = Convert.FromBase64String(token);
                var decryptedBytes = CryptoServiceProvider.CreateDecryptor().TransformFinalBlock(buffer, 0, buffer.Length);
                var decrypted = Encoding.ASCII.GetString(decryptedBytes);

                return JObject.Parse(decrypted).ToObject<DecryptedTicket>();
            }
            catch (CryptographicException ex)
            {
                throw new ArgumentException(exceptionMessage, ex);
            }
            catch (FormatException ex)
            {
                throw new ArgumentException(exceptionMessage, ex);
            }
            catch (ArgumentException ex)
            {
                throw new ArgumentException(exceptionMessage, ex);
            }
            catch (JsonReaderException ex)
            {
                throw new ArgumentException(exceptionMessage, ex);
            }
        }

        public static string Encrypt(IGuestPassTicket ticket)
        {
            if (ticket == null) throw new ArgumentNullException("ticket");

            var source = JObject.FromObject(TicketToEncrypt.From(ticket)).ToString(Formatting.None);
            var buffer = Encoding.ASCII.GetBytes(source);
            var encrypedBytes = CryptoServiceProvider.CreateEncryptor().TransformFinalBlock(buffer, 0, buffer.Length);
            return Convert.ToBase64String(encrypedBytes);
        }

        private class TicketToEncrypt : IGuestPassTicket
        {
            public string Id { get; private set; }

            public static TicketToEncrypt From(IGuestPassTicket source)
            {
                return new TicketToEncrypt {Id = source.Id};
            }
        }

        private class DecryptedTicket : IGuestPassTicketDecrypted
        {
            public string Id { get; set; }

            // Below properties are preserved for old style (un-presisted) guest passes. 
            // They can be removed from 2014-01-01
            public string Source { get; set; }
            public string Campaign { get; set; }
            public TimeSpan TimeToLive { get; set; }
        }
    }
}