using System.Collections.Generic;
using System.Security.Principal;

namespace Sfs.EStore.Web.App.Code.Security
{
    public interface IWebApplicationPrincipal : IPrincipal
    {
        string ApiAccessKeyId { get; }
        string UserAuthorizationToken { get; }

        new IWebApplicationIdentity Identity { get; }
        IEnumerable<string> Roles();
    }
}