using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Code.Widgets.Liquid.Drops;
using Slugify;

namespace Sfs.EStore.Web.App.Code.Liquid
{
    public static class LiquidFormatFilters
    {
        public static string CfImage(string imageName, string containerName, string size = "", string overlay = "")
        {
            var model = new ImageModel
                        {
                            Container = containerName,
                            Name = imageName
                        };

            return UrlImageExtensions.ImageUrl(model, size, overlay);
            // return string.Format("i={0}::c={1}::s={2}::o={3}", imageName, containerName, size, overlay);
        }

        public static string FormatAsMoney(object money, int dp = 0)
        {
            if (money == null) return null;

            var model = money as MoneyDrop;
            if (model != null)
                return MoneyHelper.Stringify(new MoneyViewModel(model.Amount, model.Currency), dp);

            return money.ToString();
        }

        public static string FormatAsNumber(object value, int decimalPlaces = 0)
        {
            if (value == null) return null;

            decimal number;
            return decimal.TryParse(value.ToString(), out number)
                ? number.ToString(string.Format("n{0}", decimalPlaces))
                : null;
        }

        public static MoneyDrop ListingSavingOverRetail(ListingSummaryDrop drop)
        {
            if (drop == null || drop.DiscountPriceInfo == null || drop.DiscountPriceInfo.RetailPrice == null)
                return null;

            return ListingSavingOver(drop, drop.DiscountPriceInfo.RetailPrice);
        }

        public static MoneyDrop ListingSavingOver(ListingSummaryDrop drop, MoneyDrop priceToCompare)
        {
            if (drop == null || priceToCompare == null)
                return null;

            var currentPrice = drop.SellingStatus.CurrentPrice.Amount;

            var saving = priceToCompare.Amount - currentPrice;

            if (saving <= 0) return null;

            return new MoneyDrop(new MoneyViewModel(saving, drop.SellingStatus.CurrentPrice.Currency));
        }

        public static string Slugify(string value)
        {
            return new SlugHelper().GenerateSlug(value);
        }
    }
}