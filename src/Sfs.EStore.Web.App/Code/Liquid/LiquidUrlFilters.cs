﻿using System;
using System.Web;
using System.Web.Mvc;
using Sfs.EStore.Web.App.Areas.Vehicles;
using Sfs.EStore.Web.App.Code.Widgets.Liquid.Drops;
using Slugify;

namespace Sfs.EStore.Web.App.Code.Liquid
{
    public static class LiquidUrlFilters
    {
        [Obsolete("Use UserContentUrl (user_content_url) instead")]
        public static string Usercontenturl(string input)
        {
            return UserContentUrl(input);
        }

        [Obsolete("Use SiteUrl (site_url) instead")]
        public static string Siteurl(string input)
        {
            return SiteUrl(input);
        }

        [Obsolete("Use CmsPageUrl (cms_page_url) instead")]
        public static string Cmspageurl(string input)
        {
            return CmsPageUrl(input);
        }

        public static string UserContentUrl(string input)
        {
            var tenant = new HttpContextWrapper(HttpContext.Current).CurrentTenant();
            return UrlHelper().Content(string.Format("~/content/usercontent/{0}/{1}", tenant.Key, input));
        }

        public static string ThemeUrl(string input)
        {
            var tenant = new HttpContextWrapper(HttpContext.Current).CurrentTenant();
            return UrlHelper().Content(string.Format("~/content/themes/{0}/{1}", tenant.Key, input));
        }

        public static string SiteUrl(string input)
        {
            return UrlHelper().Content(input);
        }

        public static string CmsPageUrl(string input)
        {
            return UrlHelper().Content(string.Format("~/{0}", input));
        }

        public static string ListingUrl(object listing)
        {
            var urlHelper = UrlHelper();

            if (listing == null) return urlHelper.Content("~/");

            var model = listing as ListingSummaryDrop;
            if (model != null)
                return urlHelper.Lot(model.ListingId, new SlugHelper().GenerateSlug(model.Title));

            var id = 0;
            if (int.TryParse(listing.ToString(), out id)) return urlHelper.Lot(id, "");

            return urlHelper.Content("~/");
        }

        public static string ImageUrl(object image, string size = null, string overlay = null)
        {
            var drop = image as ImageDrop;

            return drop != null
                ? drop.Model().AsUrl(size, overlay)
                : UrlImageExtensions.MissingImage().AsUrl(size);
        }

        private static UrlHelper UrlHelper()
        {
            return new UrlHelper(HttpContext.Current.Request.RequestContext);
        }
    }
}