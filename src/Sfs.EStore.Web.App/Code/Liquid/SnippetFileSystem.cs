using System.Web;
using DotLiquid;
using DotLiquid.FileSystems;

namespace Sfs.EStore.Web.App.Code.Liquid
{
    public class SnippetFileSystem : IFileSystem
    {
        public string ReadTemplateFile(Context context, string templateName)
        {
            CmsSnippet snippet;
            SnippetRegistrar.GetOrCreate(new HttpContextWrapper(HttpContext.Current))
                .Get(templateName, out snippet);

            return snippet.Body;
        }
    }
}