using Newtonsoft.Json;

namespace Sfs.EStore.Web.App.Code
{
    public class CmsSnippet
    {
        [JsonConstructor]
        public CmsSnippet(string key)
        {
            Body = "";
            Key = key;
        }

        public string Id { get; protected set; }
        public string Body { get; protected set; }
        public string Key { get; protected set; }
        public string ExpectedType { get; set; }

        public void SetBody(string value)
        {
            Body = value ?? "";
        }

        public static string IdFromKey(string key)
        {
            const string prefix = "CmsSnippets/";
            if (key.StartsWith(prefix)) return key;

            return string.Format("{0}{1}", prefix, key);
        }
    }
}
