using System;
using System.Text;

namespace Sfs.EStore.Web.App.Code
{
    public static class StringExtensions
    {
        public static string ToBase64String(this string source)
        {
            return Convert.ToBase64String(Encoding.ASCII.GetBytes(source));
        }

        public static string ToCapitalizedWords(this string source)
        {
            var sb = new StringBuilder();
            var prevIsSeparator = true;
            foreach (var c in source)
            {
                if (char.IsSeparator(c))
                {
                    prevIsSeparator = true;
                    sb.Append(c);
                    continue;
                }

                if (char.IsLetter(c) && prevIsSeparator && char.IsLower(c))
                    sb.Append(c.ToString().ToUpper());
                else
                    sb.Append(c);

                prevIsSeparator = false;
            }
            return sb.ToString();
        }
    }
}