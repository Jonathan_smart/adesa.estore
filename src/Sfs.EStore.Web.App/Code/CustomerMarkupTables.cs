﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sfs.EStore.Web.App.Code
{
    public class CustomerMarkupTables
    {
        /// <exception cref="ArgumentOutOfRangeException">customerCode</exception>
        /// <exception cref="ArgumentNullException"><paramref name="customerCode"/> is <see langword="null" />.</exception>
        public CustomerMarkupTables(string customerCode, double minMarkup, Table[] tables)
        {
            if (customerCode == null) throw new ArgumentNullException("customerCode");
            if (customerCode == "")
                throw new ArgumentOutOfRangeException("customerCode", "customerCode must be specified");
            if (tables == null) throw new ArgumentNullException("tables");

            CustomerCode = customerCode;
            MinMarkUp = minMarkup;
            Tables = tables;
        }

        /// <exception cref="ArgumentOutOfRangeException">customerCode</exception>
        internal static string CreateIdFor(string customerCode)
        {
            if (string.IsNullOrWhiteSpace(customerCode))
                throw new ArgumentOutOfRangeException("customerCode", customerCode, "customerCode must be not be null or empty");

            return string.Format("Customers/{0}/MarkupTables", customerCode);
        }

        public string CustomerCode { get; private set; }
        public double MinMarkUp { get; set; }
        public Table[] Tables { get; private set; }

        public IEnumerable<Table> ActiveTables()
        {
            return Tables.Where(x => !x.Deleted);
        }

        public void AddTable(Table newTable)
        {
            Tables = Tables.Concat(new[] { newTable }).ToArray();
        }

        public void UpdateTable(Guid id, Table newTable)
        {
            foreach (Table t in Tables)
            {
                if (t.Id == id)
                {
                    for (int i = t.Lines.Count() - 1; i >= 0; i++)
                    {
                        if (i <= newTable.Lines.Length)
                        {
                            t.Lines[i] = newTable.Lines[i];
                        }
                        else
                        {
                            t.Lines[i] = null;
                        }
                    }
                }
            }
        }

        public void RemoveTable(Guid tableToDelete)
        {
            var table = Tables.ToList().FirstOrDefault(x => x.Id == tableToDelete);
            if (table == null) return;

            table.Delete();
        }

        public class Table
        {
            /// <exception cref="ArgumentNullException"><paramref name="lines"/> is <see langword="null" />.</exception>
            public Table(string name, string createdBy, TableLine[] lines)
            {
                if (lines == null) throw new ArgumentNullException("lines");

                Name = name;
                Lines = lines.ToArray();
                CreatedDate = SystemTime.UtcNow;
                CreatedBy = createdBy;
                Id = Guid.NewGuid();
            }


            public Guid Id { get; protected set; }
            public string Name { get; protected set; }
            public DateTime CreatedDate { get; protected set; }
            public string CreatedBy { get; protected set; }
            public TableLine[] Lines { get; protected set; }
            public int MinMarkup { get; protected set; }

            public bool Deleted { get; protected set; }
            public DateTime? DeletedOn { get; protected set; }

            public void Delete()
            {
                Deleted = true;
                DeletedOn = SystemTime.UtcNow;
            }
        }

        public class TableLine
        {
            public TableLine(double @from, double? to, double markup)
            {
                From = @from;
                To = to;
                Markup = markup;
            }

            public double From { get; private set; }
            public double? To { get; private set; }
            public double Markup { get; private set; }
        }
    }
}