namespace Sfs.EStore.Web.App.Code
{
    public class UsernameObfuscator
    {
        private readonly string _allowedUsername;
        private const string ObfuscatedUsername = "********";

        public UsernameObfuscator(string allowedUsername)
        {
            _allowedUsername = allowedUsername;
        }

        public string Obfuscate(string username)
        {
            return _allowedUsername.Equals(username)
                       ? username
                       : ObfuscatedUsername;
        }
    }
}