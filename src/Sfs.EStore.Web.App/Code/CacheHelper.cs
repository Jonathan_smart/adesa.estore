﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sfs.EStore.Web.App.Code
{
    public class CacheHelper
    {

        static readonly Dictionary<string, object> _cache = new Dictionary<string, object>();

        public static T Run<T>(Func<T> func, params object[] prms)
        {
            var key = GetKey(func, prms);

            if (!_cache.ContainsKey(key) || !(_cache[key] is T))
                _cache.Add(key, func());

            return (T)_cache[key];
        }

        private static string GetKey<T>(Func<T> func, params object[] prms)
        {
            var method = func.Method.DeclaringType + "." + func.Method.Name;
            var type = typeof(T).FullName;

            return string.Format("{0}_{1}_{2}", method, type, string.Join("_", prms));
        }

    }
}