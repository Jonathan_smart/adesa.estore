using System;
using System.Linq;
using Sfs.EStore.Web.App.Code.Cms;
using Sfs.EStore.Web.App.Code.Security;

namespace Sfs.EStore.Web.App.Code
{
    public class PrincipalUserRestrictedAccess
    {
        private readonly string _value;

        public PrincipalUserRestrictedAccess(string value)
        {
            _value = value;
        }

        public virtual bool IsAllowedFor(IWebApplicationPrincipal user)
        {
            if (user == null) return false;

            if (_value == PageAccess.Deny) return false;

            if (_value.StartsWith("roles:"))
            {
                var allowedRoles = _value.Replace("roles:", "")
                    .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                return allowedRoles.Any(user.IsInRole);
            }

            if (_value == PageAccess.Guest && !(user.Identity.IsAuthenticated || user.Identity.IsGuest))
                return false;

            if (_value == PageAccess.Authenticated && !user.Identity.IsAuthenticated)
                return false;

            return true;
        }
    }
}