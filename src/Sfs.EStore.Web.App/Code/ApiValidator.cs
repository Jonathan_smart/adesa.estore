﻿using System.Net.Http;
using System.Threading.Tasks;
using Sfs.EStore.Web.App.Code.Security;

namespace Sfs.EStore.Web.App.Code
{
    public class ApiValidator : IUserValidator
    {
        private readonly AuthorizedHttpClient _apiProxy;

        public ApiValidator(IWebApplicationPrincipal user)
        {
            _apiProxy = ApiProxyFactory.Create(user);
        }

        public UserIdentityValidation Validate(string username, string password)
        {
            return _apiProxy.PostAsJson("authenticate", new { username, password })
                .AcceptJson().SendAsync()
                .ContinueWith<UserIdentityValidation>(GetUserFronResult)
                .Result;
        }

        private UserIdentityValidation GetUserFronResult(Task<HttpResponseMessage> responseTask)
        {
            if (!responseTask.Result.IsSuccessStatusCode)
                return new UserIdentityValidation { IsAuthenticated = false, Reason = "AUTH_FAILED", UserIdentity = null };

            var res = responseTask.Result.Content.ReadAsAsync<AuthenticationResponse>().Result;

            if (!res.IsAuthenticated)
                return new UserIdentityValidation
                    { IsAuthenticated = res.IsAuthenticated, Reason = res.Message };

            var userIdentity = new UserIdentity(res.Username, res.Roles, res.Token)
                {
                    FirstName = res.FirstName,
                    LastName = res.LastName,
                    PhoneNumber = res.PhoneNumber,
                    EmailAddress = res.EmailAddress
                };

            return new UserIdentityValidation
                {
                    IsAuthenticated = res.IsAuthenticated,
                    Reason = res.Message,
                    UserIdentity = userIdentity,
                };
        }

        internal class AuthenticationResponse
        {
            public bool IsAuthenticated { get; set; }
            public string Username { get; set; }
            public string[] Roles { get; set; }
            public string Token { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string PhoneNumber { get; set; }
            public string EmailAddress { get; set; }
            public string Reason { get; set; }
            public string Message { get; set; }
        }
    }
}
