using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web;
using Raven.Client;
using Sfs.EStore.Web.App.Code;

namespace Sfs.EStore.Web.App
{
    public class CmsHttpModule : IHttpModule
    {
        private const string ItemsRavenDbTenantSessionKey = "CmsHttpModule/RavenDbTenantSession";

        public static IDocumentSession CurrentTenantSession()
        {
            var context = HttpContext.Current;
            return (IDocumentSession)context.Items[ItemsRavenDbTenantSessionKey];
        }

        private static Dictionary<string, string> _hosts;

        public void Init(HttpApplication context)
        {
            context.BeginRequest += (sender, args) => ApplicationBeginRequest(new HttpContextWrapper(context.Context));
            context.EndRequest += (sender, args) => ApplicationEndRequest(new HttpContextWrapper(context.Context));

            using (var session = WebApiApplication.WebAppDocumentStore.OpenSession())
            {
                _hosts = session.Load<Dictionary<string, string>>("Hosts");
                if (_hosts == null)
                    throw new InvalidOperationException("Missing Hosts file");
            }
        }

        private static void ApplicationBeginRequest(HttpContextBase context)
        {
            using (var session = WebApiApplication.WebAppDocumentStore.OpenSession())
            {
                var tenant = GetSiteTenantFor(context, session);

                var tenantSession = WebApiApplication.GetTenantDocumentStoreFor(tenant.DatabaseName).OpenSession();
                context.Items[ItemsRavenDbTenantSessionKey] = tenantSession;

                context.Items["Cms.Snippets.Registrar"] = new SnippetRegistrar(tenantSession);
            }
        }

        private static void ApplicationEndRequest(HttpContextBase context)
        {
            if (context.Error != null) return;
            
            using (var session = CurrentTenantSession())
            {
                if (session != null) session.SaveChanges();
            }
        }

        private static SiteTenant GetSiteTenantFor(HttpContextBase context, IDocumentSession session)
        {
            Debug.Assert(context.Request.Url != null, "context.Request.Url != null");

            var hostName = context.Request.Url.Host;
            string tentantId;

            if (!_hosts.TryGetValue(hostName, out tentantId))
                throw new InvalidOperationException(string.Format("No tenant setup for host '{0}'", hostName));

            var tenant = session.Include<SiteTenant>(t => t.Id + "/ActivityPermissions").Load<SiteTenant>(tentantId);

            if (tenant == null)
                throw new InvalidOperationException(string.Format("Tenant with id '{0}' as specified for host '{1}' doesn't exist.", tentantId, hostName));

            context.Items.Add("Tenant", tenant);
            context.Items.Add("TenantPermissions",
                              session.Load<SiteTenantActivityPermissions>(
                                  SiteTenantActivityPermissions.IdFromTenant(tenant))
                              ?? new SiteTenantActivityPermissions());
            context.Response.AppendHeader("X-Tenant", string.Format("{0}:{1}", tenant.Id, tenant.Key));

            return tenant;
        }

        public void Dispose()
        {
            
        }
    }
}
