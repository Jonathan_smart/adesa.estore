using System;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;
using Sfs.EStore.Web.App.App_Start;
using Sfs.EStore.Web.App.Areas.Retail;
using Sfs.EStore.Web.App.Areas.Vehicles.Models;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.Security;
using Sfs.EStore.Web.App.Code.Widgets;
using Sfs.Core;
using Sfs.EStore.Web.App.Code.Widgets.Liquid;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(NinjectWebCommon), "Stop")]

namespace Sfs.EStore.Web.App.App_Start
{
    internal class NinjectHttpRequestMessageHandler : DelegatingHandler
    {
        protected async override Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request, CancellationToken cancellationToken)
        {
            Debug.WriteLine("Process request");
            SimpleInjectorHttpRequestMessageProvider.CurrentMessage = request;
            var response = await base.SendAsync(request, cancellationToken);
            Debug.WriteLine("Process response");
            return response;
        }
    }

    internal static class SimpleInjectorHttpRequestMessageProvider
    {
        private static readonly string Key = Guid.NewGuid().ToString("N").Substring(0, 12);

        internal static HttpRequestMessage CurrentMessage
        {
            get
            {
                var wrapper = (HttpRequestMessageWrapper)CallContext.LogicalGetData(Key);

                return wrapper != null ? wrapper.Message : null;
            }

            set
            {
                var wrapper = value == null ? null : new HttpRequestMessageWrapper(value);

                CallContext.LogicalSetData(Key, wrapper);
            }
        }

        [Serializable]
        internal sealed class HttpRequestMessageWrapper : MarshalByRefObject
        {
            [NonSerializedAttribute]
            internal readonly HttpRequestMessage Message;

            internal HttpRequestMessageWrapper(HttpRequestMessage message)
            {
                this.Message = message;
            }
        }
    }

    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper Bootstrapper = new Bootstrapper();
        
        private static StandardKernel _kernel;

        public static IKernel Kernel { get { return _kernel; } }

        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof (OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof (NinjectHttpModule));
            Bootstrapper.Initialize(CreateKernel);

            var configuration = GlobalConfiguration.Configuration;
            if (!configuration.MessageHandlers.OfType<NinjectHttpRequestMessageHandler>().Any())
            {
                configuration.MessageHandlers.Add(new NinjectHttpRequestMessageHandler());
            }
        }

        public static void Stop()
        {
            Bootstrapper.ShutDown();
        }

        private static IKernel CreateKernel()
        {
            if (_kernel != null)
                throw new InvalidOperationException("CreateKernal can only be called once");

            _kernel = new StandardKernel();
            DomainEvent.Container = _kernel;
            _kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            
            RegisterServices(_kernel);

            GlobalConfiguration.Configuration.DependencyResolver = new NinjectResolver(_kernel);

            return _kernel;
        }

        internal static HttpContextWrapper GetHttpContext(HttpRequestMessage request = null)
        {
            if (request != null && request.Properties.ContainsKey("MS_HttpContext"))
                return ((HttpContextWrapper)request.Properties["MS_HttpContext"]);

            if (HttpContext.Current != null)
                return new HttpContextWrapper(HttpContext.Current);

            return null;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Load(Assembly.GetExecutingAssembly());

            kernel.Bind<SiteTenant>().ToMethod(ctx =>
            {
                var httpContext = GetHttpContext(SimpleInjectorHttpRequestMessageProvider.CurrentMessage);
                return httpContext.CurrentTenant();
            }).InRequestScope();

            kernel.Bind<SiteTenantActivityPermissions>().ToMethod(ctx =>
            {
                var httpContext = GetHttpContext(SimpleInjectorHttpRequestMessageProvider.CurrentMessage);
                return httpContext.CurrentTenantPermissions();
            }).InRequestScope();

            kernel.Bind<IWebApplicationPrincipal>()
                .ToMethod(ctx =>
                              {
                                  var context = HttpContext.Current;
                                  if (context == null) return null;

                                  return context.User as IWebApplicationPrincipal;
                              })
                .InRequestScope();
            BindUserValidator(kernel);
            BindWidgets(kernel);
            kernel.Bind<AuthorizedHttpClient>()
                .ToMethod(ctx => ApiProxyFactory.Create(kernel.Get<IWebApplicationPrincipal>()))
                .When(ctx => ctx.Target == null || ctx.Target.Name == "apiProxy")
                .InRequestScope();

            kernel.Bind<RetailModeDealerStore>()
                .ToMethod((ctx) => new RetailModeDealerStore(kernel.Get<IWebApplicationPrincipal>()))
                .InRequestScope();

            Kernel.Bind<SalesChannel>().ToMethod(ctx =>
            {
                var tenant = ctx.Kernel.Get<SiteTenant>();
                var value = tenant.IntegrationSettings["Sfs/SalesChannel"];
                return new SalesChannel(int.Parse(value));
            }).InRequestScope();

            ImageModel.CloudFilesImageProxyUrl = ConfigurationManager.AppSettings["CloudFiles/ImageProxyBaseUrl"];

            if (ConfigurationManager.AppSettings["GuestPass/CryptoServiceProvider/Key"] != null)
            {
                var key = ConfigurationManager.AppSettings["GuestPass/CryptoServiceProvider/Key"];
                GuestPass.Key = Convert.FromBase64String(key);
            }

            if (ConfigurationManager.AppSettings["GuestPass/CryptoServiceProvider/IV"] != null)
            {
                var iv = ConfigurationManager.AppSettings["GuestPass/CryptoServiceProvider/IV"];
                GuestPass.IV = Convert.FromBase64String(iv);
            }
        }

        private static void BindWidgets(IKernel kernel)
        {
            kernel.Bind<RecentlyViewedWidget>().ToSelf();
            kernel.Bind<ViewLotWidget>().ToSelf();
            kernel.Bind<QuickSearchWidget>().ToSelf();
            kernel.Bind<DealerWidget>().ToSelf();
        }

        private static void BindUserValidator(IKernel kernel)
        {
            kernel.Bind<IUserValidator>().To<ApiValidator>()
                .InRequestScope();
        }
    }
}
