using DotLiquid;
using Sfs.EStore.Web.App.Code.Liquid;
using Sfs.EStore.Web.App.Code.Widgets;
using Sfs.EStore.Web.App.Code.Widgets.Liquid;

namespace Sfs.EStore.Web.App.App_Start
{
    public static class LiquidConfig
    {
        public static void Bootstrap()
        {
            //used from within blocks to output the widget
            Template.RegisterTag<DisplayWidgetTag>("displaywidget");
            
            Template.RegisterTag<MostViewedBlock>("most_viewed");
            Template.RegisterTag<WidgetBlock<RecentlyViewedWidget>>("recentlyviewed");
            Template.RegisterTag<ViewLotBlock>("viewlot");
            Template.RegisterTag<QuickSearchWidget>("quicksearch");
            Template.RegisterTag<SingleMostViewedBlock>("singlemostviewed");
            Template.RegisterTag<LoginTag>("login");

            Template.RegisterTag<CurrentSalesBlock>("current_sales");
            Template.RegisterTag<ScheduledAuctionsBlock>("scheduled_auctions");

            Template.RegisterTag<DealerWidget>("dealer");

            Template.RegisterTag<FacetsBlock>("facets");
            Template.RegisterTag<PaginatedPageContextTag>("paginated_page_context");
            Template.RegisterTag<SearchResultsBlock>("search");
            Template.RegisterTag<PaginationTag>("pagination");
            Template.RegisterTag<IfTenantIntegratesWithBlock>("if_tenant_integrates_with");
            Template.RegisterTag<IfUserIsInRolesBlock>("if_in_roles");
            Template.RegisterTag<AvailableFinanceBlock>("available_finance");

            Template.RegisterTag<SnippetTag>("snippet");
            Template.RegisterTag<ListingCardTag>("listing_card");
            Template.RegisterTag<NgViewTag>("ng_view");

            Template.RegisterFilter(typeof (LiquidUrlFilters));
            Template.RegisterFilter(typeof(LiquidFormatFilters));

            Template.FileSystem = new SnippetFileSystem();
        }
    }
}