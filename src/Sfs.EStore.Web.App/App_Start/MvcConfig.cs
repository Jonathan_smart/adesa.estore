﻿using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using Sfs.EStore.Web.App.Areas.Retail;
using Sfs.EStore.Web.App.Code.Cms;
using Sfs.EStore.Web.App.Code.Mvc;
using System.Web;

namespace Sfs.EStore.Web.App.App_Start
{
    public static class MvcConfig
    {
        public static void Bootstrap()
        {
            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
        }

        private static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new AuthoriseAttribute());
            filters.Add(new HandleErrorAttribute());
            filters.Add(new InjectMarkupTableIntoRetailViewbagAttribute());
        }

        private static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "Templates",
                url: "templates/{*id}",
                defaults: new { controller = "templates", action = "index" }
            );
            routes.MapRoute(
                "Search:CmsPage",
                "search/{cmspostfix}",
                new { action = "viewpage", controller = "search" }
            );
            routes.MapRoute(
                "Search",
                "search",
                new { action = "index", controller = "search" },
                new[] { "Sfs.EStore.Web.App.Controllers" }
            );

            routes.MapRoute(
                name: "UsedCars:Manufacturer",
                url: "used-cars/{manufacturer}",
                defaults: new { controller = "ManufacturerCmsPages", action = "index", vehicleType = "CAR" },
                namespaces: new[] { "Sfs.EStore.Web.App.Controllers" }
            );

            routes.MapRoute(
                name: "UsedCars:Manufacturer:Model",
                url: "used-cars/{manufacturer}/{model}",
                defaults: new { controller = "ManufacturerModelCmsPages", action = "index", vehicleType = "CAR" },
                namespaces: new[] { "Sfs.EStore.Web.App.Controllers" }
            );

            routes.MapRoute(
                name: "UsedVans:Manufacturer",
                url: "used-vans/{manufacturer}",
                defaults: new { controller = "ManufacturerCmsPages", action = "index", vehicleType = "VAN" },
                namespaces: new[] { "Sfs.EStore.Web.App.Controllers" }
            );

            routes.MapRoute(
                name: "UsedVans:Manufacturer:Model",
                url: "used-vans/{manufacturer}/{model}",
                defaults: new { controller = "ManufacturerModelCmsPages", action = "index", vehicleType = "VAN" },
                namespaces: new[] { "Sfs.EStore.Web.App.Controllers" }
            );

            routes.MapRoute(
                name: "Sessions:Login",
                url: "login",
                defaults: new { controller = "sessions", action = "login" }
            );
            routes.MapRoute(
                name: "Sessions:Logout",
                url: "logout",
                defaults: new { controller = "sessions", action = "logout" }
            );
            routes.MapRoute(
                "PartExchange",
                "partexchange/{*local}",
                new { action = "index", controller = "partexchange", local = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Affiliate:GuestPass",
                url: "affiliate",
                defaults: new { controller = "affiliate", action = "index" }
            );

            routes.MapRoute(
                name: "CMS:Content",
                url: "{*permalink}",
                defaults: new { controller = "cmspages", action = "index" },
                constraints: new { permalink = new CmsContentPageUrlConstraint() }
            );
            routes.MapRoute(
                name: "CMS:Content:BackwardCompatibility",
                url: "{permalink}.p",
                defaults: new { controller = "cmspages", action = "index" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "cmspages", action = "index", id = UrlParameter.Optional }
            );
        }
    }

    public class CmsContentPageUrlConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            if (values[parameterName] == null) return false;

            var permalink = CmsPagePermanameHelper.Normalize(values[parameterName].ToString());

            using (var session = WebApiApplication.GetTenantDocumentStoreFor(httpContext.CurrentTenant().DatabaseName).OpenSession())
            {
                var found = session.Query<CmsPage>().Any(x => x.PermaName == permalink);
                return found;
            }
        }
    }
}