﻿using System.Web.Http;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.Http;

namespace Sfs.EStore.Web.App.App_Start
{
    public static class WebApiConfig
    {
        public static void Bootstrap(HttpConfiguration config)
        {
            //config.MessageHandlers.Add(new Controllers.Api.EncodingDelegateHandler());

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "LotSubRouteApi",
                routeTemplate: "api/lots/{lotnumber}/{controller}"
                );
            config.Routes.MapHttpRoute(
                name: "ListingsSubRouteApi",
                routeTemplate: "api/listings/{listingId}/{controller}"
                );
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
                );

            config.Filters.Add(new AuthoriseAttribute());
            config.Filters.Add(new ExceptionLoggingAttribute());
        }
    }
}
