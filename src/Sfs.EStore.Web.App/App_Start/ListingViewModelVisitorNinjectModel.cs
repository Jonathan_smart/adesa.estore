﻿using Ninject;
using Ninject.Modules;
using Ninject.Web.Common;
using Sfs.EStore.Web.App.Code;
using Sfs.EStore.Web.App.Code.ListingViewModelVisitors;
using Sfs.EStore.Web.App.Code.Security;

namespace Sfs.EStore.Web.App
{
    public class ListingViewModelVisitorNinjectModel : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IListingViewModelVisitor, IListingSummaryViewModelVisitor>()
                .ToMethod(ctx =>
                {
                    var lotPriceVisibility =
                        new LotPriceVisibilitySwitcherVisitor(Kernel.Get<SiteTenantActivityPermissions>(),
                            Kernel.Get<IWebApplicationPrincipal>());

                    return new CompositeListingModelVisitor(
                        new IListingSummaryViewModelVisitor[] {lotPriceVisibility},
                        new IListingViewModelVisitor[] {lotPriceVisibility});
                })
                .InRequestScope();
        }
    }
}