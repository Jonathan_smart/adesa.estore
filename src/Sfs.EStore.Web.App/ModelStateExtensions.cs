﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;

namespace Sfs.EStore.Web.App
{
    public static class ModelStateExtensions
    {
        public static object[] ToJsonErrors(this ModelStateDictionary @this)
        {
            return @this.Keys.SelectMany(
                key => @this[key].Errors.Select(x => (object)new
                {
                    field = key,
                    message = x.ErrorMessage
                }))
                .ToArray();
        }

        public static void AddModelErrorFromJsonResponse(this ModelStateDictionary @this, HttpResponseMessage jsonResponse)
        {
            jsonResponse.Content.ReadAsAsync<JObject>()
                .ContinueWith(t => @this.AddModelError("", t.Result.Value<JToken>("error").Value<string>("message")))
                .Wait();
        }

        public static void AddModelErrorFromJsonResponse(this ModelStateDictionary @this, JObject result)
        {
            @this.AddModelError("", result.Value<JToken>("error").Value<string>("message"));
        }

        public static bool ContainsKey<TModel>(this ModelStateDictionary @this, TModel model, Expression<Func<TModel, object>> expression)
        {
            return @this.ContainsKey(ExpressionHelper.GetExpressionText(expression));
        }

        public static bool IsValidField<TModel, TValue>(this ModelStateDictionary @this, TModel model, Expression<Func<TModel, TValue>> expression)
        {
            return @this.IsValidField(ExpressionHelper.GetExpressionText(expression));
        }

        public static string ControlGroupFieldValidationCssClass<TModel, TValue>(this HtmlHelper<TModel> @this, Expression<Func<TModel, TValue>> expression, ModelStateDictionary modelState)
        {
            return modelState.IsValidField(ExpressionHelper.GetExpressionText(expression)) ? "" : "error";
        }

        public static void AddModelError<TModel, TValue>(this ModelStateDictionary @this, TModel model, Expression<Func<TModel, TValue>> expression, string errorMessage)
        {
            @this.AddModelError(ExpressionHelper.GetExpressionText(expression), errorMessage);
        }

        public static void AddModelError<TModel, TValue>(this ModelStateDictionary @this, TModel model, Expression<Func<TModel, TValue>> expression, string format, params object[] args)
        {
            @this.AddModelError(model, expression, string.Format(format, args));
        }
    }

    public static class ControllerExtensions
    {
        public static void Flash(this Controller @this, MvcHtmlString message, string type = null)
        {
            @this.TempData["flash"] = message;
            @this.TempData["flashtype"] = type;
        }

        public static void Flash(this Controller @this, string message, params object[] args)
        {
            @this.Flash(MvcHtmlString.Create(string.Format(message, args)));
        }

        public static void Alert(this Controller @this, string message, params object[] args)
        {
            @this.Flash(MvcHtmlString.Create(string.Format(message, args)), "error");
        }

        public static ActionResult AjaxJsonSuccessResult(this Controller @this)
        {
            return @this.AjaxJsonResult(new object[0], JsonRequestBehavior.DenyGet);
        }

        public static ActionResult AjaxJsonErrorResult(this Controller @this, object[] errors)
        {
            return @this.AjaxJsonResult(errors, JsonRequestBehavior.DenyGet);
        }

        public static ActionResult AjaxJsonResult(this Controller @this, object[] errors)
        {
            return @this.AjaxJsonResult(errors, JsonRequestBehavior.DenyGet);
        }

        public static ActionResult AjaxJsonResult(this Controller @this, object[] errors, JsonRequestBehavior behavior)
        {
            return new JsonResult
            {
                Data = new
                {
                    success = !errors.Any(),
                    errors = errors.ToArray()
                },
                //ContentType = contentType,
                //ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior
            };
        }
    }
}