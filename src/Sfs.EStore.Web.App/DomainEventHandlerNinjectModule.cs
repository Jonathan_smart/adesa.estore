using Ninject.Modules;
using Ninject.Extensions.Conventions;
using Ninject.Web.Common;

namespace Sfs.EStore.Web.App
{
    public class DomainEventHandlerNinjectModule: NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind(x =>
                        x.FromThisAssembly()
                            .SelectAllClasses()
                            .InheritedFrom(typeof (IHandles<>))
                            .BindSingleInterface()
                            .Configure(cfg => cfg.InRequestScope()));
        }
    }
}