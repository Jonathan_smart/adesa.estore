using System;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Sfs.EStore.Web.App.Code.Security;

namespace Sfs.EStore.Web.App
{
    public interface IAuthorizedHttpRequestBuilder
    {
        IAuthorizedHttpRequestBuilder AcceptJson();
        IAuthorizedHttpRequestBuilder Accept(Action<IAuthorizedHttpRequestAcceptBuilder> build);
        //IAuthorizedHttpRequestBuilder Configure(Action<HttpRequestMessage> config);
        Task<HttpResponseMessage> SendAsync();
    }

    public interface IAuthorizedHttpRequestAcceptBuilder
    {
        IAuthorizedHttpRequestAcceptBuilder Json();
        IAuthorizedHttpRequestAcceptBuilder ParseAdd(string input);
        IAuthorizedHttpRequestAcceptBuilder Add(MediaTypeWithQualityHeaderValue item);
    }

    public class AuthorizedHttpClient
    {
        private readonly HttpClient _client;
        private readonly AuthenticationHeaderValue _authenticationHeader;

        public AuthorizedHttpClient(HttpClient client, IWebApplicationPrincipal user)
        {
            _client = client;

            var authentication = string.Format("{0}:{1}",
                                               user.ApiAccessKeyId ?? "",
                                               user.UserAuthorizationToken);
            var encodedAuthentication = Convert.ToBase64String(Encoding.ASCII.GetBytes(authentication));
            _authenticationHeader = new AuthenticationHeaderValue("SFS", encodedAuthentication);
        }

        public class AuthorizedHttpRequestAcceptBuilder : IAuthorizedHttpRequestAcceptBuilder
        {
            private readonly HttpRequestMessage _requestMessage;

            public AuthorizedHttpRequestAcceptBuilder(HttpRequestMessage requestMessage)
            {
                _requestMessage = requestMessage;
            }

            public IAuthorizedHttpRequestAcceptBuilder Json()
            {
                _requestMessage.Headers.Accept.ParseAdd("application/json");
                return this;
            }

            public IAuthorizedHttpRequestAcceptBuilder ParseAdd(string input)
            {
                _requestMessage.Headers.Accept.ParseAdd(input);
                return this;
            }

            public IAuthorizedHttpRequestAcceptBuilder Add(MediaTypeWithQualityHeaderValue item)
            {
                _requestMessage.Headers.Accept.Add(item);
                return this;
            }
        }

        public class AuthorizedHttpRequestBuilder : IAuthorizedHttpRequestBuilder
        {
            private readonly HttpClient _client;
            private readonly HttpRequestMessage _requestMessage;

            public AuthorizedHttpRequestBuilder(HttpClient client, HttpRequestMessage requestMessage)
            {
                _client = client;
                _requestMessage = requestMessage;
            }

            public IAuthorizedHttpRequestBuilder Accept(Action<IAuthorizedHttpRequestAcceptBuilder> build)
            {
                build(new AuthorizedHttpRequestAcceptBuilder(_requestMessage));
                return this;
            }

            public IAuthorizedHttpRequestBuilder AcceptJson()
            {
                return Accept(x => x.Json());
            }

            //public IAuthorizedHttpRequestBuilder Configure(Action<HttpRequestMessage> config)
            //{
            //    config(_requestMessage);
            //    return this;
            //}

            public Task<HttpResponseMessage> SendAsync()
            {
                return _client.SendAsync(_requestMessage);
            }
        }

        public IAuthorizedHttpRequestBuilder Get(string requestUri)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, requestUri);
            request.Headers.Authorization = _authenticationHeader;
            return new AuthorizedHttpRequestBuilder(_client, request);
        }

        public IAuthorizedHttpRequestBuilder Put(string requestUri, HttpContent content)
        {
            return BuildRequest(HttpMethod.Put, requestUri, content);
        }

        public IAuthorizedHttpRequestBuilder PutAsJson<T>(string requestUri, T value)
        {
            var content = new ObjectContent<T>(value, new JsonMediaTypeFormatter(), (MediaTypeHeaderValue)null);
            return BuildRequest(HttpMethod.Put, requestUri, content);
        }

        public IAuthorizedHttpRequestBuilder Post(string requestUri, HttpContent content)
        {
            return BuildRequest(HttpMethod.Post, requestUri, content);
        }

        public IAuthorizedHttpRequestBuilder PostAsJson<T>(string requestUri, T value)
        {
            var content = new ObjectContent<T>(value, new JsonMediaTypeFormatter(), (MediaTypeHeaderValue)null);
            return BuildRequest(HttpMethod.Post, requestUri, content);
        }

        public IAuthorizedHttpRequestBuilder Delete(string requestUri)
        {
            var request = new HttpRequestMessage(HttpMethod.Delete, requestUri);
            request.Headers.Authorization = _authenticationHeader;

            return new AuthorizedHttpRequestBuilder(_client, request);
        }

        private IAuthorizedHttpRequestBuilder BuildRequest(HttpMethod method, string requestUri, HttpContent content)
        {
            var request = new HttpRequestMessage(method, requestUri) { Content = content };
            request.Headers.Authorization = _authenticationHeader;

            return new AuthorizedHttpRequestBuilder(_client, request);
        }
    }
}