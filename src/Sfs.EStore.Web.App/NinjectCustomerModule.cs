using Ninject;
using Ninject.Modules;
using Sfs.EStore.Web.App.Code;

namespace Sfs.EStore.Web.App
{
    public class NinjectCustomerModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<ShipToAddressQueryFactory>().ToMethod(ctx =>
            {
                var tenant = ctx.Kernel.Get<SiteTenant>();

                return new ShipToAddressQueryFactory(tenant.IntegrationSettings["Sfs/Navision/InstanceName"]);
            });
        }
    }
}