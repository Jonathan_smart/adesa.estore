using System;
using System.Collections.Concurrent;
using Raven.Abstractions.Util;
using Raven.Client;
using Raven.Client.Document;
using Sfs.EStore.Web.App.Areas.Retail.Controllers;
using Sfs.EStore.Web.App.Code;

namespace Sfs.EStore.Web.App
{
    public class LazyTenantDocumentStoreFactory
    {
        private readonly ConcurrentDictionary<string, Lazy<IDocumentStore>> _stores;
        private readonly string _ravenUrl;

        public LazyTenantDocumentStoreFactory(ConcurrentDictionary<string, Lazy<IDocumentStore>> stores,
                                              string ravenUrl)
        {
            _stores = stores;
            _ravenUrl = ravenUrl;
        }

        public IDocumentStore GetTenantDocumentStoreFor(string databaseName)
        {
            Action<DocumentConvention> customizeConventions =
                conventions => conventions
                                   .RegisterIdConvention<CmsSnippet>((dbname, commands, snippet) => CmsSnippet.IdFromKey(snippet.Key))
                                   .RegisterAsyncIdConvention<CmsSnippet>(
                                       (dbname, commands, snippet) => new CompletedTask<string>(CmsSnippet.IdFromKey(snippet.Key)))
                                   .RegisterIdConvention<CustomerMarkupTables>((dbname, commands, entity) => CustomerMarkupTables.CreateIdFor(entity.CustomerCode))
                                   .RegisterAsyncIdConvention<CustomerMarkupTables>(
                                       (dbname, commands, entity) => new CompletedTask<string>(CmsSnippet.IdFromKey(entity.CustomerCode)));

            return _stores.GetOrAdd(databaseName, dbName => CreateDocumentStore(dbName, customizeConventions)).Value;
        }

        private Lazy<IDocumentStore> CreateDocumentStore(string databaseName,
                                                         Action<DocumentConvention> customizeConventions = null)
        {
            return new Lazy<IDocumentStore>(
                () =>
                    {
                        var docStore = new DocumentStore
                                           {
                                               Url = _ravenUrl,
                                               DefaultDatabase = databaseName
                                           };

                        if (customizeConventions != null)
                            customizeConventions(docStore.Conventions);

                        docStore.Initialize();

                        return docStore;
                    });
        }
    }
}