using System;
using System.Data.Entity;
using Sfs.Customers.Adapters.DataAccess;

namespace Sfs.EStore.Web.App
{
    public class ShipToAddressQueryFactory
    {
        private readonly string _navisionInstanceName;

        public ShipToAddressQueryFactory(string navisionInstanceName)
        {
            _navisionInstanceName = navisionInstanceName;
        }

        public IShipToAddressQuery Create(string customerNo, DbContext context)
        {
            if ("SFS".Equals(_navisionInstanceName, StringComparison.InvariantCultureIgnoreCase))
                return new ShipToAddressQuery(customerNo, context);

            if ("GM".Equals(_navisionInstanceName, StringComparison.InvariantCultureIgnoreCase))
                return new Gm.Customers.Adapters.ShipToAddressQuery(customerNo, context);

            throw new InvalidOperationException(string.Format("'{0}' is not recognised as a valid navision instance name", _navisionInstanceName));
        }
    }
}