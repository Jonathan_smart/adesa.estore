using System;
using System.Configuration;
using System.Net.Http;
using Sfs.EStore.Web.App.Code.Security;

namespace Sfs.EStore.Web.App
{
    public static class ApiProxyFactory
    {
        static ApiProxyFactory()
        {
            var proxy = new HttpClient { BaseAddress = new Uri(ApiHttpClientBaseAddress) };

            Client = proxy;
        }

        private static readonly string ApiHttpClientBaseAddress = ConfigurationManager.AppSettings["Api/BaseAddress"];
        private static readonly HttpClient Client;

        public static AuthorizedHttpClient Create(IWebApplicationPrincipal user)
        {
            return new AuthorizedHttpClient(Client, user);
        }
    }
}