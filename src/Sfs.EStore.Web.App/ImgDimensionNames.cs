﻿namespace Sfs.EStore.Web.App
{
    public class ImgDimensionNames
    {
        private ImgDimensionNames() { }

        public const string Thumb = "thumb";
        public const string Featured = "featured";
        public const string Zoomed = "zoomed";
    }
}
