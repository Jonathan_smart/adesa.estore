﻿using Ninject;

namespace Sfs.EStore.Web.App
{
    public interface IDomainEvent
    {
    }
    public interface IHandles<in T> where T : IDomainEvent
    {
        void Handle(T args); 
    }

    public static class DomainEvent
    {
        public static IKernel Container { get; set; }

        public static void Raise<T>(T args) where T : IDomainEvent
        {
            if (Container != null)
                foreach (var handler in Container.GetAll<IHandles<T>>())
                    handler.Handle(args);
        }
    }
}