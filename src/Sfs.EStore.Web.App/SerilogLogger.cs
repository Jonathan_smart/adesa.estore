using System;
using Sfs.Darker.CommandProcessor;
using ILogger = Serilog.ILogger;

namespace Sfs.EStore.Web.App
{
    internal class SerilogLogger : ILog
    {
        private readonly ILogger _logger;

        public SerilogLogger(ILogger logger)
        {
            _logger = logger;
        }

        public void Write(LogEventLevel level, Exception exception, string messageTemplate, params object[] propertyValues)
        {
            _logger.Write(Convert(level), exception, messageTemplate, propertyValues);
        }

        private static Serilog.Events.LogEventLevel Convert(LogEventLevel level)
        {
            switch (level)
            {
                case LogEventLevel.Verbose:
                    return Serilog.Events.LogEventLevel.Verbose;
                case LogEventLevel.Debug:
                    return Serilog.Events.LogEventLevel.Debug;
                case LogEventLevel.Information:
                    return Serilog.Events.LogEventLevel.Information;
                case LogEventLevel.Warning:
                    return Serilog.Events.LogEventLevel.Warning;
                case LogEventLevel.Error:
                    return Serilog.Events.LogEventLevel.Error;
                case LogEventLevel.Fatal:
                    return Serilog.Events.LogEventLevel.Fatal;
            }

            return Serilog.Events.LogEventLevel.Information;
        }
    }
}