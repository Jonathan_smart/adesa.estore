﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Raven.Client;
using Sfs.EStore.Web.App.Code;

namespace Sfs.EStore.Web.App
{
    public static class HtmlHelperSnippets
    {
        public static IRenderSnippets Snippet(this HtmlHelper @this, string slug)
        {
            return new LiquidSnippetRenderer(SnippetRegistrar.GetOrCreate(@this.ViewContext.HttpContext),
                                       @this.ViewContext.Writer, @this, slug);
        }

        public static SnippetRegistrar Snippets(this HtmlHelper @this)
        {
            return SnippetRegistrar.GetOrCreate(@this.ViewContext.HttpContext);
        }
    }

    public class SnippetRegistrar
    {
        private readonly IDocumentSession _session;
        private Lazy<Dictionary<string, CmsSnippet>> _list;
        private readonly List<string> _ids = new List<string>(); 

        internal SnippetRegistrar(IDocumentSession session)
        {
            _session = session;
            ResetLazyList();
        }

        private void ResetLazyList()
        {
            if (_list != null && !_list.IsValueCreated) return;

            _list = new Lazy<Dictionary<string, CmsSnippet>>(
                () => _session.Load<CmsSnippet>(_ids)
                          .Where(x => x != null)
                          .ToList()
                          .ToDictionary(x => x.Id), false);
        }

        public static SnippetRegistrar GetOrCreate(HttpContextBase context)
        {
            return (SnippetRegistrar)context.Items["Cms.Snippets.Registrar"];
        }

        public SnippetRegistrar Register(params string[] keys)
        {
            var keysToAdd = keys
                .Select(CmsSnippet.IdFromKey)
                .Where(x => _ids.All(k => k != x))
                .ToArray();

            _ids.AddRange(keysToAdd);
            ResetLazyList();
            return this;
        }

        public bool Get(string key, out CmsSnippet snippet)
        {
            var id = CmsSnippet.IdFromKey(key);

            if (_list.Value.TryGetValue(id, out snippet))
                return true;

            snippet = _session.Load<CmsSnippet>(id);

            if (snippet == null)
            {
                snippet = new CmsSnippet(key);
                _session.Store(snippet);
                return false;
            }

            _list.Value.Add(snippet.Id, snippet);

            return true;
        }
    }

    public interface IRenderSnippets
    {
        IRenderSnippets HtmlExpected();

        IRenderSnippets JavascriptExpected();

        IRenderSnippets PlainTextExpected();

        IRenderSnippets Template(Func<object, object> value);

        IRenderSnippets Otherwise(Func<object, object> value);

        IRenderSnippets Otherwise(string value);

        IRenderSnippets Otherwise(IHtmlString value);

        void Render();

        IHtmlString HtmlString();
    }

    public class LiquidSnippetRenderer : IRenderSnippets
    {
        private readonly SnippetRegistrar _registrar;
        private readonly TextWriter _writer;
        private readonly HtmlHelper _htmlHelper;
        private readonly string _key;
        private string _expectedType = "htmlmixed";
        private Func<object, object> _template;
        private IHtmlString _ifNotSnippetValue;

        public LiquidSnippetRenderer(SnippetRegistrar registrar, TextWriter writer, HtmlHelper htmlHelper, string key)
        {
            _registrar = registrar;
            _writer = writer;
            _htmlHelper = htmlHelper;
            _key = key;
            _template = x => x;
        }

        public IRenderSnippets HtmlExpected()
        {
            _expectedType = "htmlmixed";
            return this;
        }

        public IRenderSnippets JavascriptExpected()
        {
            _expectedType = "javascript";
            return this;
        }

        public IRenderSnippets PlainTextExpected()
        {
            _expectedType = "plaintext";
            return this;
        }

        public IRenderSnippets Template(Func<object, object> value)
        {
            _template = value;
            return this;
        }

        public IRenderSnippets Otherwise(Func<object, object> value)
        {
            return Otherwise(new MvcHtmlString(value(null).ToString()));
        }

        public IRenderSnippets Otherwise(string value)
        {
            return Otherwise(new MvcHtmlString(value));
        }

        public IRenderSnippets Otherwise(IHtmlString value)
        {
            _ifNotSnippetValue = value;
            return this;
        }

        public void Render()
        {
            _writer.Write("<!-- [snippet:{0}] -->", _key);

            var snippet = GetSnippet();

            if (!string.IsNullOrEmpty(snippet.Body))
                RenderBody(snippet);

            _writer.Write("<!-- [/snippet:{0}] -->", _key);
        }

        public IHtmlString HtmlString()
        {
            var snippet = GetSnippet();

            return new HtmlString(_template(new HtmlString(snippet.Body)).ToString());
        }

        private CmsSnippet GetSnippet()
        {
            CmsSnippet snippet;
            if (!_registrar.Get(_key, out snippet) && _ifNotSnippetValue != null)
            {
                snippet.SetBody(_ifNotSnippetValue.ToString());
            }

            snippet.ExpectedType = _expectedType;

            return snippet;
        }

        private void RenderBody(CmsSnippet snippet)
        {
            new LiquidTemplateRenderer(_htmlHelper, _writer)
                .Render(_template(new HtmlString(snippet.Body)).ToString());
        }
    }
}