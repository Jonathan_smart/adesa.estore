﻿using System.Collections.Generic;

namespace Sfs.Customers.Adapters.DataAccess
{
    public interface IShipToAddressQuery
    {
        IEnumerable<Address> Query(QueryParams query);
    }

    public class QueryParams
    {
        public QueryParams(ShippedItemType shippedItem)
        {
            ShippedItem = shippedItem;
        }

        public ShippedItemType ShippedItem { get; private set; }

        public enum ShippedItemType
        {
            Vehicle,
            V5
        }
    }

    public class Address
    {
        public Address(string code, string name, string line1, string line2, string city, string county, string postCode)
        {
            Code = code;
            Name = name;
            Line1 = line1;
            Line2 = line2;
            City = city;
            County = county;
            PostCode = postCode;
        }

        public string Code { get; private set; }
        public string Name { get; private set; }
        public string Line1 { get; private set; }
        public string Line2 { get; private set; }
        public string City { get; private set; }
        public string County { get; private set; }
        public string PostCode { get; private set; }
    }
}