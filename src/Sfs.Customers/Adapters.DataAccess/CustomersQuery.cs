using System.Data.Entity;
using System.Linq;

namespace Sfs.Customers.Adapters.DataAccess
{
    public class CustomersQuery
    {
        private readonly DbContext _dbContext;

        public CustomersQuery(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Customer Query(string customerNo)
        {
            var customer = _dbContext.Set<Entities.Customer>().FirstOrDefault(x => x.No == customerNo);

            if (customer == null) return null;

            return new Customer(customer.No, customer.Name, customer.City);
        }

        public class Customer
        {
            public Customer(string no, string name, string city)
            {
                No = no;
                Name = name;
                City = city;
            }

            public string No { get; private set; }
            public string Name { get; private set; }
            public string City { get; private set; }
        }
    }
}