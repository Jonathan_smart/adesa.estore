using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Sfs.Customers.Adapters.DataAccess.Entities;

namespace Sfs.Customers.Adapters.DataAccess
{
    public class ShipToAddressQuery : IShipToAddressQuery
    {
        private readonly string _customerNo;
        private readonly DbContext _dbContext;

        public ShipToAddressQuery(string customerNo, DbContext dbContext)
        {
            _customerNo = customerNo;
            _dbContext = dbContext;
        }

        public IEnumerable<Address> Query(QueryParams args)
        {
            if (args == null) throw new ArgumentNullException("args");

            var query = _dbContext.Set<ShipToAddress>()
                .Where(x => x.CustomerNo == _customerNo)
                .Select(x => new {x.Code, x.Name, x.Address1, x.Address2, x.City, x.County, x.PostCode});
            
            var result = query
                .ToList()
                .Select(x => new Address(x.Code, x.Name, x.Address1, x.Address2, x.City, x.County, x.PostCode))
                .ToArray();

            return result;
        }
    }
}