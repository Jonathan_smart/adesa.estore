﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Sfs.Customers.Adapters.DataAccess.Entities;

namespace Sfs.Customers.Adapters.DataAccess
{
    public class ContactCustomersQuery
    {
        private readonly string _businessUnit;
        private readonly string _contactNo;
        private readonly DbContext _dbContext;

        public ContactCustomersQuery(string contactNo, string BusinessUnit, DbContext dbContext)
        {
            _businessUnit = BusinessUnit;
            _contactNo = contactNo;
            _dbContext = dbContext;
        }

        public IEnumerable<ContactCustomer> Query()
        {
            var query = _dbContext.Set<Contact>()
                    .Where(x => x.No == _contactNo)
                    .Join(_dbContext.Set<ContactRelationship>(), c => c.No, cr => cr.ContactNo, (c, cr) => cr)
                    .Where(x => x.SourceType == 1)
                    .Where(x => x.AuthorizedBy != "")
                    .Join(_dbContext.Set<Customer>(), cr => cr.SourceNo, c => c.No,
                        (cr, c) => new { IsDefault = cr.IsDefault == 1, Customer = c });

            var customers = query
                .OrderBy(x => x.Customer.Name).ThenBy(x => x.Customer.City)
                .Select(x => new { x.Customer.No, x.Customer.Name, x.Customer.City })
                .ToList();

            return customers.Select(x => new ContactCustomer(x.No, x.Name, x.City)).ToArray();
        }

        public class ContactCustomer
        {
            public ContactCustomer(string no, string name, string city)
            {
                No = no;
                Name = name;
                City = city;
            }

            public string No { get; private set; }
            public string Name { get; private set; }
            public string City { get; private set; }
        }
    }
}
