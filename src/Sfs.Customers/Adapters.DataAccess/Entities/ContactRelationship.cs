﻿using System.Data.Entity.ModelConfiguration;

namespace Sfs.Customers.Adapters.DataAccess.Entities
{
    public class ContactRelationship
    {
        public int EntryNo { get; set; }
        public string ContactNo { get; set; }
        public int SourceType { get; set; }
        public string SourceNo { get; set; }
        public byte IsDefault { get; set; }
        public string AuthorizedBy { get; set; }
    }

    internal class ContactRelationshipMapper : EntityTypeConfiguration<ContactRelationship>
    {
        public ContactRelationshipMapper()
        {
            ToTable("SFS$Contact Relationship");
            HasKey(e => e.EntryNo);
            Property(e => e.EntryNo).HasColumnName("Entry No_");
            Property(e => e.ContactNo).HasColumnName("Contact No_");
            Property(e => e.SourceType).HasColumnName("Source Type");
            Property(e => e.SourceNo).HasColumnName("Source No_");
            Property(e => e.IsDefault).HasColumnName("Default");
            Property(e => e.AuthorizedBy).HasColumnName("Authorized By");
        }
    }
}
