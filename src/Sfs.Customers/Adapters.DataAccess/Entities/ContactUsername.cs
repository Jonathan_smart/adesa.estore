﻿using System.Data.Entity.ModelConfiguration;

namespace Sfs.Customers.Adapters.DataAccess.Entities
{
    public class ContactUsername
    {
        public int EntryNo { get; set; }
        public string ContactNo { get; set; }
        public string ApplicationName { get; set; }
        public string ApplicationAccount { get; set; }
        public int SalesChannelNo { get; set; }
        public string Username { get; set; }
    }

    internal class ContactUsernameMapper : EntityTypeConfiguration<ContactUsername>
    {
        public ContactUsernameMapper()
        {
            ToTable("SFS$Contact Usernames");
            HasKey(e => e.EntryNo);
            Property(e => e.EntryNo).HasColumnName("Entry No_");
            Property(e => e.ContactNo).HasColumnName("Contact No_");
            Property(e => e.ApplicationName).HasColumnName("Application Name");
            Property(e => e.ApplicationAccount).HasColumnName("Application Account");
            Property(e => e.SalesChannelNo).HasColumnName("Sales Channel No_");
            Property(e => e.Username).HasColumnName("Username");
        }
    }
}
