﻿using System.Data.Entity.ModelConfiguration;

namespace Sfs.Customers.Adapters.DataAccess.Entities
{
    internal class Contact
    {
        public string No { get; set; }
        public string GlobalDimension1Code { get; set; }
    }

    internal class ContactMapper : EntityTypeConfiguration<Contact>
    {
        public ContactMapper()
        {
            ToTable("SFS$Contact");
            HasKey(e => e.No);
            Property(e => e.No).HasColumnName("No_");
            Property(e => e.GlobalDimension1Code).HasColumnName("Global Dimension 1 Code");
        }
    }
}
