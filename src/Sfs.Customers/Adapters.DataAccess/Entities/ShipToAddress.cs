﻿using System.Data.Entity.ModelConfiguration;

namespace Sfs.Customers.Adapters.DataAccess.Entities
{
    internal class ShipToAddress
    {
        public string CustomerNo { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string PostCode { get; set; }
        public byte IsV5Default { get; set; }
    }

    internal class ShipToAddressMapper : EntityTypeConfiguration<ShipToAddress>
    {
        public ShipToAddressMapper()
        {
            ToTable("SFS$Ship-to Address");
            HasKey(e => new { e.CustomerNo, e.Code });
            Property(e => e.CustomerNo).HasColumnName("Customer No_");
            Property(e => e.Address1).HasColumnName("Address");
            Property(e => e.Address2).HasColumnName("Address 2");
            Property(e => e.PostCode).HasColumnName("Post Code");
            Property(e => e.IsV5Default).HasColumnName("V5 Default Address");
        }
    }
}
