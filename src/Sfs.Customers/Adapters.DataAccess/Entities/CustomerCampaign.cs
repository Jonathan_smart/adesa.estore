﻿using System.Data.Entity.ModelConfiguration;

namespace Sfs.Customers.Adapters.DataAccess.Entities
{
    public class CustomerCampaign
    {
        public int Type { get; set; }
        public string CustomerNo { get; set; }
        public string FranchiseCode { get; set; }
    }

    internal class CustomerCampaignMapper : EntityTypeConfiguration<CustomerCampaign>
    {
        public CustomerCampaignMapper()
        {
            ToTable("SFS$Customer Campaign");
            HasKey(e => new { e.CustomerNo, e.FranchiseCode });
            Property(e => e.CustomerNo).HasColumnName("Customer No_");
            Property(e => e.FranchiseCode).HasColumnName("No_");
            Property(e => e.Type).HasColumnName("Type");
        }
    }
}
