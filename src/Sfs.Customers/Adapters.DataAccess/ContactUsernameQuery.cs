﻿using System.Data.Entity;
using System.Linq;
using Sfs.Core;
using Sfs.Customers.Adapters.DataAccess.Entities;

namespace Sfs.Customers.Adapters.DataAccess
{
    public class ContactUsernameQuery
    {
        private readonly DbContext _dbContext;
        private readonly SalesChannel _salesChannel;

        public ContactUsernameQuery(SalesChannel salesChannel, DbContext dbContext)
        {
            _dbContext = dbContext;
            _salesChannel = salesChannel;
        }

        public QueryResult Query(string username)
        {
            var contactUsername = _dbContext.Set<ContactUsername>()
                    .FirstOrDefault(x => x.Username == username &&
                                         x.SalesChannelNo == _salesChannel);

            if (contactUsername == null) return null;

            return new QueryResult(contactUsername.ContactNo, contactUsername.ApplicationAccount);
        }

        public class QueryResult
        {
            public QueryResult(string contactNo, string businessUnit)
            {
                ContactNo = contactNo;
                BusinessUnit = businessUnit;
            }

            public string ContactNo { get; private set; }
            public string BusinessUnit { get; private set; }
        }
    }
}
