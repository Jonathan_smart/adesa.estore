using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Sfs.Customers.Adapters.DataAccess;
using Sfs.Customers.Adapters.DataAccess.Entities;

namespace Sfs.Gm.Customers.Adapters
{
    public class ShipToAddressQuery : IShipToAddressQuery
    {
        private const string V5AddressCode = "V5";
        private readonly string _customerNo;
        private readonly DbContext _dbContext;

        public ShipToAddressQuery(string customerNo, DbContext dbContext)
        {
            _customerNo = customerNo;
            _dbContext = dbContext;
        }

        public IEnumerable<Address> Query(QueryParams args)
        {
            if (args == null) throw new ArgumentNullException("args");

            var query = _dbContext.Set<ShipToAddress>()
                .Where(x => x.CustomerNo == _customerNo)
                .Select(x => new {x.Code, x.Name, x.Address1, x.Address2, x.City, x.County, x.PostCode});

            if (args.ShippedItem == QueryParams.ShippedItemType.V5)
                query = query.Where(x => x.Code == V5AddressCode);
            else if (args.ShippedItem == QueryParams.ShippedItemType.Vehicle)
                query = query.Where(x => x.Code != V5AddressCode);
            
            var result = query
                .ToList()
                .Select(x => new Address(x.Code, x.Name, x.Address1, x.Address2, x.City, x.County, x.PostCode))
                .ToArray();

            return result;
        }
    }
}