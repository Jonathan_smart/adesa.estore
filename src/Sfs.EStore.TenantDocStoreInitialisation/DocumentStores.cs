using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Raven.Client;
using Raven.Client.Document;
using Raven.Client.Linq;
using Sfs.EStore.Web.App;
using Sfs.EStore.Web.App.Code;

namespace Sfs.EStore.AccountDocStoreInitialisation
{
    public class DocumentStores
    {
        private readonly string _ravenUrl;
        private readonly LazyTenantDocumentStoreFactory _accountStoresFactory;
        private readonly ConcurrentDictionary<string, Lazy<IDocumentStore>> _stores;

        public DocumentStores(string ravenUrl)
        {
            _ravenUrl = ravenUrl;
            _stores = new ConcurrentDictionary<string, Lazy<IDocumentStore>>();
            _accountStoresFactory = new LazyTenantDocumentStoreFactory(_stores,
                                                                        _ravenUrl);
        }

        public IDocumentStore WebApp()
        {
            const string databaseName = "eStore.webApp";

            var store = _stores.GetOrAdd(databaseName,
                                         dbName => new Lazy<IDocumentStore>(
                                                       () => new DocumentStore
                                                                 {
                                                                     Url = _ravenUrl,
                                                                     DefaultDatabase = databaseName
                                                                 }.Initialize()));
            return store.Value;
        }

        public IEnumerable<DocumentStore> Tenants()
        {
            var apiDocStore = WebApp();

            var tenantDocStoreNames = apiDocStore.OpenSession().Query<SiteTenant>()
                .Select(x => x.DatabaseName)
                .Distinct()
                .ToList();

            foreach (var docStoreName in tenantDocStoreNames)
            {
                if (!string.IsNullOrEmpty(docStoreName))
                {
                    var store = _accountStoresFactory.GetTenantDocumentStoreFor(docStoreName);
                    store.Identifier = docStoreName;
                    yield return (DocumentStore)store;
                }
            }
        }
    }
}