﻿using System;

namespace Sfs.EStore.AccountDocStoreInitialisation
{
    internal static class ConsolePrompts
    {
        public static bool ShouldRunAgainst(string databaseName)
        {
            Console.Write("Run against database '{0}' (y/N):", databaseName);
            var shouldInitialise = "y".Equals(Console.ReadLine(), StringComparison.InvariantCultureIgnoreCase);

            return shouldInitialise;
        }
    }
}