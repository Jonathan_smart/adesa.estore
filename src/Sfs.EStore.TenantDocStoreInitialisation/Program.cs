﻿using System;
using System.IO;
using System.Linq;
using Ninject;
using Sfs.EStore.AccountDocStoreInitialisation.Commands;
using log4net.Config;

namespace Sfs.EStore.AccountDocStoreInitialisation
{
    internal class Program
    {
        private static void Main()
        {
            XmlConfigurator.ConfigureAndWatch(new FileInfo(Path.Combine(
                AppDomain.CurrentDomain.BaseDirectory, "log4net.config")));

            var server = GetServer();
            var command = GetCommandHandler(server);

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("{0}: {1}", "Server".PadRight(15), server);
            Console.WriteLine("{0}: {1}", "Command".PadRight(15), command.DisplayName);
            Console.WriteLine();
            Console.Write("Press <enter> to continue");
            Console.ReadLine();

            command.Execute();

            Console.WriteLine("--- DONE ---");
            Console.ReadLine();
        }

        private static string GetServer()
        {
            const string defaultRavenUrl = "http://ravendb01_staging:8080/";

            Console.Write("Raven server ({0}):", defaultRavenUrl);
            var server = Console.ReadLine();
            if (string.IsNullOrEmpty(server)) server = defaultRavenUrl;

            return server;
        }

        private static IUiCommand GetCommandHandler(string server)
        {
            var kernel = new StandardKernel(new CommandsNinjectModule());
            kernel.Bind<string>().ToConstant(server)
                .When(req => req.Target.Name == "ravenUrl");

            var handlers = kernel.GetAll<IUiCommand>().OrderByDescending(x => x.ZIndex).ToList();

            Console.WriteLine();
            Console.WriteLine("Available commands...");
            Console.WriteLine();

            foreach (var command in handlers)
            {
                Console.WriteLine("{0}) {1}", handlers.IndexOf(command) + 1, command.DisplayName);
            }

            Console.WriteLine();
            Console.Write("Select a command (1): ");
            var selectedIndex = Console.ReadLine();
            if (string.IsNullOrEmpty(selectedIndex))
                selectedIndex = "1";

            int index;
            return !int.TryParse(selectedIndex, out index) || index < 0 || index > handlers.Count
                       ? GetCommandHandler(server)
                       : handlers[index - 1];
        }
    }
}
