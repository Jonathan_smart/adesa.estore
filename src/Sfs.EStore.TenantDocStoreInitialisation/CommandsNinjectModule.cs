﻿using Ninject.Modules;
using Ninject.Extensions.Conventions;
using Sfs.EStore.AccountDocStoreInitialisation.Commands;

namespace Sfs.EStore.AccountDocStoreInitialisation
{
    public class CommandsNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind(x => x.FromThisAssembly()
                                 .SelectAllClasses()
                                 .InheritedFrom(typeof (IUiCommand))
                                 .BindAllInterfaces());
        }
    }
}
