using System;
using System.Net.Http;
using Raven.Client;

namespace Sfs.EStore.AccountDocStoreInitialisation.Commands
{
    public abstract class NetDatabaseCommand : ICommand
    {
        protected NetDatabaseCommand(string ravenUrl)
        {
            RavenUrl = ravenUrl;
            DocumentStores = new Lazy<DocumentStores>(() => new DocumentStores(ravenUrl));
        }

        protected string RavenUrl { get; private set; }
        protected Lazy<DocumentStores> DocumentStores { get; private set; }

        public abstract void Execute();
    }

    public abstract class HttpDatabaseCommand : NetDatabaseCommand
    {
        protected HttpDatabaseCommand(IDocumentStore store)
            : base(store.Url)
        {
            var baseAddress = string.Format("{0}/databases/{1}/", store.Url, store.Identifier);
            HttpClient = new HttpClient { BaseAddress = new Uri(baseAddress) };
        }

        protected HttpClient HttpClient { get; private set; }
    }
}