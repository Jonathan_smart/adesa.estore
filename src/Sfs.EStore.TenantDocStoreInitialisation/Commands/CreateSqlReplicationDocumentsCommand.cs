﻿using System;
using System.Collections.Generic;

namespace Sfs.EStore.AccountDocStoreInitialisation.Commands
{
    public class CreateSqlReplicationDocumentsUiCommand : UiCommandWrapper<CreateSqlReplicationDocumentsCommand>
    {
        public CreateSqlReplicationDocumentsUiCommand(string ravenUrl)
            : base("Create/update SQL replication documents", () => new CreateSqlReplicationDocumentsCommand(ravenUrl))
        {
        }
    }

    public class CreateSqlReplicationDocumentsCommand : NetDatabaseCommand
    {
        public CreateSqlReplicationDocumentsCommand(string ravenUrl) : base(ravenUrl)
        {
        }

        public override void Execute()
        {
            foreach (var docStore in DocumentStores.Value.Tenants())
            {
                Console.WriteLine();

                if (!ConsolePrompts.ShouldRunAgainst(docStore.DefaultDatabase))
                {
                    Console.WriteLine("Skipped by user");
                    Console.WriteLine();
                    continue;
                }

                using (var session = docStore.OpenSession())
                {
                    Console.WriteLine("###################  UNCOMMENT THIS LINE WHEN HAVE MOVED TO Raven v3 ###########");
                    //session.Store(ManufacturerCmsPagesReplicationConfig());
                    //session.Store(ManufacturerModelCmsPagesReplicationConfig());

                    //session.SaveChanges();
                }
            }
        }

//        private static SqlReplicationConfig ManufacturerCmsPagesReplicationConfig()
//        {
//            return new SqlReplicationConfig
//            {
//                Id = "Raven/SqlReplication/Configuration/ManufacturerCmsPages",
//                Name = "ManufacturerCmsPages",
//                RavenEntityName = "ManufacturerCmsPages",
//                PredefinedConnectionStringSettingName = "Replication",
//                PerformTableQuatation = true,
//                SqlReplicationTables = new List<SqlReplicationTable>
//                {
//                    new SqlReplicationTable
//                    {
//                        TableName = "Tenant_ManufacturerCmsPages",
//                        DocumentKeyColumn = "Id"
//                    }
//                },
//                Script = @"
//var data = {
//	Id: documentId,
//	VehicleTypeQueryTerm: this.VehicleTypeQueryTerm,
//	ManufacturerQueryTerm: this.ManufacturerQueryTerm,
//	ManufacturerSlug: this.ManufacturerSlug
//};
//
//replicateToTenant_ManufacturerCmsPages(data);"
//            };
//        }

//        private static SqlReplicationConfig ManufacturerModelCmsPagesReplicationConfig()
//        {
//            return new SqlReplicationConfig
//            {
//                Id = "Raven/SqlReplication/Configuration/ManufacturerModelCmsPages",
//                Name = "ManufacturerModelCmsPages",
//                RavenEntityName = "ManufacturerModelCmsPages",
//                PredefinedConnectionStringSettingName = "Replication",
//                PerformTableQuatation = true,
//                SqlReplicationTables = new List<SqlReplicationTable>
//                {
//                    new SqlReplicationTable
//                    {
//                        TableName = "Tenant_ManufacturerModelCmsPages",
//                        DocumentKeyColumn = "Id"
//                    }
//                },
//                Script = @"
//var data = {
//	Id: documentId,
//	VehicleTypeQueryTerm: this.VehicleTypeQueryTerm,
//	ManufacturerQueryTerm: this.ManufacturerQueryTerm,
//	ModelQueryTerm: this.ModelQueryTerm,
//	ManufacturerSlug: this.ManufacturerSlug,
//	ModelSlug: this.ModelSlug
//};
//
//replicateToTenant_ManufacturerModelCmsPages(data);"
//            };
//        }
    }
}
