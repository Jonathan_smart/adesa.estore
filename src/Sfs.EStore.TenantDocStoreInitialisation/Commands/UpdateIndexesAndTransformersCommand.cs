using System.ComponentModel.Composition.Hosting;
using System.Reflection;
using Raven.Client;
using Raven.Client.Indexes;
using Sfs.EStore.Web.App.Code;
using log4net;

namespace Sfs.EStore.AccountDocStoreInitialisation.Commands
{
    public class UpdateIndexesAndTransformersUiCommand :
        UiCommandWrapper<UpdateIndexesAndTransformersCommand>
    {
        public UpdateIndexesAndTransformersUiCommand(string ravenUrl)
            : base("Update indexes and transformers",
                   () => new UpdateIndexesAndTransformersCommand(ravenUrl))
        {
            ZIndex = 100;
        }
    }

    public class UpdateIndexesAndTransformersCommand : NetDatabaseCommand
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(UpdateIndexesAndTransformersCommand));

        public UpdateIndexesAndTransformersCommand(string ravenUrl) : base(ravenUrl)
        {
        }

        public override void Execute()
        {
            foreach (var docStore in DocumentStores.Value.Tenants())
            {
                Logger.DebugFormat("Tenant: {0}", docStore.Identifier);

                var assemblyToScanForIndexingTasks = typeof (SiteTenant).Assembly;
                IndexCreation.CreateIndexes(assemblyToScanForIndexingTasks, docStore);
                CreateTransformers(docStore, assemblyToScanForIndexingTasks);
            }
        }

        private static void CreateTransformers(IDocumentStore docStore, Assembly assemblyToScanForIndexingTasks)
        {
            var exportProvider = (ExportProvider)new CompositionContainer(new AssemblyCatalog(assemblyToScanForIndexingTasks), new ExportProvider[0]);

            foreach (var transformerCreationTask in exportProvider.GetExportedValues<AbstractTransformerCreationTask>())
                transformerCreationTask.Execute(docStore);
        }
    }
}