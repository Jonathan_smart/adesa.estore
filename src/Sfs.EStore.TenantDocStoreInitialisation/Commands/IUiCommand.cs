using System;

namespace Sfs.EStore.AccountDocStoreInitialisation.Commands
{
    public interface IUiCommand : ICommand
    {
        string DisplayName { get; }
        int ZIndex { get; }
    }

    public abstract class UiCommandWrapper<T> : IUiCommand where T : ICommand
    {
        private readonly Func<T> _command;

        protected UiCommandWrapper(string name, Func<T> command)
        {
            _command = command;
            DisplayName = name;
        }

        public void Execute()
        {
            _command().Execute();
        }

        public string DisplayName { get; private set; }

        public int ZIndex { get; protected set; }
    }
}