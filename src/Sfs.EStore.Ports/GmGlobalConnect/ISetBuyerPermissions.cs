namespace Sfs.EStore.Ports.GmGlobalConnect
{
    public interface ISetBuyerPermissions
    {
        bool Allow(Buyer buyer);
        bool Revoke(Buyer buyer);
    }

    public class Buyer
    {
        public Buyer(string username, string dealerCode)
        {
            Username = username;
            DealerCode = dealerCode;
        }

        public string Username { get; private set; }
        public string DealerCode { get; private set; }
    }
}