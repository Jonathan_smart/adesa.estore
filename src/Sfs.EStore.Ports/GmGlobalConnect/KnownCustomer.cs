namespace Sfs.EStore.Ports.GmGlobalConnect
{
    public class KnownCustomer
    {
        public KnownCustomer(string code, string name)
        {
            Code = code;
            Name = name;
        }

        public string Code { get; private set; }
        public string Name { get; private set; }
    }
}