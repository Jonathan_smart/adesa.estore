namespace Sfs.EStore.Ports.GmGlobalConnect
{
    public interface IUserIdentityStore
    {
        AuthenticatedUser AuthenticateAs(User identity);
    }

    public class User
    {
        public User(string email, string firstName, string username, string[] roles)
        {
            Email = email;
            FirstName = firstName;
            Username = username;
            Roles = roles;
        }

        public string Email { get; private set; }
        public string FirstName { get; private set; }
        public string Username { get; private set; }
        public string[] Roles { get; private set; }
    }
}