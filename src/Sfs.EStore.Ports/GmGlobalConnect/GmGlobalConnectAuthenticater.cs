using System;
using Sfs.Darker.CommandProcessor;

namespace Sfs.EStore.Ports.GmGlobalConnect
{
    public class GmGlobalConnectAuthenticater : RequestHandler<Authenticate>
    {
        private const string BuyerRoleName = "APP USED VEHICLES SALES";
        private const string AdminRoleName = "APP USED VEHICLES SALES MANAGER";
        private readonly IRemoteAddressAuthenticator _remoteAddressAuthenticator;
        private readonly ICustomerStore _customerStore;
        private readonly ISetBuyerPermissions _buyerPermissions;
        private readonly IUserIdentityStore _identityStore;
        private readonly bool _disableIvUserCheck;

        public GmGlobalConnectAuthenticater(
            ICustomerStore customerStore,
            ISetBuyerPermissions buyerPermissions, IUserIdentityStore identityStore,
            IRemoteAddressAuthenticator remoteAddressAuthenticator,
            ILog logger, bool disableIvUserCheck = false)
            : base(logger)
        {
            _customerStore = customerStore;
            _buyerPermissions = buyerPermissions;
            _identityStore = identityStore;
            _remoteAddressAuthenticator = remoteAddressAuthenticator;
            _disableIvUserCheck = disableIvUserCheck;
        }

        public override Authenticate Handle(Authenticate cmd)
        { 
            var setupForPurchasing = false;

            if (cmd.DealerCode.Contains("FGA"))
            {
                Logger.Information("Attempting login for FGA Retail{@authenticateCmd}", cmd);
                cmd.Role = "FCA-RETAIL";

                return cmd;
            }
            else
            {

                Logger.Information("Attempting login for {@authenticateCmd}", cmd);

                if (!_remoteAddressAuthenticator.IsAuthorised(cmd.RemoteAddress))
                {
                    Logger.Information("Access denied to remote address {remoteAddress}", cmd.RemoteAddress);
                    cmd.Result = AuthenticateResult.Unauthenticated();
                    return cmd;
                }

                if (HasMissingOrMissmatchedData(cmd))
                {
                    Logger.Information("Access denied due to missing information");
                    cmd.Result = AuthenticateResult.Unauthenticated();
                    return cmd;
                }

                if (!IsValidRole(cmd.Role))
                {
                    Logger.Information("Access denied to role {role}", cmd.Role);
                    cmd.Result = AuthenticateResult.Unauthenticated();
                    return cmd;
                }

               

                if (BuyerRoleName.Equals(cmd.Role, StringComparison.InvariantCultureIgnoreCase) &&
                    _buyerPermissions.Allow(new Buyer(cmd.UserId, cmd.DealerCode)))
                    setupForPurchasing = true;
                else
                    _buyerPermissions.Revoke(new Buyer(cmd.UserId, cmd.DealerCode));
            }
            var customer = _customerStore.Get(cmd.DealerCode);
            if (customer == null)
            {
                Logger.Information("Access denied to dealer code {dealerCode}", cmd.DealerCode);
                cmd.Result = AuthenticateResult.Unauthenticated();
                return cmd;
            }

            var authenticatedUser =
                _identityStore.AuthenticateAs(new User(cmd.Email, firstName: cmd.UserId, username: cmd.UserId, roles: new[] { cmd.Role }));

            cmd.Result = AuthenticateResult.AuthenticatedAs(authenticatedUser, customer,cmd.DealerCode.Contains("FGA") || setupForPurchasing);

            return cmd;
        }

        private bool HasMissingOrMissmatchedData(Authenticate request)
        {
            return string.IsNullOrEmpty(request.UserId) ||
                   (!_disableIvUserCheck && (string.IsNullOrEmpty(request.IvUser) ||
                                             !string.Equals(request.UserId, request.IvUser,
                                                 StringComparison.InvariantCulture))) ||
                   string.IsNullOrEmpty(request.DealerCode) ||
                   string.IsNullOrEmpty(request.Email) ||
                   string.IsNullOrWhiteSpace(request.Role);
        }

        private static bool IsValidRole(string role)
        {
            return AdminRoleName.Equals(role, StringComparison.InvariantCultureIgnoreCase) ||
                   BuyerRoleName.Equals(role, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}