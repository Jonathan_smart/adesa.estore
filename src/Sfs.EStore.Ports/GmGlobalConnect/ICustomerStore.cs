namespace Sfs.EStore.Ports.GmGlobalConnect
{
    public interface ICustomerStore
    {
        KnownCustomer Get(string dealerCode);
    }
}