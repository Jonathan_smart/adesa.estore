namespace Sfs.EStore.Ports.GmGlobalConnect
{
    public class AuthenticatedUser
    {
        public AuthenticatedUser(string username, string token, string[] roles, string email)
        {
            Username = username;
            Token = token;
            Roles = roles;
            Email = email;
        }

        public string Username { get; private set; }
        public string Token { get; private set; }
        public string[] Roles { get; private set; }
        public string Email { get; private set; }
        public string FirstName { get; set; }
    }
}