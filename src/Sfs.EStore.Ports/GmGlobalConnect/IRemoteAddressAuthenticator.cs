using System;
using System.Net;
using System.Text.RegularExpressions;

namespace Sfs.EStore.Ports.GmGlobalConnect
{
    public interface IRemoteAddressAuthenticator
    {
        bool IsAuthorised(IPAddress remoteAddress);
    }

    public class RegExTestRemoteAddressAuthenticator : IRemoteAddressAuthenticator
    {
        private readonly Regex _regex;

        /// <exception cref="ArgumentNullException"><paramref name="pattern"/> is <see langword="null" />.</exception>
        public RegExTestRemoteAddressAuthenticator(string pattern)
        {
            if (pattern == null) throw new ArgumentNullException("pattern");
            
            _regex = new Regex(pattern);
        }

        public bool IsAuthorised(IPAddress remoteAddress)
        {
            return _regex.IsMatch(remoteAddress.ToString());
        }
    }
}