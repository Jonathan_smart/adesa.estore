﻿using System;
using System.Net;
using Sfs.Darker.CommandProcessor;

namespace Sfs.EStore.Ports.GmGlobalConnect
{
    public class Authenticate : Command
    {
        public Authenticate() : base(Guid.NewGuid())
        {
        }

        public AuthenticateResult Result { get; set; }

        public string UserId { get; set; }
        public string IvUser { get; set; }
        public string DealerCode { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public IPAddress RemoteAddress { get; set; }
    }

    public class AuthenticateResult
    {
        protected AuthenticateResult(bool authenticated, bool setupForPurchasing = false,
            AuthenticatedUser authenticatedUser = null,
            KnownCustomer customer = null)
        {
            Authenticated = authenticated;
            SetupForPurchasing = setupForPurchasing;
            AuthenticatedUser = authenticatedUser;
            KnownCustomer = customer;
        }

        public bool Authenticated { get; private set; }
        
        public bool SetupForPurchasing { get; private set; }

        public AuthenticatedUser AuthenticatedUser { get; private set; }

        public KnownCustomer KnownCustomer { get; private set; }

        public static AuthenticateResult Unauthenticated()
        {
            return new AuthenticateResult(false);
        }

        public static AuthenticateResult AuthenticatedAs(AuthenticatedUser authenticatedUser,
            KnownCustomer customer,
            bool setupForPurchasing)
        {
            return new AuthenticateResult(true, setupForPurchasing, authenticatedUser, customer);
        }
    }
}
