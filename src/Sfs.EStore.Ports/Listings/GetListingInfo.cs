﻿using System;
using System.Security.Policy;
using Sfs.Darker.CommandProcessor;

namespace Sfs.EStore.Ports.Listings
{
    public class GetListingInfo : Command
    {
        public GetListingInfo(int listingId)
            : base(Guid.NewGuid())
        {
            ListingId = listingId;
        }

        public int ListingId { get; private set; }

        public ListingDetail Result { get; set; }
    }

    public class ListingImage
    {
        public ListingImage(string container, string name, Url url = null)
        {
            Container = container;
            Name = name;
            Url = url;
        }

        public ListingImage(Url url)
        {
            Url = url;
        }

        public Url Url { get; private set; }
        public string Container { get; private set; }
        public string Name { get; private set; }
    }

    public class Money
    {
        public Money(decimal amount, string currency)
        {
            Amount = amount;
            Currency = currency;
        }

        public decimal Amount { get; set; }
        public string Currency { get; set; }

        public override string ToString()
        {
            return string.Format("{0}{1}", Amount.ToString("n2"), Currency);
        }
    }

    public class ListingDetail
    {
        public int ListingId { get; set; }
        public string Title { get; set; }
        public string AttentionGrabber { get; set; }
        public string Programme { get; set; }

        public ListingImage PrimaryImage { get; set; }
        public ListingImage[] MoreImages { get; set; }
        public ListingImage[] Images360 { get; set; }

        public ListingListing ListingInfo { get; set; }

        public ListingSellingStatus SellingStatus { get; set; }

        public ListingVehicle VehicleInfo { get; set; }

        public bool PricesIncludeVat { get; set; }
        public bool AllowOffers { get; set; }
        public class ListingSellingStatus
        {
            public Money CurrentPrice { get; set; }
            public ListingStatus SellingState { get; set; }
            public bool HasBeenReserved { get; set; }
            public double? TimeLeft { get; set; }
        }

        public class ListingListing
        {
            public bool BuyItNowAvailable { get; set; }
            public BuyitNowSale BuyitNowSale { get; set; }
            public Money BuyItNowPrice { get; set; }
            public DateTime? EndAt { get; set; }
            public DateTime? StartAt { get; set; }

            public long? TimeRemaining { get; set; }

            public ListingType ListingType { get; set; }
        }

        public class BuyitNowSale
        {
            public string User { get; set; }
            public DateTime? Datetime { get; set; }
        }

        public class ListingVehicle
        {
            public string Make { get; set; }
            public string Model { get; set; }
            public string Derivative { get; set; }
            public int? CapId { get; set; }
            public string VatStatus { get; set; }
            public string FuelType { get; set; }
            public string Transmission { get; set; }
            public VehicleRegistration Registration { get; set; }
            public int? Mileage { get; set; }
            public string VehicleType { get; set; }
            public VehicleSpecification Specification { get; set; }
            public string ModelYear { get; set; }
            public DateTime CurrentData
            {
                get { return DateTime.Now; }
                set { }
            }

            public TrimInfo Trim { get; set; }
            public SourceInfo Source { get; set; }

            public string EngineDescription { get; set; }
            public string BodyColour { get; set; }

            public class TrimInfo
            {
                public TrimInfo(string code, string name)
                {
                    Code = code;
                    Name = name;
                }

                public string Code { get; private set; }
                public string Name { get; private set; }
            }

            public class SourceInfo
            {
                public SourceInfo(string code, string name)
                {
                    Code = code;
                    Name = name;
                }

                public string Code { get; private set; }
                public string Name { get; private set; }
            }

            public class VehicleRegistration
            {
                public VehicleRegistration(string plate, DateTime? date, string yap)
                {
                    Plate = plate;
                    Date = date;
                    Yap = yap;
                }

                public string Plate { get; private set; }
                public DateTime? Date { get; private set; }
                public string Yap { get; private set; }
            }

            public class VehicleSpecification
            {
                public VehicleEconomy Economy { get; set; }
                public decimal? Co2 { get; set; }
                public string InsuranceGroup { get; set; }
                public VehiclePerformance Performance { get; set; }
                public VehicleDimensions Dimensions { get; set; }
                public VehicleEngine Engine { get; set; }
                public VehicleEquipment Equipment { get; set; }
                public VehicleTaxRate TaxRate { get; set; }
            }

            public class VehicleTaxRate
            {
                public VehicleTaxRate(char? band, Money roadTax12Months)
                {
                    Band = band;
                    RoadTax12Months = roadTax12Months;
                }

                public char? Band { get; private set; }
                public Money RoadTax12Months { get; private set; }
            }
        }

        public class VehicleEquipment
        {
            public VehicleEquipmentItem[] Items { get; set; }
        }

        public class VehicleEquipmentItem
        {
            public VehicleEquipmentItem(string category, string name)
            {
                Category = category;
                Name = name;
            }

            public string Category { get; private set; }
            public string Name { get; private set; }

            public override string ToString()
            {
                return string.Format("Category: {0}, Name: {1}", Category, Name);
            }
        }

        public class VehicleEngine
        {
            public int? NumberOfCylinders { get; set; }
            public int? NumberOfValvesPerCylinder { get; set; }
            public string ArrangementOfCylinders { get; set; }
            public int? CapacityCc { get; set; }
        }

        public class VehiclePerformance
        {
            public decimal? MaximumSpeedMph { get; set; }
            public decimal? AccelerationTo100KphInSecs { get; set; }
            public decimal? MaximumTorqueNm { get; set; }
            public decimal? MaximumTorqueLbFt { get; set; }
            public decimal? MaximumTorqueAtRpm { get; set; }
            public decimal? MaximumPowerInBhp { get; set; }
            public decimal? MaximumPowerInKw { get; set; }
            public decimal? MaximumPowerAtRpm { get; set; }
        }

        public class VehicleEconomy
        {
            public decimal? Urban { get; set; }
            public decimal? ExtraUrban { get; set; }
            public decimal? Combined { get; set; }

            public Money CostPerUnitDistanceTravelled { get; set; }
        }

        public class VehicleDimensions
        {
            public decimal? KerbWeightMin { get; set; }
            public decimal? KerbWeightMax { get; set; }
            public int? LengthMm { get; set; }
            public int? WidthMm { get; set; }
            public int? HeightMm { get; set; }
            public decimal? WheelbaseForVans { get; set; }
        }
    }

    public enum ListingStatus
    {
        Scheduled,
        Active,
        EndedWithSales,
        EndedWithoutSales,
        Cancelled,
        Archived
    }

    public enum ListingType
    {
        Auction,
        AuctionWithBin,
        FixedPrice
    }
}
